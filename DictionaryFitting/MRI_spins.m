classdef MRI_spins
    % Created by Dirk Poot, TUDelft, 10-10-2013
    properties
        % General abbeviations:
        %  Nv = number of voxels
        %  Tp = number of time points at which RF pulses are applied
        %  Ts = number of time points at which Msample is sampled.
        %  Tg = number of time points at which gradient is applied.
        %  Ns = number of spins per voxel
        
        % properties:
        
        %Mz0: Nv element vector with rest z-magnetization
        % Also used as initial value for M  (Magnetisation array)
        % when that is not explicitly provided.
        Mz0;
        
        % T1: Nv element vector with T1 relaxation time (that is : z magnetisation relaxation time)
        % keep units consistent; recommended time unit = ms.
        T1;
        
        % T2: Nv element vector with T2 relaxation time (that is : xy magnetisation relaxation time)
        T2;
        
        % B0: Ns x (1 or Nv) matrix with offresonance frequencies (rad/unit time,
        %      where unit time is the unit in which the T1 and T2 are provided)
        B0;
        
        % B0w: % Ns x (1 or Nv) matrix with weight of each offresonance spin.
        % B0 and B0w can be obtained by: [B0w, B0] = T2star_spins( T2star, Ns );
        B0w;
        
        % xyz: (1 or Ns) x (1 or Nv) x 3 x (1 or timepoints): spatial location of each of the (sub)spins
        % unit: be constent with MRI_pulses, reccomended spatial unit = mm
        xyz;
        
        % step_time: scalar time between two steps in the xyz trajectories array,
        % in unit time. Only used when size( xyz , 4) > 1; i.e. when explicitly providing a diffusion trajectory.
        step_time;
        
        % diffusionTensors : 3x3 x (1 or Nvoxels) matrix with diffusion tensor
        diffusionTensors;
        
        % diffusion_rngstate: random number generator state used for the
        % diffusion simulation.
        diffusion_rngstate;
        
        % voxelSize: 3 x 3 x (1 or Nvoxels) Voxel size (covariance matrix of the Gaussian spin
        % distribution) (only used for diffusion simulation)
        voxelSize;
        
        % voxelCenter: 3 x (1 or Nvoxels) Voxel center position; (only used for diffusion simulation)
        voxelCenter;
        
        % RFscale: (1 or Tp) x Nv  matrix with power scale factors of RF pulses.
        %    If used, scale all RF pulses by a low power (e.g. 0.1) and set this
        %    RFscale to (approx) 10. This avoids ambiguities in roots. (example: what is 1.1
        %    times a 180 degree pulse? The answer depends on the exact way the RF pulse
        %    is applied. By specifing the (approx) 18 degree 10th root of the pulse it
        %    is obvious which one is used.)
        RFscale;
    end;
    methods
        function this = MRI_spins(T1, T2, T2prime, B0, Mz, numspins, xyz, RFscale, step_time )
            % new_spins_object = MRI_spins(T1, T2, T2prime, B0, Mz, numspins, xyz, RFscale, step_time )
            % T1 : row vector with for each spin ensemble (voxel) the T1 relaxation time in ms
            % T2 : same as T1, but now for T2 time.
            % T2prime: same as T1, but now for T2prime time. (1/T2star = 1/T2 + 1/T2prime )
            % B0 : Default = 0; offresonance frequency in rad/ms, specify for each
            %      each voxel. (T2star and numspins are used to create an
            %      ensembe of spins centered around this frequency)
            % Mz : Default = 1; rest magnetization, same size as T1
            % numspins : default =1000 ; number of spins to model T2star effect.
            % xyz : spin positions of each voxel
            %           Default=[]: no spin location differences (only possible without gradient)
            %       OR  scalar:     each voxel has normal spin distribution with std given by scalar.
            %       OR  num_spins x 1 x 3 : specified spin positions for all voxels
            %       OR: A num_spins x 1 x 3 x timepoints diffusion trajectory array
            % RFscale: default = []: no RF scaling, otherwise power factor
            %       for RF.
            % step_time: time between two steps in the xyz trajectories array,
            %            in ms.
            if nargin<4
                B0 =0;
            end;
            if nargin<5 || isempty(Mz)
                Mz = ones(size(T1));
            end;
            if nargin<6 || isempty(numspins)
                numspins = 1000;
            end;
            if nargin<7
                xyz =[];
            end;
            if nargin<8
                RFscale=[];
            end;
            if nargin<9
                step_time=1;
            end;
            this.Mz0 = Mz;
            this.T1 = T1;
            this.T2 = T2;
            
            %T2prime = 1./(1./T2star-1./T2);
            [this.B0w, this.B0] = T2prime_spins( T2prime, numspins, B0);
            this.xyz = [];
            this.step_time = step_time;
            if numel(xyz)==1
                % scalar: actual std of voxel size:
                this.xyz = xyz*randn(numspins, 1, 3);
                %                 tmpxyz = xyz*randn(numspins, 3);
                %                 mn = (this.B0w(:,1)'*tmpxyz)/sum(this.B0w(:,1));
                %                 cv = cov(tmpxyz);
                %                 this.xyz = permute( bsxfun(@minus, tmpxyz, mn) / chol(cv) , [ 1 3 2] );
            elseif ndims(xyz) == 3
                % specified spin positions for all voxels
                this.xyz = xyz ;
            elseif ndims(xyz) == 4
                % This is a trajectories list
                this.xyz = xyz ;
            elseif ~isempty(xyz)
                error('xyz cannot be interpreted');
            end;
            this.RFscale = RFscale;
        end;
        
        function [this] = setSliceprofile( this, RFslice, z_pos )
            
            Nslice  = numel( RFslice );
            Nvox    = numel( this.T1);
            Nspins  = size( this.B0, 1 );
            Nt      = size( this.xyz, 4 );
            
            if rem( Nspins, Nslice ) ~= 0
                error( 'NUMBER OF SLICE POSITIONS SHOULD DIVIDE NUMBER OF SPINS' );
            end
            
            if isempty( this.xyz )
                this.xyz = randn( Nspins, 1, 3 );
            end
            
            if nargin < 3 || isempty( z_pos ); % KEEP SAME DISTRIBUTION
                
            else % set z positions
                this.xyz(:,1,3,:)   = repmat( kron( double( z_pos(:) ), ones( Nspins/Nslice, 1 ) ), [1 1 1 Nt] );
            end
                        
            idx_vox = kron( 1:Nvox, ones( 1, Nslice ) );
            
            this.RFscale            = kron( this.RFscale, RFslice );
            this.Mz0                = this.Mz0( idx_vox );
            this.T1                 = this.T1( idx_vox );
            this.T2                 = this.T2( idx_vox ); 
            this.xyz                = repmat( reshape( this.xyz, Nspins/Nslice, [], 3, Nt ), [ 1 Nvox 1 1] );
            this.B0                 = reshape( this.B0 , Nspins/Nslice, []) ;
            this.B0w                = reshape( this.B0w, Nspins/Nslice, [] ) ;
                        
            if ~isempty( this.voxelCenter )
                this.voxelCenter        = this.voxelCenter( :, :, idx_vox ) ;
            end
            if ~isempty( this.diffusionTensors )
                this.diffusionTensors        = this.diffusionTensors( :, :, idx_vox ) ;
            end
            if ~isempty( this.voxelSize )
                this.voxelSize        = this.voxelSize( :, :, idx_vox ) ;
            end
            
        end
        
        function [this] = shuffleSpins( this, spin_idx )
            this.B0                 = this.B0( spin_idx, :);
            this.B0w                = this.B0w( spin_idx, :);            
        end
        
        function [this]= setDiffusion( this, voxelSize, D , voxelCenter, rng_state)
            % spins.setDiffusion( voxelSize, D )
            % Sets the diffusion property of the spins.
            % voxelSize : 3 x (1 or Nvoxels) element matrix with the voxel size (in units space)
            %           [1 1 1]' (for roughly 1 mm voxels)
            %
            % D : 3x3 x (1 or Nvoxels) matrix with the diffusion tensor of the voxel
            %     (in unit space^2/unit time (mm^2/s) )
            %     Typical cortical gray matter : D= diag([1 1 1]*900e-6)  mm^2/s
            %                       =>  D = diag([1 1 1]*900e-9)    mm^2/ms
            % center : 3 x (1 or Nvoxels) matrix with voxel center (in units space)
            %          default = [0;0;0];
            % rng_state : optional state of random number generator that is
            %       used in the diffusion simulation. (Otherwise 'shuffle'
            %       is used). Note: the spin simulation does not modify
            %       the MATLAB random number state. Also, the spin
            %       simulation is reproducible as the same random numbers
            %       will be generated in each simulation.
            %
            % NOTE: the diffusion is limited to the voxel size. So when
            % the diffusion distance approaches the voxel size deviations
            % start to occur. (With the above numbers the effective
            % diffusion is approx 80% of the requested diffusion after 1000
            % seconds. So in this case it is irrelevant as T1 and T2 are
            % much shorter )
            if nargin<4
                voxelCenter = [0;0;0];
            end;
            
            Nopt = [1 numel(this.Mz0)];
            if isequal(size(voxelSize),[1 3])
                voxelSize = voxelSize';
            end;
            if ndims(D)>3 || size(D,1)~=3 || size(D,2)~=3 || ~any(size(D,3)==Nopt) || ...
                    ndims(voxelSize)>2 || size(voxelSize,1)~=3 || ~any(size(voxelSize,2)==Nopt) || ...
                    ndims(voxelCenter)>2 || size(voxelCenter,1)~=3 || ~any(size(voxelCenter,2)==Nopt)
                error('Diffusion tensor should be a positive definite 3x3 x (1 or nvoxels) matrix and voxelSize and voxelCenter should be a 3 x (1 or nvoxels) matrix.');
            end;
            % Since diffusion is a random process we save memory by just
            % specifying the parameters of this random process.
            % We store a random number generator state to enable
            % reproducible simulations.
            this.diffusionTensors = D;
            tmp = zeros(9, size(voxelSize,2));
            tmp([1 5 9],:) = voxelSize.^2;
            this.voxelSize = reshape(tmp, 3, 3, size(voxelSize,2));
            
            this.voxelCenter = voxelCenter;
            if nargin>=5 && ~isempty(rng_state)
                this.diffusion_rngstate = rng_state;
            else
                rngstate = rng;
                rng('shuffle')
                this.diffusion_rngstate = rng;
                rng( rngstate ) ;
            end;
            this.xyz = 'diffusion';
        end;
        
        function this = subsref( this , S )
            if isequal( S(1).type, '.')
                this = this.(S(1).subs);% calls builtin referencing.
                if numel(S)>1
                    this = subsref(this, S(2:end)); % calls referencing on a field of MRI_spins.
                end;
            elseif isequal( S(1).type, '()') && numel(S)==1 && numel(S(1).subs)==1
                ind = S(1).subs{1};
                this.Mz0 = this.Mz0(:, ind);
                this.T1  = this.T1( :, ind);
                this.T2  = this.T2( :, ind);
                this.B0  = this.B0( :, ind);
                this.B0w = this.B0w(:, ind);
                if size(this.xyz,2)>1
                    this.xyz = this.xyz(:,ind,:,:); %CHANGED TO ALLOW VOXEL SELECTION
                end;
                if size(this.RFscale ,2)>1
                    this.RFscale = this.RFscale(:,ind);
                end;
                % diffusiontensors, diffusion_rngstate, voxelSize , voxelCenter: all have one specification for all voxels
                if size(this.diffusionTensors,3)>1
                    this.diffusionTensors = this.diffusionTensors(:,:,ind);
                end;
                if size(this.voxelSize,3)>1
                    this.voxelSize = this.voxelSize(:,:,ind);
                end;
                if size(this.voxelCenter,2)>1
                    this.voxelCenter = this.voxelCenter(:,ind);
                end;
            else
                error('MRI_spin objects do not allow this type of subscripting');
            end;
        end;
        %         function this = horzcat( varargin )
        %             % combine spins.
        %             if size( this.B0,1) ~=size(right.B0,1)
        %                 error('number of spins should match to combine voxels');
        %             end;
        %             this.Mz0 =[this.Mz0 right.Mz0];
        %             this.T1 = [this.T1 right.T1];
        %             this.T2 = [this.T2 right.T2];
        %             this.B0w=[this.B0w right.B0w];
        %             this.
        %         end;
    end;
end