function [pulses] = pulses_delay( delaytime )
% [pulses] = pulses_delay( delaytime )
% generates MRI_pulses object that introduces (only) the specified delay.
%
% INPUTS:
%  delaytime: delay time that is introduced. 
%
%
% Created by Dirk Poot, TUDelft
% 15-10-2013

pulses = MRI_pulses( delaytime, 0, zeros(3,3,0), [] , zeros(3,0) );