function [img_path, img_name] = select_acquisition( basepath, identifyer )

if nargin < 2 || isempty( identifyer )
    identifyer = 'P*.mat';
end

%show parameter maps first level
list_acq = dir( [ basepath '/' identifyer ] );
idx_mask = strfind( {list_acq.name}, 'mask' );
list_acq( cellfun( @(k) ~isempty(k), idx_mask ) ) = [];
for k_acq=1:numel( list_acq )
    fprintf( '%2d: %s, %s \n', k_acq, list_acq(k_acq).date,  list_acq(k_acq).name);
end
idx_all = numel( list_acq );

%show parameter maps sub-level
list_dir = dir( [basepath '/']);
idx_mask = zeros( size( list_dir ) );
for k_acq = 1:numel( list_dir )
    idx_mask( k_acq ) = ~list_dir(k_acq).isdir | any( strcmp( list_dir(k_acq).name, {'.', '..', 'dictionaries', 'reduced_dictionares' } ) );
    %idx_rem( k_acq ) =  any( strcmp( list_acq(k_acq).name, {'.', '..', 'dictionaries', 'reduced_dictionares' } ) );
end
list_dir( idx_mask>0 ) = [];

for k_dir = 1:numel( list_dir )
    list_dir_acq    = dir( [ basepath '/' list_dir(k_dir).name '/' identifyer ] );
    idx_settings    = cellfun( @(s) ~isempty(s), strfind( {list_dir_acq.name}, 'fit_settings' ) );
    idx_mask        = cellfun( @(s) ~isempty(s), strfind( {list_dir_acq.name}, 'mask' ) );
    
    list_dir_acq( idx_settings | idx_mask ) = []; % remove settings files
    for k_acq=1:numel( list_dir_acq )
         list_dir_acq(k_acq).name = [ list_dir(k_dir).name '/' list_dir_acq(k_acq).name];
        fprintf( '%2d: %s, %s \n', k_acq + idx_all,  list_dir_acq(k_acq).date,  list_dir_acq(k_acq).name );
    end
    if ~isempty( list_dir_acq )
        idx_all = idx_all + numel( list_dir_acq );    
        if ~isempty( list_acq )
            list_acq = cat( 1, list_acq, list_dir_acq );
        else
            list_acq = list_dir_acq;
        end
    end
end
%idx_map  = input( 'select acquisition: ' );
%path_acq = [ basepath list_acq(idx_map).name ];

cmd_str = input( 'select acquisition(s): ', 's' );
idx_loc = strsplit(cmd_str,' ');
for k = 1:numel( idx_loc )
    idx = strfind( idx_loc{k}, ':' );
    if ~isempty( idx )
       idx_loc{k} = str2double( idx_loc{k}( 1:(idx-1) ) ) : str2double( idx_loc{k}( (idx+1) : end ) );
    else
       idx_loc{k} = str2double( idx_loc{k} );
    end
end

idx_all = cat( 2, idx_loc{:} )';

img_path = cell( size( idx_all ) );
img_name = cell( size( idx_all ) );

for k = 1:numel( idx_all )
    if nargout > 1
        [img_path{k}, img_name{k}] = fileparts( list_acq(idx_all(k)).name );
    else
        img_path{k} = [ basepath filesep list_acq(idx_all(k)).name ];
    end
end

end