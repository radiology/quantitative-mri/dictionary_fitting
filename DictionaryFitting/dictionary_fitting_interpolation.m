function dictionary_fitting_interpolation( file_fitsettings )
%  dictionary_fitting_interpolation.m does parameter estimation through the
%  fitting of a B-spline interpolated signal
%
%  as used in: 
%   An Efficient Method for Multi-Parameter Mapping in Quantitative MRI
%   using B-Spline Interpolation.
%
%   van Valenberg W, Klein S, Vos FM, Koolstra K, van Vliet LJ, Poot DHJ. 
%   
%     Copyright (C) 2019 Willem van Valenberg
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or (at
%     your option) any later version.
% 
%     This program is distributed in the hope that it will be useful, but
%     WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%     General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see
%     <https://www.gnu.org/licenses/>.
%% set default options

if isdeployed
    progressbarGUI = 'no';
else
    progressbarGUI = 'yes';
end

path_mask_background= [];
fit_opts            = [];
bsplineord          = 2;     % order of bspline
boundary_condition  = 5;     % 0 = zero, 1 = mirror
maxSize             = 1e5;   % maximal number of elements in memory during dictionary matching
dictReduced         = false;
useMex              = true;
file_dictmatch      = [];
p_prior             = [];
fitConstraints      = true;
p_init.map          = []; % 'HPD_system_T1_3.0T';
p_init.T1geqT2      = true;
p_init.val          = { [],  [],  [],  [],  [],  [],  []}; %T1, T2, T2', B0, PD, B1, phi0
dict_segment        = {':', ':', ':', ':', ':', ':', ':'}; %T1, T2, T2', B0, PD, B1, phi0
saveSignal          = true;
estT2star           = false;
I_prior             = [];
nSVD                = [];
data_scale          = [];

% load set options
load( file_fitsettings );


%% LOAD DICTIONARY INFORMATION

fprintf('LOADING DICTIONARY INFORMATION FROM: \n %s \n', [path_dict 'info_dictionary.mat'])
load([path_dict 'info_dictionary'], 'blocksize', 'edgeAtoms', 'pulseseq', 'Nimg', 'p');
par_int = find( cellfun( @numel, p ) > 1 ).';

if dictReduced
    if exist( [ path_dict 'dict_covariance_matrix.mat' ], 'file' )
        load( [ path_dict 'dict_covariance_matrix.mat' ] );
    else
        error('COVARIANCE MATRIX NOT FOUND');
    end
    if isempty( nSVD )
        dict_type = 'reduced_dictionaries';
    else
        dict_type   = ['dictionaries_svd' num2str( nSVD )];
        Ncoef       = nSVD;
    end
else
    dict_type = 'dictionaries';
    Ncoef = Nimg;
end

p           = reshape( p, 1, [] );          % discretization of each parameter
dim_p       = cellfun( @numel, p );         % number of discretized values of each parameter
dim_dict    = [ Ncoef,  dim_p( par_int ) ]; % size of dictionary
Natoms      = prod( dim_p( par_int ) );     % total number of atoms
Nblocks     = ceil( Natoms / blocksize );   % number of blocks for dictionary matching
pidx        = cell( numel(dim_p), 1 );
[pidx{:}]   = ind2sub( dim_p, 1:Natoms );   % index of each atom in parameter grid
pidx        = cellfun( @uint16, pidx, 'UniformOutput', false);

timings = struct; % time different segments
tic

%% SETUP IMAGE DATA FOR FITTING
% load data and initial best parameter index

fprintf('LOADING MEASUREMENT DATA FROM: \n %s \n', path_img )

data_img          = load( path_img );
datasize_raw      = size( data_img.rawimage ); datasize_raw( end+1:6 ) = 1;

if datasize_raw(4) == Nimg
    if datasize_raw(1) == Nimg
        warning( 'dimension 1 and 4 both have %d images, supposing dimension 4 is temporal', Nimg );
    end
    data_img.rawimage = permute( data_img.rawimage, [4 1 2 3 5] ); %put images in front
elseif datasize_raw(1) ~= Nimg
    error( 'NUMBER OF IMAGES DOES NOT MATCH DICTIONARY' )
end

if exist( path_mask_fit, 'file')0
    fprintf('LOADING MASK FROM: \n %s \n', path_mask_fit )
    data_mask = load( path_mask_fit );
    if isfield( data_mask, 'mask' )
        warning( 'collapsing seperate masks into one' );
        mask_map = sum( cat( 4, data_mask.mask{:} ), 4 ) > 0;
    else
        mask_map = data_mask.mask_map;
    end
    mask_map_allimg = repmat( permute( mask_map, [4 1 2 3 5] ), Nimg, 1, 1, 1, 1);
    data = reshape( data_img.rawimage( mask_map_allimg ), Nimg, [] );
    
    clear mask_map_allimg
else
    fprintf('MASK NOT FOUND. FIT ALL VOXELS \n')
    data = reshape( data_img.rawimage, Nimg, [] );
    mask_map = true( size( data, 2 ), 1);
end

path_map = [ fileparts( file_fitsettings ) '/fit_' fit_name ];

Nvoxels = size( data, 2 );

if dictReduced
    fprintf('PROJECTING DATA ON FIRST %d SINGULAR VECTORS. \n ', Ncoef )
    [~, idx] = sort( real( sqrt( diag( D ) ) ), 'descend' );
    
    proj    = V( :, idx(1:Ncoef)  )';
    data    = reshape( proj * data( meas_sel, :), Ncoef, []);
end

if ~strcmp( fit_opts.imageType, 'complex' )
    data = abs( data );
end

% scale data

srtimg       = sort(reshape(max(abs(data),[],1),[],1));
data_scale95 = double(srtimg(round(numel(srtimg)*.95)));
fprintf( '95 PERCENT OF VOXELS HAVE A MAXIMAL VALUE OF %e OVER ALL IMAGES \n' , data_scale95 );

if isempty( data_scale )
    data_scale  = data_scale95 / p{5};
    %data_scale  = double(srtimg(round(numel(srtimg)*.95)));
    clear srtimg
end
fprintf( 'DATA SCALE BY DIVISION WITH: %e \n' , data_scale );

data_sc        = data / data_scale;

if ~isempty( path_mask_background );
    
    stats_background     = stats_mask( permute( data_img.rawimage, [1:3 5 4] ), path_mask_background );
    fit_opts.noiseLevel  = median( cat( 1, stats_background(:).std ) ) / data_scale;
    
    fprintf( 'ESTIMATED NOISE LEVEL FROM BACKGROUND MASK (RELATIVE TO PD OF 1): %f \n', fit_opts.noiseLevel / p{5} );
end

data_img   = rmfield( data_img, 'rawimage' );
par_names  = {pulseseq.param.name};

%% LOAD DICTIONARIES

%if ~exist( 'dict_comp', 'var' )

dict_comp    = zeros( dim_dict );

progressbar('start', [], 'Loading dictionaries. Please wait...', 'hasGUI', progressbarGUI, 'EstTimeLeft', 'on');
for k = 1 : Nblocks
    if exist([path_dict dict_type '/dictionary' num2str(k) '.mat'], 'file')
        dict_data = load([path_dict dict_type '/dictionary' num2str(k)], 'S');
        
        lin_idx          = 1 + (k-1)*blocksize : min( k*blocksize, Natoms );
        p_idx_sub        = cellfun( @(x) x(lin_idx), pidx(par_int), 'UniformOutput', false);
        p_idx_ind        = [ {':'}; sub2ind( dim_p( par_int ), p_idx_sub{:} ) ];
        
        if ~strcmp( fit_opts.imageType, 'complex' )
            dict_comp( p_idx_ind{:} ) = abs( dict_data.S );
        else
            dict_comp( p_idx_ind{:} ) = dict_data.S;
        end
    else
        warning(['dictionary' num2str(k) ' does not exist']);
    end
    
    if strcmp( progressbarGUI, 'yes' )
        progressbar(k / Nblocks);
    else
        if mod(k,100) == 0
            fprintf(1, '.');
        end
    end
end
progressbar('ready');
clear dict_data lin_idx p_idx_sub p_idx_ind

timings.loaddictionary = toc;
fprintf('FINISHED LOADING DICTIONARY, time: %f min \n', timings.loaddictionary/60);

%% SUBSAMPLE DICTIONARY
if any( cellfun( @(X) ~strcmp( X, ':'), dict_segment ) )
    fprintf('SEGMENTING DICTIONARY \n');
    fprintf(['INITIAL SIZE:     '])
    for k=1:numel(pulseseq.param) fprintf( '%s: %d, ', pulseseq.param(k).name, numel(p{k})); end
    fprintf('\n')
    
    dict_comp = squeeze( dict_comp( :, dict_segment{ par_int } ) );
    p = cellfun( @(P,k) P(k), p, dict_segment, 'UniformOutput', false );
    par_int = find( cellfun( @numel, p ) > 1 );
    
    dim_p     = cellfun( @numel, p );
    Natoms    = prod( dim_p );
    Nblocks   = ceil( Natoms / blocksize );
    
    dim_dict  = [ Ncoef,  dim_p( par_int ) ];
    pidx      = cell( numel(dim_p), 1 );
    [pidx{:}] = ind2sub( dim_p, 1:Natoms );
    
    fprintf(['SEGMENTED SIZE:   '])
    for k=1:numel(pulseseq.param) fprintf( '%s: %d, ', pulseseq.param(k).name, numel(p{k})); end
    fprintf('\n')
end

%% DICTIONARY MATCHING
% if strcmp( fit_opts.imageType, 'complex' )
%     conv_fun        = @(T) dict2par( T(1,:,:,:) + 1i*T(2,:,:,:), T(3:end,:,:,:) - edgeAtoms, pulseseq.param, par_int, boundary_condition, estT2star );
% else
%     conv_fun        = @(T) dict2par( T(1,:,:,:) , T(2:end,:,:,:) - edgeAtoms, pulseseq.param, par_int, boundary_condition, estT2star );
% end

conv_fun        = @(T) dict2par( T(1,:,:,:) + 1i*T(2,:,:,:), T(3:end,:,:,:) - edgeAtoms, pulseseq.param, par_int, boundary_condition, estT2star );

if ~isempty( file_dictmatch );
    fprintf('LOADING DICTIONARY MATCH FILE %s \n', file_dictmatch );
    load( file_dictmatch );
else
    % determine which atoms are in interior (don't use boundary atoms for
    % fitting)
    tic;
    
    interior_index_grid                 = false( dim_dict(2:end) );
    idx                                 = cellfun( @(n) (1+edgeAtoms):(n-edgeAtoms), num2cell(dim_dict(2:end)), 'UniformOutput', false );
    
    for dim = find( ~cellfun( @isempty, p_init.val( par_int ) ) )
        [val, idx{ dim }] = min( abs( p_init.val{par_int(dim)} - p{ par_int(dim) } ) );
        if val ~= 0
            warning( [ pulseseq.param(par_int(dim)).name ' initialized to ' num2str( p{par_int(dim)}( idx{ dim } ) ) ] );
        end
    end
    
    interior_index_grid( idx{:} )       = true;
    
    if p_init.T1geqT2
        
        p_grid = cell( size( par_int ) );
        [p_grid{:}] = ndgrid( p{ par_int } );
        interior_index_grid( p_grid{1} < p_grid{2} ) = false;
        
        clear p_grid;
    end
    idx_int                             = find(interior_index_grid);
    
    Natoms_in                           = numel( idx_int ); % number of atoms in interior (without edge atoms)
    blocksize_match                     = maxSize / ( 2* Ncoef );
    idx_sted                            = [1:blocksize_match: Natoms_in  Natoms_in+1];
    Nblocks_match                       = numel( idx_sted ) - 1;
    
    %idx_grid = 1:blocksize_match;
    max_idx  = zeros( Nvoxels, 1 );
    max_val  = zeros( Nvoxels, 1 );
    PD_phase = zeros( Nvoxels, 1 );
    fun_norm = zeros( Nvoxels, 1 );
    
    progressbar( 'start', [1 Nblocks_match], 'Dictionary matching' );
    for iblock = 1:Nblocks_match
        idx_cur         = idx_int( idx_sted( iblock ) : idx_sted( iblock + 1 ) -1 );
        fun_val         = dict_comp( :, idx_cur );
        local_fun_norm  = sqrt( dot( fun_val, fun_val ) );
        corr            = data_sc'*bsxfun( @rdivide, fun_val, local_fun_norm );
        
        [local_max_val, local_max_idx]  = max( abs( corr ), [], 2 );
        local_phase                     = angle( corr( sub2ind( size( corr ), (1:numel( local_max_idx ))', local_max_idx ) ) );
        
        idx_improve = local_max_val >= max_val;
        
        max_idx( idx_improve )  = idx_cur( local_max_idx( idx_improve ) );
        max_val( idx_improve )  = local_max_val( idx_improve );
        PD_phase( idx_improve ) = local_phase( idx_improve );
        fun_norm( idx_improve ) = local_fun_norm( local_max_idx( idx_improve ) );
        
        %idx_grid = idx_grid + blocksize_match;
        progressbar( iblock );
    end
    progressbar('ready');
    
    timings.matching = toc;
    fprintf('FINISHED DICTIONARY MATCHING, time: %f min \n', timings.matching/60);
    
    mu0             = ( exp( -1i*PD_phase  )./fun_norm ).'.*sqrt( dot( data_sc, data_sc ) );
    idx_atom        = cell( size( par_int ) );
    [idx_atom{:}]   = ind2sub( dim_p( par_int ), max_idx );
    
    theta0          = double( [ real( mu0 ) ; imag( mu0 );  cat( 2, idx_atom{:} ).' - 1 ] ); % spline interpolation uses zero based counting.
    for dim = find( ~cellfun( @isempty, p_init.val( par_int ) ) )
        theta0(2+dim,:) = par2grid( p_init.val{ par_int( dim ) }, pulseseq.param( par_int( dim ) ) ) + edgeAtoms;
    end
    pval0           = conv_fun( theta0 );
    param_init      = cell( size( pval0(:,1) ) );
    
    for k = 1:numel( param_init );
        param_init{k} = pval0(k,:).';
    end
    
    % SAVE DICTIONARY FIT
    fprintf(['SAVING DICTIONARY FIT TO: %s \n'], path_map);
    mkdir( fileparts( path_map ) );
    
    saved_var = {'bsplineord','boundary_condition', 'Nimg', 'Nvoxels', 'p', 'par_int', 'param_init', 'theta0', 'dim_dict', 'data_scale', 'fit_opts'};
    if saveSignal
        signal_matched  = single( bsxfun( @times, dict_comp( :, max_idx ), data_scale * mu0 ) );
        saved_var = cat(2, saved_var, {'signal_matched'} );
    end
    save( path_map, saved_var{:} );
    
    clear corr
end

%% SETUP BSPLINE

tic;
if bsplineord > 1 % prefilter dictionary
    
    progressbar('start', [], 'Prefiltering dictionary. Please wait...', 'hasGUI', progressbarGUI, 'EstTimeLeft', 'on');
    
    % define spline
    xval = floor(-bsplineord/2) : ceil(bsplineord/2);
    [bspline , dbsplinedx] = bsplinefunction(bsplineord, xval);
    
    for d = 2:ndims(dict_comp)
        
        % create matrix for deconvolution
        conv_matrix = toeplitz( [ bspline( ceil(numel(xval)/2):end ) zeros(1, dim_dict( d ) - ceil(numel(xval)/2))], [bspline(ceil(numel(xval)/2):end) zeros(1, dim_dict(d)-ceil(numel(xval)/2))]);
        
        switch boundary_condition
            case 0 % zero extend
                % dont need to update conv_matrix
            case 1 % mirror boundary conditions
                for x = 1 : dim_dict( d )
                    for yidx = 1 : numel( xval );
                        y  = x + xval( yidx );
                        if y < 1
                            y = -y+1;
                            conv_matrix(x,y) = conv_matrix(x,y) + bspline(yidx);
                        elseif y > dim_dict( d )
                            y = 2*dim_dict( d )-y+1;
                            conv_matrix(x,y) = conv_matrix(x,y) + bspline(yidx);
                        end;
                    end;
                end;
            case 2 % periodic domain:
                nneg = sum(xval <0);
                addd = toeplitz([bspline(1) zeros(1,nneg-1)], bspline(1:nneg));
                conv_matrix( 1 : size(addd,1), end-size(addd,2)+1 : end ) = conv_matrix( 1 : size(addd,1), end-size(addd,2)+1 : end ) + addd;
                conv_matrix( end-size(addd,2)+1 : end, 1 : size(addd,1) ) = conv_matrix( end-size(addd,2)+1 : end, 1 : size(addd,1) ) + addd';
            case {4 5 6} % match derivative of one but last point.
                % NOTE: not interpolating at last element of dictionary, but improving smoothness.
                % Numerical derivative to match at dictionary(2) and dictionary(end-1)
                % obs = [dictionary(3)-dictionary(1);dictionary(2:end-1);dictionary(end)-dictionary(end-1)]
                %     == [-.5 0 .5 0 ... ;
                %          0 1 0 0 ...
                %          ...continue diagonal...
                %          ... 0 -.5 0 .5]  * dictionary.
                %             if bsplineorder~=2
                %                 error('currently  boundary_condition == 2 only supported for  bsplineorder==2' );
                %             end;
                ddict = [-.5 0 .5];
                if bsplineord==3
                    % set second derivative:
                    ddict = [1 -2 1];
                    dbsplinedx = [0 -1 2 -1 0];
                end;
                idx1 = xval+1+1 ; idx1(idx1<=0) = idx1(idx1<=0) + size( conv_matrix,2);
                conv_matrix(1,idx1) = -dbsplinedx;
                idxe = xval-1+size( conv_matrix,2) ; idxe(idxe>size( conv_matrix,2)) = idxe(idxe>size( conv_matrix,2)) - size( conv_matrix,2);
                conv_matrix(end,idxe) = -dbsplinedx;
                dictToObs = eye(size(conv_matrix));
                dictToObs(1,1:3) = ddict;
                dictToObs(end,end-2:end) = ddict;
                conv_matrix = dictToObs\conv_matrix ;
            otherwise
                error('unsupported value for boundary_condition provided.');
        end;
        
        deconv_matrix = inv( conv_matrix );
        
        dim_fix = setdiff( 1:ndims(dict_comp), d );
        % determine number of atoms in other dimensions
        maxAtoms = floor( maxSize / dim_dict(d) );
        Nsteps   = ceil(  numel(dict_comp) / ( size( dict_comp, d ) * maxAtoms ) );
        idx_sub = cell( ndims(dict_comp), 1);
        
        progressbar('start', [], 'Prefiltering dictionary. Please wait...', 'hasGUI', progressbarGUI, 'EstTimeLeft', 'on');
        for k = 1:Nsteps
            
            lin_idx            = 1 + (k-1)*maxAtoms : min( k*maxAtoms, numel(dict_comp) /  dim_dict(d) );
            [idx_sub{dim_fix}] = ind2sub( dim_dict( dim_fix ), repmat( lin_idx, dim_dict(d), 1 ) );
            idx_sub{d}         = repmat( ( 1:dim_dict(d) )', 1, numel(lin_idx) );
            idx_ind            = sub2ind( dim_dict, idx_sub{:} );
            
            dict_comp( idx_ind ) = deconv_matrix * dict_comp( idx_ind );
            progressbar(k / Nsteps);
        end
        progressbar('ready');
        
        progressbar( (d-1) / (ndims(dict_comp)-1), pulseseq.param( par_int(d-1) ).name );
    end
    progressbar('ready');
end

timings.bsplinefilter = toc;
fprintf('FINISHED SPLINE PREFILTER, time: %f min \n', timings.bsplinefilter/60);

clear xval bspline conv_matrix deconv_matrix tmp Nidx dim_fix lin_idx idx_sub idx_ind

%% SETUP FITTING

if boundary_condition > 3
    boundary_condition_spline = boundary_condition - 4;
else
    boundary_condition_spline = boundary_condition;
end

func_PD_spline_abs = @(theta) spline_optimization_function( dict_comp , theta(1,:), [],  theta(2:end,:), [], [], [], bsplineord, [], [],boundary_condition_spline, useMex);
func_PD_spline_c = @(theta) spline_optimization_function( dict_comp , theta(1,:), theta(2,:), theta(3:end,:), [], [], [], bsplineord, [], [],boundary_condition_spline, useMex);

if ~strcmp( fit_opts.imageType, 'complex' )
    func_PD_spline  = func_PD_spline_abs;
    par_opt         = [1, 3:numel(par_int)+2];
else
    func_PD_spline  = func_PD_spline_c;
    par_opt         = [1:numel(par_int)+2];
end

if fitConstraints
    fprintf( 'STARTING CONSTRAINED OPTIMIZATION\n' );
    fit_opts.constraints.lb = [ -Inf; -Inf; zeros( numel( par_int ), 1 ) + edgeAtoms ];
    fit_opts.constraints.ub = [  Inf;  Inf; cellfun( @numel, p( par_int ).' ) - edgeAtoms - 1];
    fit_opts.constraints.lb = fit_opts.constraints.lb( par_opt );
    fit_opts.constraints.ub = fit_opts.constraints.ub( par_opt );
else
    fprintf( 'STARTING UNCONSTRAINED OPTIMIZATION\n' );
end

if ~isempty( p_prior ) % do regularization
    
    parameterPrior.fun              = p_prior.fun;
    
    [Tprior, dTprior]               = par2grid( p_prior.mean', pulseseq.param, par_int );
    %idx_T                       = (1:numel( par_int )) + 1 + strcmp( fit_opts.imageType, 'complex' );
    idx_nonlin                      = cat( 2, false( 1, 2 ), true( 1, numel( par_int ) ) );
    parameterPrior.mu               = zeros( size( idx_nonlin.' ) );
    parameterPrior.sigma            = zeros( size( idx_nonlin.' ) );
    parameterPrior.mu(idx_nonlin)   = Tprior + edgeAtoms;
    parameterPrior.sigma(idx_nonlin)= p_prior.std( par_int )'.*dTprior;
    parameterPrior.mu(~idx_nonlin)  = p_prior.mean( strcmp( par_names, 'Mz0' ) | strcmp( par_names, 'PD' ) );
    parameterPrior.sigma(~idx_nonlin)= p_prior.std( strcmp( par_names, 'Mz0' ) | strcmp( par_names, 'PD' ) );
    
    idx_T = find( idx_nonlin );
    for k = find( strcmp( {pulseseq.param( par_int ).scale}, 'log' ) );
        lb = pulseseq.param( par_int( k ) ).range(1);
        ub = pulseseq.param( par_int( k ) ).range(2);
        n  = pulseseq.param( par_int( k ) ).steps;
        c  = (ub/lb)^(1/(n-1));
        parameterPrior.sigma( idx_T(k) ) = p_prior.std( par_int( k ) )/log10( c ); % convert from orders of magnitude to steps in grid
    end
    
    parameterPrior.sigma = diag( parameterPrior.sigma.^2 );
    
    fit_opts.parameterPrior = parameterPrior;
    fit_opts.parameterPrior.mu = parameterPrior.mu( par_opt );
    fit_opts.parameterPrior.sigma = parameterPrior.sigma( par_opt, par_opt );
end

if ~isempty( p_init.map ) % initialize with reference parameters
    pval_true   = load_voxels( p_init.map );
    par0        = cat( 1, pval_true.val );
    T           = par2grid( par0, pulseseq.param, par_int ) + edgeAtoms;
    
    Nvoxel_mask = cellfun( @nnz, data_mask.mask );
    
    theta0_img = nan( [ size( theta0, 1 ) size( mask_map ) ] );
    
    for kmask = 1:numel( data_mask.mask )
        tmp         = false( size( data_mask.mask{ kmask } ) );
        tmp( data_mask.mask{kmask} ) = true;
        cur_mask    = find( tmp( mask_map ) );
        fun_val     = func_PD_spline( [1; 0; T(:, kmask)] );
        fun_norm    = sqrt( dot( fun_val, fun_val ) );
        corr        = data_sc( :, cur_mask )'*fun_val/fun_norm;
        mu0_mask    = ( exp( -1i*angle( corr )  )./fun_norm ).'.*sqrt( dot( data_sc( :, cur_mask ), data_sc( :, cur_mask ) ) );
        theta0_img(:,data_mask.mask{ kmask } ) = cat( 1, real( mu0_mask ), imag( mu0_mask ), repmat( T(:, kmask), 1,  nnz( data_mask.mask{ kmask } ) ) );
    end
    
    %theta0 = cat( 2, theta0_mask{:} );
    theta0 = cat( 2, theta0_img(:, mask_map ) );
    
    pval0           = conv_fun( theta0 );
    param_init      = cell( size( pval0(:,1) ) );
    for k = 1:numel( param_init );
        param_init{k} = pval0(k,:).';
    end
    if saveSignal
        signal_init = single( data_scale*func_PD_spline( theta0 ) );
        saved_var = cat(2, saved_var, 'signal_init' );
    end
    save( path_map, saved_var{:} );
end

if isfield( fit_opts, 'initialValueSpecifierVect' )
    if fit_opts.initialValueSpecifierVect(5)
        fit_opts.initialValueRange       = zeros( numel( par_opt.' ), 2 );
        
        fit_opts.initialValueRange( (end - numel(par_int) + 1 ) : end, 1) = edgeAtoms;
        fit_opts.initialValueRange( (end - numel(par_int) + 1 ) : end, 2) = dim_dict(2:end).' - edgeAtoms;
    end
end

%% DO FITTING

if bsplineord > 0
    
    theta = theta0;
    tic
    [theta(par_opt,:,:,:), residue, opts] = fit_MRI( func_PD_spline, data_sc, theta0(par_opt,:,:,:), fit_opts );
    timings.optimization = toc;
    
    % evaluate speed spline interpolation
    if isfield( data_img, 'p_test' )
        T_test = zeros( numel( par_int ), Nvoxels );
        for k = 1:numel( par_int ) % map test points to grid
            lb =  pulseseq.param(par_int(k) ).range(1);
            ub =  pulseseq.param(par_int(k) ).range(2);
            N  =  pulseseq.param(par_int(k) ).steps;
            
            switch pulseseq.param( par_int(k) ).scale
                case 'linear';
                    T_test(k,:) = ( data_img.p_test{ par_int( k ) }( mask_map ) - lb ) / (ub -lb) * (N-1);
                case 'log'
                    c = ( ub/lb ) ^ ( 1/(N-1) );
                    T_test(k,:) = log( data_img.p_test{ par_int( k ) }( mask_map ) / lb ) / log( c );
                otherwise
                    warning(['parameter ' pulseseq.param(par_int(k)).name ' not mapped'])
            end
        end
    else
        T_test = theta( 3:end, : );
    end
    t_spline = zeros( size( T_test(1,:) ) );
    newSz = [nan ones(1,ndims(dict_comp)-1) ];
    switch boundary_condition
        case {4, 5, 6}
            bnd_cnd = boundary_condition - 4;
        otherwise
            bnd_cnd = boundary_condition;
    end
    for k = 1:size( T_test, 2 )
        tic
        [S, dSdT] = TransformNDbspline( dict_comp, T_test(:, k), [], newSz, [], bsplineord, [], [], bnd_cnd );
        t_spline(k) = toc;
    end
    timings.spline = mean( t_spline );
    
    for k = 1:size( T_test, 2 )
        tic
        [S, dSdT] = func_PD_spline( theta(par_opt,k) );
        t_spline2(k) = toc;
    end
    
    timings.funeval = mean( t_spline2 );
    
    clear S dSdT t_spline
else
    theta   = theta0;
    opts    = fit_opts;
    opts.profile = false;
    
    timings.optimization    = 0;
    timings.spline          = 0;
end

if opts.profile
    profile_output = opts.profile_output;
else
    profile_output = [];
end
mu = theta(1,:) + 1i * theta(2,:);
T_opt  = theta(3:end,:);

fprintf('FINISHED OPTIMIZATION, time: %f min \n', timings.optimization/60 );

%% DETERMINE PARAMETER  MAP

param_opt       = cell( size( param_init ) );

par = conv_fun( theta );
for k = 1:size( par, 1 )
    param_opt{k}        = par(k,:).';
end

%% DETERMINE INTERPOLATION ERROR SIMULATED SIGNALS
if isfield( data_img, 'p_test' ) % determine the interpolated signal at the true value points ( for interpolation accuracy)
    
    % get dictionary positions test signals
    T_test      = cell( numel(par_int), 1 );
    for k1 = 1:numel( pulseseq.param )
        lb =  pulseseq.param(k1).range(1);
        ub =  pulseseq.param(k1).range(2);
        N  =  pulseseq.param(k1).steps;
        
        switch pulseseq.param(k1).scale
            case 'linear'
                c = ( ub - lb ) / ( N - 1 );
                T_test{k1} = ( data_img.p_test{k1} - lb ) / c + edgeAtoms;
            case 'log'
                c = ( ub / lb ) ^ ( 1 / (N - 1) );
                T_test{k1} = log( data_img.p_test{k1} / lb ) / log( c ) + edgeAtoms;
            case {'none', 'val'}
                T_test{k1} = data_img.p_test{k1};
            otherwise
                error('unknown scale for steps');
        end
    end
    
    % determine error
    PD_test = T_test{5} .* exp( 1i.* T_test{7} ) ./ p{5} / data_scale;
    theta_test = cat(1, real( PD_test ), imag( PD_test ), T_test{par_int} );
    signal_spline_test = single( func_PD_spline( theta_test ) ) * data_scale ;
else
    signal_spline_test = [];
end

%% SAVE

saved_var = cat( 2, saved_var, {'profile_output', 'param_opt', 'timings', 'signal_spline_test'} );

if saveSignal
    if bsplineord > 0
        tic
        signal_fitted = single( data_scale*func_PD_spline( theta( par_opt, : ) ) );
        timings.funeval_all = toc;
    else
        signal_fitted = signal_matched;
    end
    saved_var = cat(2, saved_var, 'signal_fitted' );
end

fprintf(['SAVING SPLINE FIT TO: %s \n'], path_map);
save( path_map, saved_var{:} );
