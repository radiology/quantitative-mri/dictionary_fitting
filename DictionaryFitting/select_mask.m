function mask_in = select_mask( path_data, mask_name )
%path_mask = select_mask( path_data, mask_name )
%   load mask with given name from location of data

[ ~, mask_name ] = fileparts( mask_name );
mask_in  = [ fileparts( path_data ) '/' mask_name '.mat'];

if ~exist( mask_in, 'file' )
    
    fprintf(  [ 'NO MASK FOUND AT %s\n' 'GIVE INPUT FOR NUMBER OF ROIS TO DRAW (0 = SELECT MASK TO COPY):' ], mask_in )
    Nmask  = input('\n');
    
    if Nmask > 0
        
        data_img = load( path_data );
        
        mask    = cell(Nmask,1);
        sliceno = cell(Nmask,1);
        Mx      = cell(Nmask,1);
        My      = cell(Nmask,1);
        for k = 1:Nmask
            fprintf('DRAW ROI %d \n', k);
            [mask{k}, Mx{k}, My{k}, sliceno{k}] = drawROI( data_img.rawimage, 1, [], sprintf('DRAW ROI %d', k));
        end
        
    else
        [FileName,PathName] = uigetfile( mask_in, 'SELECT MASK TO COPY' );
        
        fprintf('LOADING MASK FROM %s \n', [PathName FileName]);
        load( [PathName FileName] );
    end
    fprintf('SAVING MASK TO %s \n', mask_in);
    save( mask_in, 'mask', 'sliceno', 'Mx', 'My' );
end
end
