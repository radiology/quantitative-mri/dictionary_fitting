function [ Mout ] = fun_signal_wrapper( RFscale, z_disc, spins, pulseseq, Minit )
% wrapper for the bloch simulations for simulating slice profile

spins_new = setSliceprofile( spins, RFscale, z_disc );
Mout = bloch_sim2( spins_new, pulseseq, Minit );

end

