function [path_map, path_map_settings] = select_map( basepath, identifyer )

if nargin < 2 || isempty( identifyer )
    identifyer = 'fit_*';
end

%show parameter maps first level
list_maps = dir( [ basepath '/' identifyer ] );
for k_map=1:numel( list_maps )
    fprintf( '%2d: %s, %s \n', k_map,  list_maps(k_map).name, list_maps(k_map).date );
end
idx_map = numel( list_maps );

%show parameter maps sub-level
list_acq = dir( basepath );
idx_rem = zeros( size( list_acq ) );
for k_acq = 1:numel( list_acq )
    idx_rem( k_acq ) = ~list_acq(k_acq).isdir | any( strcmp( list_acq(k_acq).name, {'.', '..', 'dictionaries', 'reduced_dictionares' } ) );
    %idx_rem( k_acq ) =  any( strcmp( list_acq(k_acq).name, {'.', '..', 'dictionaries', 'reduced_dictionares' } ) );
end
list_acq( idx_rem>0 ) = [];

for k_acq = 1:numel( list_acq )
    list_acq_maps = dir( [ basepath list_acq(k_acq).name '/' identifyer ] );
    idx_settings = cellfun( @(s) ~isempty(s), strfind( {list_acq_maps.name}, 'fit_settings' ), 'UniformOutput', false );
    list_acq_maps( [idx_settings{:} ] ) = []; % remove settings files
    for k_map=1:numel( list_acq_maps )
        list_acq_maps(k_map).name = [ list_acq(k_acq).name '/' list_acq_maps(k_map).name];
        fprintf( '%2d: %s, %s \n', k_map + idx_map, list_acq_maps(k_map).name, list_acq_maps(k_map).date );
    end
    if ~isempty( list_acq_maps )
        idx_map = idx_map + numel( list_acq_maps );    
        list_maps = cat( 1, list_maps, list_acq_maps );
    end
end
idx_map  = input( 'select map: ' );
path_map = [ basepath list_maps(idx_map).name ];
path_map_settings = [ basepath strrep( list_maps(idx_map).name, 'fit_', 'fit_settings_' ) ];

end