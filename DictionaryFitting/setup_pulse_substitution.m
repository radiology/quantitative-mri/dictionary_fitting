function [ sub_idx, sub_mod, slice_width ] = setup_pulse_substitution( acq, path_rf, mod_samples )

mod_names = fieldnames( mod_samples );

sub_idx = cell( size( acq ) );
sub_mod = cell( size( acq ) );

slice_width = nan( numel( acq ), numel( mod_names ) );

for k = 1:numel( acq )
    
    idx_rf_type     = get_RF_type( acq{k} );
    idx_rf_loc      = find( bitand( acq{k}.actionid , 6) ~= 0 );
    
    sub_idx{k} = zeros( size( acq{k}.actionid ) );
    
    for k_mod = 1:numel( mod_names )
        idx_sub = idx_rf_loc( idx_rf_type == k_mod );
        if any( idx_sub ) && ~isempty( getfield( mod_samples, mod_names{ k_mod } ) )
            sub_idx{k}( idx_sub ) = k_mod;
            data_mod = load( [ path_rf mod_names{k_mod} '_red.mat' ] );
            
            idx_steps = find( data_mod.Nsamp == getfield( mod_samples, mod_names{ k_mod } ) );
            sub_mod{k}(k_mod) = data_mod.rf_opt{idx_steps};
            
            if isfield( data_mod, 'slice_width' )
                slice_width( k, k_mod ) = data_mod.slice_width;
            end
            if k == 1
                fprintf( 'APPROXIMATION ERROR %s PULSE WITH %d STEPS: %e\n', mod_names{k_mod}, data_mod.Nsamp( idx_steps )+2, data_mod.error_opt{idx_steps} );
            end
        end
    end
end

end

