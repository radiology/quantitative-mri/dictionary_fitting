function [theta, Dtheta] = grid2par( T, param, par_int, boundary_condition )
%theta = grid2par( T, param, boundary_condition )
%   maps dictionary grid positions to parameter values
%   INPUT:  T:      matrix containing P zero-based grid indices of N objects (PxN),
%                   
%           param:  structure with properties of P parameters described by
%                   fields: 
%                   - steps, number of discretized values
%                   - range, lower and upper bound [2x1]
%                   - scale, scaling of parameter: 'linear' or 'log'
%           par_int: indices of the parameters of T in structure param,
%                   default = find( [param.steps] > 1 )
%           boundary_condition: how to handle values outside range:
%                   - if 0, extrapolate values
%                   - if 1, mirror grid values to range (default)
%
%   OUTPUT: theta:  parameter values (PxN)
%           Dtheta: derivative of paramater values (PxN)

if nargin < 3 || isempty( par_int )
    par_int = find( [param.steps] > 1 );
end
if nargin < 4 || isempty( boundary_condition )
    boundary_condition = 1;
end

theta = zeros( size( T ) );
Dtheta = zeros( size( T ) );

for k = 1:numel( par_int ) % map test points to grid
    lb =  param(par_int(k) ).range(1);
    ub =  param(par_int(k) ).range(2);
    n  =  param(par_int(k) ).steps;
    
    if any( boundary_condition == [ 1 5 ] ); % mirror boundary conditions
        
        div_mod         = mod( floor( T(k, :)/n ), 2 ) == 1;
        T_mod           = mod( T(k, :), n );
        T_mod( div_mod ) = n - T_mod( div_mod );
    else
        T_mod = T(k, :);
    end
    
    switch param( par_int(k) ).scale
        case 'linear';
            theta(k,:) = lb + ( ub - lb ) / (n-1) * T_mod;
            Dtheta(k,:) = ( ub - lb ) / (n-1);
        case 'log'
            c = ( ub/lb ) ^ ( 1/(n-1) );
            theta(k,:)  = lb * c .^ T_mod;
            Dtheta(k,:) = lb * log(c).* c .^ T_mod;
        otherwise
            warning(['parameter ' pulseseq.param(par_int(k)).name ' not mapped'])
    end
end

end

