% demo_drawShowSaveMaskOverlay.m
% 
% Shows a random image (here generated with randn)
% and asks to draw and ROI in that image. This ROI is automatically saved and 
% will be reloaded when you run the script again. 
% Next the image is shown with the synthetic map (e.g. T2 map) (here again generated with randn)
% Finally every slice of the image with as overlay the map within the drawn ROI is saved
% to png's.
% 
% 9-11-2018, D.Poot, Erasmus MC: Created version
images = randn(10,10, 4);
maps = rand(10,10, 4);

ROIfilename = 'showmask.mat';
ROI1 = drawROI(permute( images,[5 1 2 3 4]),inf,{ROIfilename,ROIfilename},'Please draw an ROI in every slice you want.');


figh = imagebrowse( images, [],'overlays',struct('mask',ROI1,'image',maps),'subfigdims',[1 2])

save_orthoview( figh, [5 5 1],5,'T2overays','iterateDim',3);