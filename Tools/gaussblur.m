function [out, filt] = gaussblur( img, sigmas, edges, adjoint)
% [out] = gaussblur( img, sigmas [, edges  , adjoint])
% [out] = gaussblur( img, sigmas [, weights, adjoint])
% [prepweights] = gaussblur( 'prepweight', sigmas , weights)
% [out] = gaussblur( img, sigmas , prepweights [, adjoint])
%
% Performs a accurate and quick gaussian blurr of the N dimensional image img.
% 
% INPUTS
% img  : arbitrary ND image (single or double precision values)
% sigmas: scalar or vector that specifies the smoothing in each dimension. 
%         A sigma value of 0 indicates absolutely no smoothing in that dimension.
% edges : optional. Default = 'correct'
%         If 'correct' then the edge intensity is corrected for the reduced number of samples
%         that these have. 
%         If empty it is just a gaussian blur with (implicitly) extending the 
%         image with zeros in all dimensions.
% weights: array of size img, with the weight of each point. Usually real and positive. 
%          If noise suppression is the aim, weights(i) should be 1/var(img(i)), where the variance of 
%          img(i) it typically approximated by the CRLB. 
% prepweights: Internal information structure. When repeatively the same weights 
%          are to be used for blurring, constructing prepweights once and using 
%          it many times saves computation time. 
% adjoint : default : false.  The gaussian blurring process is a linear operator on img (when 'edges'/'weights' are fixed). 
%           When adjoint = true, then the adjoint (complex conjuate transpose) of this linear
%           operator is applied. Convenient for optimization routines where this may be needed. 
%
% created by Dirk Poot (d.poot@erasmusmc.nl), Erasmus Medical center, Rotterdam
% 31-8-2011

if nargin<3
    edges = 'correct';
end;
if nargin<4 || isempty(adjoint)
    adjoint = false;
end;
docorrect = isequal(edges,'correct');
if ~docorrect && ~isempty(edges)
    w=[];
    if isstruct( edges )
        w = edges;
        if ~isequal(size(w.weights),size(img)) || ~isequal(w.sigmas, sigmas)
            error('Incorrect prepared weights provided. Check the logic in the calling functions as apparently something went wrong.');
        end;
    else
        if ~isequal(size(edges),size(img)) && ~isequal(img,'prepweight')
            warning('GAUSSBLUR:ARGUMENTCHECK', 'Cannot interpret ''edges'' argument, ignoring it');
        else
            w.weights = edges;
            w.bluredweights = gaussblur( w.weights, sigmas, [] );
            w.sigmas = sigmas;
            if isequal(img,'prepweight')
                out = w;
                return;
            end;
        end;
    end;
    if ~isempty(w)
        % weights providided:
        % out = inv( diag( B * weights) ) * B * diag(weights) * im  % NOTE: B = symmetric
        % adjoint:
        %       (inv( diag( B * weights) ) * B * diag(weights) )' 
        %     = diag(weights) * B * inv( diag(B * weights) )'
        if adjoint
            % adjoint version
            out = conj(w.weights) .* gaussblur( img./conj(w.bluredweights), sigmas, []);
        else
            % normal
            out = gaussblur( img.*w.weights, sigmas,[]) ./w.bluredweights;
        end;
        return;
    end;
end;
    
ndim = ndims(img);
if numel(sigmas)==1
    sigmas = sigmas(ones(1,ndim));
end;

if ~isreal(img)
    out = complex( gaussblur( real(img), sigmas, edges), gaussblur( imag(img), sigmas, edges));
    return;
end;

filt = cell(1,ndim);
for dim=1:ndim
    if sigmas(dim)==0
        filt{dim} = [];
    else
        tmp = exp(-(0:max(1,floor(3.5*sigmas(dim)))).^2/(2*sigmas(dim).^2));
        filt{dim} = [tmp(end:-1:2) tmp];
        filt{dim} = filt{dim} / sum(filt{dim});
    end;
end;

out = separableConvN_c( img , filt );    
if docorrect
    if adjoint
        error('adjoint not implement for ''correct''');
    end;
    % compensate edges:
    for dim=1:ndim
        comp_dim = 1./separableConvN_c( ones(size(img,dim),1) , filt(dim) ); 
        comp_dim = reshape(comp_dim,[ones(1,dim-1) size(img,dim) 1]);
        out = bsxfun(@times, out, comp_dim);
    end;
end;
