%% time multiply add:


sz = 2000;
nlp = 10;
A = randn(sz);
B = randn(sz);
C = randn(1);

D =  A + B.*C;
tic;
for k=1:nlp
    D = A + B.*C;
end;
tu1 = toc;

tic;
for k= 1: nlp
    D = B.*C;
    D = A + D;
end;
tu2 = toc;

tic;
for k= 1: nlp
    D = A + B;
end;
tu3 = toc;

tic;
for k=1:nlp
    D4 = A + B.*C;
end;
tu4 = toc;

[tu1 tu2 tu3 tu4] /(numel(A)*nlp)*2.26e9
%%
%% time multiply add:


sz = 2000;
nlp = 10;
A = randn(sz);
B = randn(sz);
C = randn(sz);
a = randn(1);
b = randn(1);
c = randn(1);

D =  a*A + b*B+c*C;
tic;
for k=1:nlp
    D = a*A + b*B+c*C;
end;
tu1 = toc;

tic;
for k= 1: nlp
    D1 = a*A;
    D2  =b*B;
    D3 = c*C;
    D = A + B + C;
end;
tu2 = toc;

tic;
for k= 1: nlp
    D = A + B + C;
end;
tu3 = toc;

tic;
for k=1:nlp
    D = a.*A + b.*B+c.*C;
end;
tu4 = toc;

[tu1 tu2 tu3 tu4] /(numel(A)*nlp)*2.26e9