%%
% fit_MRI_test_MCMC
mn_limit = [];
mx_limit = [];
if 0
    % Test with simple linear model
    X = rand(10,3);
    % tht = rand(3,4,3);
    tht = repmat(rand(3,1),[1 ,4 , 3] );
    thtinit = ones(size(tht));
    noiselevel = 1;

    predfun = @(par) verysimpletestfitfun( par, X ,2);
elseif 0
    % Test with T2 decay model, large enough A to avoid sign flips
    % (hopefully)
    TE = [10 20 40 60 80 100]'/1000;
    
    tht = repmat([10; 1000/50],[1 3 3]);
    thtinit = 10*ones(size(tht));
    noiselevel = .7;
    predfun = @(par) predict_MRI_T2( par, TE ,false);
elseif  0
    % iMSDE prepared T1 & T2 quantification:
    t2preptimes = [11.5, 26, 26, 26, 26, 11.5, 11.5, 11.5]'/1000; 
    flipangles = [4, 8.5, 8.5, 8.5 8.5, 20, 20, 20]'*pi/180;
    Npulses = [160, 32, 32, 32, 32, 32, 32, 32]';
    TR = 10/1000 * ones(size(t2preptimes));
    
    tht = repmat([10; 1000/810;1000/51],[1 4 4]);
    thtinit = repmat([10;1.2;20], [1, size(tht,2), size(tht,3)]);
    noiselevel = .01;
    predfun = @(tht) pred_iMSDEprep_VFAT1( tht, flipangles, TR, t2preptimes , Npulses);
else
    % inversion recovery T1 measurement with magnitude MR images:
    TI = (100:150:1200)'/1000;
    TR = 2*ones(size(TI));
    
    mn_limit = [0  nan 0];
    mx_limit = [10 nan 6];
    tht = bsxfun(@plus,[0;2;2],[1;0;0]*linspace(.3,3,20));
    thtinit = tht*1.1;
    noiselevel = .2;
    predfun = @(tht) predict_IRT1TR( tht, TI, TR);
end;

ynoisefree = predfun(tht(:,:)); 
if 0 
    ynoise = ynoisefree+noiselevel*randn(size(ynoisefree)) 
else
    ynoise = AddRiceNoise( ynoisefree, noiselevel );
end;
y= reshape( ynoise, [size(ynoisefree,1), size(tht,2) size(tht,3)] );
%% ML fit:
clear fitopt;
fitopt.optimizer = 3; % fminfast
fitopt.noiseLevel = noiselevel;
fitopt.imageType = 'magnitude';
fitopt.blockSize = [1 1 ];
MLestpar = fit_MRI( predfun, y, thtinit, fitopt);

%% MCMC simulation:
% profile on;
clear fitopt;
fitopt.optimizer = 4; % MCMC
fitopt.noiseLevel = noiselevel;
fitopt.imageType = 'magnitude';
fitopt.blockSize = [1 1 ];
fitopt.maxIter = 4;
tstpar = cell(1,100000);
tstpar{1} = MLestpar;%thtinit;
progressbar('start',numel(tstpar));
for k=2:numel(tstpar)
    tstpar{k} = fit_MRI( predfun, y, tstpar{k-1}, fitopt);
    if mod(k,100)==0
        progressbar(k);
    end;
end;
progressbar('ready');
% %
tmp = cat(4,tstpar{2:end});
tmp1 =permute(tmp(:,1,1,:),[4 1 2 3]);
c=cov(tmp1)
tmp2 = tmp(:,:)';

rejectratio = (mean(all(diff(tmp,[],4)==0,1),4))
% profile viewer
plot(gaussblur(squeeze(mean(all(diff(reshape(tmp,size(tmp,1),[],size(tmp,4)),[],3)==0,1),2)),10))
%% 'correct' sign:
sgnadj = sum(X*tmp(:,:),1)<0;
tmp(:,sgnadj) = -tmp(:,sgnadj);

%% Compute actual Log likelihood function:
mn = min(tmp2);
mx = max(tmp2);
if ~isempty(mn_limit) 
    mn(isfinite(mn_limit)) = max( mn(isfinite(mn_limit)), mn_limit(isfinite(mn_limit)));
end;
if ~isempty(mx_limit) 
    mx(isfinite(mx_limit)) = min( mx(isfinite(mx_limit)), mx_limit(isfinite(mx_limit)));
end;
ndim = numel(mn);
if ndim==2
    nsamp = 200;
else
    nsamp = 100;
end;
x_in = cell(1,ndim);
for k=1:ndim
    x_in{k} = linspace(mn(k),mx(k),nsamp);
end;
x = cell(1,ndim);
[x{:}] = ndgrid(x_in{:});
szx = size(x{1});
x = permute( cat(ndim+1, x{:}),[ ndim+1 1:ndim]);
szp = size(tstpar{2});
numvox = prod(szp(2:end));
LL = zeros([numvox szx]);

kst = 1;
% for voxid = 1: numvox
%     y_tst = y(:,voxid);
    for k=1:prod(szx);
        LL(:, kst) = sum( logricepdf( y(:,:), predfun(x(:,k))*ones(1,numvox) , noiselevel) ,1)';
        kst = kst+1;
    end;
% end;
LL = permute(LL,[1+(1:numel(szx)) 1]);
%% Compute posterior distribution from MCMC samples:
pnts = max(1,min(nsamp,bsxfun(@times, bsxfun(@minus, tmp2, mn ) , (nsamp-1)./(mx-mn))+1));
gaussigm = .7;
LLsmp = zeros([szx numvox]);
imgsel = repmat({':'},1,ndim);
for k=1:numvox
    LLsmp(imgsel{:},k)  = genAddGaussBumps( szx, pnts(k:numvox:end,:), [] , gaussigm );
    LLsmp(imgsel{:},k) = LLsmp(imgsel{:},k)./sum(reshape(LLsmp(imgsel{:},k),[],1));
end;

% also blur the computed log likelihood (as otherwise it will be 'sharper'
% than the MCMC distribution only due to the 'gauss bumps' needed to smooth
% the histogram.)
LLb = gaussblur( exp(LL) ,[gaussigm*ones(1,numel(szx)) 0]);

%% display log likelihood true and estimated:
LLsc = reshape( log( sum(reshape(LLb,[],size(LL,numel(szx)+1)),1) ) , [ones(1,numel(szx)),size(LL,numel(szx)+1) ]) ;
figure(1)
% imagebrowse(cat(ndim+2,LL-max(LL(:)),log(LLsmp./max(LLsmp(:)))),[-10 0],'xrange',[mn 1 1;mx numvox 2] )
LLpred_disp = bsxfun(@minus, log(LLb),LLsc);
LLsmp_disp = log(LLsmp);
imagebrowse(cat(ndim+2,LLpred_disp,LLsmp_disp),[-20 0],'xrange',[mn 1 1;mx numvox 2] )
figure(2);
% % display log ratio:
% sumLLsmp = sum(LLsmp,4);
imagebrowse(LLpred_disp-LLsmp_disp,[-5 5],'xrange',[mn 1;mx numvox] )