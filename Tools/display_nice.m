function [str] = display_nice( s , varargin) 
% str = display_struct( s , [options] ) 
%
% Displays the information in the scalar structure s with the defaut settings or
% provided options. Options can be passed as scalar structure or option value pairs
% (uses parse_defaults_optionvaluepairs).
% 
% Valid options:
%  name : name of the variable that is printed
%  maxNumCell, maxNumStruct, maxNumString : 
%         vector with for each level the maximum number of elements (of the
%         type in the name) that will be displayed. 
%  maxColString : number of colums of text before wrapping
%  maxRows, maxCols, maxNum : 
%         for each level the maximum number of Rows/columns/elements that
%         will be printed of a numeric array.
%  significants : for each level the number of significants that are
%          printed. 
%  identstep_struct, identstep_cell, identstep_num : additional indentation
%          ('tab size') used for structure, cell, and numeric values, respectively.
% -see start of the file for additional options- 
%
% OUTPUTS:
%  -   : if no output is requested it is shown on screen
%  str : cell array with strings that can be displayed on the screen:
%        disp([objTstr{:}])
%
% Created by Dirk Poot, Erasmus MC. 4-5-2015

% error('function needs to be finished');
%%
opt.name = [];
opt.min_ident_struct = 20;
opt.identstep_struct = 2;
opt.identstep_cell = 2;
opt.identstep_num = 2;

opt.maxLevel = 4;  % maximum nesting level. 

opt.maxNumCell    = [15 5 1];
opt.maxNumStruct  = [15 5 1];
opt.maxNumString  = 1000;
opt.maxColString  = 80;
opt.maxRows = [20 10 7 3];      % (1 x maxLevel) maximum number of rows of numeric array that is shown. 
opt.maxCols = [25 10 10 5];    % (1 x maxLevel) maximum number of columns of numeric array that is shown. 
opt.maxNum  = [1000 100 100 100];% (1 x maxLevel) maximum number of elements of numeric array that is shown. 
opt.significants = [5 5 5 5];      % (1 x maxLevel) maximum number of columns of numeric array that is shown. 
opt.scaleformat = '1e%+04d';  % or e.g.: '10^%d'
opt.startString = '''';
opt.endString = '''';

%%
% if ~isstruct(s) || numel(s)~=1
%     error('first input to display_struct should be a scalar structure');
% end;
if nargin>=2
    opt = parse_defaults_optionvaluepairs( opt, varargin{:} );
end;
if nargout>=1
    outputbuffer = cell(10,1);
    outputbuffer_nextindex = 1;
else
    outputbuffer =[];
end;
col = display_iternal( s, opt , 1, 1, 1);
if col>1
    print_('\n');
end;
if nargout>=1
    str = outputbuffer(1:outputbuffer_nextindex-1);
end;
return;

function print_( varargin )
    if isempty(outputbuffer)
        fprintf(varargin{:});
    else
        if numel( outputbuffer )<outputbuffer_nextindex 
            outputbuffer(outputbuffer_nextindex*2+10) = {};
        end;
        outputbuffer{ outputbuffer_nextindex } = sprintf(varargin{:});
        outputbuffer_nextindex = outputbuffer_nextindex + 1;
    end;
end

function [curcol] = display_iternal( s, opt, level, curindent, curcol)
% first define internal functions:
    function spaces=tab
        spaces = repmat(' ',1, curindent-curcol);
    end
    function prt( str , doalign )
        % prt( str [, doalign])
        % prints the string str to the screen. 
        % INPUTS
        %   str : character array that is printed to the screen. This
        %         function updates the current position and possibly aligns
        %         the output with correct indentation.
        %   doalign: default = true; If true the str is printed starting at
        %         the current indentation level. If needed, a new line is
        %         used. Any newline character inside str is adjusted to also 
        %         proved the correct indentation
        %         if false: the caller should be carefull about
        %         indentation. Should typically only be used to print a few
        %         characters that will always fit and should not be forced
        %         on a new line, like '{' for starting the printing of a
        %         cell array. Make sure the current position is
        %         sufficiently indented.
        if nargin<2
            doalign = true;
        end;
        if doalign
            str = strrep(str,char(10),[char(10) repmat(' ',1,curindent-1)]);
        end;
        if doalign && curcol > curindent
            curcol = 1;
            fullstr =[tab str];

            print_('\n%s', fullstr);
            curcol =  numel(fullstr)+1;
        else
            if doalign 
                str = [tab str];
            end;
            print_('%s',str);
            id = find(str==10);
            if numel(id)>=1
                curcol = numel(str)-id(end)+1;
            else
                curcol = curcol + numel(str);
            end;
        end;
    end
% print s:
if isstruct( s ) 
    % print structure (array):
    f = fieldnames(s) ;
    if level~=1 || numel(s)~=1
        szstr = sprintf('%d x ', size(s ) );
        prt(sprintf('[ %s struct ]',szstr(1:end-2)));
    end;
    if level <= opt.maxLevel && opt.maxNumStruct(level) >= numel(s)
        % print full content:
        oldident = curindent;
        if curindent < opt.min_ident_struct
            curindent = opt.min_ident_struct;
        end;
        newident = curindent;
        for k = 1 : numel(s)
            if numel(s)>1
                index = cell(1,ndims(s));
                [index{:}]=ind2sub(size(s),k);
                indexstr = sprintf('%d, ',index{:});
                curindent = oldident;
                prt( sprintf(' element [ %s ]:', indexstr(1:end-2) ), k~=1);
                curindent = newident;
            elseif level~=1
                prt( ' with content:', false);
            end;
            for fi = 1 : numel( f ) 
                if curcol>1
                    term = '\n';
                else
                    term = '';
                end;
                prt(sprintf( [term '%*s :'], curindent-2,  f{fi} ), false);
                curcol = display_iternal( s(k).(f{fi}) , opt, level+1, curindent+opt.identstep_struct, curcol);
            end;
        end;
    elseif level <= opt.maxLevel
        % print only fieldnames:
        prt( ' with fields:' ,false);
        curindent = curindent + opt.identstep_struct;
        for fi = 1 : numel( f ) 
            prt( f{fi} );
        end;
   %else: print nothing (except the size and that it is a structure).     
    end;
    
elseif iscell( s ) && level <= opt.maxLevel &&  numel(s) <= opt.maxNumCell(level)
    % print cell array
%     if level < opt.maxLevel &&  numel(s) <= opt.maxNumCell(level)
        % also print content:
        szs = size(s);
        if numel(szs)>=3
            s = s(:)';
            prt( 'reshape( {' );
        else
            prt( '{' );
        end;
        for k1 = 1 : size(s,1)
            for k2= 1: size(s,2)
                curcol = display_iternal( s{k1,k2} , opt, level+1, curindent+opt.identstep_cell, curcol);
                if k2 < size(s,2)
                    prt( ',' ,false);
                end;
            end;
            if k1 < size(s,1)
                prt( ';' ,false);
            end;
        end;
        if numel(szs)>=3
            prt( [' }, [ ' sprintf('%d ',szs) '])'] ,false);
        else
            prt( ' }' ,false);
        end;
%     else
%         % print size only:
%         szstr = sprintf('%d x ', size(s ) );
%         prt( sprintf('{ %s cell }', szstr(1:end-2)) );
%     end;
    
elseif (isnumeric(s) || islogical(s)) && level <= opt.maxLevel && ndims(s)<3 && size(s,1)<=opt.maxRows(level) && size(s,2)<=opt.maxCols(level) && numel(s)<=opt.maxNum(level)
    % print numeric arrays:
%     if level > opt.maxLevel || ndims(s)>=3 || size(s,1)>opt.maxRows(level) || size(s,2)>opt.maxCols(level) || numel(s)>opt.maxNum(level)
%         % print size and class only:
%         szstr = sprintf('%d x ', size(s ) );
%         prt( sprintf('[ %s %s ]',  szstr(1:end-2) , class(s) ) );
%     else
        % print content:
        srng = abs( s( isfinite( s ) ) );
        if isempty(srng)
            mx = 1;
            leaddigit = 0 ;
        else
            mx = max( srng ); % find maximum value.
            leaddigit = max(0,floor(log10(double(mx))));
        end;
        isint = all(srng==round(srng));
        if isint && leaddigit <= opt.significants(level)+1
            % print as integer:
            if islogical(s)
                fmt = ' %1d';
                prt( 'logical( [' );
            else
                fmt = [' %' num2str(leaddigit+2) 'd'];
                prt( '[' );
            end;
        else
            % print with fixed point notation:
            if leaddigit > opt.significants(level) || leaddigit<-3
                % scaling:
                if floor(log10( double( mx + .5*10^(leaddigit-opt.significants(level)+1 ) )))>leaddigit
                    % rounding almost certainly will cause 1 extra digit before the decimal
                    % point:
                    leaddigit = leaddigit +1;
                end;
                fmt = [' %' num2str(opt.significants(level)+2) '.' num2str(opt.significants(level)-1) 'f'];
                prt( sprintf([ opt.scaleformat ' * ['],leaddigit) );
                s = double(s).*(10^(-leaddigit));
            else
                fmt = [' %' num2str(opt.significants(level)+2+max(0,-leaddigit)) '.' num2str(max(0,opt.significants(level)-leaddigit-1)) 'f'];
                prt( '[' );
            end;
        end;
        is1col = size(s,2)==1 && size(s,1)>1;
        if is1col 
            fmt = [fmt ';'];
            s = s.';
        end;
        doalign = size(s,1)>1;
        level = level+1;curindent = curindent+opt.identstep_num;
        for r = 1 : size(s,1)
            numstr = sprintf( fmt, double(s(r,:)) );
            if is1col
                last=numel(numstr)-1; % remove trailing ';' from a column vector.
            else
                last=numel(numstr);
            end;
            prt( numstr(2:last) , doalign); % remove leading space. 
            if r < size(s,1)
                prt( ';' ,false);
            end;
        end;
        if islogical(s)
            prt('] )',false);
        else
            prt(' ]',false);
        end;
%     end;
elseif ischar(s)
    if numel(s) > opt.maxNumString 
        s = [s(1:opt.maxNumString-2) '...'];
    end;
    if numel(s) > opt.maxColString
        %%
        break_candidates = strfind(s,' ');
        breaks = 1;
        while breaks(end) + opt.maxColString<numel(s)
            nextbreak = breaks(end)+opt.maxColString;
            [dummy, nextbreakidx ] = min(abs(break_candidates-nextbreak));
            if abs( break_candidates(nextbreakidx)-nextbreak )*3 < opt.maxColString
                nextbreak = break_candidates(nextbreakidx);
            end;
            breaks(end+1)=nextbreak;
        end;
        breaks(end+1) = numel(s)+1;
        cs = mat2cell(s,1,diff(breaks));
        cs(2,:)={[opt.endString ' ...' sprintf('\n') opt.startString]};cs(2,end)={''};
        s = [cs{:}];
    end;
    prt( [opt.startString s opt.endString] ); 
else
    % default printing; size and type:
    szstr = sprintf('%d x ', size(s ) );
    prt( sprintf('[ %s %s ]',  szstr(1:end-2) , class(s) ) );
end;
end
end
