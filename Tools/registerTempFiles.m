function registerTempFiles( filenames )
% registerTempFiles( filenames )
%
% Collects the filenames of temporary files that the caller creates.
% When calling registerTempFiles( 'DeleteAll!' ), these are deleted. 
%
% INPUTS:
%  filenames:   char array with a single full file name
%            OR cell array with such file names. 
%               empty elements in the cell array are discarded. 
%               Note that existence of files is only checked in the 'DeleteAll!' call. 
%  'DeleteAll!' : Argument to delete all registered temp files. Note the capitalization and exlamation mark
%                 A summary message is printed on the deletion process. 
%
% As this function uses persistent variables to remember the already registered
% files, clear all, or clear registerTempFiles will clear this list. 
%
% 1-2-2018, D. Poot, Erasmus MC: Created


persistent filelist

if isempty(filelist)
    filelist = {};
end;
if ischar( filenames )
    if isequal(filenames , 'DeleteAll!' )
        % delete all registered files.
        deleteRegisteredFiles;
        return;
    else
        filenames = {filenames };
    end;
end;
if ~iscell( filenames )
    error( 'Only a char array with a single file name or a cell array with file names can be registered as temporary file');
end;
filelist = [filelist;filenames(:)];
    
    function deleteRegisteredFiles
        num_deleted = 0;
        num_skipped = 0; 
        num_empty   = 0;
        k = 1 ; 
        while k < numel( filelist )
            if isempty( filelist{ k } )
                num_empty = num_empty+1;
            elseif iscell( filelist{k} )
                % expand cells with lisst of filenames. 
                filelist = [filelist(1:k-1);filelist{k}(:);filelist(k+1:end)];
                k=k-1;
            elseif exist( filelist{ k } , 'file' )
                delete( filelist{ k } )
                num_deleted  = num_deleted  + 1;
            else
                num_skipped =num_skipped +1;
            end;
            k= k+1;
        end;
        filelist = {};
        fprintf('Deleted %d temporary files; %d ''files'' skipped as those entries did not specify an existing file and additionally %d empty elements were provided to registerTempFiles.\n', num_deleted, num_skipped , num_empty  );
    end
end