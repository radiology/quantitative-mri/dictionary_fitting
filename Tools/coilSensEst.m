addpath( genpath( 'D:\MATLAB\Tools' ) );

slice_idx = 4:5;
datF3 = permute( cat( 4,  acq_data( 1 ).images( :, :, :, :, slice_idx, : ) ), [1 2 5 6 4 3] );
sz = size(datF3)

coilC = ones( sz( 1:4 ) );
size( coilC )
datF4 = ones( [ sz( 1:3 ) 1 sz( 5 ) ] );
size( datF4 )

%%
% input : datF3 = [x y z coils images]
%         coilC = [x y z coils]
%         datF4 = [x y z   1   images]
sigma = 139;
imgP = permute(datF4,[5 1 2 3 4])/sigma;
coilP = permute(conj(coilC),[4 1 2 3 ]);
dataP = reshape( permute(datF3,[5 4 1 2 3]) , [sz(5)*sz(4) sz(1:3)])/sigma;
dataP = [real(dataP);imag(dataP);zeros([1,sz(1:3)])];
theta_in = [real(imgP);imag(imgP);real(coilP);imag(coilP)];
nImages = sz(5);
nCoils  = sz(4);
outputCoilNorm = 10;
fitfun = @(t) predict_imagePerCoil( t, nImages, nCoils, outputCoilNorm);
%%
imgRidx  = 0        + (1:nImages);
imgIidx  = nImages  + (1:nImages);
coilRidx = 2*nImages+ (1:nCoils);
coilIidx = 2*nImages+nCoils+ (1:nCoils);


%%
fitopts = struct;
if 1
    fitopts.spatialRegularizer= 'laplacian';
    w = 100*ones(size(theta_in,1),1);
    w(1:size(imgP,1)*2)=0; 
    fitopts.spatialRegularizerWeights = w;
else
    fitopts.spatialRegularizer= 'laplacianlog';
    ws = struct;
    ws.weights = complex(100,100);
    ws.selReal = coilRidx ;
    ws.selImag = coilIidx ;
    fitopts.spatialRegularizerWeights = ws;
end;
fitopts.imageType = 'real';
fitopts.noiseLevel = 1; 
fitopts.blockSize = [5 5 5];
fitopts.blockOverlap = [1 1 1];
fitopts.optimizer = 3;
tht_out = fit_MRI( fitfun, dataP, theta_in , fitopts);
%%
tht_lp = {tht_out};
for k=2:2
    tht_lp{k} = fit_MRI( fitfun, dataP, tht_lp{k-1}, fitopts);    
end;
%%
images_out = cell(size(tht_lp));
coils_out = cell(size(tht_lp));
for k = 1 : numel(tht_lp)
    images_out{k}   = complex( tht_lp{k}(imgRidx, :,:,:), tht_lp{k}(imgIidx, :,:,:) );
    coils_in        = complex( tht_lp{k}(coilRidx,:,:,:), tht_lp{k}(coilIidx,:,:,:) );
    coils_nrm       = sqrt( sum(coils_in.*conj(coils_in) ,1 ) ); % norm of coils. 
    coils_out{k}     = coils_in ./ coils_nrm( ones(nCoils,1), :,:,:) ; % normalize coils. 
end;
%% show images:
imagebrowse(cat(6,imgP, images_out{:} ) )
%% show coil sensitivities:
imagebrowse(cat(6,coilP,coils_out{:}) )
%%

[condest, maxeigv, mineigv, approxmaxeigv, approxmineigv] = inverse_free_condest( @(x) fun_hessmul( H_info, x) , numel(G) , [1 1], 1000, @(x2) fun_mulprecon( precon_info, x2) );
[condest2, maxeigv2, mineigv2, approxmaxeigv2, approxmineigv2] = inverse_free_condest( @(x) fun_hessmul( H_info, x) , numel(G) , [1 1], 1000 );