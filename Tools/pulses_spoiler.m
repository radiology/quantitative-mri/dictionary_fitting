function [pulses] = pulses_spoiler( Gspoil, Tspoil )
% [pulses] = pulses_spoiler( delaytime )
% generates MRI_pulses object for gradient spoiling
%
% INPUTS:
%  Gspoil: 3x1 gradient pulse [rad/unit space]
%  Tspoil: scalar indicating wait before pulse
%
%
% Created by Willem van Valenberg
% 06-12-2018

pulses = MRI_pulses( Tspoil, 8, zeros(3,3,0), [] , Gspoil );