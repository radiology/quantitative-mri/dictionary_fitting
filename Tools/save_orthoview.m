function [imgfiles, legendfile] = save_orthoview( img , focuspoint , upsampfact , filenamebase , varargin)
% [imgfiles, legendfile] = save_orthoview( img , focuspoint , upsampfact , filenamebase , [option, value]);
% Saves 3 orthogonal view through focuspoint of the 3D image img.
% as png's, 'neirest neighbor' upsampled in each dimension by 
% an integer factor upsampfact to the files [filenamebase num2str(i) '.png'], 
% for the views i=1:3
%
% INPUTS:
%  img : 
%    If image a scalar, and ishandle(img), it is assumed to be an
%    imagebrowse figure handle. When focuspoint is non empty it is set and the
%    orthogonal views are stored, including the colorbar of the imagebrowse figure.
%
%    If img is non-scalar and integer, it is passed as is, otherwise it should be scaled
%    between 0 (Black) and 1 (white).
%    A 4D image with the fourth dimension of size 3 is output as color image.
%  focuspoint : ndims(img) element integer vector which specifies the point through 
%               which the orthogonal views are taken. A cell array of focuspoints is 
%               supported for imagebrowse figures as well. 
%  upsampfact : default =1; scalar or vector with (preferably) integer upsample factor 
%               in each dimension. Since nearest neighbor interpolation is used
%               non-integer values will produce bad looking images. 
%  filenamebase : base of the filename of the orthogonal views. Do not include extension.
% option value pairs:
%  combinePanels   : merge subfigures into 1 image in the layout similar to the imagebrowse figure. 
%                    choose imagebrowse subfigdims and upsampfact smartly. 
%  backgroundColor : When combinePanels : color for pixels not filled with image pixels.
%  video           : default = false; when true write a video file instead of png files.
%  videoWriters    : custom open video writer objects; default (if video): 
%                       videoWriters = VideoWriter( filename , 'Motion JPEG AVI' ) ;
%                       open( videoWriters )
%  iterateDim      : convenience input. Specifies dimensions over which focuspoint
%                    is automatically iterated. focuspoint should be a single vector. 
% OUTPUTS:
%  imgfiles : cell array with file names of the (png) images. 
%  legendfile : character array with the file name of the legend.
%
% Created by Dirk Poot, ErasmusMC, 28-2-2012
opt.combinePanels = false;
opt.backgroundColor = [ 0 0 0 ];
opt.video = [];
opt.videoWriters = [];
opt.iterateDim = []; 
opt = parse_defaults_optionvaluepairs( opt, varargin{:});

    
if numel(img)==1 && ishandle(img)
    h = img;
    % imagebrowse figure handle. Get info:
    info = get( h, 'UserData');
    cellfocuspoints = iscell(focuspoint) || ~isempty( opt.iterateDim );
    if cellfocuspoints
        if ~isempty( opt.iterateDim )
            if iscell(focuspoint)
                error( 'iterateDim may only be specified for non-cell focuspoints');
            end;
            sz = size( info.IMG );
            it = cell(1, numel( opt.iterateDim ) );
            for k = 1 : numel( opt.iterateDim )
                it{k} = 1: sz( opt.iterateDim( k ) );
            end;
            [it{:}] = ndgrid( it{:} ,1);
            focuspointsmat = ones(numel(it{1}), 1) * focuspoint(:)';
            for k = 1 : numel( opt.iterateDim )
                focuspointsmat(:, opt.iterateDim( k ) ) = it{k}(:);
            end;
            focuspoints = mat2cell( focuspointsmat, ones(size( focuspointsmat,1),1), size(focuspointsmat,2) );
        else 
            focuspoints = focuspoint;
        end;
    else
        focuspoints = {focuspoint};
    end;
    if numel(upsampfact)==1
        upsampfact = upsampfact(ones(1,max(info.subfigdims(:))));
    end
    
    if ~isempty( opt.videoWriters )
        opt.video = true;
    elseif isempty(opt.video)
        opt.video = false;
    elseif opt.video
        fn = [filenamebase  '.avi'];
        opt.videoWriters = VideoWriter(fn,'Motion JPEG AVI');
        open( opt.videoWriters );
    end;
    
    imgfiles = cell(size(info.subfigdims,1), numel(focuspoints));
    % loop over all points:
    for idx = 1 : numel(focuspoints)
        % update point in imagebrowse figure: 
        focuspoint = focuspoints{idx};
        if ~isempty(focuspoint)
            for k=1:numel(info.tileDims)
                set( info.sliders( k ) , 'Value', focuspoint( info.tileDims(k) ) );
            end;
            if numel(info.tileDims)>1
                cb = get(info.sliders(1),'Callback');
                cb{1}([], [], cb{2});
            end;
        elseif cellfocuspoints && idx>1
            error('valid focuspoints should be provided in the cell array; only the first element may be empty (to keep the original position).');
        end;
        if cellfocuspoints
            locationstr = sprintf('-%d',focuspoint);
            locationstr(1) = '_';
        else
            locationstr = '';
        end;
        drawnow;pause(.1); % update the figure. 
        
        % read all subpanels:
        dinfo = get(info.panel,'UserData');
        sel = cell(1,3);sel{end}=':';
        subfigs =  struct('cdata',[],'colormap',[]);
        subfigstr = cell(1, size(info.subfigdims,1));
        for k = 1 : size(info.subfigdims,1)
            % get image:
            cursubfigimage = get( info.imgs(k),'CData' );
            for d = 1:2
                td = info.subfigdims( k, d );
                sel{d} = floor( (0:upsampfact(td)*size(cursubfigimage,d)-1)/upsampfact(td))+1;
            end;
            cursubfigimage = cursubfigimage(sel{:});
            iscolormapped = size(cursubfigimage,3)==1;
            if iscolormapped
                colrmap = get(h,'colormap');
            else
                colrmap =[];
            end;
            
            % get crosshair (only thing that is not drawn into the image, but a 'line' added to the axis)
            if ~isempty(info.crosshaircolor)
                for d = 1:2
                    td = info.subfigdims( k, d );
                    pos = dinfo.sliderpos(  info.tileDims == td );
                    crosshairidx = round( upsampfact(td)/2 ) + upsampfact(td) * (pos-1);
                    if size(info.crosshaircolor,1)==1
                        curcolor = info.crosshaircolor;
                    else
                        curcolor = info.crosshaircolor(td,:);
                    end;
                    if iscolormapped
                        [colrmap, curcolor]= addcolor(colrmap, curcolor);
                        if curcolor>=256 && isa( cursubfigimage, 'uint8' )
                            cursubfigimage = uint16(cursubfigimage);
                        end;
                    else
                        curcolor = uint8( curcolor *255 );
                    end;
                    selcrosshair = {':',':',':'};
                    selcrosshair{d} = crosshairidx;
                    cursubfigimage(selcrosshair{:}) = repmat(curcolor,size(cursubfigimage,3-d),1);
                end;
            end;
            
            % store internally:
            subfigs(k).cdata = cursubfigimage;
            subfigs(k).colormap = colrmap;
            subfigstr{k} = sprintf('%d',info.subfigdims(k,:));
        end;
        
        if opt.combinePanels
            gridsz = cell(1);
            gridsubfigidx = 0;
            row = 0; col = 0;
            lastpos = [-1 -1 -1 -1];
            for k = 1 : numel( info.subfigs )
                curpos = get(info.subfigs(k), 'Position');
                if curpos(2)~=lastpos(2)
                    row = row+1;
                    col = 1;
                else
                    col = col+1;
                end;
                gridsz{row, col} = size( subfigs(k).cdata );
                gridsubfigidx(row, col) = k;
                lastpos = curpos;
            end; 
            colwidth = zeros(1,size(gridsz,2));
            rowheight =  zeros(1,size(gridsz,1));
            for row = 1 : size(gridsz,1)
                for col = 1 : size(gridsz,2)
                    if ~isempty(gridsz{row,col})
                        colwidth( col ) = max( colwidth( col ) , gridsz{row,col}( 2 ) );
                        rowheight( row ) = max( rowheight( row ) , gridsz{row,col}( 1 ) );
                    end;
                end;
            end;
            colstart = cumsum([0 colwidth]);
            rowstart = cumsum([0 rowheight]);
            curimg = zeros( rowstart(end)+1, colstart(end)+1 , size(subfigs(1).cdata,3), class(subfigs(k).cdata) );
            iscolormapped = ~isempty(subfigs(1).colormap);
            if iscolormapped 
                colrmap = opt.backgroundColor; 
            else
                colrmap = [];
                %TODO: apply opt.backgroundColor in curimg. 
            end;
            for k = 1 : numel( subfigs )
                [row, col] = find( gridsubfigidx == k );
                curcdata = subfigs(k).cdata;
                if iscolormapped
                    [colrmap, newindex] = addcolor(colrmap, subfigs(k).colormap );
                    if max(newindex)>=256 && isa( curimg, 'uint8' ) 
                        curimg = uint16( curimg );
                    end;
                    curcdata = reshape( newindex( curcdata+1 ), size(curcdata) );
                end;
                curimg( rowstart( row ) + (1: size(subfigs(k).cdata,1)), colstart( col ) + (1: size(subfigs(k).cdata,2)) , : ) = curcdata;
            end;
            subfigs =  struct('cdata',curimg,'colormap',colrmap);
            subfigstr = {''};
        end;
        
        for k = 1 : numel( subfigs )
            % check if colormap is acceptable, if not use true-color: 
            if iscolormapped && size(subfigs(k).colormap ,1)>256
                subfigs(k).cdata = reshape( subfigs(k).colormap(subfigs(k).cdata+1,:),[size(subfigs(k).cdata) size(subfigs(k).colormap,2)]);
                iscolormapped = false;
            end;

            if opt.video
                writeVideo( opt.videoWriters(k) , subfigs(k) );
            else
                % save as png's
                % construct filename:
                fn = [filenamebase subfigstr{k} locationstr '.png'];
                imgfiles{k,idx} = fn;

                % write file:
                if iscolormapped                     
                    imwrite(subfigs(k).cdata, subfigs(k).colormap , fn, 'png');
                else
                    imwrite(subfigs(k).cdata, fn, 'png');
                end;
            end;
        end;
    end;
    if opt.video
        for k = 1 : numel( opt.videoWriters )
            close( opt.videoWriters(k) );
        end;
    end;
    % Save the currently active legend to a pdf file:
    lf = figure(); % create new figure that temporarily holds the legend;
    oldpos = get(info.colorbar,'position');
    oldunits = get(info.colorbar,'units');
    bottom = 0.02;
    top = .98;
    set(info.colorbar,'parent',lf,'Units','normalized','Position',[.01 bottom .3 top-bottom]);
    set(lf,'colormap',get(h,'colormap'));
    
    ytick = get(info.colorbar,'Ytick');ytick = ytick(:);
    yticklbl = str2num(get(info.colorbar,'YTickLabel'));
    nonzero = isfinite(ytick)& isfinite(yticklbl) & yticklbl~=0 & ytick~=0;
    tickscale = log( median( ytick(nonzero)./yticklbl(nonzero)));
    ylim_val = get(info.colorbar,'Ylim');
    if abs(tickscale)>1
        top = .89; % x 10^(b)  included in axis.
    else
        % reduce hight when largest tick is close to top
        top  = min(1, .97- (max(ytick)-max(ylim_val))/diff(ylim_val)); 
    end;
    % increase bottom spacing when smallest tick is close to bottom:
    bottom = max(.02, .04-(min(ytick)-min(ylim_val))/diff(ylim_val));
    set(info.colorbar,'Position',[.01 bottom .3 top-bottom]);

    legendfile = [filenamebase 'legend'];
    drawnow;pause(.1); % make sure all drawing is complete.
    PrintToPdf( legendfile, [1.5 5]);
    drawnow;pause(.1); % make sure all drawing is complete.
    set(info.colorbar,'parent',h,'Units',oldunits,'Position',oldpos);
    close(lf);
    return;
end;

sel = repmat({':'},1,4);
if numel(upsampfact)==1
    upsampfact = [1 1 1 1]*upsampfact;
end;
for k=1:3
   sel{k} = floor( (0:upsampfact(k)*size(img,k)-1)/upsampfact(k))+1;
end;
for k=1:3
    fn = [filenamebase num2str(k) '.png'];
    selk = sel;
    selk{k} = focuspoint(k);
    selimg = permute( img(selk{:}) ,[1:k-1 k+1:4 k]);
    imwrite(selimg, fn, 'png');
end;


function [colrmap, newindex]= addcolor(colrmap, curcolors)
% [colrmap, newindex]= addcolor(colrmap, curcolor)
% adds a new color to the colormap if it is not in yet.
%
% Created by Dirk Poot, Erasmus MC, 11-10-2016
% 8-6-2017: DPoot, Added support for adding multiple colors. 

[mindst,minidx] = min( sum( bsxfun(@minus, permute( colrmap, [3 2 1]), curcolors ).^2, 2),[],3);
isout = mindst >= 1/256^2;
newindex = minidx-1; % initialization assuming added colors already in colrmap
if any(isout)
    newindex(isout) = size(colrmap,1)+(0:nnz(isout)-1);% adjust for 0 based counting in indexed images
    colrmap= [colrmap;curcolors(isout,:)];
end;