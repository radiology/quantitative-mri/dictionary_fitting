function [out] = invertfun( fun, val , sugestedRange, extra_args ) 
% [out] = invertfun( fun, val , sugestedRange, extra_args) 
% Evaluates the inverse of the scalar function fun.
% So this function tries to compute out such that
%   val = fun( out )
%   OR: val = fun( out, extra_args{:} )
% INPUTS:
%   fun  : a scalar function; should accept vector input. (e.g. fun = @sin)
%          The function should be monotonic on suggestedRange, and the span of val.
%   val  : The locations for which the inverse should be computed
%          Scalar, vector, matrix or ND-array.
%   suggestedRange: A 2 element vector with lower and upper bound of out
%                   (as with fzero). 
%   extra_args : cell array with extra arguments for fun. 
%                if the size of an element of extra_args is equal to the size of val
%                it is selected for each val. 
% Created by Dirk Poot, Erasmus MC. 
% 11-7-2012

if nargin<4
    extra_args = {};
end;
if ~iscell( extra_args )
    extra_args  = { extra_args };
end;
has_extra_args = ~isempty(extra_args);
if has_extra_args
    sel_extra_args = false( size( extra_args ) ); 
    for k= 1: numel( sel_extra_args );
        sel_extra_args( k ) = isequal( size( val ), size( extra_args{k} ));
    end;
    sel_extra_args = find( sel_extra_args); 
    cur_extra_args = extra_args;
end;
out = zeros( size(val));
for k=1:numel(out)
    if has_extra_args
        for argid = sel_extra_args
            cur_extra_args{argid} = extra_args{argid}( k );
        end;
        fun_k = @(x) fun(x, cur_extra_args{:} )-val(k);
    else
        fun_k = @(x) fun(x)-val(k);
    end;
    outk = fzero( fun_k , sugestedRange);
    out(k) = outk;
end;

function test
%% scalar function
fun = @sin
val = -1+2*rand(100,1);
o = invertfun( fun, val , [-pi/2 pi/2] );
o_GT = asin( val );
disp('norm of difference wrt ground truth ,  norm of ground truth');
norm( o - o_GT ), norm( o_GT )
%% 2 argument function
fun2 = @atan2; % atan2( o , xval ) = atan( o/xval ) = val
               % => o/xval = tan( val )
               % => o = tan(val)*xval
xval = rand(size(val)); %for negative x, atan2 has a discontinuity at o==0 that we want to avoid. 
o = invertfun( fun2, val , [-10 10] , xval);
o_GT = tan( val ) .*xval;
disp('norm of difference wrt ground truth ,  norm of ground truth');
norm( o - o_GT ), norm( o_GT )