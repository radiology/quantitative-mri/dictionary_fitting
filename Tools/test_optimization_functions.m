function [results, problemnames , optimizernames] = test_optimization_functions( testno, optimizerno )
% test_optimization_functions()
%
% Performs a few test optimizations with several optimization methods. 
% Can be used to compare performance of methods and for regression testing
% as the optimization problems are deterministic and the results cached. 

cachefilename = 'test_optimization_functions_results.mat';
cachefilevars = {'results','problemnames','optimizernames'};
if exist( cachefilename ,'file');
    tmp = load( cachefilename ,cachefilevars{:});
    referenceresults = tmp.results;
    referenceproblemnames = tmp.problemnames;
    referenceoptimizernames = tmp.optimizernames;
else
    referenceresults = {};
    referenceproblemnames = {};
    referenceoptimizernames = {};
end;
referencemodified = false; 

rnginit = rng;
rng( 0, 'twister');rngstart =rng;
testid =0 ; testname =''; curoptimizer = '-none-';
try
    % problems: cell array with in each element a function handle of a function with no arguments:
    %   [problem , groundtruth] = problems{i}();
    %   problem : struct specifying the current problem with the fields:
    %     init : vector/array with initial position. 
    %     fun : function handle of function to be optimized
    %       [f, g, hinfo ] = fun( x ) 
    %           or a two element cell array in which the first one is an (almost) 
    %           quadratic function and the second one is more non-linear. (This option is provided for conjgrad_nonlin that uses this structure)
    %           optimizers: Combine by makeOneCostFunction
    %     hessmul : hessian multiply function (or cell array of them for cell array fun)
    %       [Hv] = hessmul( hinfo, v)
    %     name : name of the current problem for display and test identification purposes.   
    %   groundtruth : parameter vector/array with ground truth solution. 
    %                 can be empty if ground truth solution is unknown. 
    % NOTE1: The reason to use a cell array with function handles is that each problem
    %       potentially migth take some time and memory to setup and with this format
    %       only 1 problem is in memory at the same time. Also future interface extensions 
    %       might allow running only a selection of tests. 
    % NOTE2: When modifying a test always modify 'name' as well. Otherwise the comparision of 
    %        the results will incorrectly identify failure. 
    %
    % optimizers : 2 column cell array [ optimizer_fun, optimizer_name]      
    %   [result , comparator] = optimizer_fun( problem )
    %   result: probably a struct with the optimized x as well as diagnostic
    %           information on CPU time used, number of iterations, number of function
    %           calls, number of hessian multiplications, ... 
    %   comparator : function that compares two 'result's, one computed now and one 
    %           reference result that was stored. Also compares to ground truth (when available)
    %           [isOK] = comparator( result, referenceresult, groundtruth )
    %           suggested to use defaultResultComparator (but that might not be fully suitable for all optimizers)
    % NOTE: optimizer_fun is a wrapper around an actual optimization routine with 
    %       certain options. 
    
    problem_default = struct;
    problem_default.name = '';
    problem_default.init = [];  % real valued array
    problem_default.fun = [];   % [f, g, hinfo ] = fun( x );  % size(x)==size(init)
    problem_default.hessmul = []; % Hv = hessmul( hinfo, v);  % size(v)==[numel(init) 1];
    problem_default.preconditionerMake = []; % precon_info = preconditionerMake( x, f, g, H_info , old_precon_info);
    problem_default.preconditionerInitialInfo = []; % initial old_precon_info for preconditionerMake. 
    problem_default.preconditionerMultiply = []; % d = precon_fun( precon_info, y );
                                                 %  approximates  inv( Hessian_of_fun ) * y
    
    problems = {@() test_rosenbrock(1);
                @() test_rosenbrock(10);
                @() test_quadratic(43) ;
                @() test_fitMRIinternals(12 , false);
                @() test_fitMRIinternals(23 , true);
                @() test_sparserecon( true , false, false) 
                @() test_sparserecon( true , false, true) };
    optimizers = {@wrap_fminfast, 'fminfast';
                  @wrap_fminfast_N, 'fminfast_N';
                  @wrap_fminunc,  'fminunc';
                  @wrap_conjgrad_nonlin,  'conjgrad_nonlin'};
    if nargin>=1 && ~isempty( testno ) 
        problems = problems( testno );
    else
        testno = [];
    end;
    if nargin>=2 && ~isempty( optimizerno )
        optimizers = optimizers( optimizerno, : );
    else
        optimizerno = [];
    end;
    optimizernames = optimizers(:,2);
    [dummy, referenceoptimizerids] = ismember( optimizernames, referenceoptimizernames );
    isOK = false( numel( problems ), size( optimizers ,1 ) );
    problemnames = cell( numel( problems ) , 1 );
    results = cell( numel( problems ) , size( optimizers ,1 ) );
    for testid = 1 : numel( problems )
        [curtest , groundtruth] = problems{testid}(); 
        curtest = parse_defaults_optionvaluepairs( problem_default, curtest);
        testname = curtest.name;
        problemnames{testid } = testname;
        [dummy , reftestid ] = ismember( testname, referenceproblemnames ) ;
%         if reftestid == 0
%             reftestid = size( referenceresults, 1 )+1;
%             referenceresults{reftestid,1}={}; % expand array. 
%             referenceproblemnames{reftestid} = testname;
%         end;
        % TODO: find correct reference test by matching problemnames and using that index below for referenceresults.
        
        for optid = 1 : size( optimizers ,1 )
            rng( rngstart );
            refoptid = referenceoptimizerids( optid );
            curoptimizer = optimizers{optid,2}();
            
            [result , comparator]= optimizers{optid,1}( curtest );
            results{ testid, optid } = result; 
            if reftestid==0 || refoptid==0 || isempty( referenceresults{ reftestid, refoptid } )  %size( referenceresults, 1 )< testid || size(referenceresults,2)<optid || isempty( referenceresults{ testid, optid } ) 
                fprintf('Test %d (%s) with optimizer %s has no old results. Adding current result to the test results repository.', testid, testname, curoptimizer );
%                 referenceresults{ testid, optid } = result; 
                referencemodified = true; 
                if ~isempty( groundtruth )
                    disp(['relative error : ' num2str( norm( result.x(:)-groundtruth(:) ) /norm(groundtruth(:) ) )]);
                end;
            else
                % compare current results with reference results:
                isOK( testid, optid ) = comparator( result, referenceresults{  reftestid, refoptid }, groundtruth);
            end; 
            curoptimizer = '';
        end;     
        testname ='';
    end;
    if referencemodified 
        if isempty( testno ) && isempty( optimizerno )
            str = input(sprintf('New results are computed.\n Do you want to overwrite all reference results stored in "%s"\n with the results computed in this run? (Y/N): ',cachefilename ) ,'s');
        else
            str = 'N';
            disp(sprintf('Only a selection of tests did run.\nResults of some of these tests are not in the reference file.\nHowever, to avoid removing results of test that did not run, I dont save the current results.\nPlease run the complete batch of tests to update the cached results.'));
        end;
        if isequal( str, 'Y');
            if exist( cachefilename , 'file')
                [dum, backupfn , ext]= fileparts( cachefilename );
                backupfn = sprintf( '%s_backup%s%s', backupfn , datestr( now, 'YYYYmmdd_HHMMSS') , ext);
                disp( ['Backup of old results in: ' backupfn ])
                movefile( cachefilename , backupfn );
            end;
            disp( 'Storing new results ');
            save( cachefilename ,cachefilevars{:});        
        else
            disp( 'currently computed (and returned) results not stored.');
        end;
    end;
catch ME
    rng( rnginit); % restore random number generator
    warning('test_optimization_functions:ErrorOccurred', 'Error occured in test %d (%s) with optimizer %s', testid, testname, curoptimizer );
    rethrow(ME)
end;
rng( rnginit);
end

%% %%%%%%%%%%%%%%%%%%%%%%% TEST FUNCTION DEFINITIONS BELOW %%%%%%%%%%%%%%%%%


function [s, GT] = test_rosenbrock( n )
s = struct;
s.fun = @rosenbrock;
if n==1
    s.init = [-1 1];
else
    s.init = randn(2,n);
end;
s.name = sprintf( 'Rosenbrock_%dD',2*n );
GT = ones(2,n);
end

function [s, GT] = test_quadratic( n )
s = struct;
if n==1
    s.init = [-1 1];
else
    s.init = randn(2,n);
end;
Amat = randn(n+1,n);
A = @(x) Amat*x(:,:);
At = @(x) Amat'*x;
Y = randn(n+1,1);
s.init = randn(n,1);
s.fun = @(tht) leastSquaresCostFunction(tht, A, At , Y) ;
s.hessmul = s.fun( 'hessmulfun' );
s.name = sprintf( 'quadratic_%d', n );
GT = Amat\Y;
end

function [s, GT] = test_fitMRIinternals(n , hasprior )
s = struct;
s.init = rand(2,10);
TE = [0 10 20 50 80]'/1000;
thtGT = [10 + 100*rand(1,n); 3+rand(1,n)];
noiseLevel = .1;

predfun = @(tht) predict_SET2( tht, TE );
logpdffun = @(data, A) logricepdf(data, A, noiseLevel, [false true false]);
logpriorfun = struct;
if hasprior 
    parameterPrior_mu = {[30;0]};
    invSigma = {[1 .1; .1 3]};
    logpriorfun.fun = @(tht) logdoubleExpPrior(tht, parameterPrior_mu, invSigma);
    logpriorfun.gradient_linindex = 1:2;
    logpriorfun.hessian_write_funhessindex = 1:3;
    s.name = sprintf( 'T2fitting_%d_prior', n );
    thtGT( 2,4:5:end)=0;
else
    logpriorfun.fun = [];
    s.name = sprintf( 'T2fitting_%d', n );
end;

data_GT = predfun(thtGT);
data = AddRiceNoise( data_GT, noiseLevel );
s.init = 10+rand(2,n);
[funHessian_I, funHessian_J] = find( triu( ones( size( thtGT,1 ) ) ) );
s.fun = @(tht) voxelLLfun_m(tht, predfun, logpdffun, data, 2, false, logpriorfun, funHessian_I, funHessian_J) ;
s.hessmul = @(hessinfo, Y) hessinfo.hessMulFun(hessinfo, full(Y));
GT = [];
end


function [s, GT] = test_sparserecon( hasprior, onefunction, doprecon )
%%
s = struct;
s.init = rand(2,10);
TE = [0 10 20 50 80]'/1000;
% 'Modified Shepp-Logan' - modified to be a bit more MRI like:
toft = [  1   .69   .92    0     0     0   
        -.4  .6624 .8740   0  -.0184   0
         .2  .1100 .3100  .22    0    -18
         .2  .1600 .4100 -.22    0     18
         .1  .2100 .2500   0    .35    0
         .1  .0460 .0460   0    .1     0
         .1  .0460 .0460   0   -.1     0
         .1  .0460 .0230 -.08  -.605   0 
         .1  .0230 .0230   0   -.606   0
         .1  .0230 .0460  .06  -.605   0   ];
phsz = 64;     
A = phantom( toft, phsz) * 20;
toft(:,1) = [20;60;-50;-40;-10;-30;-30;20;20;20];
R2 = phantom( toft, phsz );
thtGT = permute( cat( 3, R2, A),[3 1 2]);

opts = struct;
opts.maxfunArgsOut = 2;
opts.function = @(tht) predict_SET2( tht, TE , false);
opts.noiseLevel = .67;
opts.fields = [];
opts.maxTracesInBlock = [inf inf];
opts.function_jacMul = @function_jacMul;
opts.function_jacMulAdj = @function_jacMulAdj;
opts.numPDFoptpar = 0;
opts.spatialSize = size(A);
opts.voxelSpacing = [1 1];
opts.numImages = numel(TE);
opts.numParam = size(thtGT,1);
opts.logPDFfun = @lognormComplexpdf;
logpriorfun = struct;
if hasprior 
    parameterPrior_mu = {[30;0]};
    invSigma = {[1 .1; .1 3]};
    logpriorfun.fun = @(tht) logdoubleExpPrior(tht, parameterPrior_mu, invSigma);
    logpriorfun.gradient_linindex = 1:2;
    logpriorfun.hessian_write_funhessindex = 1:3;
    logpriorfun.hessian_I = [1;1;2];
    logpriorfun.hessian_J = [1;2;2];
    
    s.name = sprintf( 'T2fittingsparsekspace_prior' );
else
    logpriorfun.fun = [];
    s.name = sprintf( 'T2fittingsparsekspace' );
end;
opts.parameterPrior = logpriorfun;
opts.doRegularize = true;
if opts.doRegularize 
    opts.spatialRegularizer = 'TV_cyclic';
    opts.spatialRegularizerWeights = [1;1];
    opts.spatialRegularizerLS =[];
    [opts.spatialRegularizer, opts.spatialRegularizeBorder] = parse_spatialRegularizer( opts );
end;
pattern = cell(numel(TE),1);
for k = 1 : numel( pattern )
    pattern{k} = false( opts.spatialSize );
    pattern{k}( ceil( rand( prod(opts.spatialSize )/16 ,1)*prod( opts.spatialSize )) ) = true;
    opts.projectSelectSource{k} = k;
end;
coilssens = complex(randn( [opts.spatialSize 2]), randn( [opts.spatialSize 2]));
if exist('makeKspaceProjectFunctions') <= 1
    error('make sure makeKspaceProjectFunctions is on the path; e.g:\n addpath(''c:\Users\Dirk\BitBucket\MATLAB\ImageRecon\'')');
end;
[opts.project, opts.projectParameters ]= makeKspaceProjectFunctions( pattern, coilssens, [], true);
opts.doComputeDerivativeRegularizationScale = false;
data_GT = reshape( opts.function(thtGT), [opts.numImages opts.spatialSize]);
for k = 1 : numel( pattern )
    opts.data_in{k} = opts.project{k,1}( permute(data_GT(k,:,:),[2 3 1]), opts.projectParameters{k} );
    opts.data_in{k} = opts.data_in{k} + opts.noiseLevel.* complex( randn( size( opts.data_in{k})), randn( size( opts.data_in{k}) ));
end;
s.init = 10+rand(size(thtGT));
[opts.funHessian_I, opts.funHessian_J] = find( triu( ones( size( thtGT,1 ) ) ) );

if onefunction
    % 1 function 
    s.fun = @(tht)  fullThetaCostfun(tht, opts) ;
    s.hessmul = @(hessinfo, Y) fullThetaCostfun_HessMul(hessinfo, Y,opts);
    s.name = [s.name '1'];
else
    s.name = [s.name '2'];
    % split (almost) quadratic and nonlinear part (priors):
    optsq = opts;
    optsq.doRegularize = false;   % turn regularization off (fullThetaCostfun)
    optsq.parameterPrior.fun = [];% turn prior off (fullThetaCostfun)
    optsn = opts;
    optsn.function = @(x) deal_relaxed(zeros(0,size(x,2)),[],[]); % make function evaluation in predictAllImagesfun no work
    optsn.function_jacMul = @(ifo,x)[];
    szx = size( s.init(:,:) );
    optsn.function_jacMulAdj = @(ifo, x) zeros(szx);
    optsn.numImages = 0;
    optsn.data_in = [];
    optsn.project = [];
    optsn.logPDFfun = @(d, p, n, s) deal_relaxed( 0, 0,0);
    s.fun = {@(tht)  fullThetaCostfun(tht, optsq), @(tht)  fullThetaCostfun(tht, optsn)} ;
    s.hessmul = {@(hessinfo, Y) fullThetaCostfun_HessMul(hessinfo, Y,optsq), @(hessinfo, Y) fullThetaCostfun_HessMul(hessinfo, Y,optsn)};    
    if 0
        %% debug test if function is validly splitted. 
        [f,g,h] = fullThetaCostfun(s.init, opts) ;
        [f1,g1,h1]= s.fun{1}(s.init);
        [f2,g2,h2]= s.fun{2}(s.init);
        (f1+f2)-f
        norm(g1+g2 - g)
        r=randn(numel( s.init),3);
        Hx = fullThetaCostfun_HessMul(h, r,opts);
        H1x = s.hessmul{1}( h1, r );
        H2x = s.hessmul{2}( h2, r );
        norm(H1x+H2x - Hx)
    end;
end;
if doprecon
    s.name = [s.name '_precon'];
    [s.preconditionerInitialInfo.Q_structure , s.preconditionerInitialInfo.Q ] = SGD_make_preconditioner_structure( size(s.init), 100*ones(1,4) );
    s.preconditionerInitialInfo.x = s.init;
    s.preconditionerInitialInfo.g = [];
    s.preconditionerMake = @SGD_update_preconditioner_wrapper;
    s.preconditionerMultiply = @SGD_apply_preconditioner_wrapper;
end;
GT = [];
end

function precon = SGD_update_preconditioner_wrapper( x, f, g, Hinfo, precon ) 
if ~isempty( precon.g ) % first call old g is empty (by initialization). 
    precon.Q = SGD_update_preconditioner( precon.Q , precon.x-x, precon.g - g , precon.Q_structure );
end;
precon.x = x;
precon.g = g;
end
function Px = SGD_apply_preconditioner_wrapper( preconinfo, x )
Px = SGD_apply_preconditioner( preconinfo.Q, x, preconinfo.Q_structure, 1);
end
%% %%%%%%%%%%%%%%%%%%%%%%% OPTIMIZATION FUNCTION DEFINITIONS BELOW %%%%%%%%%%%%%%%%%


function [r, comp] = wrap_fminfast( problem )
problem = makeOneCostFunction( problem );
opt = struct;
if ~isempty( problem.hessmul )
    opt.HessMult = problem.hessmul;
end;
if ~isempty(  problem.preconditionerMake )
    opt.Preconditioner = problem.preconditionerMake;
    opt.Preconditioner_initialInfo = problem.preconditionerInitialInfo;
    opt.Preconditioner_Multiply = problem.preconditionerMultiply;
end;
opt = parse_defaults_optionvaluepairs( fmin_fast(), opt ); % check that we only added valid options. 
r = struct;
tic
[r.x, r.fval, r.exflag, r.G, r.H, r.output] = fmin_fast( problem.fun, problem.init, opt );
r.CPUtime = toc;
comp = @defaultResultComparator;
end

function [r, comp] = wrap_fminfast_N( problem )
problem = makeOneCostFunction( problem );
opt = struct;
if ~isempty( problem.hessmul )
    opt.HessMult = problem.hessmul;
end;
if ~isempty(  problem.preconditionerMake )
    opt.Preconditioner = problem.preconditionerMake;
    opt.Preconditioner_initialInfo = problem.preconditionerInitialInfo;
    opt.Preconditioner_Multiply = problem.preconditionerMultiply;
end;
opt.dynamicGradientTrustRegion = true;
opt.MaxFunEvals = 200;
opt.maxIter = 200;
opt.lineSearchMethod = 2;
opt = parse_defaults_optionvaluepairs( fmin_fast(), opt );
r = struct;
tic
[r.x, r.fval, r.exflag, r.G, r.H, r.output] = fmin_fast( problem.fun, problem.init, opt );
r.CPUtime = toc;
comp = @defaultResultComparator;
end

function [r, comp] = wrap_fminunc( problem )
problem = makeOneCostFunction( problem );
opt = struct;
if ~isempty( problem.hessmul )
    opt.HessMult = problem.hessmul;
end;
opt.GradObj = 'on';
opt.Hessian = 'user-supplied';
opt.LargeScale = 'on';
opt.Display = 'off';
opt.MaxIter = 100; 
% opt.Preconditioner = problem.preconditionerMake;% disabled preconditioner because currently cannot convert preconditioner make (and fminunc has no preconditioner multiply)
opt = parse_defaults_optionvaluepairs(optimset('fminunc'), opt );
% opt.PreconditionerMult =  problem.preconditionerMultiply;
r = struct;
tic
[r.x, r.fval, r.exflag, r.output, r.G, r.H] = fminunc( problem.fun, problem.init, opt );
r.CPUtime = toc;
comp = @defaultResultComparator;
end

function [r, comp] = wrap_conjgrad_nonlin( problem )
fun = struct;
if iscell( problem.fun) 
    if numel( problem.fun ) ~=2
        error('wrong number of functions');
    end;
    fun(1).fun = problem.fun{1};
    fun(1).hessmul = problem.hessmul{1};
    fun(2).fun = problem.fun{2};
    fun(2).hessmul = problem.hessmul{2};
else
    fun(1).fun = [];
    fun(1).hessmul = [];
    fun(2).fun = problem.fun;
    fun(2).hessmul = problem.hessmul;
end;
opt = struct;
opt.display = 0;
opt.max_iter = 3000;opt.epsilon =1e-5;opt.revaluate_quadratic_it = 4;
tic
[r.x, r.fval, r.G] = conjgrad_nonlin( fun, problem.init, opt);
r.CPUtime = toc;
comp = @defaultResultComparator;
end


%% %%%%%%%%%%%%%%%%%%%%%%% INTERNAL HELPER FUNCTIONS BELOW %%%%%%%%%%%%%%%%%%%


function isOK = defaultResultComparator( r1, r2, GT )
isOK = true;
f = fieldnames( r1 );
if ~isequal( f, fieldnames( r2 ) )
    error('fieldnames not equal between reference and new result. Cannot compare');
end;
specialfields = {'CPUtime' };
for k = 1 : numel( f) 
    if ismember( f{k}, specialfields)
        switch f{k}
            case 'CPUtime'
                if abs(log( (r1.CPUtime+.5)/(r2.CPUtime+.5) ))>.5
                    disp('CPU time differs')
                end;
            case 'x'
                if nargin>=3 && ~isempty( GT )
                    disp(['relative error : ' num2str( [norm( r1.x(:)-GT(:) ) norm( r2.x(:)-GT(:) )]/norm(GT(:) ))])
                end;
                if ~isequal( r1.(f{k}), r2.(f{k}) )
                    disp(['result field ' f{k} ' differs.']);
                end;     
        end;
    else
        if ~isequal( r1.(f{k}), r2.(f{k}) )
            disp(['result field ' f{k} ' differs.']);
        end;
    end;
end;
end

function [p] = makeOneCostFunction( p )
if iscell( p.fun) 
    origfun = p.fun;
    orighess = p.hessmul;
    p.fun = @(x) addCostFunctions( x, origfun , orighess);
    p.hessmul = p.fun( 'hessmulfun' );
end;
end