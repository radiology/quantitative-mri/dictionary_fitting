function [R ] = make_minHessian_preconditioner( x, f, g,  H, oldR, minHess)
% [R ] = make_default_preconditioner( x, f, g, H, oldR, minHess)
% Created default preconditioner for the fmin_fast optimization routine.
% Does a cholesky decomposition of H, with some adjustments when H is not
% positive definite.
%
% INPUTS
% x,f,g H : input and outputs of current point for which a preconditioner should be constructed. 
% oldR : old preconditioner info
% minHess : min hessian: <0 : Relative hessian modification: add (-minHess)*max(diag( H )) to diagonal of H
%                        >0 : Absolute hessian modification: add (minHess) to diagonal of H
% 
% Created by Dirk Poot, Erasmus MC
% 6-2-2013

if minHess < 0
    initlamba = (-minHess)*max(abs( H(:)));
else
    initlamba = minHess;
end;
H(1:size(H,1)+1:end) = H(1:size(H,1)+1:end) + initlamba ;
[R, info] = chol(H);
if info>0
    % if still not positive definite:
    diagH = full(diag(H));
    if any(~isfinite(diagH))
        maxlpcount = -1;
    else 
        maxlpcount = 4; % .01*4^4 > 1 => we have added a diagonal matrix larger than the maximum absolute eigenvalue. Hence the matrix should be positive. If not it probably has nan,inf, or other strange things. 
    end;
    lambda = .01* max(abs(diagH)); % set initial lambda to 1% of a very rough estimate of the maximum eigenvalue
    n = size(H,1);
    lpcnt = 0;
    while info>0 && lpcnt <= maxlpcount
        H = H + lambda * eye(n);
        [R, info] = chol(H);
        lambda = lambda * 4;
        lpcnt = lpcnt + 1;
    end;
    if info>0
        % we seem to be unable to find a preconditioner
        % Probably not all elements of H are finite
        % (or maybe non symmetric??)
        % total lambda =approx = 4^4*.01*max(abs(eig(H)) 
        %                      = 2.56 * max(abs(eig(H)) 
        % So even when H was negative definite, we should have solved
        % it before exitting the loop above.
        if any(isfinite(diagH))
            R = eye(n) * max(abs(diagH(isfinite(diagH)))) ;
        else
            % no finite element in H, so give up on finding a good preconditioner and just return the identity matrix:
            R = eye(n);
        end;
    end;
end;

