function [ x , fval, final_grad ] = conjgrad_nonlin( funcs , x , varargin)
% [xout, fval, final_grad] = conjgrad_nonlin( funcs , xinit , [option, value]);
%
% Non linear conjugate gradient method.
% Best if hessian is always positive definite.
%
% Implementation based on section B4 of 
% "An Introduction to the Conjugate Gradient Method Without the Agonizing Pain.pdf"
%
% INPUTS:
% funcs : a 2 element structure that specifies two functions, the sum of
%         which is minimized.
%         The structure should contain the fields:
%    fun     : the actual functions; format as for fminunc:
%               [fval , grad, hess_info] = fun( x );
%              third output is hessian information used for:
%    hessmul : an function that multiplies with the hessian.
%               H*x = hessmul( hess_info , x);
%              (same format as for fminunc)
%     funcs(1) : should specify a quadratic function 
%     funcs(2) : should specify a non-quadratic function. 
%
% option-value pairs or scalar structure with options; valid options:
%  epsilon : scalar convergence tolerance. When gradient norm reduces by at least 
%            this factor from it's initial value, optimization stops. 
%  max_iter  : maximum number of (outer) iterations
%  revaluate_quadratic_it : Default = 50; number of iterations after which the 
%            quadratic function is revaluated. Can be used to avoid accumulation 
%            of roundoff error (default, although higher might be acceptable as
%            well). When the 'quadratic' function is not perfectly quadratic 
%            this should be set to a lower value. 
%  revaluate_quadratic_hessian : boolean; default=true; Update hessian
%            information when revaluating the quadratic function.
%  searchHistory : default 0; 
%            Number of previous search directions that are cached and used
%            in the non linear search part. Set to 5 to 20 when the
%            nonlinear function evaluates much quicker (CPU time) than a
%            hessian multiplication of the quadratic function. For very
%            large problems: take the memory consumption 2 x 'searchHistory'
%            copies of x into account, in addition to approx 10 variables
%            of the size of x and the hessian info of the two functions.    
%  othogonalizationmode : 0 = Fletcher-Reeves search vector update (default and usually prefered)
%                        -1 = Polak-Ribiere search vector update
%                        >0 = Explicit orthogonalization w.r.t. previous
%                             search vectors, requires searchHistory > 0.
%                             This typically reduces the number of
%                             iterations at the expense of extra hessian
%                             multiplication of the non linear part.
%                             Typically 1 has optimal performance when the
%                             hessian multiplies of the non-linear function
%                             are substantially, but not extremely much, faster than hessian
%                             multiplies with the quadratic function. 
%  collect_statistics : default false => fval is 2 element vector with
%            final function value of quadratic and non linear parts.
%            When true: fval is a structure with information at each iteration.
%  
% OUTPUTS:
%  xout : best x that I found.
%  fval : function cost at xout
%  final_grad : gradient of cost function at xout.
%
% Created by Dirk Poot, Erasmus MC, 31-10-2012
%       May 2013: Added searchHistory and orthogonalization mode

% Constants/initialisation:
options.epsilon = .0001; % convergence tolerance.
options.max_iter  = 200; % maximum number of iterations.
options.make_precionditioner = []; % function that constructs the preconditioner M and preconinfo
                                   % [Preconditioner_Multiply, preconinfo] = make_precionditioner( {hess_info_func_i ,.. } )
                                   % if make_precionditioner is empty:
                                   %     preconinfo = {hess_info_func_i ,.. } 
                                   %     and the input argument Preconditioner_Multiply is used.
options.Preconditioner_Multiply = []; % function that multiplies witht the preconditioner M
                                      %  Mx = Preconditioner_Multiply( preconinfo, x ) 
                                      % Note that cond( M * (H_quadratic + H_nonlinear) ) should be 
                                      % much smaller than cond( H_quadratic + H_nonlinear )
                                      % So M =approx= inv( H_quadratic + H_nonlinear )
options.linesearch_method = []; % Linesearch method number. 
                               % 1 : default if searchHistory==0; Newton-Raphson (like)
                               % 2 : Secant
                               % 3 : default if searchHistory>0: subspace search 
options.max_iter_linesearch = 3;     % maximum number of newton-raphson iterations.
options.tol_linesearch = .1; % Line search tolerance.
options.revaluate_quadratic_it = 50; % number of iterations after which the function value and gradient of the quadratic part are re-evaluated.
options.revaluate_quadratic_hessian = true;
options.searchHistory = 0; % search history cache. Requires extra memory and quadratic 
                           % part to be much more computationally expensive than non linear 
                           % part.
options.othogonalizationmode = 0; 
options.collect_statistics = false;
options.display = 3;  % 0 : no display of progress, 
                      % 1 : final summary (not implemented yet)
                      % 2 : display each iteration.
options.parse_options.obsolete.max_NR = 'max_iter_linesearch';
options.parse_options.obsolete.epsilon_NewtRaphs = 'tol_linesearch';

if nargin>=3
    options = parse_defaults_optionvaluepairs( options, varargin{:});
end;
if isempty(options.linesearch_method)
    if options.searchHistory == 0
        options.linesearch_method = 1; % Newton-Raphson (like)
    else
        options.linesearch_method = 3; % subspace search
    end;
end;
Preconditioner_Multiply = options.Preconditioner_Multiply;
if options.linesearch_method ~=2 && (~isempty(Preconditioner_Multiply) || ~isempty(options.make_precionditioner))
    error('Preconditioner currently only supported for secant line search')
end;
szx = size(x); 
n = numel(x);

% parse inputs:
fun_quad = funcs(1).fun;
hessmul_q = funcs(1).hessmul;
if isempty(fun_quad)
    z = zeros(prod(szx),1);
    fun_quad = @(x) deal_relaxed(0,z,[]);
    hessmul_q = @(Hinfo, d) z;
end;
fun_nonl = funcs(2).fun;
if isempty( funcs(2).hessmul )
    hessmul_n = @(Hinfo, d) Hinfo*d;
else
    hessmul_n = funcs(2).hessmul;
end;

if options.linesearch_method == 3
    opt = fmin_fast();
    opt.HessMult = @(h,x) subspace_fun_hessmul( h, x , hessmul_n);
    opt.Preconditioner = @(h,x) [];
    opt.Preconditioner_Multiply = @(dum, x) x ;
end;

% start of algorithm
it =0 ;
k = 0;
[f_q, g_q, h_q_info] = fun_quad( x );
g_q = g_q(:);
[f_n, g_n, h_n_info] = fun_nonl( x );
old_f_q = [];

r = -(g_q + g_n(:));
if ~isempty(options.make_precionditioner)
    [Preconditioner_Multiply , preconinfo] = options.make_precionditioner( {h_q_info, h_n_info} );
else
    preconinfo = {h_q_info, h_n_info};
end;
if isempty(Preconditioner_Multiply)
    d = r;
else
    s = Preconditioner_Multiply( preconinfo , r );
    d = s;
end;
delta_0 = dot(r,d);
delta_new = delta_0;
if options.othogonalizationmode<0
    r_old =r ;
end;
clear r
if options.collect_statistics
    % define statistics structure; update fields in each loop
    numstats =1;
    stats.iter = it;         % ok
    stats.f_q = f_q;         % ok
    stats.f_q_approx = f_q;  % ok
    stats.f_n = f_n;         % ok
    stats.num_f_q = 1;       % ok
    stats.num_f_n = 1;       % ok
    stats.num_hessmul_q = 0; % ok
    stats.num_hessmul_n = 0; % ok
    stats.x_step_norm = nan; % stats(1) indicates starting conditions; no step taken.
    stats.step_scale = [];  
    stats(options.max_iter).iter = []; % make stats correct length.
end;
if options.display>1    
    fprintf(' Iteration   f(x)    |update x|      |d(f(x))|   [predicted f(x)]\n');
    fprintf('%5d   %12.9g           %15.3g\n',it, f_q+f_n , sqrt(delta_0) );
end;
while it < options.max_iter && delta_new > options.epsilon^2*delta_0
    it = it+1;
    
    delta_d = dot( d , d);
    if delta_d >10^30 || delta_d < 10^-30
        % scale d by power of 2 since overflow/underflow may occur 
        scale2 = 2^round( -.5* log2(delta_d) );
        d = d * scale2;
        delta_d = delta_d *   scale2* scale2;
    end;
    
    Hd_q =  hessmul_q( h_q_info, d);
    dHd_q = real( dot(d , Hd_q) ); % any imaginary part should be due to roundoff errors. 
    
    % first 1D search step: (this is the standard conjugate gradient step)
    Hd_n = hessmul_n( h_n_info, d);
    clear h_n_info % clear memory as early as possible.
    dHd_n = real( dot(d, Hd_n) );
    dHd = dHd_n + dHd_q;
    if dHd <0
        dHd = abs(dHd_n) + abs(dHd_q); % prevent (start of) searching in up-hill direction. 
    end;
    gd = dot( g_q+g_n(:), d);
    alpha = -real( gd )/( dHd);
    clear g_n 
    if options.collect_statistics
        numstats = numstats + 1;
        stats(numstats).iter = it;
        stats(numstats).num_f_q = 0;
        stats(numstats).num_hessmul_q = 1;
        stats(numstats).num_hessmul_n = 1;
    end;

    switch options.linesearch_method
        case 1  % Newton-Raphson (like)
            % no history
            % check if this step reduced the gradient enough:
            gd_q = real(g_q'*d);
            j=0;numhessmul = 0;
            while true 
                % f = .5 * x H x + b x + c
                % df/dx = H x + b
                % df/dx | (x -> x + alpha*d) - df/dx | (x -> x ) = H * (alpha*d)
                x_n = x + alpha * reshape(d, szx);
                [f_n, g_n, h_n_info] = fun_nonl( x_n );
                j = j + 1;
                doNewtRaphs = (j<options.max_iter_linesearch) &&  (alpha^2*delta_d > options.tol_linesearch);
                if ~doNewtRaphs 
                    break;
                end;
                Hd_n = hessmul_n( h_n_info, d);numhessmul = numhessmul +1;
                clear h_n_info % save memory as early as possible.
                alpha = alpha -real((g_q+alpha * Hd_q+g_n(:))'*d)/real( d'*Hd_n + dHd_q);
                clear g_n % save memory as early as possible.
            end;
            x = x_n;
            f_q = f_q + gd_q * alpha + .5*alpha^2 * dHd_q;
            g_q = g_q + alpha * Hd_q; % update gradient of quadratic part.
            x_step_norm = sqrt(delta_d)*alpha ; 
            if options.collect_statistics
                stats(numstats).f_q_approx = f_q;
                stats(numstats).f_n = f_n;
                stats(numstats).num_f_n       =  j;
                stats(numstats).num_hessmul_n = stats(numstats).num_hessmul_n + numhessmul;
                stats(numstats).x_step_norm   = x_step_norm;
                stats(numstats).step_scale    = alpha/sqrt(dHd);
            end;
        case 2 % secant line search (does not require hessian, so can be done with preconditioner). 
            
            % secant line search on the gradient:
            %   linear interpolation; try to null y
            %      y = (f'( x_m1 ) - f'( x_m2 ))/( x_m1 - x_m2 ) * (x - x_m1) + f'( x_m1 )
            %   => solve y==0 for x  (which is the new x):
            %      x = x_m1 - f'(x_m1)*( x_m1 - x_m2 )/(f'( x_m1 ) - f'( x_m2 ))
            %        = (x_m1* (f'( x_m1 ) - f'( x_m2 )) - f'(x_m1)*( x_m1 - x_m2 )) / (f'( x_m1 ) - f'( x_m2 ))
            %        = ( x_m1* f'( x_m1 ) - x_m1 *f'( x_m2 ) - x_m1 * f'(x_m1) +  x_m2 f'(x_m1) ) / (f'( x_m1 ) - f'( x_m2 ))
            %        = ( x_m2 f'(x_m1) - x_m1 *f'( x_m2 )    ) / (f'( x_m1 ) - f'( x_m2 ))
            
            % additional constraints:
            % - we want to get and keep y==0 bracketed, which means that the lowest of 
            %   x_m1 and x_m2 should have f' <0 and the highest of x_m1 and x_m2 should have f'>0.
            %   as long as we did not bracket the minimum, we extend the interval by at least a factor 2. 
            % - we want to get the lowest f, so if f indicates that the function increased between
            %   x_m1 and x_m2, then we take that as evidence that f' is positive at the end (and similar for decreasing => negative at start of interval)
            %   since interpolation will not work in that case, we use bisection. 
            
            % f'( alpha ) = gd_q + dHd_q * alpha + g_n( x + alpha * d )' * d 
            
            % assume d is a descend direction, so alpha should be positive for the minimum (else negate d prior to here)
            gd_q = real( dot(g_q , d ) );
            pnt = struct('alpha',{0 0 alpha},'f',{f_q + f_n, f_q + f_n, f_q + f_n},'df',{gd gd gd});
            f_n_start = f_n;
            id1 = 1;
            id2 = 2;
            idnew = 3;
            pntbest = pnt(1);
            pntbest.x = x;
            isbracketed = false;
            for j = 1 : options.max_iter_linesearch
                % make a next step. 
%                 alpha1 = alpha2;
%                 f1  = f2;
%                 df1 = df2;
                
%                 alpha2 = alpha_new ;
                x_new = x + pnt(idnew).alpha * reshape(d, szx);
                [fnew_n, gnew_n, hnew_n_info] = fun_nonl( x_new );
                gnewd_n = dot(gnew_n(:), d);
                pnt(idnew).f = fnew_n + f_q + gd_q * pnt(idnew).alpha + .5*pnt(idnew).alpha^2 * dHd_q;
                pnt(idnew).df = gd_q + dHd_q * pnt(idnew).alpha + gnewd_n;
                acceptpnt = pnt(idnew).f < pntbest.f;
                if acceptpnt
                    pntbest = pnt(idnew);
                    pntbest.x = x_new;
                    f_n = fnew_n;
                    g_n = gnew_n;
                    h_n_info = hnew_n_info;
                end;
                clear x_new fnew_n gnew_n hnew_n_info;
                if acceptpnt && abs(pnt(idnew).df)<  options.tol_linesearch * abs(gd)
                    break; % new gradient zero to within tolerance; so we converged. 
                end;
                % Remark: 
                % We only get here (j==1) if the quadratic approximation that is 
                % used to compute the initial alpha is not correctly predicting the minimum. 
                % For higher j, we (also) only get here if the newly proposed alpha is not close to the
                % minimum in the line search direction. 
                if ~isbracketed
                    % Check if with the test point the minimum is bracketed:
                    if pnt(idnew).f >= pnt(id2).f || pnt(idnew).df > 0
                        isbracketed = true;
                        [id2, idnew]=deal(idnew, id2); % order on alpha. 
                    end
                end;
                if ~isbracketed
                    % need to bracket:
                    [id1, id2, idnew]=deal(id2, idnew, id1); 
                    % in decreasing order of importance:
                    % new alpha should be at least twice the last evaluated alpha
                    % new alpha should not extrapolate by more than a factor 4
                    % new alpha is secant solution of last two evaluations
                    pnt(idnew).alpha = max( 2*pnt(id2).alpha, min( 4*(pnt(id2).alpha-pnt(id1).alpha), (pnt(id2).alpha * pnt(id1).df + pnt(id1).alpha * pnt(id2).df)/(pnt(id1).df - pnt(id2).df)));
                else
                    % minimum already bracketed; alpha of new point in between alpha of point 1 and 2. 
                    inFirstInterval = pnt(id1).df<0 && (pnt(idnew).df>0 || pnt(idnew).f>pnt(id1).f); % can we actually include pnt(idnew).f>pnt(id1).f here? It implies that the function had positive gradient at least somewhere in that interval. Note that equal f also happens in the initial bracketing phase where second interval should be selected (as alpha_new = 0)
                    inSecondInterval = pnt(idnew).df<0 && pnt(id2).df>0  ; 
                    if inFirstInterval || inSecondInterval
                        if inFirstInterval 
                            % zero gradient is within first part, replace second point

                            % make sure that we average at least an interval length reduction factor of 0.7 .
                            low_lim_alpha = pnt(id1).alpha + .3*(pnt(idnew).alpha-pnt(id1).alpha).^2/(pnt(id2).alpha-pnt(id1).alpha); %allow substantial reduction if previous step succesfully reduced interval size, if that was not the case make sure it reduces to most a factor .7 in the next step. 
                            upp_lim_alpha = pnt(id1).alpha + min( .7*(pnt(id2).alpha-pnt(id1).alpha), .95*(pnt(idnew).alpha-pnt(id1).alpha));% make sure the interval is at least reduced by a factor .7 over two iterations and there always is some minimal reduction.

                            [id2, idnew]=deal(idnew, id2);  % new point is new end point of the interval. 
                        else % inSecondInterval
                            % zero in second part, replace first point:

                            % make sure that we average at least an interval length reduction factor of 0.7 .
                            upp_lim_alpha = pnt(id2).alpha - .3*(pnt(idnew).alpha-pnt(id2).alpha).^2/(pnt(id2).alpha-pnt(id1).alpha); %allow substantial reduction if previous step succesfully reduced interval size, if that was not the case make sure it reduces to most a factor .7 in the next step. 
                            low_lim_alpha = pnt(id2).alpha - min(.7*(pnt(id2).alpha-pnt(id1).alpha), .95*(pnt(id2).alpha-pnt(idnew).alpha));% make sure the interval is at least reduced by a factor .7 over two iterations and there always is some minimal reduction.
                            [id1, idnew]=deal(idnew, id1);  % new point is new end point of the interval. 
                        end;
                        % solve third order polynomial (a x^3 + b x^2  + c x + d) from f and df at current interval. 
                        X_alpha_fit_3rd =[  1 pnt(id1).alpha   pnt(id1).alpha.^2   pnt(id1).alpha.^3;
                                            0       1        2*pnt(id1).alpha    3*pnt(id1).alpha.^2;
                                            1 pnt(id2).alpha   pnt(id2).alpha.^2   pnt(id2).alpha.^3;
                                            0       1        2*pnt(id2).alpha    3*pnt(id2).alpha.^2];
                        alpha_fit_3rd = X_alpha_fit_3rd\[pnt(id1).f;pnt(id1).df;pnt(id2).f;pnt(id2).df];
                        if 0 
                            %% Show fit:
                            alpha_plot = linspace(pnt(id1).alpha, pnt(id2  ).alpha,100)';
                            lh_alphaplot = plot(alpha_plot, [ones(numel(alpha_plot),1) alpha_plot alpha_plot.^2 alpha_plot.^3]*alpha_fit_3rd);
                            xlabel('alpha');ylabel('function value');
                            %% show actual values of nonlinear part only. TODO: add quadratic part: (requires evaluation of f, so may take long and require substantial memory)
                            alpha_plot_fval = linspace( pnt(id1).alpha, pnt(id2  ).alpha,10)';
                            fval_alpha_plot = zeros(size(alpha_plot_fval));
                            fval_alpha_plot([1 end]) = [pnt(id1).f pnt(id2).f];
                            for dummy_it = 2 : numel(fval_alpha_plot)-1
                                x_alphatest = x + alpha_plot_fval(dummy_it )  * reshape(d, szx);
                                fval_alpha_plot( dummy_it ) = fun_nonl( x_alphatest );
                            end;
                            hold on
                            fv_alphaplot = plot( alpha_plot_fval, fval_alpha_plot );
                            set(fv_alphaplot,'color',get(lh_alphaplot,'color'),'LineStyle','none','Marker','*')
                        end;
                        % take derivative wrt x and solve for zero of this derivative
                        dalpha_fit_3rd = alpha_fit_3rd(2:4).*[1 2 3]'; % dt = [ c b a]  in   a x^2 + b x + c = 0
                        root_alpha_fit = dalpha_fit_3rd(2)^2-4*dalpha_fit_3rd(1)*dalpha_fit_3rd(3); % b^2 - 4 a c 
                        alpha_newcandidates = (-dalpha_fit_3rd(2) + [-1 1]*sqrt( root_alpha_fit ) ) / (2 * dalpha_fit_3rd(3) ); 
                        delanew = imag(alpha_newcandidates)~=0 | alpha_newcandidates<pnt(id1).alpha | alpha_newcandidates>pnt(id2).alpha; % delete roots that are out of bound.
                        alpha_newcandidates(delanew) = [];
                        if numel(alpha_newcandidates)>1
                            alpha_newcandidates = min(alpha_newcandidates); % If both roots are in the interval, we want the smallest alpha as that is the minimum (derivative at start of interval is negative). 
                        elseif isempty(alpha_newcandidates)
                            % find alpha where df == 0, assuming linear change of df between pnt1 and pnt2 (as we already switched pnt2 and new pnt). 
                            alpha_newcandidates = (pnt(id2).alpha * pnt(id1).df + pnt(id1).alpha * pnt(id2).df)/(pnt(id1).df - pnt(id2).df) ;
                        end;
                        % store anew within the bounds:
                        pnt(idnew).alpha = max( low_lim_alpha, min(upp_lim_alpha,  alpha_newcandidates )); 
                    else
                        % non usual bracketing of minimum (gradients of end points have wrong sign); 
                        if pnt(id1).df>0
                            error('should not happen');
                        end;
                        if pnt(id1).f < pnt(idnew).f
                            % minimum in first interval, replace second point
                            [id2, idnew]=deal(idnew, id2); 
                        elseif pnt(idnew).f < pnt(id2).f && pnt(idnew).df<0
                            % minimum in second interval, replace first point:
                            [id1, idnew]=deal(idnew, id1); 
                        else
                            error('what is happening? unforseen case that probably should not happen');
                        end;  
                        l = pnt(id2).alpha-pnt(id1).alpha;
                        low_lim_alpha = pnt(id1).alpha + .3*l;% make sure the interval is at least reduced by a factor .7
                        upp_lim_alpha = pnt(id1).alpha + .7*l;% make sure the interval is at least reduced by a factor .7
                        % parabolic approximation using f of point 1 and 2, and df of point 1
                        %  poly = a + b * delta_alpha + c * delta_alpha^2
                        poly = [1 0 0;1 l l.^2;0 1 0] \ [pnt(id1).f;pnt(id2).f;pnt(id1).df];
                        % solve d(poly, alpha) ==0 => b + 2*c delta_alpha ==0 => 
                        delta_alpha = poly(2)/(2*poly(3));
                        pnt(idnew).alpha = max( low_lim_alpha, min(upp_lim_alpha,  pnt(id1).alpha + delta_alpha )); 
                    end;
                    
%                     if abs(alpha_new-alpha2)/alpha< options.tol_linesearch  || abs((alpha_new-alpha2)/(alpha1-alpha2) )<.1 
%                         break; % we would only make a very small progress. 
%                     end;
                end;
            end;
            if pntbest.alpha == 0 
                warning('Line search did not find an acceptable update. Cannot continue.');
                g_n = 0; 
                break; 
            end;
            if 0 
                %% display line-search problem (also helps to validate the functions
                alpha_tst = linspace(0,max(alpha,pntbest.alpha*1.2),100)';
                ftst = zeros(numel(alpha_tst),2);
                for i_tst = 1 :numel(alpha_tst)
                    x_new =  x + alpha_tst(i_tst ) * reshape(d, szx);
                    ftst(i_tst,1) = fun_quad( x_new );
                    ftst(i_tst,2) = fun_nonl( x_new );
                end;
                f_pred_q = f_q + gd_q * alpha_tst + .5*alpha_tst.^2 * dHd_q;
                f_pred_n = f_n_start+ (gd-gd_q) * alpha_tst + .5*alpha_tst.^2 * dHd_n;
                f_new_tst = f_q + gd_q * pntbest.alpha+ .5*pntbest.alpha^2 * dHd_q + f_n ;
                plot(alpha_tst, [sum(ftst,2) f_pred_q+ftst(:,2) f_pred_q+f_pred_n],'-',pntbest.alpha, f_new_tst,'*');
                legend('observed','pred quadratic + observed nonlinear','prediction at start','chosen point')
            end;
            alpha = pntbest.alpha;
            x = pntbest.x;
            f_q = f_q + gd_q * alpha + .5*alpha^2 * dHd_q;
            g_q = g_q + alpha * Hd_q; % update gradient of quadratic part.
            clear pntbest;
            x_step_norm = sqrt(delta_d)*alpha ; 
            if options.collect_statistics
                stats(numstats).f_q_approx = f_q;
                stats(numstats).f_n = f_n;
                stats(numstats).num_f_n       =  j;
                stats(numstats).x_step_norm   = x_step_norm;
                stats(numstats).step_scale    = alpha/sqrt(dHd);
            end;
        case 3  % subspace search (using history)
            % use history and check if we need to proceed the subspace search.
            % 
            % x_new = x + D * beta
            % beta^0(hid) = alpha
            % Gradient in subspace:
            % f( x + D * beta) = f_q( x + D * beta) + f_n( x + D * beta)
            %                  = f_q(x) + g_q' * D * beta + .5* beta'* D' * H_q * D * beta + f_n( x + D * beta)
            % D[ f , beta] = g_q' * D +  DHD_q * beta + (f_n'[x+D*beta])^T * D
            % H[ f , beta] = DHD_q +  D^T * (f_n''[x+D*beta]) * D
            hid = mod(it-1, options.searchHistory)+1;
            dscale = 1./sqrt(abs(dHd)); % scale search direction so subspace hessian becomes identity (deviates only due to nonlinearities)
                                        % use abs to ensure positivity (nonlinearities may make cause negative)
            if it==1
                % initialize variables:
                D = d* dscale;
                nD = 1;
                HD_q = Hd_q *dscale;
                DHD_q = real(d'*HD_q) * dscale; % make diagonal real to ensure exact hermitian form. 
            else
                D(:,hid) = d * dscale;
                nD = size(D,2);
                HD_q(:,hid) = Hd_q *dscale;
                DHD_q(hid,1:nD) = real(d'*HD_q) * dscale; % make real as only real beta's are allowed. 
                DHD_q(1:nD,hid) = DHD_q(hid,:)';
            end;
            gD_q = real(g_q'*D);
            beta_in = zeros(nD,1);
            beta_in(hid)= alpha/dscale;
            fun = @(bta) subspace_fun( bta, DHD_q, gD_q, fun_nonl , D, x);
            [beta, fsub_opt, exflg, gsub_opt, hsub_ifo, fminfastoutp] = fmin_fast( fun, beta_in, opt );
            clear fun;
            g_n = hsub_ifo.gn;
            h_n_info = hsub_ifo.hn;
            clear hsub_ifo
            x_step = reshape( D * beta, szx);
            x = x + x_step;
            x_step_norm = sqrt( dot( x_step(:), x_step(:) ) );
            clear x_step;
            f_q_prev = f_q;
            f_q = f_q + gD_q * beta + .5*(beta'*DHD_q*beta);
            g_q = g_q + HD_q * beta; % update gradient of quadratic part.
            f_n = fsub_opt+(f_q_prev-f_q);

            if options.collect_statistics
                stats(numstats).f_q_approx = f_q;
                stats(numstats).f_n = f_n;
                stats(numstats).num_f_n       =  fminfastoutp.funcCount;
                stats(numstats).num_hessmul_n = stats(numstats).num_hessmul_n + fminfastoutp.cgiterations ;
                stats(numstats).x_step_norm   = x_step_norm;
                stats(numstats).step_scale    = beta([hid:-1:1 end:-1:hid+1]);
            end;
        otherwise 
            error('invalid linesearch_method specified');
    end;

    if mod(it, options.revaluate_quadratic_it ) == 0
        old_f_q = f_q;
        if options.revaluate_quadratic_hessian
            clear g_q h_q_info % save memory
            [f_q, g_q, h_q_info] = fun_quad( x );
            g_q = g_q(:);
        else
            clear g_q % save memory
            [f_q, g_q] = fun_quad( x );
            g_q = g_q(:);
        end;
        if options.collect_statistics
            stats(numstats).f_q = f_q; % don't set f_q if we dont explicitly compute it.
            stats(numstats).num_f_q = 1;
        end;
    end;
    
    r = -(g_q+g_n(:)); % new gradient
    if options.othogonalizationmode < 0 
        delta_mid = dot(r,s);
    end;
    if ~isempty(options.make_precionditioner)
        [Preconditioner_Multiply , preconinfo] = opt.make_precionditioner( {h_q_info, h_n_info} );
    else
        preconinfo = {h_q_info, h_n_info};
    end;
    if isempty(Preconditioner_Multiply)
        s = r;
    else
        s = Preconditioner_Multiply( preconinfo , r );
    end; 
    
    delta_old = delta_new;
    delta_new = dot(r,s);
    if options.othogonalizationmode <= 0 
        % 'standard' orthogonalization modes:
        if options.othogonalizationmode==0
            % Fletcher-Reeves search vector update
            beta = delta_new/delta_old;
        else
            % Polak-Ribiere search vector update:
            beta = max( real((delta_new- delta_mid)/delta_old) ,0 );
        end;
        d = s + beta * d;
        k = k+1;
        restartCG = (k >= n ) || (real( dot(r,d) )<=0 ) ;
        if restartCG
            d = s;
            k=0;
        end;
    else
        % make search direction H-orthogonal to current search directions:
        % =>  d_new * H * D = 0 
        %   == (r + D * beta)' * H * D = 0
        %  => r' * H * D + beta' * D' * H * D = 0 
        %  => beta' = -(r'*H*D)* inv( D'*H*D )
        selcols = mod( hid-1:-1:hid-min(nD,options.othogonalizationmode) , nD)+1;
        HD_n = zeros( numel(d), numel(selcols));
        for kH = 1:numel(selcols)
            HD_n(:,kH) = hessmul_n( h_n_info, D(:,selcols(kH) ) );
        end;
        if options.collect_statistics
            stats(numstats).num_hessmul_n = stats(numstats).num_hessmul_n + numel(selcols);
        end;
        HD = HD_n + HD_q(:,selcols);
        clear HD_n
        beta =  ( -real(r'*HD) / real( D(:,selcols)'*HD ) )';
        clear HD
        d = r + D(:,selcols)*beta;
    end;
    clear r
    if options.display>1    
        if ~isempty(old_f_q)
            fprintf('%5d   %12.9g %9.4g %15.3g  %12.9g\n',it, f_q+f_n, x_step_norm, sqrt(delta_new) , old_f_q+f_n);
            old_f_q = [];
        else
            fprintf('%5d   %12.9g %9.4g %15.3g\n',it, f_q+f_n, x_step_norm , sqrt(delta_new) );
        end;
        drawnow;
    end;
end;
if options.collect_statistics
    fval = stats(1:numstats);
else
%     fval = [f_q , f_n , fun_quad( x ) , fun_nonl( x )]; % DEBUG, explicit evaluation as well.
    fval = [f_q , f_n]; 
end;
if nargout>2
    final_grad = g_q+g_n(:);
end;

function [f, g, h] = subspace_fun( x, A, b, f_n , D, x0)
% [f,g,h] = quadratic_test_fun( x, A, b, f_n , D, x0);
%
% function that evaluates
% f = 0.5 * x'*A*x + b*x + f_n( x0 + D * x )
% 
% and is able to evaluate the derivative and hessian as well. 
%
% Created By Dirk Poot, Erasmus MC, 28-5-2013

Ax = A*x;
f = 0.5*( x'*Ax ) + b*x;
sx = size(x0);
if nargout>1
    if nargout>2
        [fn ,gn, hn] = f_n( x0 + reshape(D* x,sx) );
        h.A = A;
        h.hn = hn;
        h.gn = gn;
        h.D = D;
    else
        [fn ,gn] = f_n( x0 + reshape(D* x,sx));
    end;
    g = Ax + (b + real(gn(:)'*D))';
else
    fn = f_n( x0 + reshape(D* x,sx));
end;
f = f +fn;

function [hx] = subspace_fun_hessmul( h, x , f_n_hessmul)
hx = real( h.D' * f_n_hessmul( h.hn, h.D*x) )  + h.A*x;

function test_simple_quadratic_function()
%%
A1 = randn(90,100);A1 = A1'*A1; 
A2 = randn(90,100);A2=A2'*A2; 
b1 =randn(100,1);b2=randn(100,1);
funcs(1).fun= @(x) quadratic_test_fun( x, A1,b1);
funcs(2).fun= @(x) quadratic_test_fun( x, A2,b2);
funcs(1).hessmul = @(H,x) H*x; 
funcs(2).hessmul = @(H,x) H*x;
xin = zeros(100,1);
[ x_h , fval ] = conjgrad_nonlin( funcs , xin ,'searchHistory',5);
[ x_1 , fval ] = conjgrad_nonlin( funcs , xin ,'searchHistory',0);
plot([x_h x_1])
function test_TV_regul_function()
%%
problems = struct;
% first simple quadratic problem:
A = randn(30); A = A*A';
problems(1).H0 = A;
y  = randn(size(A,1),1);
funcs = struct;
funcs(1).fun= @(x) quadratic_test_fun( x(:), A, y );
funcs(1).hessmul = @(H,x) H*x; 
funcs(2).fun= @(x) deal_relaxed( 0, zeros(size(x)) ); % no nonlinear cost;
funcs(2).hessmul = @(H,x) zeros(size(x));
problems(1).funcs = funcs;
problems(1).xin = zeros(size(y));
problems(1).x_GT = A\(-y);
problems(1).fullhessian = @(h) h{1};

% add some sinus functions. See 'special_convex_test_functions.m' for why these are scaled 
% this way:
p=2;
problems(p) = problems(1);
% H0 = A; is only approximate in this case. 
offset = problems(1).x_GT;
b = [0.724611353776710/2 , .5 ,  0.1086168141056108287041]';
a = [-1 , 0 , 0]';
phase = [ 0 , pi/2 ,-pi/2]';
% choice = ceil(numel(b)*rand(size(problems(1).x_GT)) );
choice = 1+ceil((numel(b)-1)*rand(size(problems(1).x_GT)) ); % prevent choice = 1 as that changes the global minimum position. 
% scale^2 * b == diag(a) => scale = sqrt( diag(a)/b) 
scale = sqrt(.5* diag(A)./b(choice));
offsetm = offset - phase(choice)./scale;
ym = y + a(choice).*scale;
problems(p).funcs(1).fun= @(x) quadratic_test_fun( x(:), A, ym );
problems(p).funcs(2).fun = @(x) sin_scaled( x, scale, offsetm , true);
problems(p).funcs(2).hessmul = @(H,x) bsxfun(@times, H(:), x);
problems(p).fullhessian = @(hinf) hinf{1}+diag(hinf{2});

% make A diagonal; though this enables easy splitting, conjugate gradients does not do that so is equally efficient
p=3;
problems(p) = problems(1);
A_d = diag(eig(problems(1).H0));
problems(p).x_GT = A_d\(-y);
offset = problems(p).x_GT;
choice = ceil((numel(b))*rand(size(problems(1).x_GT)) ); 
scale_d = sqrt(.5* diag(A_d)./b(choice));
offsetm_d = offset - phase(choice)./scale_d;
ym_d = y + a(choice).*scale_d;
problems(p).funcs(1).fun= @(x) quadratic_test_fun( x(:), A_d, ym_d );
problems(p).funcs(2).fun = @(x) sin_scaled( x, scale_d, offsetm_d , true);
problems(p).funcs(2).hessmul = @(H,x) bsxfun(@times, H(:), x);
problems(p).fullhessian = @(hinf) hinf{1}+diag(hinf{2});

if 0
    %% check implementation is as intended  (many zero gradients)
    % NOTE: derivatives and gradients checked by validatederivative below. 
    
    xst = problems(p).x_GT;
    if p==2
        scale_plt = scale;
    else%p==3
        scale_plt = scale_d;
    end;
    step = linspace(-10,10,100);
    
    f1 = zeros(numel(step), numel(xst) );
    f2 = f1;
    fun1 = problems(p).funcs(1).fun;
    fun2 = problems(p).funcs(2).fun;
    for i = 1 : numel(xst) 
        xsti = xst;
        for k = 1 : numel(step)
            xsti(i) = xst(i) + step(k)/scale_plt(i);
            f1(k,i) = fun1(xsti);
            f2(k,i) = fun2(xsti);
        end;
    end;
 
    plot(step,f1+f2)
end;
% ncon = 3000;
% x_GT = phantom( 'Modified Shepp-Logan' , 100);
% sz = size(x_GT);
% A1 = randn(ncon,prod(sz))*20;A = A1'*A1; 
% y = -A1 * x_GT(:);
% b1 = A1'*y;
% c = y'*y *.5;
% funcs(1).fun= @(x) quadratic_test_fun( x(:), A,b1 , c);
% funcs(2).fun= @(x) totalVariationVecRegularizer( x, [],5,[],[],1);
% funcs(1).hessmul = @(H,x) H*x; 
% funcs(2).hessmul = totalVariationVecRegularizer( [], [],[],5,[],2);
% xin = randn([1,sz]);
%%
numopt = 5;
x_final = cell(numopt,numel(problems));
fval = zeros(numopt , 2 , numel(problems));
MCMCtrace = cell(10000,numel(problems));
MCMCfval = zeros(size(MCMCtrace));
numreject = MCMCfval;
for p = 1: numel(problems)
    %%
    validateDerivativeAndHessian(problems(p).funcs(1).fun, problems(p).xin, problems(p).funcs(1).hessmul);
    validateDerivativeAndHessian(problems(p).funcs(2).fun, problems(p).xin, problems(p).funcs(2).hessmul);
    
    opt = {struct};
    opt{1}.max_iter = 50;
    opt(2) = opt(1);
    opt{2}.linesearch_method = 2;
    opt(3) = opt(1);
    opt{3}.searchHistory = 5;
    opt(4) = opt(3);
    opt{4}.othogonalizationmode = 3;

    opt(5) = opt(2);
    if isempty(problems(p).H0)
        [preconopt , preconmulfun] = spatialRegularizerMakePreconditioner( struct('sizeTheta',sz), [1 -.8]); 
        preconopt.filt{1} = preconopt.filt{2};
        opt{5}.Preconditioner_Multiply = @( R, x) preconmulfun(preconopt, x);
    else
        H0i = inv(problems(p).H0);
        opt{5}.Preconditioner_Multiply = @(R, x) H0i * x;
    end;
    for k=1:numel(opt)
        [x_final{k,p}, fval(k,:,p) ] =conjgrad_nonlin( problems(p).funcs , problems(p).xin , opt{ k } );
    end;
    %%
    k = numel(opt)+1;
    [x_final{k,p}, fvalkp ] = fminsearch( @(x) problems(p).funcs(1).fun(x)+problems(p).funcs(2).fun(x) , problems(p).xin );
    fval(k,:,p) = [problems(p).funcs(1).fun(x_final{k,p}) problems(p).funcs(2).fun(x_final{k,p})]; fvalkp-sum(fval(k,:,p))
    
    k = numel(opt)+2;
    funcs = {problems(p).funcs.fun};
    hmulfun = {problems(p).funcs.hessmul};
    fun = @(x) addCostFunctions( x, funcs, hmulfun);
    fopt = optimset('fminunc');
    fopt = optimset(fopt,'GradObj','on');
    [x_final{k,p}, fvalkp ] = fminunc( fun , problems(p).xin , fopt);
    fval(k,:,p) = [problems(p).funcs(1).fun(x_final{k,p}) problems(p).funcs(2).fun(x_final{k,p})];fvalkp-sum(fval(k,:,p))
    
    k = numel(opt)+3;
    fopt = optimset(fopt,'largescale','on','Hessian','on','HessMult', fun('hessmulfun' ) );
    [x_final{k,p}, fvalkp ] = fminunc( fun , problems(p).xin , fopt);
    fval(k,:,p) = [problems(p).funcs(1).fun(x_final{k,p}) problems(p).funcs(2).fun(x_final{k,p})];fvalkp-sum(fval(k,:,p))
    
    k = numel(opt)+4;
    fopt = fmin_fast();
    fopt.HessMult = fun('hessmulfun' );
    [x_final{k,p}, fvalkp ] = fmin_fast( fun , problems(p).xin , fopt);
    fval(k,:,p) = [problems(p).funcs(1).fun(x_final{k,p}) problems(p).funcs(2).fun(x_final{k,p})];fvalkp-sum(fval(k,:,p))
    %%
    k = numel(opt)+5;
    MCMCopt = new_MCMC_sample();
    scale_hess_p = [10 2 10 10 30 100 100];
    [R_precon] = chol( problems(p).H0 *scale_hess_p(p) );
    [V,E] = eig( problems(p).H0 );
    minhess = {diag(E), V, problems(p).H0*scale_hess_p(p)};
    
    if p==1 || p==4
        MCMCopt.Preconditioner = @(hinf,x) R_precon ;
    else
        MCMCopt.Preconditioner = @(hinf,x) make_advanced_preconditioner(problems(p).fullhessian( hinf ) ,x , minhess) ;
    end;
    MCMCopt.maxIter = 40;
    
    if p==7
        MCMCtrace{1,p} = MCMCtrace{end,p-1} ;
    else
        MCMCtrace{1,p} = problems(p).xin ;
    end;
    for kMC = 2 : size(MCMCtrace,1)
        [MCMCtrace{kMC,p}, MCMCfval(kMC,p) , numreject(kMC,p)] = new_MCMC_sample( fun ,MCMCtrace{kMC-1,p} , MCMCopt);
    end;
    x_final{k,p} = MCMCtrace{end,p};
    fval(k,:,p) = [problems(p).funcs(1).fun(x_final{k,p}) problems(p).funcs(2).fun(x_final{k,p})];%fvalkp-sum(fval(k,:,p))
    
end;

%% Display results
fval
allx = cat(4,x_final{:},reshape(x_GT,[1 sz]), xin);
imagebrowse(allx,[-.2 1.2])

%% 
% % profile on
% tic;
% [ x , fval ] = conjgrad_nonlin( funcs , xin ,'searchHistory',5,'max_iter',50,'epsilon', eps);
% toc
% tic;
% [ x2 , fval2 ] = conjgrad_nonlin( funcs , x ,'searchHistory',5,'max_iter',50);
% toc
% tic;
% [ xL , fvalL ] = conjgrad_nonlin( funcs , xin ,'searchHistory',5,'max_iter',100,'epsilon', eps);
% toc
% tic;
% [ xL2 , fvalL2 ] = conjgrad_nonlin( funcs , xin ,'searchHistory',5,'max_iter',1000,'epsilon', eps);
% toc
% tic;
% [ xL3 , fvalL3 ] = conjgrad_nonlin( funcs , xL2 ,'searchHistory',5,'max_iter',1000,'epsilon', eps);
% toc
% tic
% [ x_1 , fval_1 ] = conjgrad_nonlin( funcs , xin ,'searchHistory',0, 'max_iter',51,'epsilon', eps);
% toc
% tic
% [ x_0 , fval_0 ] = conjgrad_nonlin( funcs , xin ,'searchHistory',0,'max_iter',51,'epsilon', eps);
% toc
% tic
% [ x_0L , fval_0L ] = conjgrad_nonlin( funcs , xin ,'searchHistory',0,'max_iter',1100,'epsilon', eps);
% toc
% profile viewer
%%
imagebrowse(cat(4,x_0, x_0L,x_final,x2,xL,xL2 ,xL3, reshape(x_GT,[1 sz])),[-.2 1.2])
q=[sum( fval_0), sum( fval_0L) , sum( fval) ,sum(fval2),sum(fval3),sum(fvalL),sum(fvalL2),sum(fvalL3)];
q-min(q)
% [sum(fval_0),sum(fval)]*[eye(2) [1;-1]]