% [Mstore, Mfinal] = bloch_sim_c( Mz0, T1, T2, B_offr, Mz_offr, RFpulses, RFtimes, sampletimes, Min1);


%% code test.
% Initialize variables with first part of "test_cube_simul.m"

%  Mz0, 
%  T1,
%  T2
B0 = B_offr;
B0w = Mz_offr;
% RFpulses, 
RFpulsetimes = RFtimes;
% sampletimes, 
M = Min1;

[srtTime, origtimeidx] = sort([0;RFtimes(:);sampletimes(:)]);
dtime = diff(srtTime);
[deltatimes , b, timestepid ] = unique( dtime + 100*srtTime(end) );
deltatimes = dtime(b); % non rounded delta time; should do mean over all in group
%%
mex Bloch_sim_c.cpp -I".\Tools\supportingfiles\" -g -O -v
%%
tic
relaxT2 = exp( bsxfun(@rdivide, -deltatimes, reshape( T2 , 1, [])) );
relaxT1 = expm1( bsxfun(@rdivide, -deltatimes, reshape( T1 , 1, [])) );
B0c = exp( 1i* bsxfun(@times, reshape(B0, [size(B0,1) 1 size(B0,2)]), deltatimes') );
timestepidc = int32(timestepid -1);
actionId = int32(origtimeidx(2:end)>(numel(RFtimes)+1)); % set sampling
actionId(actionId==0)= 6;                  % set RF application.
time_used_prep = toc
tic

if 1
% full problem
for k=1:10
[M_store, M_final] = Bloch_sim_c( Mz0, relaxT2, relaxT1, B0c, RFpulses, B0w, timestepidc , actionId, M);
end;
else
% reduced problem: 
selN = 1:3;
selM = 1:4;
[M_store, M_final] = Bloch_sim_c( Mz0(selN), relaxT2(:,selN), relaxT1(:,selN), B0c(selM,:,selN), RFpulses(:,:,1:2), B0w(selM,selN), timestepidc(1:1010:3000) , actionId(1:1010:3000));
end;
toc

%%
figure(1);
clf;
plot(M_store(:,:,1))
set(gcf,'name','mex simulation');
figure(2);
clf;
M_store1 =  permute(Mstore1,[3 2 1]);
plot(M_store1(:,:,1));
set(gcf,'name','reference simulation');
figure(3)
plot([M_store(:,1:3,1)-M_store1(:,1:3,1)])
set(gcf,'name','mex-reference simulation');
