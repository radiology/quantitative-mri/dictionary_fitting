/*
 * This Bloch_sim_c.cpp is a MEX-file for MATLAB.
 * Created by Dirk Poot, Erasmus Medical Center 
 * Build with: (add -g for debugging (add -O for optimizations while debugging); -v for verbose mode )
 * Windows:
 * mex Bloch_sim_c.cpp -I".\Tools\supportingfiles\" -DINCLUDE_SSE3
 *
 *
 * Or on Linux:
 * module load matlab
 * module load gcc/4.7.4
 * matlab
 * mex Bloch_sim_c.cpp -I"../Tools/supportingfiles/" CXXFLAGS="\$CXXFLAGS -msse4a -msse4.2 -march=native" -DMEX -v
 *
 * or on MAC (VCode from MATLAB2015b; does currently not support vectorization)
 * mex Bloch_sim_c.cpp -I"../Tools/supportingfiles/"  -DMEX -DVECVLEN=1 COMPFLAGS='$COMPFLAGS -Wc++11-narrowing -W#pragma-messages'
 *
 *(not updated)
 * % mex TransformCoreGeneral_c2.cpp -DUSEOMP -I"./" -I"./Tools/supportingfiles/" -v CXXFLAGS="\$CXXFLAGS -fopenmp -msse4a -msse4.2 -march=native" LDFLAGS="\$LDFLAGS -fopenmp"
 * % mex TransformCoreGeneral_c2.cpp -DUSEOMP -I"./" -I"./Tools/supportingfiles/" -v CXXFLAGS="\$CXXFLAGS -fopenmp -march=barcelona" LDFLAGS="\$LDFLAGS -fopenmp"
 *
 * % Set  'OMP_NUM_THREADS' to the desired number of threads before (!)
 * % calling TransformCoreGeneral_c2 (or any other OMP enabled mex file) 
 */


/*#ifdef _OPENMP
//#define USEOMP
#endif

#ifdef USEOMP
#include <omp.h>
#define MAXNUMTHREADS 20
#endif



#include <assert.h>
#include "math.h"
#include <complex>
#include <algorithm>

*/
//#define ADDGUARDS
//#define CHECKPOINTERBOUNDS
#ifndef VECVLEN 
#define VECVLEN 2
#endif
#define GUARDPOINTEROLDSTYLEMXGETPTR

#include "mex.h"
#include "tempSpaceManaging.cpp"
#include "lineReadWriters.cpp"
#include "guardpointer.cpp"
//#include "static_assert.hpp"
#include "emm_vec.hxx"
#include "standard_templates.hxx"
#include <complex>

#if VECVLEN !=1

std::complex< vec<double, VECVLEN> >  operator*(const std::complex< vec<double, VECVLEN> >  x, double  b ) {
	return x * vec<double, VECVLEN>( b );
};
/*std::complex< vec<double, VECVLEN> >  operator*(const std::complex< vec<double, VECVLEN> >  x, const vec<double, VECVLEN>  b ) {
	return x * vec<double, VECVLEN>( b );
};*/

#endif
/*
#include "bspline_proc.cpp"
#include "subspaceIterator.cpp"
#include "SeparableTransform_core_c.cpp"
#include <iterator>
#include <bitset>

//#define PERFORMANCECOUNTING // if defined: return some counts in the first (double) elements of the output. SO FOR DEBUG ONLY!!!
							  // Compile TransformCoreGeneral_c2_timing.cpp to get this functionality.
#ifdef PERFORMANCECOUNTING
#ifdef _WIN32  // also defined on 64 bit windows platforms
	#include <Windows.h>
	typedef volatile __int64 timeCntT;
	timeCntT time_used[5];
	#pragma intrinsic(__rdtsc)
	#define PERFORMANCECOUNTER( value ) value = __rdtsc()
#else
	#include <time.h>
	typedef timespec timeCntT;
	int64_t time_used[5];
	#define PERFORMANCECOUNTER( value ) clock_gettime( CLOCK_MONOTONIC, &value )
	//int64_t timeCntT::operator-( timeCntT b) {
	int64_t operator-( const timeCntT &a, const timeCntT &b) {
		return (int64_t) (a.tv_nsec - b.tv_nsec) + ((int64_t) (a.tv_sec - b.tv_sec))*1000000000;
	};
#endif
#endif

// overloaded floor; default implementation is extremely slow.
//#define _MM_FROUND_TO_NEG_INF 1
template< typename T> inline T fastfloor( T value ) {
	return floor( value );
}
#ifdef INCLUDE_SSE4
template<> inline double fastfloor( double val) {
	__m128d tmp = _mm_load_sd( & val );
	_mm_store_sd( &val , _mm_round_sd( tmp, tmp , _MM_FROUND_TO_NEG_INF ));
	return val;	
}
#endif
//using namespace std;

*/
#ifdef _MSC_VER
	// Compiling with MS Visual studio compiler:
	typedef __int32 int32_t;
	typedef __int64 int64_t; 
#else // add check for compiling with gcc.
#endif



template < typename int_t, typename Mtype, typename M0type, typename relaxT2type, typename relaxT1type,
		   typename B0type, typename RFpulsetype, typename gradType, typename Mstoretype, typename accumType , typename actionIdType , typename timestepIdType>
void Bloch_sim_core(Mtype M, M0type Mz0, relaxT2type relaxT2, relaxT1type relaxT1, B0type B0, RFpulsetype RFpulses, gradType grad, 
					Mstoretype Mstore, accumType accumw,
					int_t num_vox, int_t num_timepoints, int_t num_spins, int_t num_timestepidx, int_t num_store_idx,
					actionIdType actionId, timestepIdType timestepId )
{
	/*  Bloch simulation core
	Simulates num_vox voxels with num_spins spins per voxel, in num_timepoints  time points.
	RF pulses are applied when actionId&6!=0, and the net magnetisation for each voxel is stored 
	when actionId&1!=0. Gradient pulses are applied when actionId&8!=0.

	 
	Interface:
	M : initialized magnetisation array of all voxels, updated in each time step, final state returned.
		Mit = M[ vox_idx ] should be an forward (=>i+o) iterator over all spins 
		Mit.x, Mit.y, Mit.z : get/set x,y,z magnetisation of current spin.
	Mz0 : rest state magnetisation 
		Mz0[ vox_idx ] : rest magnetisation value
	relaxT2: T2 relaxation specification
		T2vox = relaxT2[ vox_idx ] : random access iterator (read only)
		T2vox[ timestep_id ] : real/complex value that multiplies x&y magnetisation at current time point
	relaxT1: T1 relaxation specification
		T1vox = relaxT1[ vox_idx ] : random access iterator (read only)
		T1vox[ timestep_id ] : real value that relaxes z magnetisation at current time point:
							   Mz_new = Mz0[vox_idx] + (Mz - Mz0[vox_idx]) * T1vox[ timestep_id ] 
	B0 : B0 and T2 star modelling offresonance, 'rotates' xy magnetisation for each spin individually.
		B0vox = B0[ vox_idx ] : random access iterator
		B0it = B0vox[ timestep_id ] : input iterator over all spins.
		(*B0it) : complex value for rotating x-y magnetisation of current spin.
	RFpulses : Models the magnetisation rotation due to RF pulses 
		RFvox = RFpulses[ vox_idx ] : input iterator over all RF time points.
		RFvox(i,j) for 0<=i<=2 & 0<=j<=2 : i,j element of RF induced spin rotation of the current time point.
    grad : models the application of the spatial gradient coils (mainly for spoiling effects)
        Can be set for each sub-spin to model (subvoxel) displacements. 
        gradvox = grad[ vox_idx ] : input iterator over all gradient pulses in the order in which they are applied
                                    (it is incremented only for the time points where actionId & 8 !=0 ).
        gradPulse = *gradvox      : input iterator over all spins of the current voxel
        gradPulse(i,j) for i,j in {0,1}: i,j element in gradient induced spin rotation in  x,y plane.
    Mstore : Stores the net magnetisation at the requested time points.
		Mstorevox = Mstore[ vox_idx ] : output iterator
		value_type associated with Mstorevox should have default constructor that initializes to 0
		and have fields x, y, and z that can be incremented ( '+=' )

	accumw : accumulation weight for the individual spins of a voxel
		accumwvox = accumw[ vox_idx ] : input iterator over all spins
		(*accumwvox) : weight of current spins
	num_vox		   : number of voxels
	num_timepoints : number of time points
	num_spins	   : number of spins
	num_timestepidx : number of valid time step id's (thus 0 <= timestepId[i] < num_timestepidx)
	actionId		: num_timepoints long array with 'actions' to be performed
	timestepId      : num_timepoints long array with indices that select the correct time step.

	*/
	using std::iterator_traits;
	using std::complex;

	// some typedefs (well... actually quite many ..)
    // to get, for each input argument,  the type of the iterator over the voxels,  
    // and (from that) the type of the interator over the time points,              (think of this as array of pointers, but due to regular storage typically that array is not explicitly stored.)
    // and (when applicable) the type of the iterator over the spins within a voxel (typically C pointer)
    // and from that the actual type that of the values that are loaded.            (typically double or complex double)
	typedef typename iterator_traits< Mtype >::value_type        Mvoxtype;
	typedef typename iterator_traits< M0type >::value_type       Mz0valtype;
	typedef typename iterator_traits< relaxT2type >::value_type  relaxT2voxtype;
	typedef typename iterator_traits< relaxT1type >::value_type  relaxT1voxtype;
	typedef typename iterator_traits< B0type >::value_type       B0voxtype;
	typedef typename iterator_traits< RFpulsetype >::value_type  RFvoxtype;
	typedef typename RFvoxtype::value_el_type                    RFeltype;
	typedef typename iterator_traits< Mstoretype >::value_type   Mstorevoxtype;
	typedef typename iterator_traits< accumType >::value_type    Accumvoxtype;
	
	typedef typename iterator_traits< relaxT2voxtype >:: value_type   relaxT2valtype;
	typedef typename iterator_traits< relaxT1voxtype >:: value_type   relaxT1valtype;
	typedef typename iterator_traits< B0voxtype >:: value_type        B0ittype; 
	typedef typename iterator_traits< B0ittype >:: value_type         B0valtype; 
    
    typedef typename iterator_traits< gradType >::value_type     gradvoxtype;
    typedef typename iterator_traits< gradvoxtype >::value_type  gradPulseType;
    typedef typename iterator_traits< gradPulseType >::value_type  gradPulseElType;
            
	typedef typename  Mvoxtype::value_type     Mvoxvaltype;
	//typedef MvalueType< double >  Mvoxvaltype;
	typedef typename Mvoxvaltype::value_type		             Mvoxeltype;
	typedef          complex< Mvoxeltype >                       Mvoxeltypec;

	//BOOST_STATIC_ASSERT_MSG( false, Mvoxvaltype );

	for (int_t vox_idx = 0; vox_idx < num_vox; ++vox_idx ) {
		Mz0valtype     Mz0vox     = Mz0[     vox_idx ];
		relaxT2voxtype relaxT2vox = relaxT2[ vox_idx ];
		relaxT1voxtype relaxT1vox = relaxT1[ vox_idx ];
		B0voxtype      B0vox      = B0[      vox_idx ];
		RFvoxtype      RFpulsest  = RFpulses[vox_idx ];
        gradvoxtype    gradvox    = grad[    vox_idx ];
		Mstorevoxtype  Mstorevox  = Mstore[  vox_idx ];
		int_t num_store_left_vox  = num_store_idx;
        int_t gradvox_idx = 0;

		for (int_t timeidx = 0; timeidx < num_timepoints; ++timeidx){
			int            timestep_id = timestepId[ timeidx ];
			if ( (timestep_id<0) | (timestep_id>=num_timestepidx) ) {
				mexErrMsgTxt("All elements of timestepidx should be >=0 and <Ti");
			}

			relaxT2valtype relaxT2voxt  = relaxT2vox[ timestep_id ];
			relaxT1valtype relaxT1voxt  = relaxT1vox[ timestep_id ];

			Mvoxtype       Mvox       = M[       vox_idx ]; 
			Accumvoxtype   accumvox   = accumw[  vox_idx ];

			B0ittype       B0lp        = B0vox[ timestep_id ] ;

			bool dostore = (actionId[ timeidx ] & 1)!=0;
			bool doRFx   = (actionId[ timeidx ] & 2)!=0;
			bool doRFy   = (actionId[ timeidx ] & 4)!=0;
            bool doGrad  = (actionId[ timeidx ] & 8)!=0;
			
			RFeltype    RFpulse_xx, RFpulse_xy, RFpulse_xz, 
						RFpulse_yx, RFpulse_yy, RFpulse_yz,
						RFpulse_zx, RFpulse_zy, RFpulse_zz;
			if (doRFx) {
				if (doRFy) {
					// load all
					RFpulse_xx = RFpulsest(0,0);
					RFpulse_xy = RFpulsest(0,1);
					RFpulse_xz = RFpulsest(0,2);
					RFpulse_yx = RFpulsest(1,0);
					RFpulse_yy = RFpulsest(1,1);
					RFpulse_yz = RFpulsest(1,2);
					RFpulse_zy = RFpulsest(2,1);
				}
				RFpulse_zz = RFpulsest(2,2);
				RFpulse_zx = RFpulsest(2,0);
				++RFpulsest; // proceed to next pulse
			} else if (doRFy) {
				// Rotation around x, so only y and z change:
				RFpulse_zz = RFpulsest(2,2);
				RFpulse_zy = RFpulsest(2,1);
				++RFpulsest; // proceed to next pulse
			}
            gradPulseType gradPulse;
            if (doGrad) {
                gradPulse =  gradvox[gradvox_idx] ;
                ++gradvox_idx;
            }
			Mvoxeltype z(0.);
			Mvoxvaltype Mstoretmp( z,z,z);// = Mvoxvaltype();
			// Could split this inner loop conditionally on dostore, doRFx, doRFy.
			// However, the conditional branches are perfectly predicted (num_spins is typically large)
			for (int_t spinidx = num_spins ; spinidx >0 ; --spinidx, ++Mvox, ++B0lp) {
				Mvoxvaltype Mupd = *Mvox;
				//complex< vec<double,2> > B0lpit = *B0lp;//.read();
				// T2 relaxation (& potential global offresonance) + T2star simulating offresonance:
				{Mvoxeltypec Mxy = ( Mvoxeltypec( Mupd.x, Mupd.y ) * relaxT2voxt ) * ( (B0valtype) *B0lp ); 
					//Mvoxeltypec Mxy = ( Mvoxeltypec( Mupd.x, Mupd.y ) * relaxT2voxt ) * B0lpit; 
				Mupd.x = Mxy.real() ;
				Mupd.y = Mxy.imag() ;}

				// T1 relaxation
				//Mupd.z = Mz0vox + ( Mupd.z - Mz0vox ) * relaxT1voxt; // relaxT1voxt = exp( - deltaT/T1)
				Mupd.z = Mupd.z + ( Mupd.z - Mz0vox ) * relaxT1voxt; // relaxT1voxt  = exp(-deltaT/T1)-1
				// NOTE: second variant more accurate (when exp()-1 is computed accurately as with expm1)

				// Rf application:
				if (doRFx) {
					// RF involving x coordinate, may involve y as well (fully general)
					if (doRFy) {
						// Fully general RF:
						Mvoxeltype Mx( Mupd.x );
						Mvoxeltype My( Mupd.y );
						Mupd.x  = Mx * RFpulse_xx + My * RFpulse_xy + Mupd.z * RFpulse_xz;
						Mupd.y  = Mx * RFpulse_yx + My * RFpulse_yy + Mupd.z * RFpulse_yz;
						Mupd.z  = Mx * RFpulse_zx + My * RFpulse_zy + Mupd.z * RFpulse_zz;
					} else {
						// RF involving only x and z
						Mvoxeltype Mx( Mupd.x );
						Mupd.x  = Mx * RFpulse_zz - Mupd.z * RFpulse_zx;
						Mupd.z  = Mx * RFpulse_zx + Mupd.z * RFpulse_zz;
					}
				} else if (doRFy) {
					// RF involving only y and z
					Mvoxeltype My(  Mupd.y );
					Mupd.y = My * RFpulse_zz - Mupd.z * RFpulse_zy;
					Mupd.z = My * RFpulse_zy + Mupd.z * RFpulse_zz;
				}
                
                // Apply the spatial gradient induced spin rotation:
                if (doGrad) {
                    {Mvoxeltypec Mxy =  Mvoxeltypec( Mupd.x, Mupd.y ) * ((gradPulseElType) (*gradPulse)) ; 
                     Mupd.x = Mxy.real() ;
                     Mupd.y = Mxy.imag() ;}
                    ++gradPulse; // next spin.
                }

				// accumulate values for net magnetisation output:
				if (dostore) {
					Mstoretmp +=  Mupd * (*accumvox);
					++accumvox;
				}

				// store updated values:
				*Mvox = Mupd;
			};

			if (dostore) {
				if (num_store_left_vox<=0) {
					mexErrMsgTxt("Too many elements stored.");
				}
				--num_store_left_vox;
				*Mstorevox = Mstoretmp.sum();
				++Mstorevox;
			}
		}
	}
};

template < typename valueT > struct MvalueType {
// MvalueType< valueType> 
//  Simple class that represents 1 3D magnetisation vector.
//	Has fields x,y,z that are publicly accesable.
// Created by Dirk Poot, Erasmus MC, 4-3-2013.  
public:
	typedef valueT value_type;
	typedef MvalueType<value_type> self;
	value_type x, y, z;
	typedef MvalueType< typename IF< IS_vec<valueT>::TF , typename STDTYPES< valueT >::value_type , valueT >::RET> sumType;
public:
	MvalueType(): x(0.), y(0.), z(0.) {;};
	MvalueType(valueT x_, valueT y_, valueT z_): x(x_), y(y_), z(z_) {;};
	inline self operator*( self other) {
		return self( x * other.x, y * other.y, z * other.z);
	}
	inline self operator*( value_type other) {
		return self( x * other, y * other, z * other);
	}
	inline void operator+=( self other) {
		x += other.x;
		y += other.y;
		z += other.z;
	}
	sumType sum( ) {
		return sumType( ::sum(x), ::sum(y), ::sum(z) );
	}
};

template < typename T > class store_MvalueType_helper {
// store_MvalueType_helper< T > 
//  Helper class that represents a reference to a MvalueType.
//  and performs the storing to MType
// Created by Dirk Poot, Erasmus MC, 4-3-2013. 
	T & b;
public:
	typedef typename T::value_type value_type;

	store_MvalueType_helper(T & b_): b(b_){;};

	inline void operator=( value_type newval ) {
		b.set( newval );
	}
	inline operator value_type() { 
		return value_type( b.x(), b.y(), b.z()); 
	};
};
using std::iterator_traits;

template < typename  valuePointerType > class MType {
// MType< valuePointerType > 
//  Class that represents a vector of 3D magnetisation vectors, 
//	where the x,y,z components are stored: first all x, then all y, then all z, 
//	with offset between the x, y, and z of a specific point.
//	
// Created by Dirk Poot, Erasmus MC, 4-3-2013.  
public:
    typedef std::random_access_iterator_tag iterator_category;
	typedef MType< valuePointerType > self;
	typedef typename iterator_traits< valuePointerType >::value_type value_el_type;
	typedef MvalueType<value_el_type> value_type;
	typedef MvalueType<value_el_type> pointer; // dummy value to get it compiled
	typedef store_MvalueType_helper< self > reference;
	typedef typename iterator_traits< valuePointerType >::difference_type difference_type;
    //typedef typename iterator_traits< valuePointerType >::distance_type distance_type;
private:
	valuePointerType curpnt;
	ptrdiff_t offset;
public:
	MType( const self * init ): curpnt(init->curpnt), offset(init->offset) {;};
	MType( valuePointerType init_ , ptrdiff_t offset_ ): curpnt(init_), offset(offset_) {;};

	inline value_el_type x() {
		return *curpnt;
	}
	inline value_el_type y() {
//		mexPrintf("Alignment requirement of value_el_type : %d", __alignof(value_el_type));
		return *(curpnt+offset);
	}
	inline value_el_type z() {
		return *(curpnt+2*offset);
	}
	inline self * operator++() {
		curpnt++;
		return this;
	};
	inline self * operator+=(ptrdiff_t i) {
		curpnt+= i;
		return this;
	};
	inline value_type operator*() const {
		value_type tmp;
		tmp.x = this->x;
		tmp.y = this->y;
		tmp.z = this->z;
		return tmp;
	};
	inline void set( value_type newval ) {
		*curpnt            = newval.x;
		*(curpnt+offset)   = newval.y;
		*(curpnt+2*offset) = newval.z;
	}
	inline reference operator*() {
		return reference(*this);
	};
};

template < typename T > class RFType {
public:
	typedef RFType<T> self;
	typedef typename iterator_traits<T>::value_type value_el_type;
private:
	T origin;
public:
	RFType( T ptr_): origin(ptr_) {;};
	inline self * operator++() {
		origin += 9;
		return this;
	}
	inline void operator+=( int step ) {
		origin += 9* step;
	};
	inline value_el_type operator()( int i, int j) {
		return *(origin + 3*i + j );
	}
};


template < typename T > class voxelize : public std::iterator< std::random_access_iterator_tag , T > {
private:
	T origin;
	ptrdiff_t voxel_step;
    voxelize(); // default constructor purposedly not implemented.
public:
	typedef T value_type;
	typedef voxelize<value_type> self;
	voxelize(const self & src ): origin(src.origin),voxel_step(src.voxel_step) {;};
	voxelize( T origin_ , ptrdiff_t voxel_step_): origin(origin_), voxel_step(voxel_step_) {;};
	void operator+=(int i) {
		origin += voxel_step*i;
	}
	T operator[](int i) const {
		T tmp(origin);
		tmp += voxel_step*i;
		return tmp;
	}
};

template < typename T , typename intItT> class voxelize_index : public voxelize<T> {
private:
    intItT index;
public:
    typedef T value_type;
    typedef voxelize<T> parent;
	typedef voxelize_index<value_type, intItT> self;
    voxelize_index(const self & src ): parent( src), index(src.index) {;};
	voxelize_index( T origin_ , ptrdiff_t voxel_step_, intItT index_): parent(origin_, voxel_step_), index(index_) {;};
	void operator+=(int i) {
		index += i;
	};
	T operator[](int i) const {
		return (*static_cast<const parent *>(this))[ index[i] ];
	};
};
        
const int n_rowscompOutArea = 5;
#define ERROR mexErrMsgTxt


typedef ptrdiff_t int_t ;



void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{

/*  
*/
#ifdef PERFORMANCECOUNTING
//	timeCntT tstart_initialization = __rdtsc(); 
	timeCntT tstart_initialization; 
	PERFORMANCECOUNTER( tstart_initialization );
#endif

	/* Check for proper number of arguments. */

	if ((nrhs!=10) && (nrhs!=11)) {
		mexErrMsgTxt("Ten or eleven inputs required: [M_store, M_final] = Bloch_sim_c( Mz0, relaxT2, relaxT1, B0, RFpulses, gradPulses, gradPulseIndex, accumw, timestepid , actionId [, M_init] ).");
	}
	if ((nlhs!=1)&&(nlhs!=2)) {
		mexErrMsgTxt("One or two outputs required [M_store, M_final].");
	}

	// N = number of voxels
	// T = number of time points
	// Tr = number of time points at which RF pulses are applied.
	// Ti = number of unique time steps.
	// Ts = numer of store time points.
    // Tg = number of gradient pulse points. 
    // Tq = number of unique gradient pulses.
	// M  = number of spins per voxel
	const mxArray * Mz0			= prhs[0]; // N element vector with magnetization scale.
	const mxArray * relaxT2		= prhs[1]; // Ti x N real or complex relaxation factors (xy magnetisation)
	const mxArray * relaxT1		= prhs[2]; // Ti x N real relaxation factors minus 1. Thus: relaxT1 = exp( - deltaT/T1)-1 
	const mxArray * B0			= prhs[3]; // M x Ti x (1 or N) complex matrix with offresonance factors.
	const mxArray * RFpulses	= prhs[4]; // 3 x 3 x Tr x (1 or N) element matrix with rotation due to RF pulses
    const mxArray * gradPulses  = prhs[5]; // M x Tq complex valued gradient pulses (i.e. x-y plane rotations)
    const mxArray * gradPulseindex= prhs[6]; // Tg x (1 or N) int32 vector, 0<= elements < Tq,  with indices into gradPulses.
	const mxArray * accumw		= prhs[7]; // M element vector with weight of the different spins in the accumulation
	const mxArray * timestepid	= prhs[8]; // T element int32 vector with indices Ti.
	const mxArray * actionId	= prhs[9]; // T element int32 vector that specifies the actions performed at each 
										   // time point:
										   // 0     : invalid ( there should always be some action.. )
										   //   + 1 : store point
										   //   + 2 : RF, rotation around y axis (so only x and z change)
										   //   + 4 : RF, involves around x axis (so only y and z change)
										   //         +2&+4 : general 3x3 matrix multiplication involving all coordinates
                                           //   + 8 : gradient pulse. There should be exactly Tg actionId's with this bit set.
	bool hasM_init = (nrhs>10);
    const mxArray * M_init;//	= prhs[10]; // M x 3 x N : real valued array with initial magnetization vectors. When not provided, initial magnetisation is rest state.
//  OUTPUTS:
//	mxArray * Mstore ;					   // Ts x 3 x N : real valued array with net magnetization at the store time points.
//	mxArray * Mcur;						   // M x 3 x N : real valued array with final magnetization state.


	/* parse inputs */
	//Mz0:
	size_t N = mxGetNumberOfElements( Mz0 );
	mxClassID procclass = mxGetClassID( Mz0 );
	if ((procclass!=mxDOUBLE_CLASS) & (procclass!= mxSINGLE_CLASS) & (!mxIsComplex( Mz0 ))) {
		mexErrMsgTxt("Mz0 input should be N element non complex single or double vector.");
	}
	//const void * Mz0p = mxGetData( Mz0 );

	// relaxT2
	bool complexT2 = mxIsComplex( relaxT2 ) ;
	size_t Ti = mxGetM( relaxT2 );
	if ( (mxGetClassID( relaxT2 ) != procclass) | (mxGetN( relaxT2) != N) ) {
		mexErrMsgTxt("relaxT2 should be (non/)complex double/single matrix with size Ti x N.");
	}
	//const void * relaxT2p = mxGetData( relaxT2 );

	// relaxT1
	if ( (mxGetClassID( relaxT1 ) != procclass) | (mxGetN( relaxT1 ) != N) | (mxGetM( relaxT1 ) != Ti ) | mxIsComplex( relaxT1) ) {
		mexErrMsgTxt("relaxT1 should be non-complex double/single matrix with size N x Ti.");
	}
	//const void * relaxT1p = mxGetData( relaxT1 );

	// B0
	const mwSize * B0size = mxGetDimensions( B0 );
	int B0numdims = mxGetNumberOfDimensions( B0 );

	size_t M = B0size[0];
	if ( (mxGetClassID( B0 ) != procclass) | (B0size[ 1 ] != Ti ) | !mxIsComplex( B0 ) | (B0numdims>3) || ((B0numdims==3) && (B0size[2]!=N)) ) {
		mexErrMsgTxt("B0 should be complex double/single matrix with size N x Ti x (1 or N).");
	}
	size_t B0voxelStep = 0; // default: all voxels same B0
	if (B0numdims==3) {
		B0voxelStep = M * Ti;
	}
	//const void * B0r = mxGetData( B0 );
	//const void * B0i = mxGetImagData( B0 );

	// RFpulses
	const mwSize * RFsize = mxGetDimensions( RFpulses );
	int RFnumdims = mxGetNumberOfDimensions( RFpulses );
	int Tr;
    int RFvoxstep =0;
	if (RFnumdims==2) {
		Tr = 1;
	} else if (RFnumdims==3) {
		Tr = RFsize[2];
	} else if (RFnumdims==4) {
        Tr = RFsize[2];
        RFvoxstep = Tr;
        if (RFsize[3]!=N)
            mexErrMsgTxt("4th dimension of RFpulses have size N.");
    } else {
		mexErrMsgTxt("RFpulses should be 3 or 4 dimensional array.");
	}
	if ( (mxGetClassID( RFpulses ) != procclass) || mxIsComplex( RFpulses ) || (RFsize[0]!=3) || (RFsize[1]!=3) ) {
		mexErrMsgTxt("RFpulses should be a 3 x 3 x Tr x (1 or N) element non complex double/single matrix .");
	}
	//const void * RFpulsesp = mxGetData( RFpulses );

    // gradPulses
	const mwSize * gradPulsesize = mxGetDimensions( gradPulses );
	int gradPulsesnumdims = mxGetNumberOfDimensions( gradPulses );
    size_t Tq = gradPulsesize[1];
	if (( (Tq>0) && ( (mxGetClassID( gradPulses ) != procclass) || (gradPulsesize[ 0 ] != M ) | !mxIsComplex( gradPulses ) ) )|| (gradPulsesnumdims>2) ) {
		mexErrMsgTxt("gradPulses should be complex double/single matrix with size M x Tq.");
	}
	//const void * gradPulsesr = mxGetData( gradPulses );
	//const void * gradPulsesi = mxGetImagData( gradPulses );
    
    // gradPulseindex
    const mwSize * gradPulseindexsize = mxGetDimensions( gradPulseindex );
	int gradPulseindexnumdims = mxGetNumberOfDimensions( gradPulseindex );
    size_t Tg = gradPulseindexsize[0];
	if ( ((Tg>0) && ((mxGetClassID( gradPulseindex ) != mxINT32_CLASS) || (gradPulseindexsize[1]!=1 && gradPulseindexsize[1]!=N) ||  mxIsComplex( gradPulseindex ) ) ) || (gradPulseindexnumdims>2) ) {
		mexErrMsgTxt("gradPulseindex should be int32 matrix with size Tg x (1 or N).");
	}
    size_t gradPulseIndexVoxStep = 0;
    if (gradPulseindexsize[1] > 1) {
		gradPulseIndexVoxStep = Tg;
	}
            
	// accumw
	int numcolsAccumw = mxGetN( accumw );
	if ( (mxGetClassID( accumw ) != procclass) || (mxGetM( accumw ) != M) || (numcolsAccumw!=1 && numcolsAccumw!=N) || mxIsComplex( accumw ) ) {
		mexErrMsgTxt("accumw should be M x (1 or N) element non complex double/single matrix.");
	}
	size_t accumw_voxStep = 0;
	if (numcolsAccumw > 1) {
		accumw_voxStep = M;
	}
	//const void * accumwp = mxGetData( accumw );

	// timestepid
	size_t T = mxGetNumberOfElements( timestepid );
	mxClassID timestepidclass = mxGetClassID( timestepid );
	if ( (timestepidclass != mxINT32_CLASS) || (T< Tr) || mxIsComplex( timestepid ) ) {
		mexErrMsgTxt("timestepid should be T (>=Tr) element non complex int32 vector.");
	}
	//int32_t * timestepid_ptr = (int32_t*) mxGetData( timestepid );
	mxGetPtr( const int32_t *, timestepid_ptr,  timestepid );

	// actionId
	mxClassID actionIdclass = mxGetClassID( actionId );
	size_t actionId_numel = mxGetNumberOfElements( actionId );
	if ( (actionIdclass  != mxINT32_CLASS) || ( actionId_numel != T) || mxIsComplex( actionId ) ) {
		mexErrMsgTxt("actionId should be T element non complex int32 vector.");
	}

	if ( hasM_init ) {
		// M_init provided
		M_init		= prhs[10];
		const mwSize * M_initsize = mxGetDimensions( M_init );
		int M_initnumdims = mxGetNumberOfDimensions( M_init );
		if ( (mxGetClassID( M_init ) != procclass) || mxIsComplex( M_init ) || (M_initsize[0]!=M) || (M_initsize[1]!=3) || !( ((M_initnumdims == 3) && (M_initsize[2]==N)) || ((M_initnumdims == 2) && (1==N))) ) {
			mexErrMsgTxt("M_init should be M x 3 x N element non complex double/single matrix.");
		}
	}

	//int32_t * actionId_ptr = (int32_t*) mxGetData( actionId );
	mxGetPtr( const int32_t *, actionId_ptr,    actionId );

	// scan actionId for validity and finding Ts
	size_t Ts = 0;
	int Tr_check = 0;
    int Tg_check = 0;
	for (int k = 0 ; k < T ; ++k ) {
		if ((actionId_ptr[k]<0)  || (actionId_ptr[k]>15) ) {
			mexErrMsgTxt("actionId elements should be >0 and <=15.");
		}
		Ts += actionId_ptr[k]&1;
		Tr_check += ((actionId_ptr[k]&6)!=0);
        Tg_check += ((actionId_ptr[k]&8)!=0);
	}
	if (Tr_check!=Tr) {
		mexErrMsgTxt("Number of RF pulses provided does not match to the number of RF actions.");
	}
	if (Tg_check!=Tg) {
		mexErrMsgTxt("Number of gradient pulse indices provided does not match to the number of gradient pulse actions (wrong size of gradPulseindex).");
	}
    
    // scan gradPulseindex for valid values
    mxGetPtr( const int32_t *, gradPulseindex_ptr,    gradPulseindex );
    for (size_t  k = 0 ; k < Tg *  gradPulseindexsize[1] ; ++k ) {
        if ((gradPulseindex_ptr[k]<0)  || (gradPulseindex_ptr[k]>=Tq) ) {
            mexErrMsgTxt("Value of gradienPulseIndex invalid since it is out of the range [ 0 .. Tq-1].");
        }
    }

	// Create output: Mstore
	 
	mwSize storedims[3];
	storedims[0] = Ts;
	storedims[1] = 3;
	storedims[2] = N;
	mxArray * Mstore = mxCreateNumericArray(3 , storedims , procclass , mxREAL);
	//void * Mstorep = mxGetData( Mstore );
	plhs[0] = Mstore;
	storedims[0] = M;
	mxArray * Mcur;
	if (hasM_init) {
		Mcur = mxDuplicateArray( M_init );
	} else {
		Mcur = mxCreateNumericArray(3 , storedims , procclass , mxREAL);
	};
	//void * Mcurp = mxGetData( Mcur );

//#define	ROBUSTDEFAULT
#ifdef ROBUSTDEFAULT
	int n_tempspace_bytes = 2000000;
	char * tempspaceptr = (char *) mxMalloc( n_tempspace_bytes );
	tempspace tmp = tempspace(tempspaceptr, n_tempspace_bytes);
#endif

	if (procclass==mxDOUBLE_CLASS ) {
		// Double precision:
		if ((M % 2)!=0) {
			mexErrMsgTxt("M should be even");
		}

		typedef double baseT;  // main type definition. 

		typedef const baseT * base_ptrrd_type;  
		typedef       baseT * base_ptrwt_type;  
#if VECVLEN==1
		typedef base_ptrrd_type base_ptrvrd_type;  
		typedef base_ptrwt_type base_ptrvwt_type;  
#else
		typedef vecptr<const baseT *, VECVLEN> base_ptrvrd_type;  
		typedef vecptr<      baseT *, VECVLEN> base_ptrvwt_type;  
#endif
		mxGetPtr( base_ptrrd_type, Mz0_ptr,     Mz0 );
        mxGetPtr( base_ptrrd_type, relaxT2_ptr, relaxT2 );
		mxGetPtr( base_ptrrd_type, relaxT1_ptr, relaxT1 );
		mxGetComplexPtri( base_ptrvrd_type, B0_ptr, B0 , base_ptrrd_type);
		mxGetPtr( base_ptrrd_type, RFpulses_ptr,RFpulses );
		mxGetComplexPtri( base_ptrvrd_type, gradPulses_ptr, gradPulses , base_ptrrd_type);
		mxGetPtri( base_ptrvrd_type, accumw_ptr,  accumw , base_ptrrd_type);

		mxGetPtr( base_ptrwt_type, Mstore_ptr , Mstore);
		mxGetPtri( base_ptrvwt_type, Mcur_ptr ,   Mcur , base_ptrwt_type);

#ifdef ROBUSTDEFAULT
		//if (true) {
			// Create pointer arrays and call without 'complex' objects:
			typedef MvalueType< baseT > Mvoxvalue_type;
			typedef Mvoxvalue_type *  Mvox_type;
			typedef Mvox_type * M_type;

			M_type    MptrArray = tmp.get<Mvox_type>( N ); 
			Mvox_type M_array   = tmp.get<Mvoxvalue_type>( M * N ); 
			for ( int i = 0 ; i < N ; i++ ) {
				MptrArray[i] = M_array + M * i;
			}

			Mvox_type Mstore_array   = tmp.get<Mvoxvalue_type>( Ts * N ); 
			M_type    MstoreptrArray = tmp.get<Mvox_type>( N ); 
			for ( int i = 0 ; i < N ; i++ ) {
				MstoreptrArray[i] = Mstore_array + Ts * i;
			}

						
			typedef ptrrd_type * rT_type;
			rT_type   T1_Array = tmp.get<ptrrd_type>( N ); 
			rT_type   T2_Array = tmp.get<ptrrd_type>( N ); 
			for ( int i = 0 ; i < N ; i++ ) {
				T1_Array[i] = ( relaxT1_t ) + Ti * i;
				T2_Array[i] = ( relaxT2_t ) + Ti * i;
			}
			
			//typedef lineTypec<const baseT *, baseT , ptrdiff_t > B0_it_type;
			typedef complex< baseT> B0_itel_type;
			typedef B0_itel_type * B0_it_type;
			typedef B0_it_type * B0_vox_type;
			typedef B0_vox_type * B0_type;
			B0_type   B0t = tmp.get<B0_vox_type>( N ); 
			for ( int i = 0 ; i < N ; i++ ) {
				B0t[i] = tmp.get<B0_it_type>( Ti );
				for (int k = 0 ; k < Ti; k++ ) {
					B0t[i][k] = tmp.get<B0_itel_type>( M );
					//B0t[i][k] = B0_it_type(((const baseT *) B0r) + B0voxelStep * i + M * k, ((const baseT *) B0i)+ B0voxelStep * i + M * k , 1 );
					const baseT * B0rt = ( B0_tR ) + B0voxelStep * i + M * k;
					const baseT * B0it = ( B0_tI ) + B0voxelStep * i + M * k;
					for (int l = 0 ; l < M ; ++l ) {
						B0t[i][k][l] = B0_itel_type( B0rt[l], B0it[l] );
					}
				}
			}
			typedef RFType< baseT > RF_vox_type;
			typedef RF_vox_type * RF_type;
			RF_type RFpulsest = tmp.get<RF_vox_type>( N ); 
			for ( int i = 0 ; i < N ; i++ ) {
				RFpulsest[i] = RF_vox_type( ( RFpulses_t) );
			}

			typedef ptrrd_type * accum_type;
			accum_type  accumwt = tmp.get<ptrrd_type>( N ); 
			for ( int i = 0 ; i < N ; i++ ) {
				accumwt[i] = ( accumw_t ) + M * i;
			}

			// call core routine:
			Bloch_sim_core< size_t, M_type, ptrrd_type, rT_type , rT_type , B0_type, RF_type, M_type, accum_type>
				( MptrArray, Mz0_t, T2_Array, T1_Array, B0t, RFpulsest, gradPulsest,
							MstoreptrArray, accumwt,
							N , T, M, Ti, Ts,
							actionId_ptr, timestepid_ptr);

			baseT *  Mstore_out = Mstore_t ;
			for ( int i = 0 ; i < N ; i++ ) {
				Mvox_type lp = MstoreptrArray[i];
				for (int k = 0; k < Ts; k++ ) {
					*Mstore_out = lp[k].x;
					++Mstore_out;
				}
				for (int k = 0; k < Ts; k++ ) {
					*Mstore_out = lp[k].y;
					++Mstore_out;
				}
				for (int k = 0; k < Ts; k++ ) {
					*Mstore_out = lp[k].z;
					++Mstore_out;
				}
			}

			tmp.checkGuard( MptrArray, N );
			tmp.checkGuard( M_array, M*N );
			tmp.checkGuard( Mstore_array, Ts*N);
			tmp.checkGuard( MstoreptrArray, N );
			tmp.checkGuard( T1_Array,N );
			tmp.checkGuard( T2_Array,N );
			tmp.checkGuard( B0t,N );
			for ( int i = 0 ; i < N ; i++ ) {
				tmp.checkGuard( B0t[i], Ti );
			}
			tmp.checkGuard( RFpulsest, N );
			tmp.checkGuard( accumwt,N );
#else   //} else {
			// temporary memory free version: 
		// Convert types & construct the wrapper objects: (pointers + some steps)
		// &define derived types for the different arguments:


		typedef MType< Mcur_ptr_type > Mvox_type;
		typedef voxelize< Mvox_type > M_type;
		M_type Mt( Mvox_type( Mcur_ptr, M/VECVLEN ), M*3/VECVLEN );

		typedef voxelize< relaxT1_ptr_type > rT1_type;
		typedef voxelize< relaxT2_ptr_type > rT2_type; // may be complex
		rT2_type relaxT2t( relaxT2_ptr , Ti);
		rT1_type relaxT1t( relaxT1_ptr , Ti);

		typedef lineTypec< B0_ptr_type, typename iterator_traits< B0_ptr_type >::value_type , ptrdiff_t > B0_it_type;
		typedef voxelize< B0_it_type > B0_vox_type;
		typedef voxelize< B0_vox_type > B0_type;
		B0_type B0t( B0_vox_type( B0_it_type( B0_ptr_R, B0_ptr_I , 1 ), M / VECVLEN), B0voxelStep/M );

		typedef RFType< RFpulses_ptr_type > RF_vox_type;
		typedef voxelize< RF_vox_type > RF_type;
		RF_type RFpulsest( RF_vox_type( RFpulses_ptr ), RFvoxstep);

		typedef lineTypec< gradPulses_ptr_type, typename iterator_traits< gradPulses_ptr_type >::value_type , ptrdiff_t > gradPulses_it_type;
		typedef voxelize_index< gradPulses_it_type , actionId_ptr_type > gradPulses_vox_type;
		typedef voxelize< gradPulses_vox_type > gradPulsesType;
		gradPulsesType gradPulsest( gradPulses_vox_type( gradPulses_it_type( gradPulses_ptr_R, gradPulses_ptr_I , 1 ), M / VECVLEN, gradPulseindex_ptr ), gradPulseIndexVoxStep );

        typedef MType< Mstore_ptr_type > Mstorevox_type;
		typedef voxelize< Mstorevox_type > Mstore_type;
		Mstore_type Mstoret( Mstorevox_type( Mstore_ptr, Ts) , Ts*3 );

		typedef voxelize< accumw_ptr_type > accum_type;
		accum_type accumwt( accumw_ptr, accumw_voxStep / VECVLEN );

		// call core routine:
		Bloch_sim_core( Mt, Mz0_ptr, relaxT2t, relaxT1t, B0t, RFpulsest, gradPulsest,
					Mstoret, accumwt,
					N , T, M / VECVLEN, Ti, Ts,
					actionId_ptr, timestepid_ptr);
#endif	//	}

	} else {
		mexErrMsgTxt("Single not yet supported.");
	}
#ifdef ROBUSTDEFAULT
	mxFree( tempspaceptr );
#endif
	if (nlhs>=2) {
		plhs[1] = Mcur;
	} else {
		mxDestroyArray( Mcur );
		Mcur = NULL;
	}

}
