function [ p ] = substitute_pulse( p, rf_pulses, idx_rf_pulse, Gslice, Gprephase, Grephase )
% [ p ] =  substitute_pulse( p, rf_pulses, idx_rf_pulse, Gslice, Gprephase, Grephase, Gend )
%
% substitutes hard pulses in MRI_pulses objects by pulse envelopes with
% slice selective gradient
%
% NOTE that the sampled pulses have nonzero pulse duration in contrast to
% instantanteous pulses. This has consequences:
%  1. first event after RF pulse has minimal spacing of Trf/2
%  2. final event has its duration decreased by Trf/2
% this keeps TR and TE consistent with the hard pulse implementation
%
% INPUTS:
% p_in          : MRI_pulses object with (#FA) hard pulses
% rf_pulses     : cell array of rf pulses,
%               where rf_pulses{i} = [deltaT(i,:); rf_amp(i,:)]
%               note that pulse should be normalized to 1 deg rotation
% idx_rf_pulse  : (#FA or 1 x 1) array that gives the sampled rf_pulse
%               for each hard RF pulse
%                => hard_pulse(i) substituted by rf_pulses{ idx_rf_pulse(i) }
%               idx_rf_pulse(i) == 0 => don't substitute pulse i
% Gslice         : cell array of grad pulses used for slice selection.
%                 the gradient magnitude (rad/unit space) is divided over
%                 the pulse discretization
%                 Default = [0 0 8*pi]
% idx_grad_pulse: (#FA or 1 x 1) indices of used grad pulse:
%                   => hard_pulse(i) is played with grad_pulses{ abs( idx_grad_pulse(i) ) }
%                   Default = 0 (i.e. no slice selection gradient)
% frac_rewinder : fraction of the gradient pulse used for rewinding
%                   Default = 0 (i.e. no rewinder => balanced)
%
%
% Created by Willem van Valenberg, TUDelft
% 26-09-2017

actionid_rf     = find( bitand( p.actionid , 6) );
gradnr          = cumsum( bitand( p.actionid , 8) ~= 0 );

if numel( idx_rf_pulse ) == 1
    idx_rf_pulse = idx_rf_pulse * ones( size( actionid_rf ) );
end

if nargin < 4 || isempty( Gslice )
    Gslice = [0 0 8*pi];
end

if nargin < 5 || isempty( Gprephase )
    Gprephase = [0 0 0];
end

if nargin < 6 || isempty( Grephase )
    Grephase = Gslice/2;
end

% if nargin < 7 || isempty( Gend )
%     Gend = [0 0 0];
% end

% substitute into pulse sequence
for k = numel( idx_rf_pulse ): -1 : 1
    
    if idx_rf_pulse(k) > 0
        
        if bitand( p.actionid( actionid_rf( k ) ) , 8)
            error( 'CANNOT RESAMPLE PULSE WITH SIMULTANIOUS GRADIENT. DEPENDS ON INTERPRETATION OF GRADIENT');
        else
            
            % determine RF pulse
            rf_dt   = rf_pulses{ idx_rf_pulse( k ) }( 1, : );
            rf_amp  = rf_pulses{ idx_rf_pulse( k ) }( 2, : );
            N       = numel( rf_amp );
            
            Trf = sum( rf_dt ); % pulse duration
            
            % reduce interval before RF pulse
            if actionid_rf( k ) ~= 1
                idx_pre = actionid_rf( k );
            else % first RF pulse
                idx_pre = numel( p.deltaT ); % remove from last interval to keep TR invariant
            end
            dt_pre  = p.deltaT( idx_pre );
            if dt_pre < Trf / 2
                error( ['At index ' num2str( idx_pre ) ' interval (' num2str( dt_pre ) ') too short for RF pulse (' num2str( Trf / 2 ) ')'] );
            else
                p.deltaT( idx_pre ) = dt_pre - Trf / 2;
            end
            
            % reduce interval after RF pulse
            dt_post = p.deltaT( actionid_rf( k ) + 1 );
            if dt_post < Trf / 2
                error( ['At index ' num2str( actionid_rf( k ) + 1) ' interval (' num2str( dt_post ) ') too short for RF pulse (' num2str( Trf / 2 ) ')'] );
            else
                p.deltaT( actionid_rf( k ) + 1 ) = dt_post - Trf / 2;
            end
            
            if 1 % keep relaxation times during RF pulse?
                add_deltaT              = zeros( 1, 2*N );
                add_actionid            = zeros( 1, 2*N );
                add_actionid(1:2:end)   = 6*ones( 1, N );    % apply pulses
                add_deltaT(2:2:end)     = rf_dt;             % relax
            else
                add_actionid            = 6*ones( 1, N );    % apply pulses
                add_deltaT              = rf_dt;             % don't relax
            end
            
            if  isnan( p.RFscalefactor(k) );
                warning( ['Pulse ' num2str(k) ' loses NaN scaling due to substitution'] );
            end                    
            
            R       = p.RFpulses(:,:,k);
            R_phase = angle( R(3,2) - R(2,3) + ( R(1,3) - R(3,1) ) * 1i );  % phase (rad) of RF pulse in xy-plane
            R_fa    = acos( ( sum( diag( R ) ) - 1 ) / 2 );                 % FA (rad) of RF pulse
            
            [RFpulses, RFscale]     = makeRFpulses( (180/pi) * R_fa * rf_amp * exp( 1i* R_phase ) );
            
            if any( Gslice )
                grad_weight             = rf_dt/Trf;
                grad                    = Gslice * grad_weight ;
                add_actionid(1:2:end)   = add_actionid(1:2:end) + 8; % combine actions (RF played first)            
            else
                grad = [];
            end
            if any( Grephase )
                if isempty( grad )
                    grad = Grephase;
                    add_actionid(1:2:end)   = add_actionid(1:2:end) + 8; % combine actions (RF played first)
                else
                    grad(:,end) = grad(:,end) + Grephase;
                end
            end
%             if any( Gprephase ) || any( Gend )
%                 grad        = cat(2, Gprephase + Gend, grad );
%                 add_actionid= [ 8 add_actionid ];
%                 add_deltaT = [ 0 add_deltaT ];                
%             end
%             
            if any( Gprephase ) %|| any( Gend )
                grad        = cat(2, Gprephase, grad );
                add_actionid= [ 8 add_actionid ];
                add_deltaT = [ 0 add_deltaT ];
            end
            
            
            p.deltaT     = [     p.deltaT( 1:actionid_rf(k) ),...
                add_deltaT,...
                p.deltaT( actionid_rf(k) + 1 : end ) ];
            
            p.actionid   = [     p.actionid( 1:actionid_rf(k) - 1 ),...
                add_actionid, 0, ...
                p.actionid( actionid_rf(k) + 1 : end ) ];
            
            p.RFpulses   = cat( 3,   p.RFpulses(:,:, 1:k - 1),...
                RFpulses,...
                p.RFpulses(:,:, k + 1 : end ) );
            
            p.RFscalefactor    = [ p.RFscalefactor(1:k - 1);...
                RFscale;...
                p.RFscalefactor(k + 1 : end ) ];
            
            p.gradpulses = cat( 2, p.gradpulses( :, 1:gradnr( actionid_rf(k) ) ),...
                grad,...
                p.gradpulses( :, ( gradnr( actionid_rf(k) ) + 1 ):end ) );
            
        end
    end
end

end

