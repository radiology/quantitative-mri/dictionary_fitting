function [pars] = fit_MRI_ROI( fun, data, par0, ROImasks, varargin )
% [pars] = fit_MRI_ROI( fun, data, par0, ROI, [option1 ,value1 ,..] )
%
% Based on Jha_MRM2016_A maximum-likelihood method to estimate a single ADC value of lesions using diffusion MRI.pdf
%
% Fit a -mean- model to rice distributed data inside an ROI. 
% That paper claims it is more robust than computing the mean of maps and 
% does not really suffer from the rice noise floor bias. 
% It is fitting to the mean of the data inside the ROI, subtracting the 
% noise and heterogeneity induced intensity biasses. 
%
% INPUTS:
%  fun   : intensity prediction function, same as for fit_MRI
%  data  : all contrast weighted images, same as for fit_MRI
%  par0  : initial parmameter vector; single column or 1 column per ROI. 
%  ROImasks:  [] = default: select all data.  
%          OR single mask/linear indexing vector selecting the ROI from data. 
%          OR a cell array of such masks. 
%          OR a cell array with multiple indices, e.g. { {1:4,2:5} } to select data(:, 1:4, 2:5 ) 
%                       
% Option+values:
%  fit_MRI options. In principle all options are allowed, but 
%      don't use any spatial-sensitive option (such as spatialRegularizer, 
%      project*, blockSize). Also do not specify an logPDFfun or ImageType 
%      as these are fixed. 
%
% 26-4-2017: Created by Dirk Poot, Erasmus MC.

if nargin<4 || isempty( ROImasks )
    ROImasks = { {':'} };
end;
if ~iscell( ROImasks )
    ROImasks = {ROImasks};
end;

dataROIs = zeros(size(data,1) ,numel( ROImasks ) ); 
sigma = zeros(size(data,1), numel( ROImasks ));
for masknr = 1:numel( ROImasks );
    if iscell( ROImasks{ masknr } )
        dataROI = data(:, ROImasks{ masknr }{:} );
    else
        dataROI = data(:, ROImasks{ masknr } );
    end;        
    dataROIs(:, masknr) = mean( dataROI(:,:) , 2);
    sigma(:, masknr)  = std( dataROI(:,:), [], 2);
end;

fun_mod = @( par, fields ) fun_adjuster( par, fun, fields );

if nargin == 5 && isstruct( varargin{1} )
    fitopt = varargin{1} ;
elseif nargin < 5
    fitopt = struct;
else
    fitopt = makestruct( varargin{:} );
end;
if any( isfield( fitopt, {'spatialRegularizer','mask'})  )
    error('Dont call fit_MRI_ROI with spatial options');
end;
fitopt.imageType = 'real';
fitopt.noiseLevel = median(sigma)/2 ; % sigma also contain tissue heterogenity, so scale it down a bit. Only relevant for determining convergence; not for the location of the optimum. 
fitopt.fields = sigma.^2;

pars = fit_MRI( fun_mod, dataROIs, par0, fitopt );


function [S, dS] = fun_adjuster( pars, fun, sigma2 )
if nargout <= 1
    [Sf] = fun( pars );
else
    [Sf, dSf ] = fun( pars );
end;
S = Sf + sigma2./(2.*Sf);
if nargout >= 2
    % D[ fun[x] + s2/(2 fun[x]), x]
    dS = dSf - bsxfun( @times, sigma2./(2.* Sf.^2), dSf );
end

function s = makestruct( varargin )
s = struct;
for k = 1 : 2 : nargin
    s.( varargin{k} ) = varargin{k+1};
end;

function test_fit_MRI_ROI
%%
bvals = [10 20 40 80 100 200 500]';
sigman = 1; 
bvalss = {-bvals/1000};
fun = @(tht) pred_ExponentialA_MultiCompartiment( tht, bvalss );
nvox = 100; 
thtGT = [10+rand(1,nvox);9+randn(1,nvox)];
dataGT = fun( thtGT );
dataN = AddRiceNoise( dataGT, sigman) ;
[tht_fit] = fit_MRI_ROI( fun, dataN, [10;5] );