function [pdfEst] = parzenSmooth(data, psfSamplePoints, gaussWidth, weights)
% [pdfEst] = parzenSmooth(data, psfSamplePoints [, gaussWidth [,weights]]);
%
%  This function estimates the parzen density of data. 
%  This is an pseudo continuous alternative to a histogram.
%
%  INPUTS:
%    data            : Column vector, or a matrix with multiple column vectors.
%                      The data of which the 'histogram' is computed.  
%    psfSamplePoints : Vector with the points at which the density is
%                      computed. (center of the histogram bins)
%                      Should be an monotonically increasing vector.
%    gaussWidth      : Scalar or 1 by size(data,2) vector. 
%                      default = 1.06 * std(data) * n^-(1/5)
%                      The width of the gauss bump that each data point is
%                      smoothed with.
%    weights         : Same size as data. Provides the weights of the
%                      individual measurements. For a normalized pdf
%                      estimate, the weights in each column should sum to 1. 
%                      default: 1/size(data,1) for all data points.
%
% Created by Dirk Poot, Visionlab, University of Antwerp
% 12-4-2014: updated help text, Dirk Poot: Erasmus MC. 

if length(size(data))>2
    error('data should be matrix or vector');
end;
doWeight = nargin>=4;
if size(data,1)==1
    data = data(:);
    if doWeight
        weights = weights(:);
    end;
end;

n=size(data,1);
if nargin<3 || isempty(gaussWidth)
    gaussWidth = 1.06 .* std(data) .* n^-(1/5);
end;

if numel(gaussWidth)==1
    gaussWidth = gaussWidth(ones(size(data,2),1));
else
    if numel(gaussWidth)~=size(data,2)
        error('gaussWidth should be scalar or specified for each data series');
%     else
%         gaussWidth = ones(size(data,1),1)*gaussWidth(:)';
    end;
end;

if doWeight
    if ~isequal(size(data),size(weights))
        error('data and weights should have equal size');
    end;
    [data,ind] = sort(data);
    for k=1:size(weights,2)
        weights(:,k) = weights(ind(:,k),k);
    end;
    clear ind
else
    data = sort(data);
end;

% for l= 1:size(pdfEst,2)
%     st = 1;
%     ed = 1;
%     for k=1:size(pdfEst,1)
% %         pdfEst(k,:) = sum( normpdf(data , psfSamplePoints(k),gaussWidth))/n;
%         while (st<n) && (data(st,l)<psfSamplePoints(k)-gaussWidth(l)*4)
%             st=st+1;
%         end;
%         while (ed<n) && (data(ed,l)<psfSamplePoints(k)+gaussWidth(l)*4)
%             ed=ed+1;
%         end;
%         pdfEst(k,l) = sum( normpdf(data(st:ed,l) , psfSamplePoints(k),gaussWidth(l)));
%     end;
% end;

% minD = data(1,:);
% maxD = data(end,:);maxD = maxD+2*eps*maxD;

nSmpls = length(psfSamplePoints);
if length(gaussWidth)>1
    gausWdiff = (nSmpls>10) & [true; abs(diff(gaussWidth(:)))>eps*2*reshape(gaussWidth(1:end-1),[],1)];
else
    gausWdiff = (nSmpls>10);
end;
pdfEst = zeros(length(psfSamplePoints),size(data,2));
doAdj = false;
origSmpls = psfSamplePoints(:);

for l= 1:size(pdfEst,2)
    if gausWdiff(l) % otherwize a speed gain cannot be reached.
        dsmplp = diff(origSmpls)<= .1000001 * gaussWidth(l);
        interpPoints =[false; dsmplp(1:end-1) & dsmplp(2:end); false];
        doAdj = any(interpPoints);
        if doAdj
            grpStEd = diff(interpPoints);
            STindx = find(grpStEd==1);
            EDindx = find(grpStEd==-1)+1;
            psfGroupLens = origSmpls(EDindx) - origSmpls(STindx);
            addSmpls = find( psfGroupLens > .2*gaussWidth(l) );
            extrSmpls = [];
            for k = addSmpls'
                nstep = ceil(psfGroupLens(k)/(.2*gaussWidth(l)));
                step = psfGroupLens(k)/nstep;
                extrSmpls = [extrSmpls origSmpls(STindx(k))+(1:nstep-1)*step];
            end;
            intpSamplePoints = origSmpls(interpPoints);
            [psfSamplePoints,origPSForder] = sort([origSmpls(~interpPoints)' extrSmpls]);
            origPSFPoints = logical([ones(sum(~interpPoints),1);zeros(length(extrSmpls),1)]);
            origPSFPoints = origPSFPoints(origPSForder);
            nSmpls = length(psfSamplePoints);
        else
            psfSamplePoints = origSmpls'; % make row vector.
        end;
        
    end;
    st = 1;
    ed = 1;
    edgs = max(data(1,l),psfSamplePoints(1)-gaussWidth(l)*4):2*gaussWidth(l):min(data(end,l)*(1+20*eps)+2*gaussWidth(l),psfSamplePoints(end)+gaussWidth(l)*5);
    nInBins = histc(data(:,l),edgs);
    cumIndx = cumsum([1;nInBins]);
    pdfEstLp = zeros(nSmpls,1);
    dataLp = data(:,l);
    for k=find(nInBins)'
%         pdfEst(k,:) = sum( normpdf(data , psfSamplePoints(k),gaussWidth))/n;
        while (st<nSmpls) && (dataLp(cumIndx(k))>psfSamplePoints(st)+gaussWidth(l)*4.5)
            st=st+1;
        end;
        ed= max(ed,st);
        st = max(1,st-1);
        while (ed<nSmpls) && (dataLp(cumIndx(k+1)-1)>psfSamplePoints(ed)-gaussWidth(l)*4.5)
            ed=ed+1;
        end;
        XtoNormpdf = dataLp(cumIndx(k):cumIndx(k+1)-1,ones(ed-st+1,1));
        MUtoNormpdf = ones(nInBins(k),1)*psfSamplePoints(st:ed);
        nrmpdfToSum = normpdf(XtoNormpdf , MUtoNormpdf, gaussWidth(l));
        if doWeight
            pdfEstLp(st:ed) = pdfEstLp(st:ed) + ( weights(cumIndx(k):cumIndx(k+1)-1,l)' * nrmpdfToSum )';
        else
            pdfEstLp(st:ed) = pdfEstLp(st:ed) + sum( nrmpdfToSum,1)';
        end;
    end;
    if doAdj
        pdfEst(~interpPoints,l) = pdfEstLp(origPSFPoints);
        pdfEst(interpPoints,l) = spline(psfSamplePoints,pdfEstLp,intpSamplePoints);
    else
        pdfEst(:,l)= pdfEstLp;
    end;
end;
if ~doWeight
    pdfEst = pdfEst/n;
end;
