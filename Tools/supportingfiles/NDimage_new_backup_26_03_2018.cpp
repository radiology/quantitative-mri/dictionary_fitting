#ifndef NDIMAGE
#define NDIMAGE
/* This file provides a NDimage
 *
 *  class NDimage: image type, templated over data type and dimensionality. Can be sampled by a sampler e.g. a bspline sampler.
 *
 *  See  function 'self_test_mex' for example usage.
 *
 * Created by Dirk Poot, Erasmus MC
 */


#include "math.h"
#include <vector>
#include <algorithm>

#include "bspline_filter.cpp"
#include "RecursiveSamplingImplementation.hxx"

/* combineVecfun : helper class for the sampling of vector images
 *
 * INPUTS:
 *
 */
template < typename ImagePointerType, typename outputPointerType, typename WeightsPointerType> class combineVecfun {
public:
    static inline void sample( outputPointerType out, IndexType vlen, ImagePointerType image, WeightsPointerType weights, int_t * stepsTable, int nsteps ) {
        // dummy baseline/reference implementation, stressing cache (operating from too many lines 'simultaneously')
        // and reading weights and stepsTable too often (vlen * nsteps reads from image, stepsTable and weights )
        // probably should 'autocast' to use vec (if possible) and process all of a limitted (fixed) number of stepsTable at a time
        // (e.g. 4 'stepsTable', and good vec_vlen )
        typedef typename iterator_traits< outputPointerType >::value_type procValueType;
        for ( int k = 0 ; k < vlen ; ++k ) {
            procValueType temp = 0;
            for (int step = 0 ; step < nsteps ; ++step ) {
                temp += image[ stepsTable[ step ] + k ] * weights[ step ];
            }
            out[k]= temp;
        }
    }
    
    static inline void sampleAdjoint( outputPointerType out, IndexType vlen, ImagePointerType image, WeightsPointerType weights, const int_t * stepsTable, int nsteps ) {
        // dummy baseline/reference implementation, stressing cache (operating from too many lines 'simultaneously')
        // and reading weights and stepsTable too often (vlen * nsteps reads from image, stepsTable and weights )
        // probably should 'autocast' to use vec (if possible) and process all of a limitted (fixed) number of stepsTable at a time
        // (e.g. 4 'stepsTable', and good vec_vlen )
        typedef typename iterator_traits< outputPointerType >::value_type procValueType;
        for ( int k = 0 ; k < vlen ; ++k ) {
            procValueType temp = out[k];
            for (int step = 0 ; step < nsteps ; ++step ) {
                image[ stepsTable[ step ] + k ] += temp * weights[ step ];
            }
        }
    }
};




template <int SpaceDimension, typename ImagePointerType> class NDimage;
template <int SpaceDimension, typename ImagePointerType> class NDvecImage;

template <typename ImagePointerType> class NDimage_base {
private:
    
    const int ndims;
    const int subType;
    const IndexType vlen;
    int mirror_boundary;
protected:
    ImagePointerType origin;
    NDimage_base( ImagePointerType origin_, int ndims_, int subType_, IndexType vlen_=1) : ndims(ndims_), subType(subType_), vlen(vlen_) {
        if ( (ndims_<1) || (ndims_>6) ) {
            mexErrMsgTxt("Image dimension should be in [1 ... 6].");
        }
        if ((subType!=0) && (subType!=1)) {
            mexErrMsgTxt("Only NDimage (subType==0) and NDvecImage (subType==1) are known to NDimage_base.");
        }
        //ndims = ndims_;
        origin = origin_;
        mirror_boundary = 0; // default: zero boundary
    }
public:
    int Ndims() { return ndims; };
    IndexType Vlen() { return vlen; };
    
    int getBoundarycondition() { return mirror_boundary; };
    void setBoundarycondition( int mirror_boundary_ ) { mirror_boundary = mirror_boundary_;};
    
    template< typename OutputValueType, typename locationType, typename samplerType> OutputValueType sample( locationType * point, samplerType & sampler) {
        // Sampling function. Calls child sampling functions.
        // Cannot be done with virtual overloading due to templates, but also I would like to (be able to) avoid the virtual call
        // by templating iteration routine over the actual subclass of the correct dimension.
        if (subType==0) {
            if (ndims==1) {
                return ((NDimage<1,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==2) {
                return ((NDimage<2,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==3) {
                return ((NDimage<3,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==4) {
                return ((NDimage<4,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==5) {
                return ((NDimage<5,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==6) {
                return ((NDimage<6,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else {
                return 0; // cannot be reached, constructor forbids.
            }
        } else { // subType ==1
            mexErrMsgTxt("Vector image does not return scalar.");
            return 0;
        }
        
        
    }
    template< typename OutputPointerType, typename locationType, typename samplerType> void sample(OutputPointerType out,  locationType * point, samplerType & sampler) {
        // Sampling function. Calls child sampling functions.
        // Cannot be done with virtual overloading due to templates, but also I would like to (be able to) avoid the virtual call
        // by templating iteration routine over the actual subclass of the correct dimension.
        if (subType==0) {
            typedef typename iterator_traits< OutputPointerType >::value_type OutputValueType;
            if (ndims==1) {
                *out = ((NDimage<1,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==2) {
                *out = ((NDimage<2,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==3) {
                *out = ((NDimage<3,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==4) {
                *out = ((NDimage<4,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==5) {
                *out = ((NDimage<5,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==6) {
                *out = ((NDimage<6,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else {
                ; // cannot be reached, constructor forbids.
            }
        } else { // subType ==1
            if (ndims==1) {
                ((NDvecImage<1,ImagePointerType>*)this)->template sample<OutputPointerType>(out, point, sampler );
            } else if (ndims==2) {
                ((NDvecImage<2,ImagePointerType>*)this)->template sample<OutputPointerType>(out, point, sampler );
            } else if (ndims==3) {
                ((NDvecImage<3,ImagePointerType>*)this)->template sample<OutputPointerType>(out, point, sampler );
            } else if (ndims==4) {
                ((NDvecImage<4,ImagePointerType>*)this)->template sample<OutputPointerType>(out, point, sampler );
            } else if (ndims==5) {
                ((NDvecImage<5,ImagePointerType>*)this)->template sample<OutputPointerType>(out, point, sampler );
            } else if (ndims==6) {
                ((NDvecImage<6,ImagePointerType>*)this)->template sample<OutputPointerType>(out, point, sampler );
            } else {
                ; // cannot be reached, constructor forbids.
            }
        }
    }
    template< typename OutputPointerType, typename locationType, typename samplerType> void sampleAndDerivative(OutputPointerType out, OutputPointerType Dout,  locationType * point, samplerType & sampler) {
        // Sampling function. Calls child sampling functions.
        // Cannot be done with virtual overloading due to templates, but also I would like to (be able to) avoid the virtual call
        // by templating iteration routine over the actual subclass of the correct dimension.
        if (subType==0) {
            if (ndims==1) {
                ((NDimage<1,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==2) {
                ((NDimage<2,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==3) {
                ((NDimage<3,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==4) {
                ((NDimage<4,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==5) {
                ((NDimage<5,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==6) {
                ((NDimage<6,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else {
                ; // cannot be reached, constructor forbids.
            }
        } else { // subType ==1
            if (ndims==1) {
                ((NDvecImage<1,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==2) {
                ((NDvecImage<2,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==3) {
                ((NDvecImage<3,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==4) {
                ((NDvecImage<4,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==5) {
                ((NDvecImage<5,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==6) {
                ((NDvecImage<6,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else {
                ; // cannot be reached, constructor forbids.
            }
        }
    }
    template< typename OutputPointerType, typename locationType, typename samplerType> void sampleAdjoint(OutputPointerType out,  locationType * point, samplerType & sampler) {
        // Sampling function. Calls child sampling functions.
        // Cannot be done with virtual overloading due to templates, but also I would like to (be able to) avoid the virtual call
        // by templating iteration routine over the actual subclass of the correct dimension.
        if (subType==0) {
            //typedef typename iterator_traits< OutputPointerType >::value_type OutputValueType;
            if (ndims==1) {
                ((NDimage<1,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==2) {
                ((NDimage<2,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==3) {
                ((NDimage<3,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==4) {
                ((NDimage<4,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==5) {
                ((NDimage<5,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==6) {
                ((NDimage<6,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else {
                ; // cannot be reached, constructor forbids.
            }
        } else { // subType ==1
            if (ndims==1) {
                ((NDvecImage<1,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==2) {
                ((NDvecImage<2,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==3) {
                ((NDvecImage<3,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==4) {
                ((NDvecImage<4,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==5) {
                ((NDvecImage<5,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==6) {
                ((NDvecImage<6,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else {
                ; // cannot be reached, constructor forbids.
            }
        }
    }
};

/* class NDimage
 * Implements the scalar valued ND image
 */
using std::iterator_traits;
template <int SpaceDimension, typename ImagePointerType> class NDimage : public NDimage_base<ImagePointerType> {
private:
    typedef typename iterator_traits< ImagePointerType >::value_type ImageValueType;
    typedef ImageValueType WeightsValueType;
    std::vector<WeightsValueType> filtcoeffs;
    std::vector<WeightsValueType> Dfiltcoeffs;
    std::vector<OffsetValueType> stepsTable;
    typedef WeightsValueType * WeightsPointerType;
protected:
    IndexType size[ SpaceDimension ];
    int_t stride[ SpaceDimension ];
public:
    NDimage( ImagePointerType origin_, const IndexType * size_, const int_t * stride_, const int maxFilterLength)
    : NDimage_base<ImagePointerType>(origin_, SpaceDimension, 0),
            filtcoeffs( SpaceDimension*maxFilterLength ),
            Dfiltcoeffs( SpaceDimension*maxFilterLength ),
            stepsTable( SpaceDimension*maxFilterLength )            
    {
        for (int i=0;i<SpaceDimension;i++) {
            size[i]=size_[i];
        }
        for (int i=0;i<SpaceDimension;i++) {
            stride[i]=stride_[i];
        }
    };
    
    template< typename OutputValueType, typename locationType, typename samplerType> OutputValueType sample( locationType * point, samplerType & sampler) {
        ImagePointerType sampleorigin = this->origin;
        bool allinrange = true;
        IndexType loc[SpaceDimension];
        locationType samplerdelay = sampler.delay();
        int length = sampler.length();
        int startEnds[2*SpaceDimension];
        for (int i=0;i<SpaceDimension;i++) {
            locationType pti = point[i] - samplerdelay ;
            loc[i] = (IndexType) floor(pti);
            pti -= loc[i];
            sampler.get_coefficients( &filtcoeffs[length *i], pti);
            allinrange &= (loc[i]>=0) && (loc[i] + length  <= size[i]);
            sampleorigin += loc[i] * stride[i];
        }
        if (!allinrange) {
            if( this->getBoundarycondition()==0 ) {
                // zero boundary conditions
                for (int i=0;i<SpaceDimension;i++) {
                    using std::min;
                    using std::max;
                    
                    int newstart = (int) max( (IndexType) 0, -loc[i]);
                    int newstop  = (int) min( (IndexType) length , size[i]-loc[i] ) ;
                    if (newstop<=newstart) {
                        return 0;
                    }
                    sampleorigin += newstart * stride[i];
                    startEnds[i*2  ] = length *i + newstart;
                    startEnds[i*2+1] = length *i + newstop;
                }
                return RecursiveSamplingImplementation_GetSample<SpaceDimension, -1 , ImagePointerType, OutputValueType, WeightsPointerType, 0>
                        ::GetSample( sampleorigin, stride, &filtcoeffs[0] , startEnds );
            }
            else {
                // mirror boundary conditions
                sampleorigin = this->origin;
                for (int i=0;i<SpaceDimension;i++) {
                    
                    for (int j=0;j<length;j++) {
                        int tmp = j+loc[i];
                        
                        while ((tmp < 0)||(tmp >= size[i] )){
                            if (tmp < 0) {
                                tmp = -1 - tmp;
                            } else if(tmp >= size[i] ) {
                                tmp = 2*size[i]-1-tmp;
                            }
                        }
                        
                        stepsTable[j+ length*i] = tmp * stride[i];
                    }
                }
                if (samplerType::fixedlength==-1) {
                    // only fill startEnds when needed because sampler has no compile time known length.
                    for (int i=0;i<SpaceDimension;i++) {
                        startEnds[i*2  ] = length *i ;
                        startEnds[i*2+1] = length *(i+1) ;
                    }
                }
                return RecursiveSamplingImplementation_GetSample<SpaceDimension, samplerType::fixedlength , ImagePointerType, OutputValueType, WeightsPointerType, USE_STEPS>
                        ::GetSample( sampleorigin, &stepsTable[0], &filtcoeffs[0] , startEnds );
            }
        } else {
            if (samplerType::fixedlength==-1) {
                // only fill startEnds when needed because sampler has no compile time known length.
                for (int i=0;i<SpaceDimension;i++) {
                    startEnds[i*2  ] = length *i ;
                    startEnds[i*2+1] = length *(i+1) ;
                }
            }
            return RecursiveSamplingImplementation_GetSample<SpaceDimension, samplerType::fixedlength , ImagePointerType, OutputValueType, WeightsPointerType, 0>
                    ::GetSample( sampleorigin, stride, &filtcoeffs[0], startEnds);
        }
    }
    
    /*  // Transform list of points:
     * template< typename OutputPointerType, typename locationType, typename samplerType> void sample(OutputPointerType out,  locationType * point, int npoints, samplerType & sampler) {
     * IndexType loc[SpaceDimension];
     * locationType samplerdelay = sampler.delay();
     * int length = sampler.length();
     * int startEnds[2*SpaceDimension];
     * int * filtStartIdxPointer = &filtStartidx[0];
     * WeightsPointerType filtCoefficientsPointer = &filtcoeffs[0];
     * sampler.get_indexAndCoefficients( filtStartIdxPointer, filtCoefficientsPointer, point, npoints*SpaceDimension);
     *
     * for (int pointidx = 0 ; pointidx < npoints ; ++pointidx , filtStartIdxPointer+=SpaceDimension, filtCoefficientsPointer+= SpaceDimension*length ){
     * bool allinrange = true;
     * ImagePointerType sampleorigin = this->origin;
     *
     * for ( int dim = 0; dim< SpaceDimension; ++dim ){
     * allinrange &= (filtStartIdxPointer[ dim ]>=0) && (filtStartIdxPointer[ dim ] + length  <= size[ dim ]);
     * sampleorigin += filtStartIdxPointer[ dim ] * stride[ dim ];
     * }
     * if (!allinrange) {
     * for (int dim = 0; dim < SpaceDimension; ++dim ) {
     * using std::min;
     * using std::max;
     *
     * int newstart = (int) max( (IndexType) 0, -filtStartIdxPointer[ dim ]);
     * int newstop  = (int) min( (IndexType) length , size[ dim ] - filtStartIdxPointer[ dim ] ) ;
     * if (newstop<=newstart) {
     * return 0;
     * }
     * sampleorigin += newstart * stride[ dim ];
     * startEnds[ dim * 2     ] = dim * length + newstart;
     * startEnds[ dim * 2 + 1 ] = dim * length + newstop;
     * }
     * return RecursiveSamplingImplementation_GetSample< SpaceDimension,                       -1 , ImagePointerType, OutputValueType, WeightsPointerType, 0>
     * ::GetSample( sampleorigin, stride, filtCoefficientsPointer, startEnds );
     * } else {
     * if (samplerType::fixedlength==-1) {
     * // only fill startEnds when needed because sampler has no compile time known length.
     * for (int dim = 0; dim < SpaceDimension; ++dim ) {
     * startEnds[ dim * 2     ] =  dim   * length ;
     * startEnds[ dim * 2 + 1 ] = (dim+1)* length ;
     * }
     * }
     * return RecursiveSamplingImplementation_GetSample< SpaceDimension, samplerType::fixedlength , ImagePointerType, OutputValueType, WeightsPointerType, 0>
     * ::GetSample( sampleorigin, stride, filtCoefficientsPointer, startEnds);
     * }
     * }
     * } */
    
    template< typename OutputPointerType, typename locationType, typename samplerType> void sampleAndDerivative( OutputPointerType out, OutputPointerType Dout, locationType * point, samplerType & sampler) {
        ImagePointerType sampleorigin = this->origin;
        bool allinrange = true;
        IndexType loc[SpaceDimension];
        locationType samplerdelay = sampler.delay();
        int length = sampler.length();
        int startEnds[2*SpaceDimension];
        for (int i=0;i<SpaceDimension;i++) {
            locationType pti = point[i] - samplerdelay ;
            loc[i] = (IndexType) floor(pti);
            pti -= loc[i];
            sampler.get_coefficients( &filtcoeffs[length *i], pti);
            sampler.get_coefficientsDerivative( &Dfiltcoeffs[length *i], pti, 1);
            allinrange &= (loc[i]>=0) && (loc[i] + length  <= size[i]);
            sampleorigin += loc[i] * stride[i];
        }
        typedef typename iterator_traits< OutputPointerType >::value_type OutputValueType;
        typedef OutputValueType * tempOutputPointerType;
        OutputValueType temp_out[ SpaceDimension +1 ];
        if (!allinrange) {
            if( this->getBoundarycondition()==0 ) {
                // zero boundary conditions
                for (int i=0;i<SpaceDimension;i++) {
                    using std::min;
                    using std::max;
                    
                    int newstart = (int) max( (IndexType) 0, -loc[i]);
                    int newstop  = (int) min( (IndexType) length , size[i]-loc[i] ) ;
                    if (newstop<=newstart) {
                        return ;
                    }
                    sampleorigin += newstart * stride[i];
                    startEnds[i*2  ] = length *i + newstart;
                    startEnds[i*2+1] = length *i + newstop;
                }
                RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, -1 , ImagePointerType, tempOutputPointerType, WeightsPointerType, 0>
                        ::GetSampleAndDerivative( temp_out, sampleorigin, stride, &filtcoeffs[0], &Dfiltcoeffs[0], startEnds  );
            }
            else {
                // mirror boundary conditions
                sampleorigin = this->origin;
                
                for (int i=0;i<SpaceDimension;i++) {
                    
                    for (int j=0;j<length;j++) {
                        int tmp = j+loc[i];
                        while ((tmp < 0)||(tmp >= size[i] )){
                            if (tmp < 0) {
                                tmp = -1 - tmp;
                            } else if(tmp >= size[i] ) {
                                tmp = 2*size[i]-1-tmp;
                            }
                        }
                        
                        stepsTable[j+ length*i] = tmp*stride[i];
                    }
                }
                if (samplerType::fixedlength==-1) {
                    // only fill startEnds when needed because sampler has no compile time known length.
                    for (int i=0;i<SpaceDimension;i++) {
                        startEnds[i*2  ] = length *i ;
                        startEnds[i*2+1] = length *(i+1) ;
                    }
                }
                RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, samplerType::fixedlength, ImagePointerType, tempOutputPointerType, WeightsPointerType, USE_STEPS>
                        ::GetSampleAndDerivative( temp_out, sampleorigin, &stepsTable[0], &filtcoeffs[0], &Dfiltcoeffs[0], startEnds  );
            }
        }
        else {
            if (samplerType::fixedlength==-1) {
                // only fill startEnds when needed because sampler has no compile time known length.
                for (int i=0;i<SpaceDimension;i++) {
                    startEnds[i*2  ] = length *i ;
                    startEnds[i*2+1] = length *(i+1) ;
                }
            }
            RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, samplerType::fixedlength, ImagePointerType, tempOutputPointerType, WeightsPointerType, 0>
                    ::GetSampleAndDerivative( temp_out, sampleorigin, stride, &filtcoeffs[0], &Dfiltcoeffs[0], startEnds );
        }
        *out = temp_out[0];
        for ( int d = 0 ; d < SpaceDimension ; ++d ) {
            Dout[ d ] = temp_out[ d+1 ];
        }
    }
    // should add sample and derivative function, where
    // a recursive sampleAndDerivative method of RecursiveSamplingImplementation_GetSample is called
    // cost of the ND sampleAndDerivative function should be
    //  2 * SpaceDimension * filterLength (coefficients and derivative coefficients of the samplers)
    // + 2 * filterLength^SpaceDimension  ( value and derivative in 1st dimension, coefficient is actually lower than 2 since the image values need to be loaded only once)
    // +  O( filterLength^(SpaceDimension-1) )  (value roundup and derivatives in other dimensions.)
    template< typename InputPointerType, typename locationType, typename samplerType> void sampleAdjoint( InputPointerType out,  locationType * point, samplerType & sampler) {
        ImagePointerType sampleorigin = this->origin;
        bool allinrange = true;
        IndexType loc[SpaceDimension];
        locationType samplerdelay = sampler.delay();
        int length = sampler.length();
        int startEnds[2*SpaceDimension];
        int tmp;
        for (int i=0;i<SpaceDimension;i++) {
            locationType pti = point[i] - samplerdelay ;
            loc[i] = (IndexType) floor(pti);
            pti -= loc[i];
            sampler.get_coefficients( &filtcoeffs[length *i], pti);
            allinrange &= (loc[i]>=0) && (loc[i] + length  <= size[i]);
            sampleorigin += loc[i] * stride[i];
        }
        typedef typename iterator_traits< InputPointerType >::value_type InputValueType;
       if (!allinrange) {
            if( this->getBoundarycondition()==0 ) {
                // zero boundary conditions
                for (int i=0;i<SpaceDimension;i++) {
                    using std::min;
                    using std::max;
                    
                    int newstart = (int) max( (IndexType) 0, -loc[i]);
                    int newstop  = (int) min( (IndexType) length , size[i]-loc[i] ) ;
                    if (newstop<=newstart) {
                        return ;
                    }
                    sampleorigin += newstart * stride[i];
                    startEnds[i*2  ] = length *i + newstart;
                    startEnds[i*2+1] = length *i + newstop;
                }
                RecursiveSamplingImplementation_MultiplyJacobianWithValue<SpaceDimension, -1 , ImagePointerType, InputValueType, WeightsPointerType, 0 >
                        ::MultiplyJacobianWithValue(  sampleorigin, stride, &filtcoeffs[0], startEnds, *out);
            }
            else {
                // mirror boundary conditions
                sampleorigin = this->origin;
                
                for (int i=0;i<SpaceDimension;i++) {
                    using std::min;
                    using std::max;
                    
                    for (int j=0;j<length;j++) {
                        tmp = j+loc[i];
                        while ((tmp < 0)||(tmp >= size[i] )){
                            if (tmp < 0) {
                                tmp = -1 - tmp;
                            } else if(tmp >= size[i] ) {
                                tmp = 2*size[i]-1-tmp;
                            }
                        }
                            
                            stepsTable[j+ length*i] = tmp*stride[i];
                    }
                }
                
                if (samplerType::fixedlength==-1) {
                    // only fill startEnds when needed because sampler has no compile time known length.
                    for (int i=0;i<SpaceDimension;i++) {
                        startEnds[i*2  ] = length *i ;
                        startEnds[i*2+1] = length *(i+1) ;
                    }
                }
                RecursiveSamplingImplementation_MultiplyJacobianWithValue<SpaceDimension, samplerType::fixedlength, ImagePointerType, InputValueType, WeightsPointerType, USE_STEPS >
                        ::MultiplyJacobianWithValue(  sampleorigin, &stepsTable[0], &filtcoeffs[0], startEnds, *out);
            }
        }
        else {
            if (samplerType::fixedlength==-1) {
                // only fill startEnds when needed because sampler has no compile time known length.
                for (int i=0;i<SpaceDimension;i++) {
                    startEnds[i*2  ] = length *i ;
                    startEnds[i*2+1] = length *(i+1) ;
                }
            }
            RecursiveSamplingImplementation_MultiplyJacobianWithValue<SpaceDimension, samplerType::fixedlength , ImagePointerType, InputValueType, WeightsPointerType, 0>
                    ::MultiplyJacobianWithValue(  sampleorigin, stride , &filtcoeffs[0] , startEnds, *out);
        }
    }
};

template <class T> inline T pow_template(const T base, unsigned const exponent)
{
    // (parentheses not required in next line)
    return (exponent == 0) ? 1 : (base * pow_template(base, exponent-1));
};

/* class NDvecImage
 * Implements the vector valued ND image
 */

template <int SpaceDimension, typename ImagePointerType> class NDvecImage : public NDimage_base<ImagePointerType> {
private:
        typedef typename std::iterator_traits< ImagePointerType >::value_type ImageValueType;
        typedef ImageValueType WeightsValueType;
        typedef WeightsValueType * WeightsPointerType;
        std::vector<WeightsValueType> filtcoeffs;
        std::vector<WeightsValueType> Dfiltcoeffs;
        std::vector<OffsetValueType> stepsTable;
        std::vector<int_t> nzji;
        std::vector<WeightsValueType> weights;
protected:
        IndexType size[ SpaceDimension ];
        int_t stride[ SpaceDimension ];
public:
        NDvecImage( ImagePointerType origin_, const IndexType vlen_, const IndexType * size_, const int_t * stride_, const int maxFilterLength)
        : NDimage_base<ImagePointerType>(origin_, SpaceDimension, 1, vlen_),
                filtcoeffs( SpaceDimension*maxFilterLength ) ,
                Dfiltcoeffs( SpaceDimension*maxFilterLength ) ,
                stepsTable( SpaceDimension*maxFilterLength ) ,
                nzji( pow_template( maxFilterLength, SpaceDimension) ) ,
                weights( pow_template( maxFilterLength , SpaceDimension) )  {
            for (int i=0;i<SpaceDimension;i++) {
                size[i]=size_[i];
            }
            for (int i=0;i<SpaceDimension;i++) {
                stride[i]=stride_[i];
            }
        };
        
        template< typename OutputPointerType, typename locationType, typename samplerType> void sample(OutputPointerType out,  locationType * point, samplerType & sampler) {
            ImagePointerType sampleorigin = this->origin;
            bool allinrange = true;
            IndexType loc[SpaceDimension];
            locationType samplerdelay = sampler.delay();
            int length = sampler.length();
            int startEnds[2*SpaceDimension];
            for (int i=0;i<SpaceDimension;i++) {
                locationType pti = point[i] - samplerdelay ;
                loc[i] = (IndexType) floor(pti);
                pti -= loc[i];
                sampler.get_coefficients( &filtcoeffs[length *i], pti);
                allinrange &= (loc[i]>=0) && (loc[i] + length  <= size[i]);
                sampleorigin += loc[i] * stride[i];
            };
            
            WeightsPointerType weightsPointer =&weights[0];
            int_t * nzjiPointer = &nzji[0];
            if (!allinrange) {
                // TODO:  If mirror boundary conditions, force different interpretation of stride by specifying gridOffsetTable0=USE_STEPS in RecursiveSamplingImplementation calls.
                // Also update code here to fill such steps table. Call as the allinrange call because with mirror boundary we have all samples.
                // Only difference: gridOffsetTable0=USE_STEPS (and the associated parameter interpretation differences)
                if( this->getBoundarycondition()==0 ) {
                    // zero boundary conditions
                    for (int i=0;i<SpaceDimension;i++) {
                        using std::min;
                        using std::max;
                        
                        int newstart = (int) max( (IndexType) 0, -loc[i]);
                        int newstop  = (int) min( (IndexType) length , size[i]-loc[i] ) ;
                        if (newstop<=newstart) {
                            for (int d= 0 ; d< this->Vlen(); ++d ){
                                out[d]=0;
                            }
                            return ;
                        }
                        sampleorigin += newstart * stride[i];
                        startEnds[i*2  ] = length *i + newstart;
                        startEnds[i*2+1] = length *i + newstop;
                    }
                    RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, -1, 0 >
                            ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &filtcoeffs[0], stride, startEnds );
                }
                else {
                    // mirror boundary conditions
                    sampleorigin = this->origin;
                    
                    for (int i=0;i<SpaceDimension;i++) {
                        
                        for (int j=0;j<length;j++) {
                            int tmp = j+loc[i];
                            while ((tmp < 0)||(tmp >= size[i] )){
                                if (tmp < 0) {
                                    tmp = -1 - tmp;
                                } else if(tmp >= size[i] ) {
                                    tmp = 2*size[i]-1-tmp;
                                }
                            }
                            
                            stepsTable[j+ length*i] = tmp*stride[i];
                        }
                    }
                    if (samplerType::fixedlength==-1) {
                        // only fill startEnds when needed because sampler has no compile time known length.
                        for (int i=0;i<SpaceDimension;i++) {
                            startEnds[i*2  ] = length *i ;
                            startEnds[i*2+1] = length *(i+1) ;
                        }
                    }
                    RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, samplerType::fixedlength, USE_STEPS >
                            ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &filtcoeffs[0], &stepsTable[0], startEnds );
                }
            }
            else {
                if (samplerType::fixedlength==-1) {
                    // only fill startEnds when needed because sampler has no compile time known length.
                    for (int i=0;i<SpaceDimension;i++) {
                        startEnds[i*2  ] = length *i ;
                        startEnds[i*2+1] = length *(i+1) ;
                    }
                }
                RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, samplerType::fixedlength, 0 >
                        ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &filtcoeffs[0], stride, startEnds );
            }
            WeightsPointerType weightsPointerOrig =&weights[0];
            int_t * nzjiPointerOrig = &nzji[0];
            int numweights = (int) (weightsPointer  - weightsPointerOrig);
            combineVecfun< ImagePointerType, OutputPointerType , WeightsPointerType>
                    ::sample( out, this->Vlen(), sampleorigin, weightsPointerOrig, nzjiPointerOrig, numweights );
        }        
        
        
        template< typename OutputPointerType, typename locationType, typename samplerType> void sampleAndDerivative( OutputPointerType out, OutputPointerType Dout, locationType * point, samplerType & sampler) {
            ImagePointerType sampleorigin = this->origin;
            bool allinrange = true;
            IndexType loc[SpaceDimension];
            locationType samplerdelay = sampler.delay();
            int length = sampler.length();
            int startEnds[2*SpaceDimension];
            for (int i=0;i<SpaceDimension;i++) {
                locationType pti = point[i] - samplerdelay ;
                loc[i] = (IndexType) floor(pti);
                pti -= loc[i];
                sampler.get_coefficients( &filtcoeffs[length *i], pti);
                sampler.get_coefficientsDerivative( &Dfiltcoeffs[length *i], pti, 1);
                allinrange &= (loc[i]>=0) && (loc[i] + length  <= size[i]);
                sampleorigin += loc[i] * stride[i];
            }
            typedef typename iterator_traits< OutputPointerType >::value_type OutputValueType;
            typedef OutputValueType * tempOutputPointerType;
            OutputValueType temp_out[ (SpaceDimension +1 ) ];
            if (!allinrange) {
                if( this->getBoundarycondition()==0 ) {
                    // zero boundary conditions
                    for (int i=0;i<SpaceDimension;i++) {
                        using std::min;
                        using std::max;
                        
                        int newstart = (int) max( (IndexType) 0, -loc[i]);
                        int newstop  = (int) min( (IndexType) length , size[i]-loc[i] ) ;
                        if (newstop<=newstart) {
                            for (int d= 0 ; d< this->Vlen(); ++d ){
                                out[d]=0;
                            }
                            return ;
                        }
                        sampleorigin += newstart * stride[i];
                        startEnds[i*2  ] = length *i + newstart;
                        startEnds[i*2+1] = length *i + newstop;
                    }
                    for ( int vec_idx = 0 ; vec_idx < this->Vlen(); vec_idx++ ) { // TODO: this loop is somewhat inefficient; probably it would be faster when moved into RecursiveSamplingImplementation_GetSampleAndDerivative
                        // Else could at least do some vectorization (using vec) on the in/output classes.
                        RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, -1 , ImagePointerType, tempOutputPointerType, WeightsPointerType, 0>
                                ::GetSampleAndDerivative( temp_out, sampleorigin+vec_idx, stride, &filtcoeffs[0], &Dfiltcoeffs[0], startEnds  );
                        out[vec_idx] = temp_out[0];
                        for ( int d = 0 ; d < SpaceDimension ; ++d ) {
                            Dout[ vec_idx + d *this->Vlen() ] = temp_out[ d+1 ];
                        }
                    }
                }
                else {
                    // mirror boundary conditions
                    sampleorigin = this->origin;
                    
                    for (int i=0;i<SpaceDimension;i++) {
                        
                        for (int j=0;j<length;j++) {
                            int tmp = j+loc[i];
                            while ((tmp < 0)||(tmp >= size[i] )){
                                if (tmp < 0) {
                                    tmp = -1 - tmp;
                                } else if(tmp >= size[i] ) {
                                    tmp = 2*size[i]-1-tmp;
                                }
                            }
                            
                            stepsTable[j+ length*i] = tmp*stride[i];
                        }
                    }
                    if (samplerType::fixedlength==-1) {
                        // only fill startEnds when needed because sampler has no compile time known length.
                        for (int i=0;i<SpaceDimension;i++) {
                            startEnds[i*2  ] = length *i ;
                            startEnds[i*2+1] = length *(i+1) ;
                        }
                    }
                    
                    for ( int vec_idx = 0 ; vec_idx < this->Vlen(); vec_idx++ ) { // TODO: this loop is somewhat inefficient; probably it would be faster when moved into RecursiveSamplingImplementation_GetSampleAndDerivative
                        // Else could at least do some vectorization (using vec) on the in/output classes.
                        RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, samplerType::fixedlength, ImagePointerType, tempOutputPointerType, WeightsPointerType, USE_STEPS>
                                ::GetSampleAndDerivative( temp_out, sampleorigin+vec_idx, &stepsTable[0], &filtcoeffs[0], &Dfiltcoeffs[0], startEnds );
                        out[vec_idx] = temp_out[0];
                        for ( int d = 0 ; d < SpaceDimension ; ++d ) {
                            Dout[ vec_idx + d *this->Vlen() ] = temp_out[ d+1 ];
                        }
                    }
                }
            }
            else {
                if (samplerType::fixedlength==-1) {
                    // only fill startEnds when needed because sampler has no compile time known length.
                    for (int i=0;i<SpaceDimension;i++) {
                        startEnds[i*2  ] = length *i ;
                        startEnds[i*2+1] = length *(i+1) ;
                    }
                }
                for ( int vec_idx = 0 ; vec_idx < this->Vlen(); vec_idx++ ) {
                    RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, samplerType::fixedlength, ImagePointerType, tempOutputPointerType, WeightsPointerType, 0>
                            ::GetSampleAndDerivative( temp_out, sampleorigin+vec_idx, stride, &filtcoeffs[0], &Dfiltcoeffs[0], startEnds );
                    out[vec_idx] = temp_out[0];
                    for ( int d = 0 ; d < SpaceDimension ; ++d ) {
                        Dout[ vec_idx + d *this->Vlen() ] = temp_out[ d+1 ];
                    }
                }
            }
        }
        
        // should add sample and derivative function, where
        // a recursive sampleAndDerivative method of RecursiveSamplingImplementation_GetSample is called
        // cost of the ND sampleAndDerivative function should be
        //  2 * SpaceDimension * filterLength (coefficients and derivative coefficients of the samplers)
        // + 2 * filterLength^SpaceDimension  ( value and derivative in 1st dimension, coefficient is actually lower than 2 since the image values need to be loaded only once)
        // +  O( filterLength^(SpaceDimension-1) )  (value roundup and derivatives in other dimensions.)
        template< typename OutputPointerType, typename locationType, typename samplerType> void sampleAdjoint(OutputPointerType out,  locationType * point, samplerType & sampler) {
            ImagePointerType sampleorigin = this->origin;
            bool allinrange = true;
            IndexType loc[SpaceDimension];
            locationType samplerdelay = sampler.delay();
            int length = sampler.length();
            int startEnds[2*SpaceDimension];
            for (int i=0;i<SpaceDimension;i++) {
                locationType pti = point[i] - samplerdelay ;
                loc[i] = (IndexType) floor(pti);
                pti -= loc[i];
                sampler.get_coefficients( &filtcoeffs[length *i], pti);
                allinrange &= (loc[i]>=0) && (loc[i] + length  <= size[i]);
                sampleorigin += loc[i] * stride[i];
            };
            
            WeightsPointerType weightsPointer =&weights[0];
            int_t * nzjiPointer = &nzji[0];
            if (!allinrange) {
                if( this->getBoundarycondition()==0 ) {                    
                    // zero boundary conditions
                    for (int i=0;i<SpaceDimension;i++) {
                        using std::min;
                        using std::max;
                        
                        int newstart = (int) max( (IndexType) 0, -loc[i]);
                        int newstop  = (int) min( (IndexType) length , size[i]-loc[i] ) ;
                        if (newstop<=newstart) {
                            return ;
                        }
                        sampleorigin += newstart * stride[i];
                        startEnds[i*2  ] = length *i + newstart;
                        startEnds[i*2+1] = length *i + newstop;
                    }
                    RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, -1, 0 >
                            ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &filtcoeffs[0], stride, startEnds );
                }
                else {
                    // mirror boundary conditions
                    sampleorigin = this->origin;
                    
                    for (int i=0;i<SpaceDimension;i++) {
                        
                        for (int j=0;j<length;j++) {
                            int tmp = j+loc[i];
                            
                            while ((tmp < 0)||(tmp >= size[i] )){
                                if (tmp < 0) {
                                    tmp = -1 - tmp;
                                } else if(tmp >= size[i] ) {
                                    tmp = 2*size[i]-1-tmp;
                                }
                            }
                            stepsTable[j+ length*i] = tmp*stride[i];
                        }
                    }
                    if (samplerType::fixedlength==-1) {
                        // only fill startEnds when needed because sampler has no compile time known length.
                        for (int i=0;i<SpaceDimension;i++) {
                            startEnds[i*2  ] = length *i ;
                            startEnds[i*2+1] = length *(i+1) ;
                        }
                    }
                    RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, samplerType::fixedlength, USE_STEPS >
                            ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &filtcoeffs[0], &stepsTable[0], startEnds );
                }
            }
            else {
                if (samplerType::fixedlength==-1) {
                    // only fill startEnds when needed because sampler has no compile time known length.
                    for (int i=0;i<SpaceDimension;i++) {
                        startEnds[i*2  ] = length *i ;
                        startEnds[i*2+1] = length *(i+1) ;
                    }
                }
                RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, samplerType::fixedlength, 0 >
                        ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &filtcoeffs[0], stride, startEnds );
            }
            WeightsPointerType weightsPointerOrig =&weights[0];
            int_t * nzjiPointerOrig = &nzji[0];
            int numweights = (int) (weightsPointer  - weightsPointerOrig);
            combineVecfun< ImagePointerType, OutputPointerType , WeightsPointerType>
                    ::sampleAdjoint( out, this->Vlen(), sampleorigin, weightsPointerOrig, nzjiPointerOrig, numweights );
        }
};

template <typename ImagePointerType> NDimage_base<ImagePointerType> * newNDimage( ImagePointerType origin_, const IndexType vlen_, const IndexType * size_, const int_t * stride_, int SpaceDimension, const int maxFilterLength) {
    if (vlen_==1) {
        if (SpaceDimension==1) {
            return new NDimage<1,ImagePointerType>(origin_, size_, stride_, maxFilterLength);
        } else if (SpaceDimension==2) {
            return new NDimage<2,ImagePointerType>(origin_, size_, stride_, maxFilterLength);
        } else if (SpaceDimension==3) {
            return new NDimage<3,ImagePointerType>(origin_, size_, stride_, maxFilterLength);
        } else if (SpaceDimension==4) {
            return new NDimage<4,ImagePointerType>(origin_, size_, stride_, maxFilterLength);
        } else if (SpaceDimension==5) {
            return new NDimage<5,ImagePointerType>(origin_, size_, stride_, maxFilterLength);
        } else if (SpaceDimension==6) {
            return new NDimage<6,ImagePointerType>(origin_, size_, stride_, maxFilterLength);
        } else {
            mexErrMsgTxt("Image dimension should be in [1 ... 6].");
            return NULL;
        }
    } else {
        if (SpaceDimension==1) {
            return new NDvecImage<1,ImagePointerType>(origin_, vlen_, size_, stride_, maxFilterLength);
        } else if (SpaceDimension==2) {
            return new NDvecImage<2,ImagePointerType>(origin_, vlen_, size_, stride_, maxFilterLength);
        } else if (SpaceDimension==3) {
            return new NDvecImage<3,ImagePointerType>(origin_, vlen_, size_, stride_, maxFilterLength);
        } else if (SpaceDimension==4) {
            return new NDvecImage<4,ImagePointerType>(origin_, vlen_, size_, stride_, maxFilterLength);
        } else if (SpaceDimension==5) {
            return new NDvecImage<5,ImagePointerType>(origin_, vlen_, size_, stride_, maxFilterLength);
        } else if (SpaceDimension==6) {
            return new NDvecImage<6,ImagePointerType>(origin_, vlen_, size_, stride_, maxFilterLength);
        } else {
            mexErrMsgTxt("Image dimension should be in [1 ... 6].");
            return NULL;
        }
    }    
};
template <typename ImagePointerType> NDimage_base<ImagePointerType> * newNDimage( ImagePointerType  origin_, const IndexType * size_, const int_t * stride_, int SpaceDimension, const int maxFilterLength) {
    return newNDimage(origin_, 1, size_, stride_, SpaceDimension, maxFilterLength);
}

#endif