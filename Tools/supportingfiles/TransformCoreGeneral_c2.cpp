#ifdef BUILDING_CORE // Code of routine starts at #else

/*
 * This TransformCoreGeneral_c2 is a MEX-file for MATLAB.
 * Created by Dirk Poot, Erasmus Medical Center 
 * Basic build with:    (add -g for debugging (add -O for optimizations while debugging); -v for verbose mode )
 * mex TransformCoreGeneral_c2.cpp -I".\Tools\supportingfiles\"
 
 * Rebuild from TransformCoreGeneral_c, now iterators, better design, more general transforms.
 * To compile a function test (call without arguments):
 *  mex TransformCoreGeneral_c.cpp -DBUILDTESTFUN_TRANSFORMCOREGENERAL

 * Advanced build, using SSE3 and open MP (openMP is not generated from MATLAB 2011b)
 * Windows:
 *   mex TransformCoreGeneral_c2.cpp -I".\Tools\supportingfiles\" -v CXXFLAGS="\$CXXFLAGS -fopenmp -mssse3 -mtune=core2" LDFLAGS="\$LDFLAGS -fopenmp"
 * Or on Linux:
 *   mex TransformCoreGeneral_c2.cpp -DUSEOMP -I"./" -I"./Tools/supportingfiles/" -v CXXFLAGS="\$CXXFLAGS -fopenmp -march=barcelona" LDFLAGS="\$LDFLAGS -fopenmp"
 *
 * Set  'OMP_NUM_THREADS' to the desired number of threads before (!)
 * calling TransformCoreGeneral_c2 (or any other OMP enabled mex file) 
 */

#ifndef ARTEMPLATES
#define ARTEMPLATES	// include AR templates only once.
/*
template <int nAR, typename int_t, typename readType, typename outType, typename ARtype> class mainPartFilterWithAR;
template <         typename int_t, typename readType, typename outType, typename ARtype> class mainPartFilterWithAR<-1, int_t, readType, outType, ARtype> {
public: static inline void fun( readType in , outType out, int_t start_i, int_t max_in, int_t last_i , int_t step_i, int nAR ) {
	typedef iterator_traits< tempType >::value_type outValueT;
	outValueT zero = (outValueT) 0.0;
	int_t i = start_i;
	for(; i != max_in; i+=step_i) {
        outValueT tmp = in[i];
        for (int_t j= nAR-1; j>0; j--) { // start with largest delay to break dependency chains.
             tmp -= temp[i-j] * AR[j];
        }
        temp[i] = tmp;
    }
	for(; i != last_i; i+=step_i) {
        outValueT tmp = zero;
        for (int_t j= nAR-1; j>0; j--) { // start with largest delay to break dependency chains.
             tmp -= temp[i-j] * AR[j];
        }
        temp[i] = tmp;
    }
} */


template <typename int_t, typename readType, typename tempType, typename outType, typename ARType  >
void readAndFilterWithAR(  readType in   , range< int_t > r_in, 
                           ARType  AR, int_t nAR,   // use * __restrict
						   tempType temp , range< int_t > r_temp,
                           outType  out  , range< int_t > r_out ) 
/* Read image line and prefilter with the specified AR model.
 * In MATLAB syntax:
 *   out = flip(filter(1,ar,flip(filter(1,ar,in))))
 *
 * Input:in(i) = / in[ i  ]   for  r_in.first<= i <= r_in.last
 *               \  0             for  i<r_in.first | i>r_in.last
 * temp[i] = in(i) - sum_{1<= j <nAR} temp[i-j]*AR[j]		 for r_temp.first <= i <= r_temp.last; zero otherwise
 * temp_out[i]  = temp[i] - sum_{1<= j <nAR} out[i+j]*AR[j]  for r_out.first  <= i <= r_temp.last
 * out[i] = temp_out[i]										 for r_out.first  <= i <= r_out.last
 * Temp may overlap with out and/or in, but only exact overlap (temp==out) or (temp==in) is allowed.
 * temp_out not explicitly constructed.
 */                             
{
    // out = filter(1,ar, x) = 
    // for k=1:numel(out); out(k)= -sum(out(k-1:-1:max(1,k-numel(ar)+1)).*ar(2:min(k,numel(ar))))/ar(1)+x(k)/ar(1);end;
    using std::max;
    using std::min;
	using std::iterator_traits;
	typedef typename iterator_traits< tempType >::value_type outValueT;

//	int_t itst = r_out.last;
//	for(; itst>=r_out.first ; itst--) {
//		out[itst] = 2;
//	}; return;

    int_t i_start = r_in.first;
	outValueT zero = (outValueT) 0.0;// = Numeric(outValueT).Zero();
    // clear temp (needed for reverse filter, and when nAR==0)
	if (( r_temp.first > r_out.first) || ( r_temp.first > r_in.first) || (r_temp.last<r_out.last) ) {
		ERROR("range of out and in should be contained in range of temp.");
	}
    int_t i = min( r_out.first, r_in.first ); // we wont need any elements prior to r_out.first (although they might be in temp)
    for(; i <= min(i_start-1,r_out.last) ; i++) {
        temp[i] = zero;
    }
    // lead in (not all of the AR filter can be used)   
    for(; i <= min( i_start + nAR-2, r_temp.last ) ; i++) {
		outValueT tmp = ((i<=r_in.last) ? in[i] : zero);
        for (int_t j= i-i_start;j >0; j--) { // start with largest delay to break dependency chains.
             tmp -=  temp[i-j] * AR[j];
        }
        temp[i] = tmp;
    }
    // main part forward filter:
    for(; i <= r_temp.last; i++) {
        //outValueT tmp = ((i<=r_in.last) ? in[i] : zero);
		register outValueT tmp = zero;
        for (int_t j= nAR-1; j>0; j--) { // start with largest delay to break dependency chains.
             tmp -= temp[i-j] * AR[j];
        }
		if (i<=r_in.last) { 
			register const outValueT tmp2 = in[i]; // separate temporary variable as GCC seems unable to compile it when I don't use this variable.
			//tmp += in[i];
			tmp += tmp2;
		}
        temp[i] = tmp;
    }
    if (nAR>0) {
		i--;
		if (i <= r_out.last)
			out[i] = temp[i];
        i--;
        // lead in -reverse- filter: 
        for(; i>= max(r_temp.last-nAR+2,r_out.first) ;i--) {
            outValueT tmp = zero;
            for (int_t j= r_temp.last-i;j >0; j--) {  // start with largest delay to break dependency chains.
                 tmp -=  (outValueT) temp[i+j] * AR[j];
            }
            temp[i] += tmp;
			if (i <= r_out.last)
				out[i] = temp[i];
        }
        // main part -reverse- filter:
        for(; i>=r_out.first ; i--) {
            outValueT tmp = zero;
            for (int_t j= nAR-1;j >0; j--) { // start with largest delay to break dependency chains.
                 tmp -= (outValueT) temp[i+j] * AR[j];
            }
            temp[i] += tmp;
			if (i <= r_out.last)
				out[i] = temp[i];
        }
	} else {
		// just copy to output:
		i--;
		for(; i >= r_out.first ; i--) {
			if (i <= r_out.last)
				out[i] = temp[i];
		}
	
    }   
}

/*
 The following function contains the main code of this mex file. It is compiled with 2 options (all 4 combinations):
 DERIVATIVE   : Also compute derivative.
 ADJOINT      : evaluate adjoint operation. 
                NOTE that in adjoint mode, the image is read from 'out' and stored into 'in'
				The adjoint mode is required by iterative reconstruction methods (like conjugated gradient).
				Unless you use such a method, you don't want to use the adjoint operation.
 
*/
#endif // end of ifdef ARTEMPLATES

#ifdef ADJOINT
    #define FILTERLINEWITHF_SPEC filterLineWithF_Specialized_adj
	#define FILTERLINEWITHF      filterLineWithF_adj
#else
    #define FILTERLINEWITHF_SPEC filterLineWithF_Specialized
	#define FILTERLINEWITHF      filterLineWithF
#endif

#define DERIVWRITEARGSDECL  , writeType dOut
#ifdef DERIVATIVE 
	#define DERIVWRITEARGS DERIVWRITEARGSDECL
	#define DERIVWRITEARGSs , dOut
#else
    #define DERIVWRITEARGS 
	#define DERIVWRITEARGSs
/*template <typename int_t,  typename readType, typename writeType, typename posType, typename filtType > 
class FILTERLINEWITHF_SPEC { 
	public: 
		static inline void fun( readType in, writeType out  DERIVWRITEARGS,
							  range< int_t > rng_o , range<int_t> rng_i,
							  posType pos, filtType F ) ;
		static inline void fun( readType in, writeType out  DERIVWRITEARGSDECL,
							  range< int_t > rng_o , range<int_t> rng_i,
							  posType pos, filtType F ) ;
};*/
#endif

template <typename int_t,  typename readType, typename writeType, typename posType, typename filtType > 
	//static inline void FILTERLINEWITHF_SPEC <int_t,  readType, writeType, posType, filtType >::fun /* 
	inline void FILTERLINEWITHF_SPEC // */
		( readType in, writeType out  DERIVWRITEARGS,
							  range< int_t > rng_o , range<int_t> rng_i,
							  posType pos, filtType F ) 
/* filters the elements provided by in with the filter F. 
NOTE: if doAdjoint==true: writer should accumulate the results (so it should be like '+=' )
*/
{
	using std::iterator_traits;
	using std::max;
	using std::min;

	typedef typename iterator_traits<posType>::value_type posElType;
	typedef typename iterator_traits<readType>::value_type procType; 
	posElType minposaccept = rng_i.first-F.length()+1;
	posElType maxposaccept = rng_i.last+1;
#ifdef PERFORMANCECOUNTING
//	timeCntT tstart = __rdtsc(); 
#endif
	for( int_t m = rng_o.first ; m<= rng_o.last; m++ ) {
		posElType curpos = pos[ m ];
		if ((curpos >= minposaccept ) && (curpos < maxposaccept )) {

			//int_t curposi = (int_t) floor(curpos); // location of start of filter in source image (in Tdim)
			int_t curposi = ((int_t) (curpos+2048))-2048; // location of start of filter in source image (in Tdim). Use truncation in integer conversion as floor; use offset to avoid problems with negative values.
			// i_start : start position in filter; 
			/*int i_start = 0; 
			if (curposi<rng_i.first) {
				i_start = (int) (rng_i.first-curposi); // adjust start position in filter due to begin of image 
			}
			/*/
			int i_start = max( 0, (int) (rng_i.first-curposi));  // */
			/*int i_end = F.length();
			if ( curposi > rng_i.last-i_end ) { // does end of filter extend after allowed range in source image
				i_end = (int) (rng_i.last-curposi+1);  // adjust so that end samples rng_i.last, last sample: i_end-1
			} /*/
			int i_end = min( F.length(), (int) (rng_i.last+1-curposi) ); // */


#ifdef DERIVATIVE 
			F.prepareValueAndDerivative( curpos - (posElType) curposi  );
#else
			F.prepare( curpos - (posElType) curposi  );
#endif
#ifndef ADJOINT
		// if (!doAdjoint) : compute normal transform:
			procType tempOut = (procType) 0.0; 
#ifdef DERIVATIVE 
			procType dtempOut = (procType) 0.0;
#endif
			readType inLp = in + curposi + i_start;
			for (int i = i_start; i< i_end; ++i, ++inLp) {   
				//tempOut   += (procType) in[ curposi + i ] * F.sample( i );
				tempOut   += (procType) *inLp * F.sample( i );
	#ifdef DERIVATIVE
				//dtempOut += (procType) in[ curposi + i] * F.sampleDerivative( i);
				dtempOut += (procType) *inLp * F.sampleDerivative( i);
	#endif
			}
//			if (i_start<i_end) {
				out[m] =  tempOut;
//			}
	#ifdef DERIVATIVE
			dOut[m] = dtempOut;
	#endif
#else	// else of (!doAdjoint) : compute adjoint operation:
			procType tempOut = in[ m ];
			for (int i = i_start; i< i_end; i++) {   
				out[ curposi + i ] += tempOut * F.sample( i );
	#ifdef DERIVATIVE
                dOut[ curposi + i ] += tempOut * F.sampleDerivative(  i );
	#endif
			}
#endif  //end of else of ifndef	ADJOINT
		} else { 
			// out of bound and/or nan/inf : set result value to zero or add zero (when adjoint)
#ifndef ADJOINT
			out[m] =  (procType) 0.0; 
	#ifdef DERIVATIVE
			dOut[m] = (procType) 0.0; 
	#endif
#endif  //end of else of ifndef	ADJOINT
		} 
	} // end loop m
#ifdef PERFORMANCECOUNTING
//	timeCntT tend = __rdtsc(); 
//	time_used[2] += tend- tstart;
#endif
};

#ifndef DERIVATIVE  //NOTDEFINED_TESTVECTORIZEPOS 
#ifndef ITERATORTYPESHELPER
#define ITERATORTYPESHELPER
template < typename T > class elementItHelper {
public:
	typedef typename T::elementIteratorType elementIteratorType;
	static inline elementIteratorType elementIterator( T obj , int k ) {
		return obj.elementIterator(k);
	}
};
template < typename T, int vlen> class elementItHelper< vec<T, vlen> * > {
public:
	typedef typename vecptr<T *, vlen>::elementIteratorType elementIteratorType;
	static inline elementIteratorType elementIterator(  vec<T, vlen> * ptr , int k ) {
		return vecptr<T *, vlen>( (T *) ptr ).elementIterator(k);
	}
};
#endif
template <typename int_t,  typename readType, typename writeType, typename posType, typename filtType , int nveclines> 
	//class FILTERLINEWITHF_SPEC<int_t, readType, writeType, vecptr_step2<posType *, nveclines>, filtType> { public: static inline void fun /*
 inline void FILTERLINEWITHF_SPEC// */
		( readType in, writeType out  DERIVWRITEARGS,
							  range< int_t > rng_o , range<int_t> rng_i,
							  vecptr<posType *, nveclines> pos, filtType F ) 
{
	typedef vecptr<posType *, nveclines> vposType;
	typedef typename elementItHelper< vposType >::elementIteratorType pT;
	typedef typename elementItHelper< readType >::elementIteratorType rT;
	typedef typename elementItHelper< writeType >::elementIteratorType wT;
	for (int k = 0 ; k < nveclines; ++k ) {
		rT ink  = elementItHelper< readType >::elementIterator( in, k );
		wT outk = elementItHelper< writeType >::elementIterator( out, k );
		pT posk = elementItHelper< vposType >::elementIterator( pos, k );
		FILTERLINEWITHF_SPEC( ink, outk, rng_o, rng_i, posk, F);
	};
};
#endif // end of #ifdef DERIVATIVE 

template <typename int_t,  typename readType, typename writeType, typename posType, typename filtType >
inline void FILTERLINEWITHF(  readType in, writeType out  DERIVWRITEARGS,
							  range< int_t > rng_o , range<int_t> rng_i,
							  posType pos, filtType F)
{
	if (F.subtype() == 0) {
		typedef typename filtType::specialization0 filtType_c;
		filtType_c F_c = filtType_c(F);
		FILTERLINEWITHF_SPEC(in,out DERIVWRITEARGSs, rng_o, rng_i, pos, F_c);
	} else if (F.subtype() == 1) {
		typedef typename filtType::specialization1 filtType_c;
		filtType_c F_c = filtType_c(F);
		FILTERLINEWITHF_SPEC(in,out DERIVWRITEARGSs, rng_o, rng_i, pos, F_c);
	} else if (F.subtype() == 2) {
		typedef typename filtType::specialization2 filtType_c;
		filtType_c F_c = filtType_c(F);
		FILTERLINEWITHF_SPEC(in,out DERIVWRITEARGSs, rng_o, rng_i, pos, F_c);
	} else {
		mexErrMsgTxt("Bad subtype value of F.");
	}
}
#undef FILTERLINEWITHF_SPEC
#undef FILTERLINEWITHF

#ifdef ADJOINT // first build normal function then adjoint, only construct filterline when both are defined.
/*#ifndef ITERATORTYPES
#define ITERATORTYPES
template < typename T > class iteratorT {
public:
	typedef T * Type;
};

template < typename T, int vlen> class iteratorT< vec<T, vlen> > {
public:
	typedef vecptr<T *, vlen> Type;
};
#endif*/
template <typename int_t, typename readType, typename writeType, typename posType, typename filtType, typename ARType>
inline void filterLine_F_AR ( readType in, writeType out DERIVWRITEARGS,
							  range< int_t > rng_o, range< int_t > rng_i ,
							  posType pos ,
							  filtType F ,
							  ARType AR, int_t nAR, int_t ARrunoutLen,
							  tempspace tmp, bool doAdjoint)
{
	using std::iterator_traits;
    if (AR!=NULL) {
		typedef typename iterator_traits< readType >::value_type tempType;
		//typedef typename iteratorT< tempType>::Type tempTypeR;
		typedef tempType* tempTypeR;
		typedef typename iterator_traits< readType >::value_type procType; 
		typedef typename iterator_traits< posType >::value_type posElType; 

		range<int_t> rng_i2 = {	rng_i.first - ARrunoutLen,			// zero before rng_i.first - ARrunoutLen, not used before istartm
								rng_i.last  + ARrunoutLen };	    // dont filter beyond maxi+ARrunoutLen (since it's 0), dont start backward filter after iendm+ARrunoutLen (since that contribution is zero), dont have more than ARrunoutLen steps after rng_i.last and before starting at iendm

        // Compute range of i used by transformation, assuming end points in pos[ rng_o ] are found by substituting the end of the ranges of m
        using std::max;
        using std::min;

/*		posElType istartmf = pos[ rng_o.first ] ;// first element of in used. 
        posElType iendmf   = pos[ rng_o.last  ] ;// last element of in used.
		using std::swap;
		if (iendmf < istartmf) { swap( iendmf , istartmf ); };	// swap if transform flips image.

		if (istartmf<=iendmf) { // compares false when either (or both) are nan.
			// try to restrict rng_i2 based on pos:
			if ( istartmf > rng_i2.first ) { // note: nan compares false.
				int_t istartm = (int_t) floor(istartmf);
				rng_i2.first = istartm;
				using std::max;
				rng_i.first  =	max( rng_i.first, istartm-ARrunoutLen ) ; // start to filter at rng_i.first or ARrunoutLen before istartm (whichever is larger)
			}
			if ( iendmf<rng_i.last ) { // note: nan compares false.
				int_t iendm = (int_t) floor( iendmf + F.length()-1 );
				rng_i2.last = min( rng_i2.last, min( iendm + ARrunoutLen, (rng_i.last + iendm + ARrunoutLen + 1) / 2 + nAR ) ); 
			}
		}*/
		range<int_t> rng_tmp = { min(rng_i.first, rng_i2.first) , rng_i2.last };

        tempTypeR ARfiltered = tmp.get<tempType>( rng_tmp.last - rng_tmp.first + 1 ) - rng_tmp.first; // offset so rng_tmp.first is at location 0 of tempspace.
        if (!doAdjoint){
			// prefilter

            readAndFilterWithAR<int_t, readType, tempTypeR, tempTypeR , ARType>( in , rng_i, AR, nAR,
                                                                     ARfiltered, rng_tmp, 
																	 ARfiltered, rng_i2);
			// sample
            filterLineWithF<int_t, tempTypeR, writeType, posType, filtType>
                            ( ARfiltered, out DERIVWRITEARGSs , rng_o, rng_i2, pos, F );
		} else { // if (doAdjoint) : 
			for (int i=rng_i2.first; i<=rng_i2.last; i++ ) {
				ARfiltered[i] = (tempType) 0.;
			}
#ifdef DERIVATIVE
            tempTypeR derivARfiltered = tmp.get< tempType >( rng_tmp.last - rng_tmp.first + 1 ) - rng_tmp.first;
			for (int i=rng_i2.first; i<=rng_i2.last; i++ ) {
				derivARfiltered[i] = (tempType) 0.;
			}
	#define ARDERIVWRITEARGSs , derivARfiltered
#else
	#define ARDERIVWRITEARGSs 
#endif 
			// adjoint sample:
			filterLineWithF_adj<int_t,  readType, tempTypeR, posType, filtType>
                                ( in, ARfiltered ARDERIVWRITEARGSs ,   rng_o, rng_i2, pos, F );
            // prefilter:
			rng_i.last = min( rng_i.last, rng_tmp.last );
            readAndFilterWithAR<int_t, tempTypeR, tempTypeR, writeType, ARType>(ARfiltered , rng_i2, AR, nAR, 
                                                                     ARfiltered , rng_tmp,
																	 out        , rng_i) ;
#ifdef DERIVATIVE
            readAndFilterWithAR<int_t, tempTypeR, tempTypeR, writeType, ARType>(derivARfiltered , rng_i2, AR, nAR, 
                                                                     derivARfiltered , rng_tmp,
																	 dOut            , rng_i) ;
#ifdef ADDGUARDS 
			tmp.checkGuard( derivARfiltered +rng_tmp.first, rng_tmp.last - rng_tmp.first + 1 );
#endif
#endif            
        }

#ifdef ADDGUARDS 
		tmp.checkGuard( ARfiltered +rng_tmp.first, rng_tmp.last - rng_tmp.first + 1 );
#endif

	} else {
		// No AR filtering:
        if (doAdjoint) {
			filterLineWithF_adj<int_t,  readType, writeType, posType, filtType>
									( in, out DERIVWRITEARGSs ,   rng_o, rng_i,
									  pos, F );
		} else {
			filterLineWithF<int_t,  readType, writeType, posType, filtType>
									( in, out DERIVWRITEARGSs ,   rng_o, rng_i,
									  pos, F );
		}
    }
};
#undef ARDERIVWRITEARGSs

#ifdef DERIVATIVE // only construct transform function when all functions with and without derivative are constructed.
template < typename int_t, typename readType, typename writeType, typename posType, typename filtType, typename ARType >
void TransformCore(readType inR, readType inI, range< int_t > range_i, int_t stepIn_Tdim, int_t n_Tag, int_t stepIn_Tag,
							posType T,
							writeType outR, writeType outI, writeType  doutR, writeType  doutI, range< int_t> range_o, int_t stepOut_Tdim, int_t stepOut_Tag, 
			   				filtType F,
							ARType AR, int_t nAR, int_t ARrunoutLen,
							tempspace tmp , bool doAdjoint)
/* affineTransformCorePlane: 
	Purpose:
	Apply a shear operation along the first dimension of a 2D sheat. 
	The output area that is computed is specified by compOutArea
	For multidimensional images   : call for all 2D sheats. 
	For general affine transforms : do some preprocessing to split that transform in a set of shear transforms

	Algorithm: (and suggestions for the transform planner)
	The memory layout of the 2D sheat should be regular, but in general any spacing between subsequent 
	elements in either direction is allowed (specified by step_*). 
	Each output pixel is obtained by interpolation of along the first dimension of in.
	The interpolation kernel is specified by F. 
	Therefore, for each output pixel, 2 columns of the filter F need to be linearly interpolated: (MATLAB ordering of dimensions) 
		Fcur = F(:,i) + (F(:,i+1)-F(:,i))*frac;
	For optimal performance a planner might select as second dimension of 'in' a direction in which 
	'frac' and 'i' are constant.
	This Fcur is muliplied with the appropriate selection of in:
	    out(.,l) = Fcur'*in(sel,l)
	The area of out that needs to be computed is specified by the n_rowscompOutArea row matrix compOutArea
	Each column of compOutArea specifies: [a position in second dim of out (increasing, minimum step 1);
										   start in first dim (at that location);
										   end in first dim(at that location)]
	the start and end positions of a specific column are linearly interpolated between the given positions.

    (non trivial) Requirements:
	When in is complex, out should be complex as well.

	Pseudo code:
    k  : 

	in[k,l]  = in[  k * stepIn_Tdim  + l * step_in_sdim ] with 0<=k<n_in_Tdim, 0<=l<=n_sdim
	out[m,l] = out[ m * stepOut_Tdim + l * step_out_sdim] with 0<=m< n_out_Tdim, 0<=l<=n_sdim
	pos[m,l] = pos +m * dpos_Tdim + l *  dpos_sdim
	posi[m,l] = (int_t) pos[m,l]	 // integer fraction, round towards -inf
	posr[m,l] = pos[m,l] - posi[m,l] // remainder

	out[m,l] = sum_i ( F[i,posr[j,l]] * in[j,l]) with j = posi[m,l]+i, 0<=i<mF, while guaranteeing that 0<=j<n_in_Tdim
	and F[i, 0<= (double) x <1 ] = linear interpolated between columns of F, 
	                 F[i, (int) x*nFsteps ] * (1-xr) + F[i, (int) x*nFsteps +1] * xr, 
						with xr = x*nFsteps - ((int) x*nFsteps) 
	
	compOutArea[s,q] = compOutArea[s+n_rowscompOutArea*q] = n_rowscompOutArea x n_compOutArea matrix, with n_compOutArea>=2
	compOutArea[0,:] = switching l position,  compOutArea[0,q] + 1 <= compOutArea[0,q+1]  and  compOutArea[0,1]>0
	compOutArea[1,:] = start m position >=0
	compOutArea[2,:] = end   m position < n_out_Tdim
	compOutArea[3,:] = start k position >=0
	compOutArea[4,:] = end   k position < n_out_Tdim
 
	I do linear interpolation in compOutArea to get start m and end m for the current l.
		So when : a = compOutArea[0,q], b = compOutArea[0,q+1], a<l<b
		then: 
		   startm[l] = (int) ((b-l)/(b-a) * compOutArea[1,q]  + (l-a)/(b-a) * compOutArea[1,q+1])
		   endm[l]   = (int) ((b-l)/(b-a) * compOutArea[2,q]  + (l-a)/(b-a) * compOutArea[2,q+1])
					endm is inclusive, so out[endm] should exist for all l.
		for max(compOutArea[0,0],0) <= l < min( n_sdim, compOutArea[0,n_compOutArea-1] ) 
		  
	Note that all inputs should be initialized and satisfy above constraints.
	The only constraint I ensure/apply is 0<=j<n_in_Tdim 
	
	This code should be specialized for:
		dpos_sdim  == 0    : to reuse tempF
		step_in_sdim == 1	  : do multiple l at the same time, for efficient cache use
								if stepIn_Tdim is even: use SSE 2 element load.
	This code should be generalized to:
		support complex F

	Note that internal computations involving the source image is done with 'filttype' precision, which is assumed to be of at least the same precision as inimgtype and outimgtype
	Computations involving positions are done in 'postype' precision, which is assumed to be at least as accurate as filttype.


	if Adjoint: only change to input should be swapping of in and out (and the corresponding step* parameters).
			Specifically: range_i and range_o should keep their non adjoint value.
	
	*/
{
    const int vlen = 4;
    using std::max;
    using std::min;
	using std::iterator_traits;
	typedef typename iterator_traits< readType >::value_type procValType;

    bool doderiv         = (doutR !=NULL);
    
    // General case:
    // for remaining l when (dspos_sdim==0)
    // for (dspos_sdim!=0)
	int_t l=0;
	#define COMMONARGSs range_o, range_i, T, F, AR, nAR, ARrunoutLen, tmp, doAdjoint
	/* Vectorized over tag along dimension:
	for( ; l<n_Tag-(vlen-1); l+=vlen) {
        if (inI==NULL) {
            typedef lineType2<const readVType , vec<readVType , vlen>, int_t> readType;
            typedef lineType2<      writeVType, vec<writeVType, vlen>, int_t> writeType;

			readVType  in  = readVType( inR + l*stepIn_Tag , stepIn_Tdim , stepIn_Tag  );
            writeType out = writeType(outR+ l*stepOut_Tag, stepOut_Tdim, stepOut_Tag );
            if (doderiv) {
	            writeType dout = writeType( doutR+ l*stepOut_Tag, stepOut_Tdim, stepOut_Tag );
                filterLine_F_AR<int_t, readVType, writeType, posType, filtType, ARType>( in, out, dout, COMMONARGSs );
            } else {
                filterLine_F_AR<int_t, readVType, writeType, posType, filtType, ARType>( in, out,       COMMONARGSs );
            }
		} else {
			//mexErrMsgTxt("implement complex data type.");            
			typedef lineType2c<const readVType , vec<readVType , vlen>, int_t> readVType;
            typedef lineType2c<      writeVType, vec<writeVType, vlen>, int_t> writeType;
			
			readVType  in  = readVType( inR + l*stepIn_Tag , inI + l*stepIn_Tag , stepIn_Tdim , stepIn_Tag  );
            writeType out = writeType(outR+ l*stepOut_Tag, outI+ l*stepOut_Tag, stepOut_Tdim, stepOut_Tag );

            if (doderiv) {
	            writeType dout = writeType( doutR+ l*stepOut_Tag, doutI+ l*stepOut_Tag, stepOut_Tdim, stepOut_Tag );
                filterLine_F_AR<int_t, readVType, writeType, posType, filtType, ARType>( in, out, dout, COMMONARGSs );
            } else {
                filterLine_F_AR<int_t, readVType, writeType, posType, filtType, ARType>( in, out,       COMMONARGSs );
            }
			// 
		}        
	} // end loop l */
	for( ; l<n_Tag; l++) {
        if (inI==NULL) {
            typedef lineType<      readType , procValType , int_t> readType_;
            typedef lineType<      writeType, procValType , int_t> writeType_;

			readType_  in  = readType_( inR + l*stepIn_Tag , stepIn_Tdim);
            writeType_ out = writeType_(outR+ l*stepOut_Tag, stepOut_Tdim);

            if (doderiv) {
	            writeType_ dout = writeType_(doutR+ l * stepOut_Tag, stepOut_Tdim) ;
                filterLine_F_AR<int_t, readType_, writeType_, posType, filtType, ARType>( in, out, dout, COMMONARGSs );
            } else {
                filterLine_F_AR<int_t, readType_, writeType_, posType, filtType, ARType>( in, out,       COMMONARGSs );
            }
		} else {
			
            typedef lineTypec<      readType , procValType , int_t> readTypec;
            typedef lineTypec<      writeType, procValType , int_t> writeTypec;

			readTypec  in  = readTypec( inR + l*stepIn_Tag , inI + l*stepIn_Tag  , stepIn_Tdim);
            writeTypec out = writeTypec(outR+ l*stepOut_Tag, outI+ l*stepOut_Tag , stepOut_Tdim);

            if (doderiv) {
	            writeTypec dout = writeTypec( doutR+ l*stepOut_Tag, doutI+ l*stepOut_Tag, stepOut_Tdim);
                filterLine_F_AR<int_t, readTypec, writeTypec, posType, filtType, ARType>( in, out, dout, COMMONARGSs );
            } else {
                filterLine_F_AR<int_t, readTypec, writeTypec, posType, filtType, ARType>( in, out,       COMMONARGSs );
            }

		}        
	} // end loop l
}


#endif // end of only construct transform function when function for without and with derivative are constructed.

#endif // end of ADJOINT (i.e. after both normal and adjoint functions are created.) 

#undef DERIVWRITEARGSDECL
#undef DERIVWRITEARGS
#undef DERIVWRITEARGSt
#undef DERIVWRITEARGSs
#undef DERIVFILTARGS 
#undef DERIVFILTARGSs 
#undef FILTERLINEWITHF_ADJ
#undef FILTERLINEWITHF

#else // else of ifdef BUILDING_CORE

#ifdef _OPENMP
#define USEOMP
#endif

#ifdef USEOMP
#include <omp.h>
#define MAXNUMTHREADS 20
#endif

//#define INCLUDE_SSE4

#define ADDGUARDS

#include <assert.h>
#include "mex.h"
#include "math.h"
#include <complex>
#include <algorithm>
#include "emm_vec.hxx"
#include "tempSpaceManaging.cpp"
#include "lineReadWriters.cpp"
#include "Tools/supportingfiles/bspline_proc.cpp"
#include "Tools/supportingfiles/subspaceIterator.cpp"
#include "Tools/supportingfiles/SeparableTransform_core_c.cpp"
#include <iterator>
#include <bitset>

//#define PERFORMANCECOUNTING // if defined: return some counts in the first (double) elements of the output. SO FOR DEBUG ONLY!!!
							  // Compile TransformCoreGeneral_c2_timing.cpp to get this functionality.
#ifdef PERFORMANCECOUNTING
#ifdef _WIN32  // also defined on 64 bit windows platforms
	#include <Windows.h>
	typedef volatile __int64 timeCntT;
	timeCntT time_used[5];
	#pragma intrinsic(__rdtsc)
	#define PERFORMANCECOUNTER( value ) value = __rdtsc()
#else
	#include <time.h>
	typedef timespec timeCntT;
	int64_t time_used[5];
	#define PERFORMANCECOUNTER( value ) clock_gettime( CLOCK_MONOTONIC, &value )
	//int64_t timeCntT::operator-( timeCntT b) {
	int64_t operator-( const timeCntT &a, const timeCntT &b) {
		return (int64_t) (a.tv_nsec - b.tv_nsec) + ((int64_t) (a.tv_sec - b.tv_sec))*1000000000;
	};
#endif
#endif

// overloaded floor; default implementation is extremely slow.
//#define _MM_FROUND_TO_NEG_INF 1
template< typename T> inline T fastfloor( T value ) {
	return floor( value );
}
#ifdef INCLUDE_SSE4
template<> inline double fastfloor( double val) {
	__m128d tmp = _mm_load_sd( & val );
	_mm_store_sd( &val , _mm_round_sd( tmp, tmp , _MM_FROUND_TO_NEG_INF ));
	return val;	
}
#endif
//using namespace std;

const int n_rowscompOutArea = 5;
#define ERROR mexErrMsgTxt

#define BUILDING_CORE
#include __FILE__	// non derivative, non adjoint functions
#define ADJOINT
#include __FILE__	// non derivative, adjoint functions
#undef ADJOINT
#define DERIVATIVE
#undef FILTERLINE_F // FILTERLINE_F builds differently with/without derivative.
#include __FILE__	// derivative, adjoint functions
#define ADJOINT
#include __FILE__	// derivative, non adjoint functions
#undef ADJOINT

template < typename int_t, typename readType1, typename readType2 , typename writeType>
void mulvecrange( readType1 in1, readType2 in2, writeType out, range< int_t> procrange ) {
	using std::iterator_traits;
	typedef typename iterator_traits< readType1 >::value_type valType1;
	for (int_t l = procrange.first; l <= procrange.last; l++ ) {
		valType1 in1l = in1[l]; // avoid reading reference to in1[l] for lineReadWriters by explicitly reading value first.
		out[l] = in1l * in2[l];
	}
}


template < typename int_t, typename readType, typename writeType, typename posType>
void mulWithTransformDerivative(readType inR, readType inI, int_t n_Tag, 
							posType T,
							writeType outR, writeType outI, range< int_t> range_o, int_t stepOut_Tdim, int_t stepOut_Tag) 
{
	using std::iterator_traits;
	//typedef iterator_traits< readType >::value_type procValType;
	typedef double procValType;
	int_t l=0;
	for( ; l<n_Tag; l++) {
        if (inI==NULL) {
            typedef lineType<      readType , procValType , int_t> readType_;
            typedef lineType<      writeType, procValType , int_t> writeType_;

			readType_  in  = readType_( inR + l*stepOut_Tag , stepOut_Tdim);
            writeType_ out = writeType_(outR+ l*stepOut_Tag , stepOut_Tdim);

			mulvecrange( in, T, out , range_o );
		} else {
			
            typedef lineTypec<      readType , procValType , int_t> readTypec;
            typedef lineTypec<      writeType, procValType , int_t> writeTypec;

			readTypec  in  = readTypec( inR + l*stepOut_Tag, inI + l*stepOut_Tag , stepOut_Tdim);
            writeTypec out = writeTypec(outR+ l*stepOut_Tag, outI+ l*stepOut_Tag , stepOut_Tdim);

			mulvecrange( in, T, out , range_o );

		}        
	} // end loop l
};

template < typename int_t, typename readType, typename TransfType >
void mulWithTransformDerivativeAdjoint( readType inR, int_t n_Tag, 
							TransfType & T,
							range< int_t> range_o, int_t stepIn_Tdim, int_t stepIn_Tag )
{
	//T.setAdjointPos( in[ currentIndex_o[0] ] );
	if (n_Tag==1) {
		for ( int_t m = range_o.first; m <= range_o.last; m++ ) {
			T.writeAdjoint( m , inR[  m * stepIn_Tdim ] );
		}
	} else {
		for ( int_t m = range_o.first; m <= range_o.last; m++ ) {
			for ( int_t l=0; l<n_Tag; l++) {
				T.writeAdjoint( m , inR[  m * stepIn_Tdim + stepIn_Tag * l ] );
			}
		}
	}
};

template< typename clipT, typename int_t > vector< clipT > build_clipranges( const mxArray * clip, int_t * size, int ndimsT) {
	vector< clipT > clips(ndimsT);
	bool doClip = !mxIsEmpty(clip);
	if (doClip) {
		if ((mxGetNumberOfElements( clip )!=ndimsT) | (!mxIsCell( clip ) ) ) {
			mexErrMsgTxt("clip_i/o should be a ndims(T) element cell array, with each element Q_k  a ( nplanes_k  x  (ndims(T)-k+2) )  double matrix, where the subset of all allowable coordinate vectors x is given by  Q*[x;-1] <=0 (if Q = [A b] =>  A*x <= b, which is the standard linear program form in MATLAB).");
		}
		for (int k = 0 ; k<ndimsT ; k++ ) {
			const mxArray * clipel = mxGetCell(clip, k);
			const double * ptr = NULL;
			int nplanes = 0; 
			if (clipel != NULL) {
				if ((mxGetNumberOfDimensions( clipel )!=2) | (mxGetClassID( clipel ) != mxDOUBLE_CLASS) | mxIsComplex( clipel ) | !((mxGetN(clipel)==ndimsT-k+1)|| mxIsEmpty(clipel)) ) {
					mexErrMsgTxt("Element k of clip_o should be a nplanes(k) x (ndims(T)-k+2) non complex double matrix.");
				}

				ptr = mxGetPr(clipel);
				nplanes = (int) mxGetM(clipel);
			}
			range<double> rng ={0, (double) size[k]-1};
			//				    pointer to A  ,     pointer to b                      ,  number of half spaces,  dimensionality , outer limits.   
			clips[k] = clipT(    ptr,   ptr + nplanes * (ndimsT-k),      nplanes,        ndimsT-k,         rng       );
		}
	} else {
		for (int k = 0 ; k<ndimsT ; k++ ) {
			const double * ptr = NULL;
			int nplanes = 0;
			range<double> rng ={ 0, (double) size[k] -1 };
			//				    pointer to A  ,     pointer to b                      ,  number of half spaces,  dimensionality , outer limits.   
			clips[k] = clipT(    ptr,   ptr + nplanes * (ndimsT-k),      nplanes,        ndimsT-k,         rng       );
		}
	}
	return clips;
}

template< typename T , typename int_t> T* adp( T* p, int_t step) {
	if (p==NULL) {
		return p;
	} else {
		return p+step;
	}
}
#undef BUILDING_CORE
#ifndef NOMEXFUN
#define MaxNrOfDims 6
struct     areaInfo {double * ptr ; mwSize  n;};
typedef ptrdiff_t int_t ;


#ifdef BUILDTESTFUN_TRANSFORMCOREGENERAL
// Test code to test the majority of the code in this function. Mainly used to debug the code during development.
#include <bitset>
void self_test_mex( mxArray * out ) {
	std::bitset<5> bl(6);
	bl[4]=1;


}
#endif


void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
#ifdef BUILDTESTFUN_TRANSFORMCOREGENERAL
	// Only compile code below for performing a code test.
	if (nlhs!=1) {
		mexErrMsgTxt("One output required.");
	}
	plhs[0] = mxCreateStructMatrix(2,1,0, NULL);
	self_test_mex( plhs[0] );
	return;
#else
/*  out = transformCorePlane_c(in, T, mapT, newszT, dimorders, F, bsplineord, AR, clip_o, clip_i )
% Transforms 'in' to 'out'
% (MATLAB-like) pseudocode of function:

if newszT>0 
	=> normal
	i = in ; 
	size_i = size(in);
	size_o = size_i; size_o(dimorders(3,1)) = newszT;
	o = zeros(size_o);
else
	=> adjoint:
	o = in ; 
	size_o = size(in); 
	size_i = size_o; size_i(dimorders(1,1)) = -newszT;
	i = zeros(size_o);
end;

i  =  permute(i, dimorders(1,:) );
o  =  permute(o, dimorders(1,:) );

T   =  permute(Ti, dimorders(2,:) );
for all 'columns' j of i
	i_j = column j of i  (reference to)
	o_j = column j of o  (reference to)
	% filter in forward and backwards with AR model: (only if normal, adjoint=> zeros)
	if normal:
		inf_j = flip( filter(1,AR, flip(filter(1,ar, i_j) )))
	for k=1:size(i , 1) % note transform dim is first dimension after permutation.
		pos_k = T( k , j);   % function T defined by input T as described below
		posi_k = floor( pos_k );
		posf_k = (pos_k - posi_k) * ( size(F,2)-bsplineord );

		if normal:
			o_j(k, :) = F(:,posf_k)' * inf_j( posi_k + [0:size(F,1)-1] , : );
		else % adjoint:
			inf_j( posi_k + [0:size(F,1)-1] ,: ) = F(:,posf_k)' * o_j(k,:) ;
		end;
		% bsplineord-Bspline interpolation between columns of F
		% (implicitly) replace elements outside in with zero. 
	end;
	if adjoint:
		i_j = flip( filter(1,AR, flip(filter(1,ar, inf_j) )))
end;
i = ipermute(i , dimorders(1,:) );
o = ipermute(o , dimorders(3,:) );
if normal:
	return o
else % adjoint:
	return i
end;
%
% INPUTS:
%  in	: (N+k)-Dimensional single or double image; 
%         Dimensions N+1:N+k are just tagged along. 
%         If normal:  i = in ; 
%		  If adjoint: o = in
%  T    : N - Dimensional sampling locations matrix. 
%         or 1 x (N+1) vector with affine transform
%  mapT : empty or 2 x N mapping matrix. Required to be [] for affine transform.
%         sample_location_t = mapT(2,:) * o_index + mapT(1,:)
%  newSzT : scalar specifying the new size in the transform dimension. Negative value indicates adjoint transform
%  dimorders : 3 x N(+k) positive integer matrix that specifies the 
%				(implicit) permutations that are performed on [i;T;o]
%               A restriction is that 'tag along dimensions' should be consecutive.
%  F    : filterbank, as for example designed by designLPfilter(_v3)
%  bsplineord : (0,1,2) b-spline interpolation order for interpolation between 
%               the columns of F (first element) and T (second element, if provided; otherwise uses first element as well)
%  AR   : Autoregressive pre-filter model. Empty => no prefiltering.
%  clip_o : N element cell matrix with clipping half spaces of permuted 'o'-image (=output if normal; input if adjoint)
%  clip_i : N element cell matrix with clipping half spaces of permuted 'i'-image (=input if normal; output if adjoint)
% OUTPUTS:
%  out    : (N+k)-Dimensional transformed 'in'
%           if normal : out = o
%           if adjoint: out = i
%  doutdT : (N+k)-Dimensional derivative of out w.r.t. to T. 
*/
#ifdef PERFORMANCECOUNTING
//	timeCntT tstart_initialization = __rdtsc(); 
	timeCntT tstart_initialization; 
	PERFORMANCECOUNTER( tstart_initialization );
#endif

	/* Check for proper number of arguments. */

	if ((nrhs!=10)) {
		mexErrMsgTxt("Ten inputs required: [out [,doutdT]] = TransformCoreGeneral_c( in, T, mapT, newszT, dimorders, F, bsplineord, AR, clip_o (clip in 'o' image space; = output in forward), clip_i (clip in 'i' image space; = input in forward) ).");
	} else if((nlhs!=1)&&(nlhs!=2)) {
		mexErrMsgTxt("One or two outputs required.");
	}
	const mxArray * in			= prhs[0]; // N-D array, single or double.
	const mxArray * T			= prhs[1]; // N-D array, double, or 1xN vector
	const mxArray * mapT		= prhs[2]; // 2 x N matrix, double, [offset;step] in T for point in out; point_t = point_out .* mapT(2,:) + mapT(1,:)
	const mxArray * newSzT		= prhs[3]; // scalar integer
	const mxArray * dimorder	= prhs[4]; // 3 x N, double; permutation vectors of [in;T;out];  first try: [1;1;1]*(1:N)
	const mxArray * F			= prhs[5]; // length x nsteps, double 
	const mxArray * bsplineord	= prhs[6]; // scalar.
	const mxArray * AR			= prhs[7]; // [] or vector.
	const mxArray * clip_o		= prhs[8]; // [] or ndims element cell array with half planes of 'o' image (output for normal, input for adjoint).
	const mxArray * clip_i		= prhs[9]; // [] or ndims element cell array with half planes of 'i' image (input for normal, output for adjoint).

	bool prefilterwithAR = !mxIsEmpty(AR);
    bool computeDerivative = (nlhs>1);
	bool doClip_o = !mxIsEmpty(clip_o);
	bool doClip_i = !mxIsEmpty(clip_i);
    int_t tempspacebytesneeded = 0;

	const int nveclines = 8; /* number of lines to process simultanuously (if that is possible) */
	bool dovecLines = false;
	/* parse inputs */
	//in:
	mwSize ndimsIn = mxGetNumberOfDimensions( in );
	if ( ndimsIn > MaxNrOfDims ) { 
		mexErrMsgTxt("Input has too many dimensions, increase 'MaxNrOfDims' in this mex file");
	}
	const mwSize * sizeIn = mxGetDimensions( in );
	if ( (ndimsIn==2) && (sizeIn[1]==1) ) {
		ndimsIn=1;
	};
	mxClassID imgclass = mxGetClassID(prhs[0]);
	if ((imgclass!=mxDOUBLE_CLASS) & (imgclass!= mxSINGLE_CLASS)) {
		mexErrMsgTxt("Input should be single or double. (Other types can easily be added if meaningfull interpolation can be performed.)");
	}
	double * inR = mxGetPr(prhs[0]); // TODO: make const
    double * inI = NULL;			 // TODO: make const	
	bool iscomplex= mxIsComplex(prhs[0]);
    mxComplexity iscomplexArg = mxREAL; // later overwritten if iscomplex
	if (iscomplex) {
		inI = mxGetPi(prhs[0]);
		iscomplexArg = mxCOMPLEX;
	};
    int_t imgtypesize = mxGetElementSize( in ) ;

	// T
	mwSize ndimsT = mxGetNumberOfDimensions( T );
	const int * sizeT = mxGetDimensions( T );
	double * Td = mxGetPr( T );
	bool affineT = (ndimsT==2) && (sizeT[0]==1) && (sizeT[1]==ndimsIn+1);
	if ((!affineT) && (ndimsT==2) && (sizeT[1]==1) ) {
		ndimsT=1;
	}
	if ( (mxGetClassID( T ) != mxDOUBLE_CLASS) | mxIsComplex( T ) | ((ndimsT>ndimsIn) && !affineT) ) {
		mexErrMsgTxt("Transform T should be non complex double matrix with at most the same number of dimensions as in.");
	}
	if (affineT) {
		ndimsT = ndimsIn;
	} 

	// mapT
	double * mapTd = NULL;
	if ( !mxIsEmpty(mapT) ) {
		if (affineT) {
			mexErrMsgTxt("MapT should be empty when T specifies an affine transformation.");
		}
		if ((mxGetClassID( mapT ) != mxDOUBLE_CLASS) || mxIsComplex( mapT ) ||  (mxGetM(mapT)!=2) || (mxGetN(mapT)!=ndimsT) ) {
			mexErrMsgTxt("MapT should be 2 x ndims(T) non complex double matrix");
		}
		mapTd = mxGetPr(mapT);
	}

	// newSzT
	if ( mxIsComplex( newSzT ) ||  (mxGetNumberOfElements(newSzT)!=1)) {
		mexErrMsgTxt("newszT should be a scalar integer.");
	}
	int_t newszT = (int_t) mxGetScalar( newSzT );
    bool doAdjoint = (newszT<0);
	if (doAdjoint) {
		newszT = -newszT;
	}
	if ( (newszT<=0) || (newszT>100000000) ) {
		mexErrMsgTxt("Invalid new size in transform dimension.");
	}
	

	//dimorder = 3 x N 
	int_t  numberorders = mxGetN( dimorder );
	if ((mxGetNumberOfDimensions( dimorder )>2) | (mxGetM( dimorder )!=3) | (numberorders>MaxNrOfDims) | (numberorders!=ndimsIn) |  (mxGetClassID( dimorder ) != mxDOUBLE_CLASS) | mxIsComplex( dimorder )) {
		mexErrMsgTxt("dimorder should be 3 x N matrix with each row a permutation of [1:N], with N = ndims(in).");
	}
	double * Tdims = mxGetPr( dimorder );
	const int dimorderIn = 0;
	const int dimorderT  = 1;
	const int dimorderOut= 2;
	const int dimorderStep = 3;
	int Tdim_i[MaxNrOfDims];
	int Tdim_T[MaxNrOfDims];
	int Tdim_o[MaxNrOfDims];
	for (int k=0;k<3;k++) {
		bool hasdim[ MaxNrOfDims ];
		bool allzero = true;
		for (int dim = 0; dim<numberorders; dim++ ) {
			hasdim[dim]= false;
		}
		for (int dim = 0; dim<numberorders; dim++ ) {
			double cv = Tdims[k+dimorderStep*dim];
			if (cv!=0) {
				allzero = false;
			}
			if ( (((double) ((int) cv) )==cv) & (cv>0) & (cv<=MaxNrOfDims) ) {
				// if cv is integer within valid range:
				int cdim = ((int) cv) - 1;
				hasdim[cdim ] = true;
				if (k==0) {
					Tdim_i[ dim ] = cdim;
				} else if (k==1 ) {
					Tdim_T[ dim ] = cdim;
				} else {
					Tdim_o[ dim ] = cdim;
				}
			} 
		}
		bool hasall = true;
		for (int dim = 0; dim<numberorders; dim++ ) {
			hasall &= hasdim[dim];
		}
		if ( (k==1) && affineT) {
			if (!allzero) {
				mexErrMsgTxt("second row of dimorder should be a all zeros for an affine transformation.");
			}
		} else {
			if (!hasall) {
				mexErrMsgTxt("Each row of dimorder should be a permutation of [1:N], with N = ndims(in), except for row 2 which should be all zeros when T specifies an affine transform.");
			}
		}
	}

	// bsplineord
	if ((mxGetNumberOfElements( bsplineord )<1) | (mxGetNumberOfElements( bsplineord )>2) | mxIsComplex( bsplineord ) ) {
		mexErrMsgTxt("bsplineord should be a non complex scalar integer, or 2 element integer vector, in the range [0 .. 3].");
	}
	int bsplineordi = (int) mxGetScalar( bsplineord ) ;
	if ( (bsplineordi<0) | (bsplineordi>3)) {
		mexErrMsgTxt("bsplineord should be 0, 1, 2, or 3.");
	}
	int bsplineordT =   (int) (mxGetPr( bsplineord)[mxGetNumberOfElements( bsplineord )-1]); // bspline order of interpolation of T
	if ( (bsplineordT<0) | (bsplineordT>3)) {
		mexErrMsgTxt("bsplineordT should be 0, 1, 2, or 3.");
	}

	// F
	mxClassID filtclass = mxGetClassID( F );
	bool mulWithDerivativeT = (filtclass== mxCHAR_CLASS);
	bool mulWithAdjointDerivativeT = false;
	if (mulWithDerivativeT) {
		mulWithAdjointDerivativeT = doAdjoint;
		if (computeDerivative || iscomplex) {
			mexErrMsgTxt("Cannot compute derivative of multiply with derivative or nor handle complex dimg.");
		}
	} else {
		if ((mxGetNumberOfDimensions( F )>2) | (filtclass != mxDOUBLE_CLASS) | mxIsComplex( F )) {
			mexErrMsgTxt("F should be a double precision non complex matrix. (Other types can easily be added, if meaningfull interpolation can be performed)");
		}
	}
	double * Fd =  mxGetPr( F );
	int mF	    = (int) mxGetM( F );				// F should not be too large for int.
	int nFsteps = (int) mxGetN( F ) - bsplineordi;	// F should not be too large for int.

	// construct and fill sizeOut
    int sizeOut[ MaxNrOfDims ];
	if (doAdjoint) {
		for(int k=0;k<ndimsIn;k++) {
			sizeOut[ Tdim_i[k] ] = sizeIn[ Tdim_o[k] ];
		}
		sizeOut[ Tdim_i[0] ] = newszT;
	} else {
		if (mulWithDerivativeT) {
			for(int k=0;k<ndimsIn;k++) {
				sizeOut[ Tdim_o[k] ] = sizeIn[ Tdim_o[k] ];
			}
			//sizeOut[ Tdim_o[0] ] = newszT;
		} else {
			for(int k=0;k<ndimsIn;k++) {
				sizeOut[ Tdim_o[k] ] = sizeIn[ Tdim_i[k] ];
			}
			sizeOut[ Tdim_o[0] ] = newszT;
		}
	}

	//const int ncropin = 0;  // = 0; for DEBUG set to >0 to only use cropped part of in.
	//const int ncropout = 0; // = 0; for DEBUG set to >0 to only fill cropped part of out.

	// store permuted size and steps after permutation of In.
	int_t size_i[MaxNrOfDims];
	int_t step_i[MaxNrOfDims+1];
	int_t size_T[MaxNrOfDims];
	int_t step_T[MaxNrOfDims+1];
	int_t size_o[MaxNrOfDims];
	int_t step_o[MaxNrOfDims+1];

	{int_t tempStep_i[MaxNrOfDims];
	int_t tempStep_o[MaxNrOfDims];
	const mwSize * size_iO = ( !doAdjoint ? &sizeIn[0]  : &sizeOut[0]);
	const mwSize * size_oO = ( !doAdjoint ? &sizeOut[0] : &sizeIn[0] );
	tempStep_i[0] = 1;
	tempStep_o[0] = 1;
	for(int k=1;k<ndimsIn;k++) {
		tempStep_i[ k] = tempStep_i[ k-1] * size_iO[ k-1];
		tempStep_o[ k] = tempStep_o[ k-1] * size_oO[ k-1];
	}
	for(int k=0;k<ndimsIn;k++) {
		step_i[ k ] = tempStep_i[ Tdim_i[k] ];
		size_i[ k ] = size_iO[    Tdim_i[k] ];//-2*ncropin; 
		//inR += ncropin * tempStep[k];
		step_o[ k ] = tempStep_o[ Tdim_o[k] ];
		size_o[ k ] = size_oO[    Tdim_o[k] ];//-2*ncropin; 
	}
	if (mulWithDerivativeT) {
		// if mulWithDerivativeT : input has size of 'o', so use those steps; size_i is only used in clipping and that should use original size in Tdim.
		for(int k=0;k<ndimsIn;k++) {
			step_i[ k ] = step_o[ k ];
			size_i[ k ] = size_o[ k ];
		}
		size_i[0] = newszT;
	};

	step_i[ ndimsIn ] = 0; // extra element to always save step of tag-along dimension.
	step_o[ ndimsIn ] = 0; // extra element to always save step of tag-along dimension.
	if (!affineT) {
		int_t tempStep_T[MaxNrOfDims];
		tempStep_T[0]=1;
		for(int k=1;k<ndimsT;k++) {
			tempStep_T[ k] = tempStep_T[ k-1] * sizeT[ k-1];
		}
		for(int k=0;k<ndimsIn;k++) {
			if (k<ndimsT) {
				step_T[ k ] = tempStep_T[ Tdim_T[k] ];
				size_T[ k ] = sizeT[    Tdim_T[k] ];
			} else {
				step_T[ k ] = 0;
			}
		}
	}
	}// end of restrict scope of tempStep*

	int_t nColumns = 1;
	int_t nTag = 1;
	for(int k=1;k<ndimsIn;k++) {
		if (k<ndimsT) {
			nColumns *= size_i[ k ]; // multiply all sizes in non transform dimensions specified in T.
		} else {
			nTag *= size_i[ k ]; // multiply all sizes in tag-along dimensions.
		}
	}
	if (mulWithAdjointDerivativeT) {
		// modify sizeOut, exclusively for actual output, not for anything else.
		int k=0;
		for ( ; (k < ndimsT) && ((k<2) || !affineT) ; k++ ) {
			sizeOut[k] = sizeT[k];
		}
		for ( ; k<ndimsIn ; k++ ) {
			sizeOut[k] = 1;
		}
	}
	if (affineT) {
		tempspacebytesneeded += sizeof(double) * ndimsT;
	} else {
		if (mapTd==NULL) {
			// check if size of in and size of T match in non transform dimensions.
			for (int k = 0 ; k<ndimsIn; k++) {
				int szT  = 1;
				if ((k<ndimsT)!=(Tdim_T[k] < ndimsT)) { // 
					mexErrMsgTxt("Tag-along dimensions are not grouped in T.");
				} else if (Tdim_T[k]< ndimsT)  {
					szT = sizeT[ Tdim_T[k] ];
				}
				int_t sz_o = size_o[ k ];
				if (szT!=sz_o) {
					if ((szT!=1)|(k<ndimsT)) {
						mexErrMsgTxt("Size of T does not match size of 'o' and mapT not specified.");
					}
				}
			}
			
		} else { // mapTd!=NULL
			// check if all points of 'o' map to valid sample locations in T.
			int_t cumszT = 1;
			for (int k=0; k < ndimsT ; k++ ) {
				tempspacebytesneeded += cumszT*sizeof(double);
				int origdimk = Tdim_T[k];
				cumszT *= sizeT[ origdimk ];
				double p1 = mapTd[0 + 2*k];
				double p2 = mapTd[0 + 2*k] + mapTd[1 + 2*k] * (size_o[k] -1);
				if ((p1<0) || (p2<0) || (p1 >= sizeT[ origdimk ] - bsplineordT) || ( p2 >= sizeT[origdimk ] -bsplineordT)) {
					mexErrMsgTxt("Invalid mapT, all points of 'o' should map inside of T. location in T permuted = mapT(1,:) + index_o_permuted .* mapT(2,:)");
				}
			}
		}
	}

	// check if tag along dimensions are consecutive in both input and output.
	bool tagDimsConsecutive = true;
	for (int k=ndimsT+1;k<ndimsIn;k++) {
		tagDimsConsecutive &= ((Tdim_i[k]-1)==Tdim_i[ k-1 ]);
		tagDimsConsecutive &= ((Tdim_o[k]-1)==Tdim_o[ k-1 ]);
	}
	if(!tagDimsConsecutive) {
		mexErrMsgTxt("Tag-along dimensions are not grouped in In or Out.");
	}

	
	// AR
	double * ARd = NULL;
	int_t nAR, ARrunoutLen;
	if (prefilterwithAR) {
		if ((mxGetNumberOfDimensions( AR )>2) | (mxGetClassID( AR ) != mxDOUBLE_CLASS) | mxIsComplex( AR )) {
			mexErrMsgTxt("AR should be a double precision non complex vector. (Other types can easily be added)");
		}
		ARd =  mxGetPr( AR );
		nAR  = mxGetNumberOfElements( AR );
		ARrunoutLen = (int_t) ARd[0]; // read runoutlength value from first element of AR vector
		int vectorisecnt = 1;
		if ((!mulWithDerivativeT) && (!computeDerivative) && (nTag==1) && (inI==NULL) ){//&& (affineT) && (Td[1]==0)) {
			vectorisecnt = nveclines;
			dovecLines = false;//true;
			tempspacebytesneeded += sizeof(int) * vectorisecnt;
		}
		tempspacebytesneeded += (size_i[ 0 ] + 2*ARrunoutLen) * ((iscomplex) ? 2 : 1 ) * imgtypesize * vectorisecnt;
	}

    

    // Read number of threads and allocate temporary memory:
#ifdef USEOMP
    int olddynamic =  omp_get_dynamic();
    omp_set_dynamic(1);
	using std::min;
	int numThreadsUse = min( omp_get_max_threads(), MAXNUMTHREADS );//omp_get_num_procs();
//	printf("omp_get_dynamic: %d, omp_get_num_procs: %d, omp_get_max_threads: %d \n",olddynamic,numThreadsUse, omp_get_max_threads() );
	if (ndimsT<2) {
		numThreadsUse = 1;
	} else if (ndimsT==2) {
		numThreadsUse = (size_o[ndimsT-1] + nveclines - 1) / nveclines;
	} else if (size_o[ndimsT-1]<numThreadsUse) {
		numThreadsUse = size_o[ndimsT-1];
	}
    omp_set_num_threads( numThreadsUse );	
	int maxthreadsuse = numThreadsUse; //omp_get_max_threads();
#else
	int maxthreadsuse = 1;
#endif
	if ((maxthreadsuse>1) && mulWithAdjointDerivativeT ) {
		int_t cumsz = 1;
		for (int k=0; k<ndimsIn; k++) {
			cumsz *= sizeOut[k];
		}
		tempspacebytesneeded += cumsz * imgtypesize;
	}
	char * tempspaceptr = NULL;
    if (tempspacebytesneeded>0) {
#ifdef ADDGUARDS 
		tempspacebytesneeded += 8*10;
#endif
		tempspacebytesneeded += 16 + 12;
		tempspacebytesneeded *= maxthreadsuse;
        tempspaceptr = (char *) mxMalloc( tempspacebytesneeded);
    }
	tempspace tmp = tempspace(tempspaceptr, tempspacebytesneeded );
        
	/* Create output. */
	if (ndimsIn==1) {
		sizeOut[1] = 1;
		plhs[0] = mxCreateNumericArray((int) 2, sizeOut , imgclass , iscomplexArg);
	} else {
		plhs[0] = mxCreateNumericArray((int) ndimsIn, sizeOut , imgclass , iscomplexArg);
	}
	double * outR = mxGetPr(plhs[0]);
    double * outI = NULL;
	if (iscomplex) {
		outI = mxGetPi(plhs[0]);
	}
	/*if (ncropout!=0) {
		for(int k=0;k<ndimsIn;k++) {
			outR += stepOut_p[ k] * ncropout;
		}
	}*/
    double * doutdposR = NULL;
    double * doutdposI = NULL;
    if (computeDerivative) {
        plhs[1] = mxCreateNumericArray((int) ndimsIn, sizeOut , imgclass , iscomplexArg);
        doutdposR = mxGetPr(plhs[1]);
        if (iscomplex) {
            doutdposI = mxGetPi(plhs[1]);
        }
    }

	int_t stepIn_Tdim  = (doAdjoint ? step_o[ 0 ] : step_i[ 0 ]);
	int_t stepOut_Tdim = (doAdjoint ? step_i[ 0 ] : step_o[ 0 ]);
	int_t stepIn_Tag   = (doAdjoint ? step_o[ ndimsT ] : step_i[ ndimsT ]);
	int_t stepOut_Tag  = (doAdjoint ? step_i[ ndimsT ] : step_o[ ndimsT ]);

	/* Final initialisations that need to be done in each computing thread seperately.

	All previous initialized variables should not be modified anymore; (except for the content of the output array(s), of which each thread computes a part);
	*/
#ifdef PERFORMANCECOUNTING
	timeCntT tend_initialization;// = __rdtsc(); 
	PERFORMANCECOUNTER( tend_initialization );
	time_used[0] = tend_initialization - tstart_initialization;
#endif

#ifdef USEOMP
	int_t errorcount = 0;
#pragma omp parallel default(none) shared(inR, inI, outR, outI, doutdposR, doutdposI, size_i, size_o, size_T, tmp, ndimsT, step_i, step_o, step_T, clip_o, clip_i, imgclass, F, mF, nFsteps, AR, nAR, ARrunoutLen, doAdjoint, affineT, mulWithAdjointDerivativeT, Td, mapTd, bsplineordT, ndimsIn, nTag, stepIn_Tdim, stepIn_Tag, stepOut_Tdim, stepOut_Tag, mulWithDerivativeT, Fd, bsplineordi, ARd , errorcount, sizeOut, dovecLines) 
//private()        
	{ try {
	int thread_num = omp_get_thread_num();
	tempspace tmpl = tmp.getParalellPart();
#else
	int thread_num = 0;
	tempspace tmpl = tmp;
#endif

	// create filter sampler:
	typedef filter<double> filtType;
	filtType filt = filtType(Fd, mF, nFsteps , bsplineordi);

	// read clipping planes:	
	typedef clippedRangeND<double> clipT;
	vector< clipT > clips_o = build_clipranges<clipT>( clip_o , size_o, ndimsT );
	vector< clipT > clips_i = build_clipranges<clipT>( clip_i , size_i, ndimsT );

	double * outRl = outR;
#ifdef USEOMP
	// if multithreading, split last dimension in number of threads parts and assign each thread 1 part:
	int num_threads = omp_get_num_threads( );
	if (num_threads>1) {
		if (mulWithAdjointDerivativeT ) {
			int_t cumsz = 1;
			for (int k=0; k<ndimsIn; k++) {
				cumsz *= sizeOut[k];
			}
			outRl = tmpl.get<double>( cumsz );
			for ( int_t k = 0 ; k< cumsz; k++ ) { 
				outRl[k]= 0; 
			}
		};
		range<double> rng_lastdim ={(double) (((int_t)  thread_num    * (int_t) size_o[ndimsT-1])/ (int_t) num_threads    ),
									(double) (((int_t) (thread_num+1) * (int_t) size_o[ndimsT-1])/ (int_t) num_threads - 1)  };// integer division.
		clips_o[ndimsT-1].setOuterLim0( rng_lastdim );
	}
#endif
	// prepare transformation:
	typedef separableTransform<double, int_t> transformType;
	transformType * transformptr = NULL;

	typedef separableTransformAdjoint<double, int_t> transformAdjointType;
	transformAdjointType * transformAdjointptr = NULL;

	typedef bSplineSampleVect<-1, double> interpolateT;
	vector< interpolateT > interpolators(0);
	/*if (false) {
		// debug test
		tempspace tmpcopy = tmpl;
		typedef vec<double,4> vT;
		vT * tst = tmpcopy.get< vT >( 100 );
		char * tstlp = (char *) tst;
		vT tot = (vT) 0.0;
		double frac = 1.23;
		for (int k = 0; k < 100; k++, tstlp+=sizeof(vT) ) {
			tot += (*((vT *) tstlp)) * frac ;
			frac+=0.234;
		}
		tot.store( inR +0 );
	}*/
	double * Tscratch = NULL;
	double ** transformed = NULL;
	if (affineT) {
		Tscratch = tmpl.get<double>( ndimsT );
		if (mulWithAdjointDerivativeT) {
			transformAdjointptr = new transformAffine1DAdjoint<MaxNrOfDims, double ,int_t>( outRl, ndimsT );
		} else {
			Tscratch[ndimsT-1] = Td[ndimsT];
			transformptr = new transformAffine1D<MaxNrOfDims, double ,int_t>( Td, ndimsT , size_o[0] , nveclines);
		}
	} else {
		// store steps of dimensions after permutation of T.
		
		if (mapTd==NULL ) {
			if (step_T[0]!=1) {
				mexErrMsgTxt("When not resampling T (mapT==[]) it is required that dimorder(2,1)==1.");
				// NOTE: test is slightly less restricive (and correct in that); but the difference is not worth mentioning in the error message.
			};
			if (mulWithAdjointDerivativeT) {
				transformAdjointptr = new transformGeneralAdjoint<MaxNrOfDims, double ,int_t>( outRl, ndimsT, step_T);
			} else {
				transformptr = new transformGeneral<MaxNrOfDims, double ,int_t>( Td, ndimsT, step_T );
			}
		} else {
			interpolators.resize(ndimsT);
			transformed = tmpl.get<double *>( ndimsT+1 );
			if (mulWithAdjointDerivativeT) {
				transformed[ ndimsT ] = outRl;
			} else {
				transformed[ ndimsT ] = Td;
			}
			for (int dim = ndimsT-1 ; dim>=0; dim-- ) { 
				int_t prodszT = 1;
				for (int k = 0 ; k<dim; k++) {
					prodszT *= size_T[k];
				}
				transformed[ dim ] = tmpl.get<double>( prodszT );
				if (mulWithAdjointDerivativeT) {
					// only clear for adjoint; forward overwrites
					for (int k=0; k< prodszT; k++ ){
						transformed[dim][k] = 0;
					}
				}
				interpolators[ dim ] = interpolateT( transformed[dim+1], 1, prodszT, prodszT, size_T[dim], bsplineordT);

			}
			if (mulWithAdjointDerivativeT) {
				transformAdjointptr = new transformGeneral1DinterpAdjoint<MaxNrOfDims,  interpolateT, double ,double ,int_t>( &transformed[0], &interpolators[0], ndimsT, mapTd);
			} else {
				transformptr = new transformGeneral1Dinterp< MaxNrOfDims,  interpolateT, double , double, int_t>( &transformed[0], &interpolators[0], ndimsT, mapTd);
			}
		}

	} 
	transformType & transform = transformptr[0];
	transformAdjointType & transformAdjoint = transformAdjointptr[0];
	// create iterator
	typedef NDsubspace<double, clipT > subRitT;
	vector<int_t> steps(ndimsIn);
	for (int k= 0 ; k < ndimsIn ; k++ ) {
		steps[k] = step_o[k];
	}
	subRitT subrit( outR, steps, clips_o );
	typedef subRitT::iterator_lineT line_itT;
	vector< int_t > currentIndex_i(ndimsIn+1);
	vector< int_t > currentIndex_o(ndimsIn+1);
	currentIndex_i[ndimsIn] =0;
	currentIndex_o[ndimsIn] =0;

	int_t * multiLinesIdx = NULL;
	if (dovecLines) {
		multiLinesIdx  = tmpl.get<int_t>( nveclines );
	}

	//nColumns = 1; // enable to DEBUG evaluate only one line.

	
	vector<int> prevpos(ndimsT);
	if (mulWithAdjointDerivativeT) {
		for (int dim = 0 ; dim < ndimsT; dim++ ) {
			prevpos[dim] = 0;
		}
	}

	/* Start the actual computations:*/
	for( line_itT lineit = subrit.begin_lines(); lineit != subrit.end() ; ++lineit ) {

		const vector<int> & curpos = lineit.curPos();
		range<int > range_o = lineit.linerange();

		for (int dim = subrit.highestDimUpdatedLastStep(); dim>0; dim-- ) {
			clips_i[0].update(dim, curpos[dim]);
		}
		range<int > range_i; //= {0, (int) (doAdjoint ? sizeOut_p[0] : size_i[0])-1};
		{
		range<clipT::value_type> tmp_rng =  clips_i[0].getRange();
		if (!(tmp_rng.first<=tmp_rng.last)) { // do check in floating point, as integer conversion might overflow to invalid numbers. Check such that nans 'continue'
			continue;
		}
		range_i.first = (int) ceil(tmp_rng.first);
		range_i.last = (int) floor(tmp_rng.last);
		// range_i.first might actually still be range_i.last+1; but dont check for that uncommon situation as always checking probably costs more than the overhead of the occasional such input. Nothing (should) breaks.
		}
		/*if (range_i.first>range_i.last) {
			// if we cant read anything from the current input line, start next iteration.
			continue;
		}*/

		for (int dim = subrit.highestDimUpdatedLastStep(); dim>0; dim-- ) {
			currentIndex_i[dim] = currentIndex_i[dim+1] + step_i[  dim  ] * curpos[dim];
			currentIndex_o[dim] = currentIndex_o[dim+1] + step_o[  dim  ] * curpos[dim];
		}
		currentIndex_i[0] = currentIndex_i[1];
		currentIndex_o[0] = currentIndex_o[1] + step_o[  0 ] * curpos[0];


		if (mulWithAdjointDerivativeT) { // mulAdjointDerivative
			for (int dim =1 ; dim <= subrit.highestDimUpdatedLastStep(); dim++ ) { // maxDimDifferent is maximum dimension in which point i (might) differs from point i-1
				transformAdjoint.updateAdjoint( dim , prevpos[dim] );
				prevpos[ dim ] = curpos[dim];
			}
			int_t currentIndex  = currentIndex_i[0];
			if ( imgclass == mxDOUBLE_CLASS ) {
				mulWithTransformDerivativeAdjoint<int>( adp(inR, currentIndex)  , nTag, 
							   transformAdjoint, range_o ,   stepIn_Tdim , stepIn_Tag );
			} else {
				mexErrMsgTxt("Bad type.");
			} 
			continue;
		}

		bool dovecLinesThisTime = dovecLines && lineit.can_get_lines_vec( nveclines );

		for (int dim = subrit.highestDimUpdatedLastStep(); dim>(dovecLinesThisTime?1:0); dim-- ) {
			transform.update(dim, curpos[dim]);
		}
		int initialcurpos1 = curpos[1];
		if (dovecLinesThisTime) {
//			typedef double * posType;
			int initialcurpos0 = curpos[0];
			range_o = lineit.get_lines_vec_range( nveclines );
			for (int extralineidx = 1; extralineidx < nveclines; extralineidx++ ) {
				clips_i[0].update(1, initialcurpos1 + extralineidx );
				{
				range<clipT::value_type> tmp_rng =  clips_i[0].getRange();
				if (tmp_rng.first<=tmp_rng.last) { 
					using std::min;
					using std::max;
					range_i.first = min( range_i.first, (int) ceil(tmp_rng.first) );
					range_i.last  = max( range_i.last , (int) floor(tmp_rng.last) );
				}
				}
			}
			currentIndex_o[0] = currentIndex_o[1] + step_o[  0 ] * range_o.first;
		}

		double * tranformline;
		if (dovecLinesThisTime && !transform.isConstantInDim(1) ) {
			for (int k = 0 ; k< nveclines; ++k) {
				multiLinesIdx[k] = initialcurpos1 + k;
			}
			transform.updateMulti(1, multiLinesIdx, nveclines); 
			tranformline = transform.readMulti( range_o , nveclines) ;
		} else {
			if (ndimsT>1) {
				int dim = 1;
				// transform might be constant in dimension 1, but we do need to call it (at least) once.
				// --- So call always: ---  (a bit inefficient)
				// we could test if subrit.highestDimUpdatedLastStep()>1
				// but currently the only transform that can be constant is the affine transform
				// and that check is about as costly (maybe even more costly) than
				// calling the update.
				transform.update(dim, curpos[dim]);
			};
			tranformline = transform.read( range_o ) ;
		}

		range_o.last = range_o.last - range_o.first;
		range_o.first = 0;
		if (range_o.last<0) {// skip computations when no output needed. 
			continue;
		}
		if (mulWithDerivativeT) {
			int_t currentIndex  = currentIndex_o[0];
			if (imgclass==mxDOUBLE_CLASS) {
				mulWithTransformDerivative<int>( adp(inR, currentIndex) , adp( inI, currentIndex)  , nTag, 
							   tranformline,
							   adp( outR, currentIndex), adp( outI, currentIndex), range_o ,   stepOut_Tdim , stepOut_Tag );
			} else {
				mexErrMsgTxt("Bad type.");
			} 
		} else {
			int_t currentIndexIn  = (doAdjoint ? currentIndex_o[0] : currentIndex_i[0]);
			int_t currentIndexOut = (doAdjoint ? currentIndex_i[0] : currentIndex_o[0]);

			if ( dovecLinesThisTime ) {
				// Special (high performance) case: do nveclines at once.
				if (transform.isConstantInDim(1)) {
					// transform is constant in dim1, so not read multiple times.
					if (imgclass==mxDOUBLE_CLASS) {
						//outR[currentIndex_o[0] + range_o.last*stepOut_Tdim] = inR[currentIndex_i[0] + std::min(range_i.last, range_i.first+((int) tranformline[1]))*stepIn_Tdim];
						typedef double T;
						/*if (false&& (step_o[1] == 1) && (step_i[1]==1) ) {
							typedef vecTptr<T *, nveclines> vecT;
							typedef lineType< vecT > readType;
							typedef lineType< vecT > writeType;
							readType  in_v( vecT(  inR  + currentIndexIn ) , stepIn_Tdim ) ;
							writeType out_v( vecT( outR + currentIndexOut ) , stepOut_Tdim );
							filterLine_F_AR<int>( in_v, out_v, newrange_o, range_i, tranformline, filt, ARd, nAR, ARrunoutLen, tmpl, doAdjoint);
						} else {*/
							/*typedef vecptr_step<T *, nveclines> vecT;
							typedef lineType< vecT > readType;
							typedef lineType< vecT > writeType;
							readType  in_v( vecT(  inR  + currentIndexIn,  (doAdjoint ? step_o[1] : step_i[1]) ) , stepIn_Tdim ) ;
							writeType out_v( vecT( outR + currentIndexOut, (doAdjoint ? step_i[1] : step_o[1]) ) , stepOut_Tdim );*/
						//if (inI==NULL) {
							typedef vecptr_step2<T *, nveclines> readType;
							typedef vecptr_step2<T * , nveclines> writeType;
							readType  in_v(  inR  + currentIndexIn,  (doAdjoint ? step_o[1] : step_i[1]) , stepIn_Tdim ) ;
							writeType out_v( outR + currentIndexOut, (doAdjoint ? step_i[1] : step_o[1]) , stepOut_Tdim );

							filterLine_F_AR<int>( in_v, out_v, range_o, range_i, tranformline, filt, ARd, nAR, ARrunoutLen, tmpl, doAdjoint);
						//} else {}
						//};
					} else if (imgclass==mxSINGLE_CLASS) {
						mexErrMsgTxt("single precision images not implemented yet.");
					} else {
						mexErrMsgTxt("Bad type.");
					} 
				} else {
					// Do multiple lines simultanuously, transform unique for each line.
					//mexErrMsgTxt("Not supported yet.");/*
					if (imgclass==mxDOUBLE_CLASS) {
						typedef double T;
						typedef vecptr_step2<T *, nveclines> readType;
						typedef vecptr<T *, nveclines> posType;
						typedef vecptr_step2<T *, nveclines> writeType;
						readType  in_v(  inR  + currentIndexIn,  (doAdjoint ? step_o[1] : step_i[1]) , stepIn_Tdim ) ;
						writeType out_v( outR + currentIndexOut, (doAdjoint ? step_i[1] : step_o[1]) , stepOut_Tdim );
						posType   transformlines(  tranformline  ) ;

						filterLine_F_AR<int>( in_v, out_v, range_o, range_i, transformlines, filt, ARd, nAR, ARrunoutLen, tmpl, doAdjoint);
					} else if (imgclass==mxSINGLE_CLASS) {
						mexErrMsgTxt("single precision images not implemented yet.");
					} else {
						mexErrMsgTxt("Bad type.");
					} // */
				}
			} else {
				// default 1 line case
				if (imgclass==mxDOUBLE_CLASS) {
					//outR[currentIndex_o[0] + range_o.last*stepOut_Tdim] = inR[currentIndex_i[0] + std::min(range_i.last, range_i.first+((int) tranformline[1]))*stepIn_Tdim];
					
					TransformCore<int>( adp(inR, currentIndexIn) , adp( inI, currentIndexIn)  ,					  							    range_i, stepIn_Tdim , nTag, stepIn_Tag  ,
								   tranformline,
								   adp( outR, currentIndexOut), adp( outI, currentIndexOut), adp(doutdposR, currentIndexOut), adp(doutdposI, currentIndexOut), range_o ,   stepOut_Tdim , stepOut_Tag , 
								   filt, ARd, (int) nAR, (int) ARrunoutLen, tmpl, doAdjoint); // */
				} else if (imgclass==mxSINGLE_CLASS) {
					mexErrMsgTxt("single precision images not implemented yet.");
					/*float * inRf = (float *) inR;
					float * inIf = (float *) inI;
					float * outRf = (float *) outR;
					float * outIf = (float *) outI;
					float * doutdposRf = (float *) doutdposR;
					float * doutdposIf = (float *) doutdposI;

					TransformCore<int>( adp(inRf, currentIndex_i[1]) , adp( inIf, currentIndex_i[1])  ,					  							    range_i, stepIn_Tdim, nTag, stepIn_Tag ,
								   tranformline,
								   adp( outRf, currentIndex_o[0]), adp( outIf, currentIndex_o[0]), adp(doutdposRf, currentIndex_o[0]), adp(doutdposIf, currentIndex_o[0]), range_o   ,   stepOut_Tdim, stepOut_Tag, 
								   filt, ARd, (int) nAR, (int) ARrunoutLen, tmpl, doAdjoint);*/
				} else {
					mexErrMsgTxt("Bad type.");
				} 
			}
		}
	}

	if (mulWithAdjointDerivativeT) { // mulAdjointDerivative
		for (int dim =1 ; dim < ndimsT; dim++ ) { // maxDimDifferent is maximum dimension in which point i (might) differs from point i-1
			transformAdjoint.updateAdjoint( dim , prevpos[dim] );
		}
#ifdef USEOMP
		// if multithreading, join transform parameters in critical section.
		if (num_threads>1) {
			int_t cumsz = 1;
			for (int k=0; k<ndimsIn; k++) {
				cumsz *= sizeOut[k];
			}	
			#pragma omp critical
			{
				for (int_t k=0; k<cumsz; k++ ) {
					outR[k] += outRl[k];
				}
				#pragma omp flush(outR)
			}
		}
#endif
	}

#ifdef ADDGUARDS 
	if (affineT) {
		tmpl.checkGuard( Tscratch, ndimsT );
	} else {
		if (mapTd!=NULL ) {
			tmpl.checkGuard( transformed , ndimsT+1 );
			for (int dim = ndimsT-1 ; dim>=0; dim-- ) { 
				int_t prodszT = 1;
				for (int k = 0 ; k<dim; k++) {
					prodszT *= size_T[k];
				}
				tmpl.checkGuard( transformed[ dim ], prodszT );
			}
		}
	}
#endif
	delete &transform;
#ifdef USEOMP
	} catch(...) {
		// catch all exceptions. 
		#pragma omp atomic
		errorcount++;
	}
	} // end of paralell section.
	if (errorcount>0) {
		mexErrMsgTxt("Some error encountered in parallel section.");
	}
	omp_set_dynamic(olddynamic);     
#endif
#ifdef PERFORMANCECOUNTING
	timeCntT tend_processing; // = __rdtsc();
	PERFORMANCECOUNTER( tend_processing );
	time_used[1] = tend_processing - tend_initialization;
#endif
	if (tempspacebytesneeded>0) {
		mxFree(tempspaceptr);
	}
#ifdef PERFORMANCECOUNTING
	timeCntT tend_mexfunction;// = __rdtsc(); 
	PERFORMANCECOUNTER( tend_mexfunction );
	time_used[2] = tend_mexfunction - tend_processing;
	for (int k = 0 ; k<2; ++k) {
		outR[k] = (double) time_used[k];
	}
#endif
#endif	// end of else of ifdef BUILDTESTFUN_TRANSFORMCOREGENERAL
}
#endif // end of ifndef NOMEXFUN
#endif // end of else of ifdef BUILDING_CORE
