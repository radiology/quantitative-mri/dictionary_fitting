//#ifndef VECB
#include "mex.h"
#include <math.h>
#define MEX

#define CHECKPOINTERBOUNDS
#include "guardpointer.cpp"
#include "emm_vec.hxx" // include vec.hxx/emm_vec.hxx to test the default/optimized implementation. 

#include "emm_vec_tests.hxx"



/*
 * This is a MEX-file for MATLAB.
 *  Compile with: (add -g for debugging, add -O for code optimization while debugging)
 *  mex test_emmvec.cpp -I".\Tools\supportingfiles\" -DINCLUDE_SSE4
 *   NOTE: Visual studio (or MATLAB on windows) occasionally misaligns the stack
 *         This causes memory errors when the xmm registers are pushed to stack. 
 *         If such errors occur, please test with gcc under linux. 
 *
 *  Or on linux:
 *    mex test_emmvec.cpp -I"./" -I"./Tools/supportingfiles/" -v CXXFLAGS="\$CXXFLAGS -march=barcelona -std=c++0x" LDFLAGS="\$LDFLAGS " -DMEX
 *
 *  Performes tests of the emm_vec implementations
 *  prints 'succes' if the tests are sucessfull, otherwise errors should appear.
 *
 * CALL EXAMPLE
 *  test_emmvec()
 * INPUTS:
 *  none
 * OUTPUTS :
 *  none
 *
 *
 */


void performtests() {
#ifdef _MSC_VER	
  //mexPrintf("Compiled with: %d\n", _MSC_VER);
#endif
  test_vecLoadStore<double, double, 2>::test();
  test_vecLoadStore<double, double, 3>::test();
  test_vecLoadStore<double, double, 4>::test();
  test_vecLoadStore<double, double, 8>::test();
//  test_vecLoadStore<double, float, 4>::test();
//  test_vecLoadStore<double, float, 8>::test();
//  test_vecLoadStore<float, double, 4>::test();
//  test_vecLoadStore<float, double, 8>::test();

  test_vecLoadStore<int32_t, int32_t, 2>::test();
  test_vecLoadStore<int32_t, uint32_t, 2>::test();
  test_vecLoadStore<int32_t, int64_t, 2>::test();
  test_vecLoadStore<int32_t, uint64_t, 2>::test();
  test_vecLoadStore<uint32_t, int32_t, 2>::test();
  test_vecLoadStore<uint32_t, uint32_t, 2>::test();
  test_vecLoadStore<uint32_t, int64_t, 2>::test();
  test_vecLoadStore<uint32_t, uint64_t, 2>::test();
  test_vecLoadStore<int64_t, int32_t, 2>::test();
  test_vecLoadStore<int64_t, uint32_t, 2>::test();
  test_vecLoadStore<int64_t, int64_t, 2>::test();
  test_vecLoadStore<int64_t, uint64_t, 2>::test();
  test_vecLoadStore<uint64_t, int64_t, 2>::test();
  test_vecLoadStore<uint64_t, uint32_t, 2>::test();
  test_vecLoadStore<uint64_t, int64_t, 2>::test();
  test_vecLoadStore<uint64_t, uint64_t, 2>::test();

  test_vecLoadStore<int32_t, int32_t, 3>::test();
  test_vecLoadStore<int32_t, uint32_t, 3>::test();
  test_vecLoadStore<int32_t, int64_t, 3>::test();
  test_vecLoadStore<int32_t, uint64_t, 3>::test();
  test_vecLoadStore<uint32_t, int32_t, 3>::test();
  test_vecLoadStore<uint32_t, uint32_t, 3>::test();
  test_vecLoadStore<uint32_t, int64_t, 3>::test();
  test_vecLoadStore<uint32_t, uint64_t, 3>::test();
  test_vecLoadStore<int64_t, int32_t, 3>::test();
  test_vecLoadStore<int64_t, uint32_t, 3>::test();
  test_vecLoadStore<int64_t, int64_t, 3>::test();
  test_vecLoadStore<int64_t, uint64_t, 3>::test();
  test_vecLoadStore<uint64_t, int32_t, 3>::test();
  test_vecLoadStore<uint64_t, uint32_t, 3>::test();
  test_vecLoadStore<uint64_t, int64_t, 3>::test();
  test_vecLoadStore<uint64_t, uint64_t, 3>::test();

  test_vecLoadStore<int32_t, int32_t, 4>::test();
  test_vecLoadStore<int32_t, uint32_t, 4>::test();
  test_vecLoadStore<int32_t, int64_t, 4>::test();
  test_vecLoadStore<int32_t, uint64_t, 4>::test();
  test_vecLoadStore<uint32_t, int32_t, 4>::test();
  test_vecLoadStore<uint32_t, uint32_t, 4>::test();
  test_vecLoadStore<uint32_t, int64_t, 4>::test();
  test_vecLoadStore<uint32_t, uint64_t, 4>::test();
  test_vecLoadStore<int64_t, int32_t, 4>::test();
  test_vecLoadStore<int64_t, uint32_t, 4>::test();
  test_vecLoadStore<int64_t, int64_t, 4>::test();
  test_vecLoadStore<int64_t, uint64_t, 4>::test();
  test_vecLoadStore<uint64_t, int32_t, 4>::test();
  test_vecLoadStore<uint64_t, uint32_t, 4>::test();
  test_vecLoadStore<uint64_t, int64_t, 4>::test();
  test_vecLoadStore<uint64_t, uint64_t, 4>::test();//*/


  // Perform test for all implemented vec types:
  test_vec< double, 2, vecptr< double* , 2> >::test();
  test_vec< double, 3, vecptr< double* , 3> >::test();
  test_vec< double, 4, vecptr< double* , 4> >::test();
  test_vec< double, 8, vecptr< double* , 8> >::test();
  //test_vec< float, 2, vecptr< float* , 2> >::test();
  //test_vec< float, 3, vecptr< float* , 3> >::test();
  test_vec< float, 4, vecptr< float* , 4> >::test();
  test_vec< float, 8, vecptr< float* , 8> >::test();
  //return;

  test_vec< int32_t , 2, vecptr< int32_t * , 2> , testLims<int32_t>, false>::test();
  test_vec< int32_t , 3, vecptr< int32_t * , 3> , testLims<int32_t>, false>::test();
  test_vec< int32_t , 4, vecptr< int32_t * , 4> , testLims<int32_t>, false>::test();
  test_vec< int64_t , 2, vecptr< int64_t * , 2> , testLims<int64_t>, false>::test();
  test_vec< int64_t , 3, vecptr< int64_t * , 3> , testLims<int64_t>, false>::test();
  test_vec< int64_t , 4, vecptr< int64_t * , 4> , testLims<int64_t>, false>::test();


  test_vec< uint32_t , 2, vecptr< uint32_t * , 2> , testLims<int32_t>, false>::test();
  test_vec< uint32_t , 3, vecptr< uint32_t * , 3> , testLims<int32_t>, false>::test();
  test_vec< uint32_t , 4, vecptr< uint32_t * , 4> , testLims<int32_t>, false>::test();
  test_vec< uint64_t , 2, vecptr< uint64_t * , 2> , testLims<int64_t>, false>::test();
  test_vec< uint64_t , 3, vecptr< uint64_t * , 3> , testLims<int64_t>, false>::test();
  test_vec< uint64_t , 4, vecptr< uint64_t * , 4> , testLims<int64_t>, false>::test();

  test_vec< long , 4, vecptr< long * , 4> , testLims<long>, false>::test();
  test_vec< unsigned long , 4, vecptr< unsigned long * , 4> , testLims<long>, false>::test();

//*/
  // test co-operation with guardpointer:  (just do 1 type since all guardpointer special things share code for all types)
/*  test_vec< double, 2, vecptr< guard_pointer_type< double* >::type , 2> >::test();

  test_vec< double, 2, vecptr_step< guard_pointer_type< double * >::type , 2> >::test();
  test_vec< double, 3, vecptr_step< guard_pointer_type< double* >::type , 3> >::test();
  test_vec< double, 4, vecptr_step< guard_pointer_type< double* >::type , 4> >::test();
  test_vec< double, 8, vecptr_step< guard_pointer_type< double* >::type , 8> >::test();
  //test_vec< float, 2, vecptr_step< float* , 2> >::test();
  //test_vec< float, 3, vecptr_step< float* , 3> >::test();
  test_vec< float, 4, vecptr_step< guard_pointer_type< float* >::type , 4> >::test();
  test_vec< float, 8, vecptr_step< guard_pointer_type< float* >::type , 8> >::test();

  test_vec< int32_t , 2, vecptr_step< guard_pointer_type< int32_t * >::type , 2> , testLims<int32_t>, false>::test();
  test_vec< int32_t , 3, vecptr_step< guard_pointer_type< int32_t * >::type , 3> , testLims<int32_t>, false>::test();
  test_vec< int32_t , 4, vecptr_step< guard_pointer_type< int32_t * >::type , 4> , testLims<int32_t>, false>::test();
  test_vec< int64_t , 2, vecptr_step< guard_pointer_type< int64_t * >::type , 2> , testLims<int64_t>, false>::test();
  test_vec< int64_t , 3, vecptr_step< guard_pointer_type< int64_t * >::type , 3> , testLims<int64_t>, false>::test();
  test_vec< int64_t , 4, vecptr_step< guard_pointer_type< int64_t * >::type , 4> , testLims<int64_t>, false>::test();


  test_vec< uint32_t , 2, vecptr_step< guard_pointer_type< uint32_t * >::type , 2> , testLims<int32_t>, false>::test();
  test_vec< uint32_t , 3, vecptr_step< guard_pointer_type< uint32_t * >::type , 3> , testLims<int32_t>, false>::test();
  test_vec< uint32_t , 4, vecptr_step< guard_pointer_type< uint32_t * >::type , 4> , testLims<int32_t>, false>::test();
  test_vec< uint64_t , 2, vecptr_step< guard_pointer_type< uint64_t * >::type , 2> , testLims<int64_t>, false>::test();
  test_vec< uint64_t , 3, vecptr_step< guard_pointer_type< uint64_t * >::type , 3> , testLims<int64_t>, false>::test();
  test_vec< uint64_t , 4, vecptr_step< guard_pointer_type< uint64_t * >::type , 4> , testLims<int64_t>, false>::test(); 

  test_vec< long , 4, vecptr_step< guard_pointer_type< long * >::type , 4> , testLims<long>, false>::test();
  test_vec< unsigned long , 4, vecptr_step< guard_pointer_type< unsigned long * >::type , 4> , testLims<long>, false>::test(); // */

  test_vec< double, 2, vecptr_step< double* , 2> >::test();
  test_vec< double, 3, vecptr_step< double* , 3> >::test();
  test_vec< double, 4, vecptr_step< double* , 4> >::test();
  //test_vec< double, 8, vecptr_step< double* , 8> >::test(); // segfault due to incomplete optimization and wrong stack alignment by the compiler
  //test_vec< float, 2, vecptr_step< float* , 2> >::test();
  //test_vec< float, 3, vecptr_step< float* , 3> >::test();
  test_vec< float, 4, vecptr_step< float* , 4> >::test();
  test_vec< float, 8, vecptr_step< float* , 8> >::test();

  test_vec< int32_t , 2, vecptr_step< int32_t * , 2> , testLims<int32_t>, false>::test();
  test_vec< int32_t , 3, vecptr_step< int32_t * , 3> , testLims<int32_t>, false>::test();
  test_vec< int32_t , 4, vecptr_step< int32_t * , 4> , testLims<int32_t>, false>::test();
  test_vec< int64_t , 2, vecptr_step< int64_t * , 2> , testLims<int64_t>, false>::test();
  test_vec< int64_t , 3, vecptr_step< int64_t * , 3> , testLims<int64_t>, false>::test();
  test_vec< int64_t , 4, vecptr_step< int64_t * , 4> , testLims<int64_t>, false>::test();


  test_vec< uint32_t , 2, vecptr_step< uint32_t * , 2> , testLims<int32_t>, false>::test();
  test_vec< uint32_t , 3, vecptr_step< uint32_t * , 3> , testLims<int32_t>, false>::test();
  test_vec< uint32_t , 4, vecptr_step< uint32_t * , 4> , testLims<int32_t>, false>::test();
  test_vec< uint64_t , 2, vecptr_step< uint64_t * , 2> , testLims<int64_t>, false>::test();
  test_vec< uint64_t , 3, vecptr_step< uint64_t * , 3> , testLims<int64_t>, false>::test();
  test_vec< uint64_t , 4, vecptr_step< uint64_t * , 4> , testLims<int64_t>, false>::test();

  test_vec< long , 4, vecptr_step< long * , 4> , testLims<long>, false>::test();
  test_vec< unsigned long , 4, vecptr_step< unsigned long * , 4> , testLims<long>, false>::test(); // */
}


void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
  /* Check for proper number of arguments. */
    //const int vlenu =2;
	//typedef vecTptr<double *,vlenu> ptrT;
// sincos
  if ((nrhs!=0)) {
    mexErrMsgTxt("Zero inputs required.");
  } else if ((nlhs!=0)) {
    mexErrMsgTxt("Zero outputs required.");
  }
  
  performtests();

  mexErrMsgTxt("Tests completed.");
}
//#endif // ifndef VECB