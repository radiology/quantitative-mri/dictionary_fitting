function [fval, scale] = designLPfilter_errorfunction(par, ref, w, FTs, compensateindx, ffiltfixed, compgrad)
% Evaluate filter quality for non linear least squares optimization.
% internal funtion of designLPfilter_v4
% Filter = ( ffiltfixed + FT{1}*par ) .* prodk=2..( F_ar_k )
%  with F_ar_k = Fourier transform of AR models
%              = 1./(Far.*conj(Far))
%      with Far = FTs{k} * ar_k
%           ar_k = [1; part_of_par];
% Minimize norm( ( prefilter .* filter -ref).* weight ) 
% fval = ( prefilter .* filter -ref).* weight
%
% INPUTS:
%  par : parameter column vector
%  ref : reference signal, column vector
%  w   : cell array with weights, {same size as ref, [size of regularization of each FT ] }
%  FTs : cell array with 'Fourier transforms' of the parameters
%        First : interpolating filter, 
%        next  : 'AR' models
%  compensateindx : cell array with indexremapping for all 'AR' transforms.
%  ffiltfixed : fixed part of filter, same size as ref or empty (==0)
%  compgrad : boolean, if true second output is jacobian instead of scale
%
% Created by Dirk Poot, Erasmus MC and TUDelft, 
%  based on previous version; this version : 9-10-2013
% 21-9-2018, D.Poot, Erasmus MC: updated, added tests, and renamed from original evaluatefiltlsq_v2.m

regularizeARwithGain = false;%true;
% Parse par:
hasAR = size(FTs{1},2)<numel(par);
scale_part = cell(size(FTs));
if hasAR
    ar = cell(size(FTs));
    startid = 1;
    nparFT = zeros(numel(FTs),1);
    for k=1:numel(FTs)
        endid = startid+max(0, size(FTs{k},2)-(k>1) );
        ar{k} = par(startid:endid-1);
        nparFT(k) = numel(ar{k});
        startid = endid;
    end;
    if endid~=numel(par)+1
        error('wrong size of par');
    end;
    filter = ar{1};
    endaddf = zeros(numel(FTs),1);
else
    filter = par;
    nparFT = [numel(par);zeros(numel(FTs)-1,1)];
    endaddf = 0;
end;

% compute ffilt = ffiltfixed + FT{1}*par :
ffilt = FTs{1}*filter ;
if ~isempty(ffiltfixed)
    ffilt = ffilt + ffiltfixed;
end;
ffilt_orig = ffilt;

fval = cell(size(FTs));
compgrad = nargin>=7 && compgrad && nargout>1;
if compgrad
    % initialize Jacobian evaluation:
    dfvaldpar = cell( numel(FTs) );  % element of f  x  block of par. 
    dscale_partdpar = cell(1,numel(FTs));
end;

% Evaluate prefilter(s) on frequency 0 .. 0.5:
for k=2:numel(FTs)
    if isempty(FTs{k})
        % frequency domain prefiltering: 
        precomplen = size(FTs{k},1); % size should be set correctly!
        scale_part{k} = ref(1:precomplen)./(ffilt(1:precomplen));
        lgscale = log(max(eps, scale_part{k}));
        fval{k} = w{k}.*lgscale.^2;
        if compgrad
            % d scale d par = d scale /d ffilt * d ffilt /d par
            %  = -ref./ffilt.^2 *  FTs{1}(1:precomplen,:);
            %  = -scale_part{k}./ffilt * FTs{1}(1:precomplen,:);
            dscale_partdpar{k} = bsxfun(@times,  -scale_part{k}./(ffilt(1:precomplen)) , FTs{1}(1:precomplen,:)) ;
            
            if 0 
                %fval{k} = (scale_part{k}-1).^2;
                % d fval{k} d par = 2*(scale_part{k}-1) * d scale_part d par
                dfvaldpar{k,1} = bsxfun(@times, 2*(scale_part{k}-1) , dscale_partdpar{k} );
            else
                %fval{k} = log(scale_part{k}).^2;
                % d fval{k} d par = 2*log(scale_part{k})/scale_part{k} * d scale_part d par
                dfvaldpar{k,1} = bsxfun(@times, 2 * w{k}.* lgscale./scale_part{k} , dscale_partdpar{k} );
            end;
        end;
        
    else
        % AR precompensation:
        rc = fastar2rc([1 ar{k}']);
        if any(~isfinite(rc)) || any(abs(rc(2:end))>=1) 
            for k2=2:numel(FTs)
                if isempty(FTs{k2})
                    endaddf(k2) = endaddf(k2-1)+1;
                elseif regularizeARwithGain 
                    endaddf(k2) = endaddf(k2-1)+1;
                else
                    endaddf(k2) = endaddf(k2-1)+numel(ar{k2});
                end;
            end;
            scale_part{k} = inf( size(FTs{k},1),1);
            dscale_partdpar{k} = zeros( size(FTs{k},1), nparFT(k) );
        else
            scale1 = (FTs{k}*[1 ;ar{k}]);
            if isempty(ffiltfixed)
                scgain = 1;
            else
                scgain = (1+sum(ar{k})).^2; % normalize gain of zero frequency when a fixed part is present.
            end;
            scale_part{k} = scgain./real(scale1.*conj(scale1));
        end;
        if 0 
            % debug plot:
            out = zeros(10000,1);out(5000,1)=1;out = filter(1,[1 ar'],out);out = filter(1,[1 ar'],out(end:-1:1)); 
            plot((0:numel(out)-1)/numel(out),abs(fft(out)),freq(1:precompensate),scale')
        end;

        if regularizeARwithGain 
            rc(1)=1; % somehow sometimes numerically not exactly 1.
            [gain,grgain]=rcgain(rc);
            [d1,d2,d3,drcdar] = fastrc2ar(rc);
            loggainsc = .001;
            fval{k} = log(gain)*loggainsc;
            endaddf(k) = endaddf(k-1)+1; % expands fval with scalar.
            if compgrad
                lastusedidx= lastusedidx+1;
                dfvaldpar{k,k} = grgain*drcdar/gain*loggainsc; 
            end;
        else
            logrootmagnsc = .0005; 
            [r, dr] = roots_d([1 ar{k}']);
            fval{k} = logrootmagnsc ./(1-abs(r)) - logrootmagnsc; % root at zero should have zero cost and root a unit circle infinite
            endaddf(k) = endaddf(k-1)+numel(ar{k}); % expands fval with one element per AR coefficient.
            if compgrad
                % regularize with roots
                dabsrda = real(dr .* conj(r*ones(1,numel(ar{k}))))./(abs(r)*ones(1,numel(ar{k}))); 
                dfvaldpar{k,k} = logrootmagnsc * (dabsrda) ./ ((1-abs(r)) * ones(1,numel(ar{k}))).^2;
            end;
        end;
%             dfvaldpar{k,k} = [zeros(endaddf(k-1),size(dfvaldpar{2,k},2));dfvaldpar{2,k};zeros(endaddf(end)-endaddf(k),size(dfvaldpar{2,k},2))];

        if compgrad
            % d scale_part d par = d scale_part d scgain * d scgain / d par + d scale_part d scale1 * d scale1 /d par
            % d scale_part d scgain = 1./real(scale1.*conj(scale1));
            % d scgain / d par  = 2 * (1+sum(ar{k}))
            % d scale_part d scale1 = -2 scale1/ scgain * dscale_partdpar{k}^2
            % d scale1 /d par = FTs{k}(:,2:end)
            dscale_partdpar{k} = real( bsxfun(@times, FTs{k}(:,2:end), (-2/scgain)*(scale_part{k}.^2.*conj(scale1))) );
            rep_ar = ones(1,numel(ar{k}));
            if ~isempty(ffiltfixed)
                % derivative due to scgain:
                dscale_partdpar{k} = dscale_partdpar{k} + scale_part{k} * ((2*(1+sum(ar{k}))./scgain)*rep_ar);
            end;            
        end;
    end;
    ffilt = ffilt .* scale_part{k}( compensateindx{k} );
    
end;

err = ffilt-ref;
fval{1} = w{1} .* err;

if compgrad 
    % d fval /d par = d w /d par * err + w * d err /d par
    % d w /d par = 0
    % d err/d par=  d (ffilt-ref)/d ffilt  * d ffilt /d par
    %               = 1 * d ((FTs{1}*filter+ffiltfixed) .* prod_k scale{k} )/d par
    %               = FTs{1} .* prod_k scale{k} * d filter/d par + (FTs{1}*filter +ffiltfixed) * sum_l d prod_k scale{k} /d ar{l} * d ar{l}/d par  
    %  d filter/d par = dirac (filter is selection of par)
    %  d ar{l}/d par = dirac (ar{l} coefficients is selection of par)
    %  d prod_k scale{k} /d ar{l} = prod_k~=l scale{k} d scale{l}/d ar{l}
    % 
    prod_scale = 1 ; dfvaldpar{1,1} = 0; 
    if numel(FTs)>=2
        for k=2:numel(FTs)
            prod_scale = prod_scale .* scale_part{k}( compensateindx{k} );
        end
        for k=2:numel(FTs)
            dapart = bsxfun(@times, w{1}.*ffilt_orig.*prod_scale./scale_part{k}( compensateindx{k} ) , dscale_partdpar{k}( compensateindx{k},: )); 
            if size(FTs{k},2)==0
                %  frequency domain prefiltering: 
                dfvaldpar{1,1} = dfvaldpar{1,1} + dapart ;
            else
                dfvaldpar{1,k} = dapart;
            end;
        end;
    end;
    dfvaldpar{1,1} = dfvaldpar{1,1} + bsxfun(@times, w{1}.*prod_scale, FTs{1}) ;
    
    
    for r = 1 : numel(fval)
        curnumf = numel(fval{r});
        for c = 1 : numel(FTs )
            if isempty( dfvaldpar{ r, c } )
                dfvaldpar{ r, c } = zeros( curnumf , nparFT(c ) );
            elseif ~isequal(size( dfvaldpar{ r, c }), [curnumf , nparFT(c )])
                error('dfvaldpar element has wrong size => solve bug in the code of this function. ');
            end;
        end;
    end;
    scale = cell2mat(dfvaldpar);
else
    scale = scale_part;
end;
fval = vertcat(fval{:});

function test
%% Test no preconditioning case:
FTs = {randn(31,7)};
compensateindx = {[]};
par = randn( 7,1); 
ref = randn( size(FTs{1},1) ,1);
w = randn(size(ref));
compgrad = true; 
ffiltfixed = randn(31,1);
fun = @(par) designLPfilter_errorfunction(par, ref, {w }, FTs, compensateindx, ffiltfixed, compgrad);
[fval, J] = fun( par );
dummy = validateJacobian( fun, par );
%% test AR and fourier domain preconditioning cases:
FTs = {randn(30,8), randn(3,0), randn(5,3)};
compensateindx = {[], repmat((1:3)',10,1), repmat((1:5)',6,1)};
par = randn( 8+2,1); par(end-1:end)=[.5 .3];
ref = randn( size(FTs{1},1) ,1);
w = randn(size(ref));
compgrad = true; 
ffiltfixed = randn(30,1);
fun = @(par) designLPfilter_errorfunction(par, ref, {w ,.01, 1}, FTs, compensateindx, ffiltfixed, compgrad);
[fval, J] = fun( par );
dummy = validateJacobian( fun, par );
