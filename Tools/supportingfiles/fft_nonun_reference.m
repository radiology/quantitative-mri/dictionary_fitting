function ksamp = fft_nonun_reference( kpos, img, varargin )
% ksamp = fft_nonun_reference( kpos, img, options )
%
% Reference implementation for fft_nonun. fft_nonun_reference evaluates the 
% DFT stated in fft_nonun_plan explicitly and exactly (to within roundoff error). 
% Despite the fft part of the name, this reference function does not use the fft 
% and hence is not 'fast' in the sense fft is. For a low number of k-space samples
% a tuned version of this function might actually be faster than the fft based non 
% uniform fourier transform. 
%
% 11-9-2018, D.Poot, Erasmus MC: Created first version

img_size = size(img );
opts = fft_nonun_plan( 'getOptions', img_size, varargin{:} );
D = numel( img_size ) ;

X = cell( 1, numel(opts.fftDim ) );
[X{:}]=ndgrid( opts.x{ opts.fftDim } );
for k= 1 : numel(X)
    X{k} = X{k}(:);
end;
X = [X{:}];
% permute and reshape image to get 'tag along' dimensions in first dimension and fft dimensions in second:
img = reshape( permute( img, [opts.nonfftDim opts.fftDim]), prod(img_size(opts.nonfftDim)) , prod( img_size(opts.fftDim) ) ); 
% Compute the DFT:
kpos_size = size(kpos);
if size(X,1)*prod( kpos_size(2:end) ) <= 1e8
    ksamp = img * exp(-1i * X * kpos(:,: ) );
else
    % split over k-samples to save memory:
    % Further optimization: Could also split over second dimension of img and thus first dimension of X. 
    nperblock = 5e7;
    nkperblock = max(10, ceil( nperblock/size(X,1) ) );
    ksamp = zeros( size(img,1), prod(kpos_size(2:end)));
    for k = 1 : nkperblock : prod(kpos_size(2:end))
        ke = min( k+nkperblock-1, prod(kpos_size(2:end)) );
        ksamp(:,k:ke) = img * exp(-1i * X * kpos(:,k:ke ) );
    end;
end;

% reshape to correct output size:

ksamp = reshape( ksamp, [img_size( opts.nonfftDim ) kpos_size(2:end) 1]);