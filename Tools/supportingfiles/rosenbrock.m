function [f,g,H] = rosenbrock( x, dosum ) 
% [f,g,h] = rosenbrock( x ) 
% The rosenbrock function
% A 'standard' test function for optimization algorithms
%
% f = sum_(i=1..numel(x)/2)  100*( x( 2*i-1)^2 - x(2*i ) )^2 + (x(2*i-1) -1)^2
%
% INPUTS:
%  x: coordinate vector. Should have even length. 
%
% Global optimum (obtained by setting derivative to zero):
% 0 = df/ dx1 = 4 * C * x1  * (x1^2-x2) + 2 * (1) * (x1-1)
% 0 = df/ dx2 = -2 * C * (x1^2-x2) 
% => x2 = x1^2 
% => 4 * C * x1  * (x1^2-x1^2) + 2 * (1) * (x1-1) ==  0 
%  => x1 = 1  & x2 = 1
%  
%
% see : http://en.wikipedia.org/wiki/Rosenbrock_function
%
% Created by Dirk Poot, Erasmus MC, 11-9-2012

x= reshape(x,2,[]);
if nargin<2
    dosum = true;
end;
    

x1 = x(1,:);
x2 = x(2,:);

doplot = false;
if doplot
    plot(x1,x2,'*');
end;

C = 100;
f =  C* (x1.^2-x2).^2 + (x1-1).^2;
if dosum
    f = sum(reshape(f,dosum,[]));
end;
if nargout>1
    % f = c (x1^2 - x2)^2 + (x1 - 1)^2
    % D[f, x1]
    % df/ dx1 = 4 * C * x1  * (x1^2-x2) + 2 * (1) * (x1-1)
    % D[f, x2]
    % df/ dx2 = -2 * C * (x1^2-x2) 
    % D[f, x1, x1]
    %   = 2 + 8 C x1^2 + 4 C (x1^2 - x2)
    % D[f, x1, x2]
    %   =-4 C x1
    % D[f, x2, x2]
    %   = 2 c
    g = [4*C.*x1.*(x1.^2-x2)+2*(x1-1);
         -2*C.*(x1.^2-x2)];
    if nargout>2
        h11 = 2+8*C.*x1.^2+4*C*(x1.^2-x2);
        h12 = -4*C*x1;
        h22 = 2*C*ones(size(x1));
        i1 = 1:2:numel(x);
        i2 = 2:2:numel(x);
        I = [i1;i1;i2;i2];
        J = [i1;i2;i1;i2];
        V = [h11;h12;h12;h22];
        H = sparse(I,J,V,numel(x),numel(x));
    end;
    g = g(:);
end;