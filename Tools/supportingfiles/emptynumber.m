function val = emptynumber( sz )
% val = emptynumber( sz )
% Returns a special NaN value that can be used to indicate that the value
% is 'empty'. 
% Use isemptynumber( value ) to find which elements are empty.
%
% Created by Dirk Poot, ErasmusMC, 28-4-2014
if nargin<1 
    sz = [];
end;

% set magic nan value:
val = char( typecast(nan,'uint8')  );
val(1:5) = 'Empty';
val = typecast( uint8(val),'double');

if ~isempty(sz)
    val = repmat(val,sz);
end;