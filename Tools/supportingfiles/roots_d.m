function [r , drda ] = roots_d(a)
% [r , drdp ] = roots_d( p )
% Computes roots and derivative of roots of the polynomial p
% Internally uses the standard MATLAB routine roots to locate the roots and
% I then compute the derivative of these roots.
% INPUT:
%  p : polynome (as specified by roots)
% OUTPUTS:
%  r    : roots of the polynome
%  drdp : numel(r) x numel(r), derivative of r w.r.t. a, assuming that the
%         leading coefficient of a is 1:
%           drdp(k,l) = D( r(k), a(l+1) ) 
%
% Created by Dirk Poot, Erasmus MC


r = roots(a);
dadr = zeros(numel(a)-1,numel(r));
for k=1:numel(r)
    % derivative wrt root k
    da = 1;
    for rtlp = 1:numel(r)
        if rtlp == k
            da = conv(da,-1);
        else
            da = conv(da,[1 -r(rtlp)]);
        end;
    end;    
    dadr(:,k) = da(:);
end;
drda = inv(dadr);