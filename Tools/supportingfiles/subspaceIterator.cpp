#ifndef SUBSPACEITERATOR_CPP
#define SUBSPACEITERATOR_CPP
/* This file provides a set of classes dealing with b-splines
 * 
 * Test code by building: mex subspaceIterator.cpp -DBUILDTESTFUN_SUBSPACEIT
 *           and call   : a = subspaceIterator({lims{:}});
 *			 Inspect a for correct results.
 *           don't define 'BUILDTESTFUN_SUBSPACEIT' when including this from external code.
 * 
 */
#include <vector>
#include <iterator>
#include <limits>
#include "lineReadWriters.cpp"

using std::vector;
using std::numeric_limits;



template< typename T> struct range { T first; T last;};

template <typename T> class clippedRangeND{
	const T * A;
	vector< T > rA0; 
	vector< T > temp;
	int ndims;
	int nplanes;
	range<T> outerlim;
public:
	typedef T value_type;
	clippedRangeND() {}; // default constructor; creates invalid object.
	clippedRangeND( const T * A_, const T * b, int nplanes_, int ndims_, range<T> outerlim_ = range<T>(-numeric_limits<T>::infinity(), numeric_limits<T>::infinity())) {
		// Clip an ndims dimensional space by half planes and (optionally) by lower and upper limits, which are only active in 'getRange'.
		// Space defined: all(A * x <= b) & all(x>=lowLim) & all(x<=highLim)  // MATLAB notation,
		// Fill in the position x from highest dimension downward by calling update
		// Calling update invalidates all positions on lower dimensions.
		// Call update for all dimensions 1..ndim-1
		// then getRange returns the range that is left in dimension 0
		// also call update for dimension 0 to call isInside
		//
		// NB: to also find ranges for the non 0 dimension, use (MATLAB) reductor function 'project_convexset'

		A = A_;
		ndims = ndims_;
		nplanes = nplanes_;
		outerlim = outerlim_;
		rA0  = vector<T>( nplanes  );
		for (int k = 0 ; k < nplanes ; k++ ){
			rA0[ k ] = 1/A[k];
		}
		temp = vector<T>( nplanes * (ndims+1) );
		for (int k = 0 ; k < nplanes ; k++ ){
			temp[ (nplanes * ndims) + k ] = -b[k];
		}
	}
	void update(int dim, T newindex) {
		for (int k =0; k< nplanes; k++) {
			temp[k +  nplanes *dim ] = temp[k +  nplanes *(dim+1) ] + newindex * A[k +  nplanes *(dim) ];
		}
	}
	bool isInside() const {
		for (int k = 0 ; k < nplanes; k++ ) {
			if (!(temp[k]<=0)) return false;
		}
		return true;
	}
	void setOuterLim0( range<T> outerlim_ ) {
		outerlim = outerlim_;
	}
	range<T> getRange() const {
		// Solve A(k,0) * x[0] + temp(k,1) <= 0
		//      =>  A>0 : x[0] <= -temp(k,1)/A(k,0)
		//		    A<0 : x[0] >= -temp(k,1)/A(k,0)
		//       for all k, and for each adjust either upper or lower limit when it is found to be more restrictive
		range<T> r = outerlim;
		for (int k =0; k< nplanes; k++) {
			T lim = -temp[ nplanes + k ] * rA0[k];
			if (A[k]<0) {
				if ( r.first < lim) {
					 r.first = lim;
				}
			} else if (A[k]>0) {
				if (r.last>lim) {
					r.last = lim;
				}
			} else {
				// A[k] is neither smaller nor larger than zero (=> 0 or nan)
				// Test temp(k,1) is acceptable
				if (!(temp[ nplanes + k ]<=0)) {
					// make range empty:
					r.first = outerlim.last;
					r.last = outerlim.first; 
				}
			}

		}
		return r;

	}
};

template<typename dataT, typename clipT, typename int_t> class subspace1DRangeIterator;
template <typename dataT, typename NDsubspaceT> class subspaceIterator;

class iteratorEndIndicator {
};

template<typename dataT, typename clipT, typename int_t> class NDsubspace  {
protected:
	dataT * dataptr;
	int ndim;
	const vector< ptrdiff_t > & step;
	vector< clipT > & subspace;
	vector< range<int_t> > ranges;
	vector< int_t > curpos;
	bool atend;
public:
	int highestdimupdated;
public:
	typedef NDsubspace<dataT, clipT, int_t> self;
	typedef subspace1DRangeIterator<dataT, self, int_t> iterator_lineT;
	typedef subspaceIterator<dataT, self> iterator;
	typedef dataT value_type;
	friend class subspace1DRangeIterator<dataT, self, int_t>; // GCC does not allow to use the typedef name instead
	friend class subspaceIterator<dataT, self>;

	NDsubspace( dataT * dataptr_, const vector<ptrdiff_t> & steps_, vector< clipT > & subspaces ) : step(steps_) , subspace(subspaces) 
	{
		dataptr = dataptr_;
		ndim = (int) subspaces.size();
		highestdimupdated = 0;
		ranges = vector< range<int_t> >(ndim);
		curpos = vector< int_t >(ndim);
		//move2begin();// move to begin; not needed since begin_lines() and begin() both move current position to the beginning. 
	}
	void move2begin() {
		// move to start of range:
		atend = false;
		for (int dim = 0; dim<ndim-1 ; dim++ ){
			curpos[dim] =0;
			ranges[dim].first = 0;
			ranges[dim].last  = 0;
		}
		range<typename clipT::value_type> tmp = subspace[ndim-1].getRange();
		ranges[ndim-1].first = (int_t) ceil(tmp.first);
		ranges[ndim-1].last = (int_t) floor(tmp.last);

		curpos[ndim-1] = ranges[ndim-1].first-1;
		next(0); //we moved to one element before beginning and now step to the next one (if there is one).
				// we do it this way since all stepping logic is in next().
	}

	const subspace1DRangeIterator<dataT, self, int_t> begin_lines() {
		move2begin();
		return subspace1DRangeIterator<dataT, self, int_t>( this );
	};

	subspaceIterator<dataT, self> begin() {
		move2begin();
		return subspaceIterator<dataT, self>( this );
	};

	vector< int_t > curPos() {
		return curpos;
	};

	inline iteratorEndIndicator end() {
		return iteratorEndIndicator();
	};
	void stepBack() {
		// set to (possibly invalid) position such that calling next will proceed to the current position.
		curpos[highestdimupdated]--;
		for (int dim =0 ; dim < highestdimupdated; dim++) {
			ranges[dim].last = curpos[dim];
		}
	}
	void next(int dim) {
		// we cant step in dimensions higher than the highest available.
		if (dim>=ndim) {
			atend = true;
			return;
		}
		// step in dimension dim.
		while (dim<ndim) {
			curpos[dim]++;
			if (curpos[dim]>ranges[dim].last) {
				// if we exceeded the current range, proceed stepping in 1 dim higher.
				dim++;
				if (dim>=ndim) {
					// exhaused the range, so we are at the end; return.
					atend = true;
					return;
				}
			} else {
				// valid point found (within current ranges).
				break;
			}
		}
		highestdimupdated = dim;
		// update ranges for all lower dimensions; 'dim' is the dimension for which we are updating the range:
		dim--;
		while (dim>=0) {
			// update full position in subspace[dim] : 
			for (int i = highestdimupdated; i>dim ; i--){ 
				subspace[dim].update( i-dim, curpos[i]);
			}
			// get new range in the subspace that contains dimensions dim and higher.
			range<typename clipT::value_type> tmp = subspace[dim].getRange();
			if (tmp.first<=tmp.last) { // floating point check to avoid problems with integer conversion.
				ranges[dim].first = (int_t) ceil(tmp.first);
				ranges[dim].last = (int_t) floor(tmp.last);
				curpos[dim] = ranges[dim].first;
			} else {
				ranges[dim].last = 0;
				curpos[dim] =1;
			}
			if (curpos[dim]<=ranges[dim].last) {
				dim--;
			} else { 
				// empty range, or range exhaused (by curpos[dim]++), so 
				while (!(curpos[dim]<=ranges[dim].last)) {
					// step to higher dim
					dim++;
					if (dim>=ndim) {
						// if all locations are exhaused, we are at end, and break
						atend = true;
						dim = -1; // exit loop.
						break;
					} else {
						// increase position in next dim, 
						curpos[dim]++;
						if (highestdimupdated<dim) {
							// check if we exceeded the maximum updated dimension so far.
							highestdimupdated = dim;
						}
					}
				}
				
				dim--;
			}
		}
	};
	inline int highestDimUpdatedLastStep() {
		return highestdimupdated;
	}

};

template <typename dataT, typename NDsubspaceT> class subspaceIterator {//: iterator< input_iterator_tag,  lineType0<typename NDsubspaceT::value_type, ptrdiff_t> > {
private:
	NDsubspaceT * src;
public:
	typedef dataT value_type;

	subspaceIterator(NDsubspaceT * src_) {
		src = src_;
	};

	inline void operator++() {
		src->next(0);
	};
	value_type operator*() const {
		dataT curdataptr = src->dataptr;
		for (int dim = 0; dim < src->ndim; dim++){
			curdataptr += src->curpos[dim] * src->step[dim];
		}
		return curdataptr;
	}
	inline bool operator!=(iteratorEndIndicator dum) {
		return !(src->atend);
	}
	inline bool operator==(iteratorEndIndicator dum) {
		return src->atend;
	}
};

template <typename dataT, typename NDsubspaceT, typename int_t> class subspace1DRangeIterator {//: iterator< input_iterator_tag,  lineType0<typename NDsubspaceT::value_type, ptrdiff_t> > {
private:
	NDsubspaceT * src;
public:
	typedef lineType0<typename NDsubspaceT::value_type, ptrdiff_t> value_type;

	subspace1DRangeIterator( NDsubspaceT * src_) {
		src = src_;
	};

	inline void operator++() {
		src->next(1);
	};
	value_type operator*() const {
		dataT curdataptr = src->dataptr;
		for (int dim = 0; dim < src->ndim; dim++){
			curdataptr += src->curpos[dim] * src->step[dim];
		}
		return value_type( curdataptr, src->ranges[0].last-src->ranges[0].first+1);
	}
	bool can_get_lines_vec( int_t nlines ) {
		return src->curpos[1]+nlines-1 <= src->ranges[1].last;
	}
	range<int_t> get_lines_vec_range( int_t nlines ) {
		// WARNING: also increments over vlen lines
		range<int_t> r = src->ranges[0];
		nlines--;
		for (; nlines>0 ; nlines-- ) {
			using std::min;
			using std::max;
			src->next(1);
			if (src->atend) 
				break;
			if ( src->highestDimUpdatedLastStep() > 1 ) {
				src->stepBack();
				break;
			}
			r.first = min( r.first, src->ranges[0].first);
			r.last  = max( r.last , src->ranges[0].last );
		}
		return r;
	}


	inline dataT linestart() {
		dataT curdataptr = src->dataptr;
		for (int dim = 1; dim<src->ndim; dim++){
			curdataptr += src->curpos[dim] * src->step[dim];
		}
		return curdataptr;
	}
	inline range<int_t> linerange() {
		return src->ranges[0];
	}
	inline bool operator!=(iteratorEndIndicator dum) {
		return !src->atend;
	}
	inline bool operator==(iteratorEndIndicator dum) {
		return src->atend;
	}
	inline const vector< int_t > & curPos() {
		return src->curpos;
	}
};

#ifdef BUILDTESTFUN_SUBSPACEIT

#include "mex.h"

// Test code to test the majority of the code in this function. Mainly used to debug the code during development.
void self_test_mex( mxArray * out , const mxArray * in) {
	int ndims = (int) mxGetNumberOfElements(in);
	vector<int> nplanes(ndims);
	vector<double * > ptrs(ndims);
	for (int dim = 0; dim < ndims ; dim++ ){
		mxArray * el = mxGetCell( in, dim );
		if (mxGetN(el)!=ndims-dim+1) {
			mexErrMsgTxt("Invalid number of columns in (at least) one of the elements.");
		}
		nplanes[dim] = (int) mxGetM(el);
		ptrs[dim]    = mxGetPr(el);
	}
	vector<mwSize> sizeOut(ndims);
	vector<ptrdiff_t> steps(ndims);
	for (int dim = 0 ; dim < ndims; dim++) {
		sizeOut[dim] = 50-dim;
	}
	steps[0] = 1;
	for (int dim = 1 ; dim < ndims; dim++) {
		steps[dim] = steps[dim-1]*sizeOut[dim-1];
	}
	mxArray * outim = mxCreateNumericArray(ndims, &sizeOut[0], mxDOUBLE_CLASS, mxREAL);
	mxAddField(out , "insidemask");
	mxSetField(out, 0, "insidemask", outim);
	double * outp = mxGetPr(outim);


	// EXAMPLE USAGE:
	typedef clippedRangeND<double> clipT;

	vector< clipT > clips(ndims);
	for (int dim = 0; dim< ndims; dim++ ) {
		range<double> rng ={0,sizeOut[dim]-1};
		//				    pointer to A  ,     pointer to b                      ,  number of half spaces,  dimensionality , outer limits.   
		clips[dim] = clipT(    ptrs[dim],   ptrs[dim] + nplanes[dim] * (ndims-dim),      nplanes[dim],        ndims-dim,         rng       );
	}
	typedef NDsubspace<double, clipT > subRitT;
	subRitT subrit( outp, steps, clips );
	typedef subRitT::iterator_lineT line_itT;

	// iterate over lines (typically prefered)
	for( line_itT lineit = subrit.begin_lines(); lineit != subrit.end() ; ++lineit ) {
		typedef line_itT::value_type lineT;
		lineT line = *lineit;
		typedef lineT::iterator lineIteratorT;
		for ( lineIteratorT it = line.begin(); it != line.end(); it++ ) {
			// set all voxels within the mask to 1:
			*it = 1;
		}

	}

	typedef subRitT::iterator itT;
	// iterate over voxels (alternative, to have just 1 loop)
	for( itT it = subrit.begin(); it != subrit.end() ; ++it ) {
		// set all voxels within the mask to 2:
		*it = 2;
	}

	// END EXAMPLE USAGE


}


void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
	// Only compile code below for performing a code test.
	if ((nrhs!=1) || (nlhs!=1) || !mxIsCell(prhs[0]) ) {
		mexErrMsgTxt("One cell array input and one output required.");
	}
	//if (

	plhs[0] = mxCreateStructMatrix(2,1,0, NULL);
	self_test_mex( plhs[0] , prhs[0] );
	return;
}
/* MATLAB test code:
	lims2D = {[1 1 170;-1 -1 -20; 1 -1 50; -1 1 50],zeros(0,2)};
	lims3D = {[1 1 .2 170;-1 -1 .2 -20; 1 -1 .2 50; -1 1 .2 50;0 0.1 1 50],zeros(0,3),zeros(0,2)};

	a = subspaceIterator( lims2D );
	a = subspaceIterator( lims3D );
*/
#endif
#endif 