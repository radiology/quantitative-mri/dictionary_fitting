function [varargout] = deal_relaxed( varargin )
% [varargout] = deal_relaxed( varargin )
% This function like deal assigns inputs directy to outputs.
% It only avoids some of the 'limitiations' of deal
% when more outputs than inputs are requested, empty arguments are provided
% for the remaining outputs. Also more inputs than outputs may be given.
% The extra inputs are not used. 
%
% Created by Dirk Poot, TUDelft, 29-11-2013


if nargin<nargout
    varargout = cell(1,nargout);
    varargout(1:nargin)=varargin(1:nargin);
elseif nargin>nargout
    varargout = varargin(1:nargout);
else
    varargout = varargin;
end;