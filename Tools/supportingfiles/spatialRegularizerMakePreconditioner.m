function [Rs, mulfun] = spatialRegularizerMakePreconditioner(hessinfo, ar )
% [Rs,mulfun] = spatialRegularizerMakePreconditioner(H,ar) 
% 
% Creates a preconditioner info structure Rs, and a preconditioner multiplication 
% function mulfun based on the autoregressive filter ar. 
% 
% INPUTS:
%  H : structure that should have a field 'sizeTheta' which specifies the size of x.
%      
%  ar : autoregressive model that is applied in all spatial dimension (fit_MRI)
%       that is: in the dimensions >= 2. 
%       default : ar = [1 -.8];
% 
% OUTPUTS:
%  Rs : structure with preconditioner info
%  mulfun :  Mx = mulfun( Rs, x )
%           multiplies with the preconditioner M
%           which is filtering x as specified in Rs. 
%
% Created by Dirk Poot (d.poot@erasmusmc.nl), TUDelft, 4-2-2014

if isequal(hessinfo,'mulfun')
    Rs =  @spatialRegularizerPreconditionerMultiply;
    return;
end;
if nargin<2
    ar = [1 -.8];
end;

Rs.size = hessinfo.sizeTheta;
Rs.filt = repmat( { ar }, 1,numel(Rs.size));
Rs.filt{1}=[];
if numel(ar)==2
    scale = 1/(1-ar(2).^2);
    Rs.scale_in = repmat({scale}, 1, numel(Rs.size));
else
    Rs.scale_in = cell(1,numel(Rs.size));
end;
mulfun = @spatialRegularizerPreconditionerMultiply;

function [Mx]=  spatialRegularizerPreconditionerMultiply(R, x)

% TODO: create a mex implementation of the below. (is essentially already available as
% prefiltering step in ND image resampling routine).
xr = reshape(x, [R.size size(x,2)]);
for dim = 1 :numel(R.filt)
    if ~isempty(R.filt{dim})
        rev = repmat({':'},1,ndims(xr));rev{dim} = size(xr,dim):-1:1;
        if ~isempty(R.scale_in{dim})
            in = rev;in{dim} = [1 size(xr,dim)];
            xr(in{:}) = xr(in{:}).*R.scale_in{dim};
        end;
        xr = filter(1,R.filt{dim},xr(rev{:}),[],dim);
        xr = filter(1,R.filt{dim},xr(rev{:}),[],dim);
    end;
end;
Mx = reshape(xr,size(x));