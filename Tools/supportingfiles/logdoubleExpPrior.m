function [f, g, h, HI,HJ ] = logdoubleExpPrior( theta, mu, W, offset)
% [f, g, h, HI,HJ ] = logdoubleExpPrior( theta, mu, W, offset)
%
% This implements the logarithm of the Laplace, or Double exponential, distribution
%  https://en.wikipedia.org/wiki/Laplace_distribution
%
% To avoid the non-differntiability, an 'offset' is added.
%
% Standard Laplace distribution:  (for scalar x, mu,b)
%  p_L( x| mu, b) = 1/(2*b) exp( - |x-mu|/b )  
% To generalize to column vector x, and add the offset here we use the not-normalized pdf:
%  p( x | mu, W ) = exp( - sqrt( (x-mu)'*W * (x-mu) + offset^2 ) +offset )
% Then:
%  f = sum_i  sum_j log( p( theta(:, i), mu{j}, W{j} )
%
% INPUTS:
%  theta : parameter vector as used by fit_MRI for which this function specifies the prior.
%  mu  : n element cell array with size(theta,1) x 1 vectors. 
%  W  : n element cell array with size(theta,1) x size(theta,1) (sparse) positive semi definite matrices. 
%  offset : scalar, default = 0.1
% 
% OUTPUTS:
%  f : scalar log likelihood cost as defined above. 
%  g : size theta gradient of f w.r.t. theta
%  h : hessian 
%      Note that h provides a lower bound for the actual hessian. (because log likelihood should be maximized, hessian is negative definite)
%      This is typically beneficial for optimization as close to mu this forces
%      to take small steps in the quadratic sub-problem, thereby avoiding excessive overshoot of the step. 
%      
%
% 27-9-2018, D.Poot, Erasmus MC : Created

% f_ij = log( p( theta(:, i), mu{j}, W{j} )
%      = log( exp( - sqrt( (theta(:, i)-mu{j})'*W{j} * (theta(:, i)-mu{j}) + offset^2 ) +offset ) )
%      = - sqrt( (theta(:, i)-mu{j})'*W{j} * (theta(:, i)-mu{j}) + offset^2 ) +offset 
% With [I,J, V]  = find( W{j} ) % implementation maps all elements of W to upper triangle
%      res = theta(:,i)-mu{j}
% f_ij = - sqrt( dot(res(I, i), V .* res(J, i),1) + offset^2 ) +offset 
%
% D f_ij D theta(k, i) = D( - sqrt( dot(res(I), V .* res(J),1) + offset^2 ) +offset)Dres * Dres/D theta(k, i)
% Dres(k)/D theta(k, i) = 1 
% D( - sqrt( sum(res(I).* V .* res(J)) + offset^2 ) +offset) D res(k)  
%   = -.5*((I==k) .* V .* res(J) +res(I) .* V .* (J==k)) / sqrt( dot(res(I, i), V .* res(J, i),1) + offset^2 )
%   = -.5*( V(I==k) .* res(J(I==k)) +res(I(J==k)) .* V(J==k) ) / sqrt( dot(res(I, i), V .* res(J, i),1) + offset^2 )
%       
% D2 f_ij D res(k) res(l) =
%   = D -.5*( V(I==k) .* res(J(I==k)) +res(I(J==k)) .* V(J==k) ) / sqrt( dot(res(I, i), V .* res(J, i),1) + offset^2 ) D res(l)
%   = -.5*( V(I==k) .* J(I==k)==l + I(J==k)==l .* V(J==k) ) / sqrt( dot(res(I, i), V .* res(J, i),1) + offset^2 )
%     + complicated term that is probably positive and small. 
if nargin<4
    offset = .1;
end;
f =0;
if nargout>1
    g = zeros(size(theta));
    if nargout>2
        hcel = cell( 1, numel(W));
        allactpar = false( size(theta,1),1);
        HindIJ = cell( 1, numel(W));
    end;
end;
for j = 1 : numel(W)
    Wj = triu( W{j} + (W{j}-triu(W{j}))' ); % add(/move) all elements below the diagonal to upper triangle. 
    actpar = find( any(Wj) );
    [I,J,V]= find(Wj(actpar,actpar)); 
    res = bsxfun(@minus, theta(actpar,:), mu{j}(actpar ) );
    sterm = sqrt( V'*(res(I, :).* res(J, :)) + offset^2 );
    f = f +  offset - sterm ;
    
    if nargout>1
        g( actpar, :) = g( actpar, :) + bsxfun(@times, (Wj(actpar,actpar)+Wj(actpar,actpar)')*res , -.5./sterm);
        if nargout>2
            hcel{j} = bsxfun(@times, -.5*(V+V.*(I==J) ) , 1./sterm);
            allactpar(actpar) = true;
            HindIJ{j} = [actpar( I )' actpar( J )'];
        end;
    end;
end;
if nargout>2
    if nnz(allactpar)==1
        h = sum(vertcat(hcel{:}),1);
        HI = find( allactpar ); HJ = HI;
    else
        [HIJ, k, j] = unique( vertcat(HindIJ{:}),'rows');
        allh = vertcat(hcel{:});
        h = allh( k, :); % efficient for common case where each element is set only once. 
        for i = 1 : size(HIJ,1)
            sel = j==i; sel( k(i ) ) = false;
            if any(sel)
                h( i ,:) = h( i ,:) + sum( allh(sel,:),1);
            end;
        end;
        HI = HIJ(:,1); HJ = HIJ(:,2);
    end;
end;

function test()
%% scalar case
mu = {[0]};
W = {[1.23]};
tht = linspace(-3,3,1001);
[f, g, h] = logdoubleExpPrior( tht, mu, W);
plot( tht, [f' g' h'], tht(1:end-1)+.5*diff(tht), [diff(f')./diff(tht'), diff(g')./diff(tht)'])
legend('f','g','h','numgrad','numhess')
%% vector case - weight 1 element:
mu = {[0;1]};
W = {[0 0;0 0.83]};
tht = [randn(1,1001);linspace(-3,3,1001)];
[f, g, h] = logdoubleExpPrior( tht, mu, W);
plot( tht(2,:)', [f' g(2,:)' h'], tht(2,1:end-1)+.5*diff(tht(2,:)), [diff(f')./diff(tht(2,:)'), diff(g(2,:)')./diff(tht(2,:))'])
legend('f','g','h','numgrad','numhess')
%% vector case - diag matrix weight:
mu = {[0;1]};
W = {[.1 0;0 0.83]};
[dum, X1,X2]= ndgrid(1, linspace(-3,3,100), linspace(-3,3,100) );
tht = [X1;X2];
[f, g, h] = logdoubleExpPrior( tht, mu, W);
f = reshape(f,size(X1));
h = reshape(h, size(h,1), size(X1,2),size(X1,3));
imagebrowse(catz(1, f, diff(f,1,2)./diff(X1,1,2), g,diff(f,1,3)./diff(X2,1,3), diff(g(1,:,:),1,2)./diff(X1,1,2), h, diff(g(2,:,:),1,3)./diff(X2,1,3)) );
%% vector case - matrix weight:
mu = {[0;1]};
W = {[.3 .1;.23 0.83]};
[dum, X1,X2]= ndgrid(1, linspace(-3,3,100), linspace(-3,3,100) );
tht = [X1;X2];
[f, g, h] = logdoubleExpPrior( tht, mu, W);
f = reshape(f,size(X1));
h = reshape(h, size(h,1), size(X1,2),size(X1,3));
imagebrowse(catz(1, f, diff(f,1,2)./diff(X1,1,2), g,diff(f,1,3)./diff(X2,1,3), diff(g(1,:,:),1,2)./diff(X1,1,2), h, diff(g(2,:,:),1,3)./diff(X2,1,3)) );
% plot( tht(2,:)', [f' g(2,:)' h'], tht(2,1:end-1)+.5*diff(tht(2,:)), [diff(f')./diff(tht(2,:)'), diff(g(2,:)')./diff(tht(2,:))'])
% legend('f','g','h','numgrad','numhess')
%% vector case - multi matrix weight:
mu = {[0;1],[0;0],[-1;-1]};
W = {[.3 .1;.23 0.83],[0 0 ;0 .234], [.34 .3 ;.12 .634]};
[dum, X1,X2]= ndgrid(1, linspace(-3,3,100), linspace(-3,3,100) );
tht = [X1;X2];
[f, g, h] = logdoubleExpPrior( tht, mu, W);
f = reshape(f,size(X1));
h = reshape(h, size(h,1), size(X1,2),size(X1,3));
description = sprintf( 'value, gradient and hessian, analytical and numerical. Dim 1:\n 1) f\n 2) DfDx1 numerical\n 3) g(1,..) \n 4) g(2,...)\n 5) DfDx2 numerical\n 6) Dg(1,..)Dx1 numerical\n 7) h(1,1,..)\n 8) h(2,2,..) \n 9) Dg(2,..)Dx2 numerical \n 10) Dg(1,..)Dx2 numerical \n 11) h(1,2,..)' );
imagebrowse(catz(1, f, diff(f,1,2)./diff(X1,1,2), g,diff(f,1,3)./diff(X2,1,3), diff(g(1,:,:),1,2)./diff(X1,1,2), h([1 3],:,:), diff(g(2,:,:),1,3)./diff(X2,1,3) ,diff(g(1,:,:),1,3)./diff(X2,1,3), h(2,:,:)) ,[],'description',description);
% plot( tht(2,:)', [f' g(2,:)' h'], tht(2,1:end-1)+.5*diff(tht(2,:)), [diff(f')./diff(tht(2,:)'), diff(g(2,:)')./diff(tht(2,:))'])
% legend('f','g','h','numgrad','numhess')