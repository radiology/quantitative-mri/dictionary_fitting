#ifndef VEC_SUPPORT_HXX
#define VEC_SUPPORT_HXX
/* This file provides a set of templates to see if vec is supported for a given type. 
 *   
 * Include after vec is defined. 
 * 
 * Created by Dirk Poot, Erasmus MC
 */

// Test template
template < typename T> struct IS_vec {
	enum { TF = false };
};
template < typename T, int vlen > struct IS_vec< vec<T, vlen > > {
	enum { TF  = vec_support<T, vlen>::supported };
};
#endif