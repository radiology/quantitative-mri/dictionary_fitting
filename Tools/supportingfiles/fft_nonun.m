function [ksamp] = fft_nonun( img, st )
% [ksamp] = fft_nonun( img, st )
%
% Applies the forward non uniform Fourier transform. I.e. samples at arbitrary 
% points the Fourier transform of img. Use fft_nonun_plan to construct the plan st. 
%
%
% INPUTS: 
%   img : ND array with size equal to the size specified when planning st.
%   st  : planned non uniform FFT structure. Create with fft_nonun_plan. 
%
% OUTPUTS:
%   ksamp : [img_size( nonfftDim ) size(kpos, 2..end)]; non-uniformly sampled k-space samples.
%  
% 11-9-2018, D.Poot, Erasmus MC: Created first version
% 14-3-2019, D.Poot, Erasmus MC: added help

if ~isempty( st.precompensate )
    % TODO: should use a tensor product multiplication to avoid numel(fftDim) passes over img. 
    %       or premultiply the precompensations to have just a single multiplication (but that has excessive memory consumption)
    for d = st.fftDim
        img = bsxfun(@times, img, st.precompensate{d} );
    end;
end;
for d = st.fftDim
    img = fft(img, st.ksize(d), d );
end;
if ~isempty( st.postcompensate )
    for d = st.fftDim
        img = bsxfun(@times, img, st.postcompensate{d} );
    end;
end;
    
if ~isempty( st.permorder )
    img = permute( img, st.permorder);
end;
[ksamp] = TransformNDbspline( img, st.sampleindex, st.mapT, st.newSz, st.F, st.bsplineord, st.AR, st.clip_i, st.boundary_condition);

if ~isempty( st.samplePhaseAdjust )
    ksamp = bsxfun(@times, ksamp, st.samplePhaseAdjust );
end;
