sz = [12 65 55 73]; 
if 1
    % complex:
    r = randn(sz)+1i*randn(sz); 
    c = randn(sz)+1i*randn(sz);
else
    % non complex:
    r = randn(sz); 
    c = randn(sz);
end;
% %
nlp = 5; 
tu1 = zeros(1,4);
tu1b = tu1;
tu2 = tu1;
tu3 = tu1;
for dim=1:numel(tu1);
    tic; for k=1:nlp; out1 = dot( r, c, dim ); end, tu1(dim) = toc; 
    if exist('dot_c','file')
    tic; for k=1:nlp; out1b= dot_c( r, c, dim ); end, tu1b(dim) = toc; 
    else
        tu1b = zeros(0,size(tu1,2));
        out1b = out1;
    end;
    tic; for k=1:nlp; out2 = sum(conj(r).*c, dim ); end, tu2(dim) = toc;
%     tic; for k=1:nlp; out3 = builtin('dot',r, c, dim ); end, tu3(dim) = toc;
end;
isequal(out1,out1b,out2, out3)
[tu1; tu1b; tu2;tu3]/(prod(sz)*nlp)*2.26e9