function [hi, lo] = accurate_gammaln( z ) 
% [hi, lo] = accurate_gammaln( z ) 
% in infinite precision arithmatic:
%  hi + lo = gammaln( z ) + e  , |e|<.5*eps
%
% Improved accuracy version of gammaln.
% This returns a high and low part of the result so that the truncation error
% is <.5 eps for all 181 < z < 1.4e14 
%
% for smaller z, gammaln is used as the precision loss is not too severe
% in those cases and the approximation used here breaks down. 
% (With a bit of work this function could be extended to that range if needed)
%
% 26-4-2017: Created by Dirk Poot, Erasmus MC

% if ~isreal(z) 
%     error('only known correct for real z');
% end;

smallz = real(z) < 65.8368;
hi = zeros(size(z));lo=hi; % initialize. 
if any(smallz(:))
    hi(smallz) = gammaln( z(smallz) );
end;
largez = ~smallz;

if any( largez(:) )
    % From https://en.wikipedia.org/wiki/Gamma_function
    % and Mathematica:
    % Series[ LogGamma[ z ], {z, \[Infinity], 7}]
    % FullSimplify[%]
    %
    % Using approximation based on Stirlings approximation:
    % ln(Gamma(z) ) = z ln( z ) - z - 1/2 ln( z/(2 pi) ) + 1/(12 z) - 1/(360 z^3) + 1/(1260 z^5) 
    %               = (z-.5) ln( z ) - z + 1/2 ln( 2 pi ) + 1/(12 z) - 1/(360 z^3) + 1/(1260 z^5) 
    % .5*eps < | next term | < 1/(1680 z^7 )    
    % => z for which this approximation is accurate:
    %     .5*eps = 1/(1680 z^7) 
    %  => z = ( 1/(1680 * .5 *eps) )^(1/7) = 65.8368
    zl = z( largez );
    
%     ref_inaccurate = (zl-.5) .*log( zl ) - zl + 1/2 .*log( 2 *pi ) + 1./(12* zl) - 1./(360 .*zl.^3) + 1./(1260 .*z.^5) 
    
    [lgz_hi, lgz_lo] = accurate_log( zl );
    
    % we want to compute hi without error, so truncate z and lgz such that
    % z * ln( z ) - z + .5*ln ( 2 *pi ) is exact. 
    % and all 'low' * 'high' terms are as small as possible. 
    %
    % p1 = 26;p2 = 26; v1 = 1 + 2^(-p1);v2 = 1 + 2^(-p2); [((v1.*v2)-1) - 2^(-p1)-2^(-p2), 2^(-p1-p2)]
    % => for z between 1 and 2, smallest step is 2^-26
    
    ez = floor( log2(zl) )-26;
    z_hi = pow2( round( pow2( zl, -ez ) ) , ez) ;
    z_lo = zl-z_hi;
    
    lgz = lgz_hi + lgz_lo;
    elgz = floor( log2( lgz ) ) - 26; % Note floor( log2( log(realmax) ) ) - 26 = -17 ,  because<=0 : -z_hi can exactly be added.
                                        % we can use -17 for rounding 1/2 ln( 2 pi ) 
    lgzr_hi = pow2( round( pow2( lgz, -elgz ) ) , elgz) ;
    lgzr_lo = (lgz_hi-lgzr_hi) + lgz_lo; % this order to avoid the roundoff error in lgz. 

    %f = 1/2  Log[2 Pi]
    %roundpow = 17;
    %2^roundpow
    %l2 = f *%;
    %l2hi = Round[ l2 ]
    %N[f - l2hi*2^(-roundpow), 30]
    lgpiterm_hi = 120447/131072;
    lgpiterm_lo = 8.50099203991780329736405617640e-7;
    
    hi1 = z_hi.* lgzr_hi - z_hi;
    addhiterm = lgpiterm_hi - .5* lgzr_hi ; % exact
    hi_l = hi1 + addhiterm;  % not exact for large z. 
    hi(largez) = hi_l;
    lo(largez) = z_lo.* lgz + z_hi.*lgzr_lo - z_lo + ((hi1-hi_l) +addhiterm)+ lgpiterm_lo -.5*lgzr_lo ...
        + 1./(12.* zl) - 1./(360.* zl.^3) + 1./(1260 .* zl.^5) ;
    % note : 
    %       hi = hi1 + lgpiterm_hi + e    % addition not exact. 
    %       hi + lo = (hi1+ lgpiterm_hi + e + ( hi1- (hi1+ lgpiterm_hi + e) ) + lgpiterm_hi )+ lgpiterm_lo
    %               = 
end;



function test()
%%
lgz = sort(100*rand(1000,1));
z = exp( lgz  );
zp1 = z +max(1,eps(z));
ref = gammaln( z );
refp1 = gammaln( zp1 ); % = log(z) + gammaln(z)
[hi,lo] = accurate_gammaln( z );
[hip1,lop1] = accurate_gammaln( zp1 );
dif = (hi-ref)+lo;
figure(1)
plot( lgz, dif./ref);
figure(2)
plot(lgz, [refp1-ref (hip1-hi)+lop1-lo]-(lgz.*(zp1-z))*[1 1])