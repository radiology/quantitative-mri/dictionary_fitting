function [S] = adjointRef( S, refs, B, szS)
% [S] = adjointRef( S, refs, B, szS)
% 
% Performs the adjoint opereration of 
% B = S( refs{:} ) 
% 
% so if there are not repeated indices it is: 
% S( refs{:} ) = S( refs{:} ) + B
% However, for repeated indices, entries in B are summed.
% So this function is like accumarray for general indexing.
%
% If S is empty and szS is provided a new zeros matrix with szS and class(B) is created. 
% (this saves the right hand side reference to S in the above equation)
%
% Created by Dirk Poot, Erasmus MC, 10-3-2016


for dim = 1 : numel( refs )
    if ~(ischar(refs{dim}) && isequal(refs{dim},':')) && ~islogical(refs{dim})
        % normal integer indexing. (logical indexing cannot have duplicate locations)
        % check for duplicate entries:
        [idx_s, pos_s] = sort(refs{dim});
        didx = diff(idx_s(:));
        if any(didx==0)
            % There are duplicate entries that we need to accumulate.
            % Proceed by identifying which entries in refs{dim} occur multiple times:
            sted = reshape( find(diff([1; didx~=0; 1])) ,2,[]);
            keep = [true;didx~=0];
            refs_s = repmat({':'},1,numel( refs ));
            refs_r = refs_s;
            for i = 1: size(sted,2)
                refs_s{dim} = pos_s( sted(1,i) ); % we accumulate everything to the first of the range (by construction of 'keep' this one is kept)
                refs_r{dim} = pos_s( sted(1,i) : sted(2,i) );
                B(refs_s{:}) = sum( B(refs_r{:}), dim);
            end;
            refs_s{dim} = pos_s( keep );
            B = B(refs_s{:}); % remove entries that have been accumulated in the loop above. 
            refs{dim} = refs{dim}(pos_s( keep )); % remove duplicates from refs{dim}
        end;
    end;
end;

if isempty(S) && nargin>=4
    S = zeros(szS,class(B));
    S(refs{:}) = B;
else
    S(refs{:}) = S(refs{:}) + B;
end;



function test_stedcreation
%%
sted = @(sidx) display_nice({find(diff([1 diff(sidx)~=0 1])) , [true diff(sidx)~=0]});
sidx = [1 1 1 1 1];
sted(sidx)

sidx = [1 1 1 1 2];
sted(sidx)

sidx = [1 1 1 1 2 2 2];
sted(sidx)

sidx = [1:6];
sted(sidx)

function test_adjoinRef
%%
S = zeros(3,5,2);
B = randn(3,6,2);
refs = {':',[1 3 3 3 2 2],':'}
[S] = adjointRef( S, refs, B)