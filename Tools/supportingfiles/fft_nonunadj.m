function [img] = fft_nonunadj( ksamp, st )
% [img] = fft_nonunadj( ksamp, st )
%
% Applies the adjoint of the non uniform Fourier transform. I.e. regarding 
% fft_nonun as linear operator on img; this function provides the adjoint of that 
% operator. Use fft_nonun_plan to construct the plan st. 
%
% INPUTS: 
%   ksamp : [img_size( nonfftDim ) size(kpos, 2..end)]; non-uniformly sampled k-space samples.
%   st  : planned non uniform FFT structure. Create with fft_nonun_plan. 
%
% OUTPUTS:
%   img : ND array with size equal to the size specified when planning st.
%  
% 11-9-2018, D.Poot, Erasmus MC: Created first version

if ~isempty( st.samplePhaseAdjustAdj )
    % Apply adjoint phase adjustment as well as density compensation: 
    ksamp = bsxfun(@times, ksamp, st.samplePhaseAdjustAdj );
end;

% TODO: permute img to get all tag-along dimensions to the first dimension. 
[img] = TransformNDbspline( ksamp, st.sampleindex, st.mapT, st.adjointSz, st.F, st.bsplineord, st.AR, st.clip_i, st.boundary_condition);

if ~isempty( st.permorder )
    img = ipermute( img, st.permorder);
end;

if ~isempty( st.postcompensate )
    for d = st.fftDim
        img = bsxfun(@times, img, conj( st.postcompensate{d} ) );
    end;
end;
img = conj(img);    
sel = repmat({':'},1,numel(st.img_size));
for d = st.fftDim
    img = fft(img, [], d );
    sel{d} = 1:st.img_size(d);
    img = img(sel{:});
    sel{d} = ':';
end;
img = conj(img);


if ~isempty( st.precompensate )
    % TODO: should use a tensor product multiplication to avoid numel(fftDim) passes over img. 
    %       or premultiply the precompensations to have just a single multiplication (but that has excessive memory consumption)
    for d = st.fftDim
        img = bsxfun(@times, img, conj(st.precompensate{d}) );
    end;
end;
