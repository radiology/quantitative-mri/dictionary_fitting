function [f, g, h_info] = leastSquaresCostFunction_nonlinear( X, F, A, At, Y, multipleX, min_Y, dot_Y)
% [f, g, h_info] = leastSquaresCostFunction_nonlinear( X, F, A, At, Y [,multipleX, min_Y, dot_Y])
%
% Computes 
%  f( X ) = .5* | Y - F(X) |^2 == .5* (Y - F(X))'*(Y - F(X))
%
% To be used as cost function for non-linear optimization routines.
% Especially, as second function argument of conjgrad_nonlin.
% Use leastSquaresCostFunction for linear functions F  (if A==F)
%
% The benefit of this function is when there are additional cost terms that
% should be combined. In that case this function allows easy conversion
% from such least squares formulation to a general cost function. 
%
% Obtain an hessian muliplication routine by calling with 'hessmulfun' as
% first input argument. The other arguments should be provided:
% hessmulfun = leastSquaresCostFunction_nonlinear( 'hessmulfun', A, At, Y)
% useage: d =  hessmulfun(h_info, x)   % multiply x with the hessian.
%
% INPUTS:
%   x  : parameter vector
%   F  : function taking 1 argument and computing:
%           [S , jacinfo] = F(X) 
%        Second output is the jacobian (information).    
%   A, At : function performing multiplications with the jacobian of F
%           A(jacinfo, x) == A*x,  where  A(i,j) = D(F_i, X_j)
%           or At(jacinfo, y) == A'*y, respectively. (Make sure complex conjugate is
%           used for complex A)
%  Y   : measurements vector. The optimum of the cost function is at the
%        least squares solution of X with the data Y. 
%  multiplerhs : default = true : multiple x can be used simultanuously.
%                if false only 1 x is supported by F, A and At, 
%  min_Y : Two argument function that subtracts the second argument from the first
%          Default : @(Y,AX) Y-AX; 
%  dot_Y : single argument function that computes the dot product of the argument with itself. 
%          Default: @(residual) dot(residual, residual)
%          NOTE: min_Y and dot_Y are introduced to allow custom 'Y' formats. 
%
% OUTPUTS:
%  f   : cost function value
%  g   : gradient of cost function value with respect to X
%  h_info : hessian info. 
%
% Created by Dirk Poot, 24-7-2015, Erasmus MC, 

if nargin<6 || isempty(multipleX)
    multipleX = true;
end;
if nargin<7 || isempty(min_Y)
    min_Y = @(Y,AX) Y-AX;
end;
if nargin<8 || isempty(dot_Y)
    dot_Y = @(A) dot(A,A);
end;
if isequal(X,'hessmulfun')
%     f = @(hinfo, x) reshape( At(A( reshape( x, [hinfo.sz size(x,2)])) ), [],size(x,2));
    f = @(hinfo, x) leastSquaresCostFunction_hessmul(hinfo, x, A, At, multipleX);
    return;
end;

% f = .5 * (Y - F( X) )'*(Y - F( X ) )
% A = jacobian of F; A(i,j) = D(F_i, X_j)
% g = df/dX = A'*(Y - F(X))
% h = A'*A
if nargout<=1
    FX = F( X );
else
    [FX , jacinfo ] = F(X);
end;
res = min_Y(Y,FX);

f = .5 * dot_Y(res);

if nargout>1
    g = - At( jacinfo, res );
    h_info.sz = size(X);
    h_info.multipleX = multipleX;
    h_info.jacinfo = jacinfo;
end;
end
function Hx = leastSquaresCostFunction_hessmul(hinfo, x, A, At, multipleX)
    if ~multipleX && size(x,2)>1 && size(x,1)==prod(hinfo.sz)
        % split over x
        Hx = cell(1,size(x,2));
        for k=1:numel(Hx)
            Hx{k} = reshape( At( hinfo.jacinfo, A(hinfo.jacinfo, reshape( x(:,k) , hinfo.sz) ) ) , [],1);
        end;
        Hx = [Hx{:}];
    else
        Hx = reshape( At( hinfo.jacinfo, A( hinfo.jacinfo, reshape(x , [hinfo.sz size(x,2)]) ) ), [], size(x,2) );
    end;
end