function [hi, lo]= accurate_log( z ) 
% [hi, lo] = accurate_log( z ) 
% Improved accuracy version of log.
% In infinite precision arithmatic:
%  hi + lo = log( z ) + e  , |e|<.5*eps
%
% Using the standard log function, the error will be larger for (z<.607 | z>1.648)
% This returns a high and low part of the result so that the truncation error
% is <.5 eps for all z > 0
%
% 26-4-2017: Created by Dirk Poot, Erasmus MC


e = round( log2( z ) ); % -1074 <= e <= 1022

zred = pow2( z, -e ); % exact; sqrt(.5) <= zred <= sqrt(2).

lo = log( zred ); % between .5*log(.5) and .5*log( 2)  ( -0.347 < lo < 0.347 )

% Mathematica: 
%f =  Log[2]
%roundpow = 30;
%2^roundpow
%l2 = f *%;
%l2hi = Round[ l2 ]
%N[f - l2hi*2^(-roundpow), 30]

log_2_hi = 744261118/1073741824;
log_2_lo = -4.20091507268108472918234319245e-11;
hi = e * log_2_hi ; % exact as 744261118 * 1074 < 2^53
lo = lo + e * log_2_lo; % e * log_2_lo < 5e-8

function test()
%% 
lgz = sort( 200*(rand(1000,1)-.5) );
z = exp(lgz);
[hi, lo]= accurate_log( z ) 
ref = log(z);
plot(lgz, [(hi-ref)+lo])