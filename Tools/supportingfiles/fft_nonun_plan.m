function st = fft_nonun_plan( kpos, img_size , varargin)
% st = fft_nonun_plan( kpos, img_size, options)
%
% This function plans a non-uniform FFT from an image with size img_size.
% Sampling at the frequency coordinates specified by kpos. 
% Apply the planned transform with:
%  ksamples = fft_nonun( img, st )
% Which (logically) performs the discrete Fourier Transform:
%  img_p = permute( img, [nonfftDim, fftDim] );
%  ksamples( j ) = sum_x img_p( ..., x ) * exp(1i * (x-imageOrigin) * kpos(:, j) )
% where x is a vector with the MATLAB index into the fftDim of img and the summation domain is in all fftDims: 1..img_size.
% The actual implementation uses the fft (when appropriate) to evaluate this more efficiently. 
% Use fft_nonun and fft_nonunadj to apply the planned non-uniform FFT.
%  
% The formal interpretation of this non uniform FFT is:
% - img consists of dirac delta pulses at [1 ... img_size]-imageOrigin and is zero elsewhere. 
%   -> dirac delta pulses => fourier transform is periodic (except for a linear phase trend when imageOrigin is non-integer)
%      with period 2pi in all dimensions.
%   -> zero outside the defined extend => Fourier transform is continuous and hence it can be sampled at arbitrary locations. 
% - This differs from the normal FFT as that assumes periodicity of the image and 
%   hence the Fourier transform also is a series of dirac delta peaks.
%
% NOTE: The inputs requested by this function differs from the inputs of the nufft_init function of Jeff Fessler. 
%       Use fft_nonun_init, which accompanies this fft_nonun_plan, for a syntax more similar to nufft_init. 
%
% INPUTS:
%  kpos : numel(fftDim) x M array with frequency domain positions.
%         Nyquist rate is: abs( kpos ) <= pi.  Note abs(kpos) > pi are perfectly allowed
%         due to the periodicity of the fft (but they typically dont make sense). 
%  img_size : 1 x D integer vector specifying the size of the img.
% Options:
%  fftDim : 1 x M  (with M<D) integer vector or 1 x D logical vector with the dimensions 
%           that are transformed.
%           Non selected dimensions are 'tagged along' (see OUTPUTS ksamples). 
%           Default = 1 : D
%  nonfftDim : 'tag along' dimensions. Default: all dimensions not selected by fftDim in 
%               numerical order. 
%  imageOrigin : 1 x D vector; should be integer for proper interpretation. 
%                The default, which corresponds to standard FFT: imageOrigin = [ 1, 1, .. ]
%  errorTolerance : Relative error that is tolerated. Default = 0.001, which corresponds 
%                to the default in nufft (Note that nuFFT requires substantially more 
%                computation time to reach this same error and does not provide explicit control over the approximation error.)
%                Higher values save computation time at the expense of reduced
%                accuracy of the non uniform fft. Typically the error can be set at approximately
%                1/SNR of the images. The adjoint transform is always the exact 
%                adjoint of the forward transform (i.e. both have exactly the same 
%                approximation errors).
%  densityCompensation : M element array with (real valued) density compensation factor. 
%                size: [ ones( 1, numel( nonfftDim )) M];
%                Default = 1. 
%                This compensation is applied in the adjoint transform and can be used
%                to make the adjoint (almost) a pseudo inverse (when sufficient samples are taken)
%                Specifically the adjoint (fft_nonunadj) is:
%            img( ..., x ) = sum_j ksamples( j ) * exp(-1i * (x-imageOrigin) * kpos(:, j) ) * densityCompensation(j) 
%
% OUTPUTS:
%  st : structure that can be passed to fft_nonun and fft_nonunadj
% OUTPUTS fft_nonun:
%  ksamples : [img_size( nonfftDim ) size(kpos, 2..end)]; 
%             So all 'tagged along' dimensions are the first output dimensions. 
%             If there are no 'tag along' dimensions, the first dimension is size(kpos,2).
%
% To developers of fft_nonun*: see/compare with fft_nonun_reference
%
% 11-9-2018, D.Poot, Erasmus MC: Created first version


opts = struct;
opts.fftDim = [];
opts.nonfftDim = [];
opts.imageOrigin = [];
opts.errorTolerance = .0001;
opts.densityCompensation = [];
if nargin>=3
    opts = parse_defaults_optionvaluepairs( opts, varargin{:});
end;
if isempty( opts.imageOrigin )
    opts.imageOrigin  = ones(size(img_size));
end;
if isempty( opts.fftDim )
    opts.fftDim = 1:numel(img_size);
elseif islogical( opts.fftDim )
    opts.fftDim = find( opts.fftDim );
end;
if isempty( opts.nonfftDim )
    nonfftdimmask = true( 1, numel(img_size ) ); nonfftdimmask( opts.fftDim ) = false;
    opts.nonfftDim = find( nonfftdimmask );
end;
if ~isequal( sort([opts.nonfftDim opts.fftDim]), 1:numel(img_size))
    error('nonfftDim (=[%s]) and fftDim (=[%s]) together do not uniquely select all %d image dimensions', num2str( opts.nonfftDim), num2str(opts.fftDim), numel(img_size));
end;
opts.x = cell(1, numel(img_size ) );
for d = opts.fftDim
    opts.x{d} = (1:img_size(d))-opts.imageOrigin(d);
end;
if isequal(kpos, 'getOptions') % output parsed options; mainly for fft_nonun_reference
    st = opts; 
    return;
end;
if numel(opts.fftDim)~=size(kpos,1)
    error('kpos should be a numel(fftDim) x M array');
end;

st = struct;
st.img_size = img_size;
st.fftDim = opts.fftDim; 

num_ksamp = size( kpos(:,:), 2 );
filters = getCandidateFilters();
[filter, st.ksize] = selectOptimalFilter( filters, img_size, num_ksamp, opts.errorTolerance);


st.bsplineord = filter.filterbank_bsplineorder;
% TODO: frequency shift F to adjust center of image. 
st.F =  filter.F;
st.AR = [];
st.clip_i = [];
st.boundary_condition = 2; % 2 = wrap around boundary condition: TODO: implement! 

x = cell(1, numel(img_size ) );
x_origin = ceil( (img_size+1)/2 ); % should be integer for proper wrap around. 
for d = opts.fftDim
    x{d} = (1:img_size(d))- x_origin(d);
end;
st.precompensate = cell(1, numel(img_size ));
for d = st.fftDim
    st.precompensate{ d } = reshape( ppval( filter.precompensation, abs( x{d}./st.ksize(d) ) ) , [ones(1,d-1) img_size(d) 1]);
    % TODO: add k-space center shift to precompensate
end;

% TODO: update TransformNDbspline to accept complex st.F; insert phase adjustment into F when faster than full image phase adjustment. 
st.postcompensate = cell( 1, numel(img_size ));
for d = st.fftDim
    st.postcompensate{ d } =  reshape( exp( 1i* (x_origin(d)-1) /st.ksize(d) *2*pi* (0:st.ksize(d)-1) ), [ones(1,d-1) st.ksize(d) 1]);
    % TODO: add k-space center shift to precompensate
end;
kpos_size = size( kpos ) ;
if isequal( x_origin, opts.imageOrigin )
    st.samplePhaseAdjust = [];
else
    st.samplePhaseAdjust = exp(-1i* (x_origin( st.fftDim) - opts.imageOrigin( st.fftDim) ) * kpos(:,:) );
    st.samplePhaseAdjust = reshape( st.samplePhaseAdjust, [ones(1, numel( opts.nonfftDim) ) kpos_size(2:end) 1]);
end;
if ~isempty( st.samplePhaseAdjust ) || ~isempty( opts.densityCompensation) 
    if isempty( st.samplePhaseAdjust )
        st.samplePhaseAdjustAdj = opts.densityCompensation ;
    elseif ~isempty( opts.densityCompensation) 
        st.samplePhaseAdjustAdj = conj( st.samplePhaseAdjust ) .*opts.densityCompensation ;
    else
        st.samplePhaseAdjustAdj = conj( st.samplePhaseAdjust ) ;
    end;
else
    st.samplePhaseAdjustAdj =[];
end;
    
kpos_unwrapped = mod( kpos, 2*pi ) ;% TransformNDbspline handles wrapping inefficiently (by design), so unwrap here 
st.sampleindex = bsxfun( @times, kpos_unwrapped, st.ksize(st.fftDim)'./(2*pi) );
st.sampleindex = reshape( st.sampleindex, [kpos_size(1) ones(1, numel( opts.nonfftDim) ) kpos_size(2:end) ]);
st.mapT = [];
st.permorder = [ opts.nonfftDim opts.fftDim ];
if isequal( st.permorder, 1:numel(img_size) )
    st.permorder = [];
end;
st.newSz = [nan( 1, numel( opts.nonfftDim) ) kpos_size(2:end) ];
st.newSz( end+1 : numel( img_size ) ) = 1; 
st.adjointSz = [nan( 1, numel( opts.nonfftDim) ) -st.ksize( opts.fftDim ) ]; % negative numbers indicate to TransformNDbspline that it should apply the adjoint sampling. 
end

function [bestfilters, filtlen_v, maxfreqinsrc_v] = getCandidateFilters()
persistent cachedCandidateFiltersOutputs;
if ~isempty(cachedCandidateFiltersOutputs)
    bestfilters = cachedCandidateFiltersOutputs{1};
    filtlen_v = cachedCandidateFiltersOutputs{2};
    maxfreqinsrc_v = cachedCandidateFiltersOutputs{3};
else
    cachefilename = 'fft_nonun_candidatefilters.mat';
    cachefilevariables = {'bestfilters', 'filtlen_v', 'maxfreqinsrc_v'};
    if exist( cachefilename )
        load( cachefilename ,cachefilevariables{:});
    else
        fprintf('Re-creating cache file "%s", which contains a set of high quality interpolation filters used by fft_nonun_plan.\nGeneration of the set of filters takes about 15 minutes of CPU time (depending on computer speed).\nAlternatively break these computations and make sure you get a copy of the cache file on the MATLAB path.\n',cachefilename );
        %% (re)create cache file. Takes about 15 minutes:
        tstart = now; 
        filtlen_v = [2:10];
        steps = 40;
        maxfreqinsrc_v = linspace( 0.125, 0.49, 30);
        [filtlen , maxfreqinsrc] = ndgrid( filtlen_v , maxfreqinsrc_v );
        modeweight = [0 1];
        doplot = 1;
        filters = struct('F',cell(size(filtlen))) ;
        computeidx = 1 : numel( filtlen );
        %%
        progressbar('start', numel( filtlen ));
        for i = computeidx
            %%
            filterbank_bsplineorder =1 ; 
            if 0 
                [filter, compensation, errormagn] = designfilter(filtlen(i), steps, maxfreqinsrc(i), modeweight, doplot);
                F = filterbank( filter, filtlen(i), steps, [], filterbank_bsplineorder);
                err = errormagn(2,2); 
            else
                weightfun = @(f) 1e4*double( abs(f-round(f))<maxfreqinsrc(i) );
                regulw = .0001; regullp = 1; 
                while true;
                wfun_reg = @(f) regulw * exp(- max(0,f-maxfreqinsrc(i)).^2/(2*.01.^2) ); 
%                 ARprefilterOrder = 6; 
%                 [filts] = designLPfilter_v3(maxfreqinsrc(i), filtlen(i), ARprefilterOrder, 'doPlot',true ,'weight',weightfun ,'filterbank_bsplineorder',filterbank_bsplineorder,'transitionBands',[maxfreqinsrc(i) 1-maxfreqinsrc(i)],'filterSteps',100);
                ARprefilterOrder = -1; 
                [filts] = designLPfilter_v4(maxfreqinsrc(i), filtlen(i), ARprefilterOrder, 'doPlot',false ,'weight',weightfun ,'filterbank_bsplineorder',filterbank_bsplineorder,'transitionBands',[maxfreqinsrc(i) 1-maxfreqinsrc(i)],'filterSteps',100, 'hardConstrain',false,'ForceExactZeroFreqTransfer',false, 'weight_prefilter',wfun_reg);
                q = fft( [filts.filterbank .5*(filts.filterbank(:,1:end-1)+filts.filterbank(:,2:end))], 200,1); % expansion lenght should be even! (element end/2+1 should be the exact center with zero gradient of the prefilter)
                prefilter = 1./sqrt(mean( abs(q.^2) ,2)); % 1./(.5*(max(( abs(q)),[],2)+min((abs(q)),[],2)));
                freq = (0:numel(prefilter)-1)'/numel(prefilter);
                compensation = spline( freq(1:end/2+1), [0; prefilter(1:end/2+1); 0]);
                range = 1+(0:maxfreqinsrc(i)*size(q,1));
                err = sqrt( mean( mean( (bsxfun(@times, abs(q(range,:)), prefilter(range))-1).^2 ,1) ,2 ) );
                F = filts.filterbank;
                prefiltersel = prefilter( freq <=maxfreqinsrc(i) ); 
                fprintf('RMS error of filter with length %d and cutoff frequency %f = %g. [min max] prefilter = [%g %g]\n',size(F,1),maxfreqinsrc(i), err, min(prefiltersel), max(prefiltersel) );
                if max( prefiltersel )/min(prefiltersel )< 64 && min(prefiltersel )<1 && regullp <10
                    break;
                end;
                regullp = regullp  + 1; 
                regulw = regulw *4;
                end;
            end;
            filters(i).filterbank_bsplineorder = filterbank_bsplineorder ; 
            filters(i).cutoffFrequency = maxfreqinsrc(i); % fraction of sampling frequency. 
            filters(i).F = F;
            filters(i).filtlen = size(F,1);
            filters(i).precompensation =compensation;
%             filters(i).errorsmat = errormagn;
            filters(i).error = err;
            progressbar(i);
        end;
        progressbar('ready');
        filt_error = reshape([filters.error], size( filters ));
        if doplot 
            imagebrowse(  filt_error ,[0 .01], 'xrange',[filtlen_v([1 end])' maxfreqinsrc_v([1 end])'],'labels',{'filtlen','maxfreqinsrc'}) ; 
        end;
        islowesterror = true(size( filt_error));
        % unselect filters for which there is a strictly better one (lower or equal error, lower or equal filtlen, higher or equal cutoff frequency)
        for k1 = 1: size( filt_error,1)
            for k2 = 1 : size( filt_error, 2) 
                sel = filt_error(1:k1,k2:end);sel(end,1)=inf;
                islowesterror(k1, k2) = filt_error(k1,k2)< min( sel(:) ); 
            end;
        end;            
        % % prepare to recompute some filters that have error > some filter with higher cutoff frequency. 
        computeidx = find( ~islowesterror)';
        bestfilters = filters( islowesterror );
        if 0
            %% create filterr matrix of stored results
            tmp = load( cachefilename);
            %%
            filtlen_old = unique([tmp.bestfilters.filtlen]);
            cutoffreq_old = unique([tmp.bestfilters.cutoffFrequency]);
            filt_error_old = nan(numel(filtlen_old), numel(cutoffreq_old));
            for k= 1: numel( tmp.bestfilters) 
                filt_error_old( find( filtlen_old ==tmp.bestfilters(k).filtlen), find( cutoffreq_old ==tmp.bestfilters(k).cutoffFrequency) ) =  tmp.bestfilters(k).error;
            end;
            imagebrowse(cat(3,filt_error_old,filt_error))
        end;
        %%
        
        tend = now;
        dth = (tend-tstart )*24;
        h = floor( dth );dtm = (dth - h)*60;
        m = floor( dtm );dts = (dtm - m)*60;
        disp(['Computing filters took: ' num2str( h ) ' hours, ' num2str(m) ' minutes and ' num2str(dts) ' seconds'])
        %%
        save(cachefilename , cachefilevariables{:} ); 
    end;
    cachedCandidateFiltersOutputs = {bestfilters, filtlen_v, maxfreqinsrc_v};
end;
end

function [filter, ksize] = selectOptimalFilter( filters, img_size, num_ksamp, errorTolerance)
filt_error = [filters.error];    
% for each filtlen find the index of the maximum maxfreqinsrc that satisfies this tolerance:
acceptable_filters = filt_error <= errorTolerance;
filters_sel = filters( acceptable_filters );
filtlen = [filters_sel.filtlen];
cutofffreq = [filters_sel.cutoffFrequency];
[sort_lenfreq, origidx ] = sortrows( [filtlen(:) cutofffreq(:)] );
selidx = origidx( diff( [sort_lenfreq(:,1); inf])~=0 ); % selidx = for each filterlen the index into filters_sel of filter with maximum cutoff frequency
newsz = zeros( numel( selidx ), numel( img_size) );
fftCost = inf( numel( selidx ),1 );
interpolcost = fftCost;
for k = 1:numel(selidx) ; % if non has low enough error freqidx == 0 
    newsz_min = .5./cutofffreq( selidx(k) ) * img_size;
    newsz(k,:) = goodFFTsize( newsz_min ) ;
    fftCost(k) = prod( newsz(k,:) ) * sum( log2( newsz(k,:) ) ); % assuming zero extending is done before any fft (which it is not)
    interpolcost(k) = num_ksamp * filtlen( selidx(k) )^numel(img_size);
end;

[mincost, minidx] = min( fftCost + interpolcost);
ksize = newsz( minidx, : );
filter = filters_sel( selidx( minidx ) );
end

function test
%% grid test for debugging:
if 0 
    img_size = [30 35];
elseif 0
    img_size = [53 59 61];% prime image sizes (make nufft perform relatively worst)
else
    img_size = [48 64 64];% good FFT image sizes (make nufft perform relatively best)
end;
compare_nufft = true; % if true make sure the nufft toolbox of Jeff Fessler is on the path. 
                      % addpath( 'c:\Users\Dirk\EclipseSVNWorkspace\MATLAB\Toolbox\irgntv\NUFFT\nufft\','c:\Users\Dirk\EclipseSVNWorkspace\MATLAB\Toolbox\irgntv\NUFFT\utilities\','c:\Users\Dirk\EclipseSVNWorkspace\MATLAB\Toolbox\irgntv\NUFFT\','c:\Users\Dirk\EclipseSVNWorkspace\MATLAB\Toolbox\irt\mex\v7\')
fulltest = true && numel(img_size)==2;
if fulltest
    [k1,k2]=ndgrid([0:floor( img_size(1)/2) -ceil(img_size(1)/2-1):-1]/img_size(1)*2*pi,...
                   [0:floor( img_size(2)/2) -ceil(img_size(2)/2-1):-1]/img_size(2)*2*pi );
    kpos = permute(cat(4,k1, k2),[ 4 1 2 3]);
else
    kpos = rand(numel(img_size) ,20000)*2*pi-pi;
end;
opts = struct;
opts.imageOrigin = ones(size(img_size));%(img_size)/2;
opts.errorTolerance = 1e-4/3;
st = fft_nonun_plan( kpos, img_size ,opts);
opts.errorTolerance = 1e-5;
st2 = fft_nonun_plan( kpos, img_size ,opts);
if 0 
    % simple test:
    img= zeros(img_size);
    img(15:17,15:18)=1;img(16,18)=0;
else
    % complex valued random image to test more thouroughly (but more difficult to debug)
    img = randn(img_size)+1i*randn(img_size);
end;
if compare_nufft
    nufftst1 = nufft_init( kpos(:,:)', img_size, size(st.F,1)*ones(1,numel(st.fftDim)), st.ksize , 'table', 2^14, 'minmax:kb');
    nufftst2 = nufft_init( kpos(:,:)', img_size, size(st.F,1)*ones(1,numel(st.fftDim)), img_size*2 , 'table', 2^14, 'minmax:kb');
end;
%%
tic
[ksamp] = fft_nonun( img, st );
tused1 = toc;tic
[ksamp2] = fft_nonun( img, st2 );
tused2 = toc;tic
ksamp_ref = fft_nonun_reference( kpos, img, opts);
tused_ref = toc;
if compare_nufft
    tic
    ksamp_nufft1 = nufft( img, nufftst1); ksamp_nufft1 = reshape( ksamp_nufft1, size(ksamp) );
    tused_nufft1 = toc; tic;
    ksamp_nufft2 = nufft( img, nufftst2); ksamp_nufft2 = reshape( ksamp_nufft2, size(ksamp) );
    tused_nufft2 = toc;
else
    ksamp_nufft1 = [];ksamp_nufft2 = [];
    tused_nufft1 =[];tused_nufft2=[];
end;
if fulltest
    tic
    k_fftref = fftn( img ); % assumes origin is [1 1]
    tused_fftref = toc;
else
    k_fftref = [];
    tused_fftref =[];
end;
allFTs = cat(5,k_fftref , ksamp_ref,ksamp,ksamp2, ksamp_nufft1, ksamp_nufft2);
alltused = [tused_fftref , tused_ref , tused1, tused2, tused_nufft1, tused_nufft2]
allFTerr = bsxfun(@minus, allFTs, ksamp_ref);
if fulltest
    figure(1);
    imagebrowse(cat(6,allFTs, allFTerr ) );
end;
nrm = sqrt( dot( ksamp_ref(:) , ksamp_ref(:) ,1 ) );
nrmerr = sqrt( dot( reshape( allFTerr,[], size( allFTerr, 5) ), reshape( allFTerr,[], size( allFTerr, 5) ), 1));
relerr = nrmerr./nrm
% Test adjoint:
if numel(img)*numel(ksamp)>1e7
    disp('Not testing adjoint because problem size is above the treshold.');
else
    AT = zeros( numel( img ), numel( ksamp) );
    for k = 1 : numel(ksamp);
        zksamp = zeros( size(ksamp) ) ;
        zksamp(k) = 1; 
        [Aimg] = fft_nonunadj( zksamp, st );
        AT(:,k) = Aimg(:);
    end;
    A = zeros( numel( ksamp), numel( img ) );
    for k = 1 : numel(img);
        zimg = zeros( img_size) ;
        zimg(k) = 1; 
        [Aksamp] = fft_nonun( zimg, st );
        A(:,k) = Aksamp(:);
    end;
    figure(2)
    imagebrowse(cat(3, A, AT', A-AT'))
end;
%% Test tag along dimenions:
img_size = [17 34];
fulltest = true;
if fulltest
    [k2]=ndgrid([0:20 -20:-1]/34*2*pi);
    kpos = [k2(:)]';
else
    kpos = rand(1,1000)*2*pi-pi;
end;
opts = struct;
opts.imageOrigin = [1 1];%(img_size)/2;
opts.fftDim = 2;
st = fft_nonun_plan( kpos, img_size ,opts);
if 0 
    % simple test:
    img= zeros(img_size);
    img(15:17,15:18)=1;img(16,18)=0;
else
    % complex valued random image to test more thouroughly (but more difficult to debug)
    img = randn(img_size)+1i*randn(img_size);
end;
[ksamp] = fft_nonun( img, st );
ksamp_ref = fft_nonun_reference( kpos, img, opts);

if fulltest
%     k_fftref = fft( img , [], opts.fftDim); % assumes origin is [1 1]
%     ksamp = reshape(ksamp, size(k1));
%     ksamp_ref= reshape(ksamp_ref, size(k1));
    imagebrowse(cat(5,ksamp_ref,ksamp, ksamp_ref-ksamp));
end;    
end