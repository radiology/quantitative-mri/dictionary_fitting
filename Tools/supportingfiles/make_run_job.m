function executablename = make_run_job( functionname )
% executablename = make_run_job( functionname )
%
% This function makes an executable that runs the function specified.
% use compileOptions to configue compilation
% e.g. set options '-I ./Tools' to include the tools folder on your path (see option -I in mcc
% or add '-R -nodisplay -R -nojvm' to disable display and java (saves memory)
%
% INPUT:
%  functionname : string specifying the MATLAB function that you want to create
%                an executable for. Can subsequently be used to run on the cluster. 
%
%
% 13-2-2018, D.Poot, Erasmus MC : created function

sourcefile = which( 'run_job_base.m' );
clusterconfig = load_clusterconfig;

jobFile = fullfile( clusterconfig.execPath, sprintf('run_job_%s.m',functionname));
dirjob = dir( jobFile ); % initially used 'exist', but on linux also identified executable as existing when checking on mfile (that did not exist). 
createJobFile = numel(dirjob)<1;
if ~createJobFile
    % if run_job_base.m is newer than run_job_$FUNCTION$, recreate the latter. 
    dir_base = dir( sourcefile );
    datenum_jobfile = datenum( dirjob.date); 
    createJobFile = datenum( dir_base.date) > datenum_jobfile;
end;
executablename =  mccname( jobFile );
dir_executable = dir( executablename );
createExecutable = numel( dir_executable ) < 1; %~exist( executablename, 'file' ) ;
if createJobFile
    % create job file:
    disp( ['creating job file for : ' functionname ]);
    fh_read = fopen( sourcefile , 'r');
    fh_write = fopen( jobFile, 'w');
    while ~feof(fh_read)
        curline = fread( fh_read ,'*char')';
        curline = strrep(curline , '$FUNCTION$', functionname );
        fwrite( fh_write, curline);
    end;
    fclose( fh_read );
    fclose( fh_write );
    createExecutable = true;
else
    % test file date of mfile called:
    if ~createExecutable
        functionfilename = which(functionname) ;
        datenum_function = -inf; 
        if ~isequal( functionfilename(1:min(8,numel(functionfilename))),'built-in')
            % NOTE: Ignoring built-in function as they have no mfile to test, but they also are never modified (I hope)
            dir_function = dir( functionfilename );
            if numel(dir_function)>=1
                datenum_function = datenum( dir_function.date);
            end;
        end;
        
        createExecutable = datenum( dir_executable.date) < max( datenum_jobfile, datenum_function);
    end;
end;
if createExecutable
    % compile job file:
    disp( ['Compile job file: ' jobFile ]);
    [opts] = compileOptions( functionname );
    cmd = sprintf(['mcc -m %s    %s'],  ...
                            opts, jobFile  ) ;
    disp(  'Compile command :')
    disp( cmd );
    eval( cmd );
    [p, f, e] = fileparts( jobFile );
    compileresultname = fullfile( pwd , f);
    if ispc
        compileresultname = [compileresultname '.exe'];
    end;
    disp( ['Moving executable to: ' executablename ]);
    movefile( compileresultname, executablename );
end;


function executableFileName = mccname( mfileName )
clusterconfig = load_clusterconfig;
[p, f, e] = fileparts( mfileName  );
if ispc
    executableFileName = fullfile( clusterconfig.execPath, [f '.exe']);
else
    executableFileName = fullfile( clusterconfig.execPath, [f '.' computer('arch')]);
end;
