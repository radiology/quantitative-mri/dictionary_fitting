
#include <assert.h>
#include "mex.h"
#include "math.h"
#include <complex>
#include <algorithm>

#include "bspline_proc.cpp"
//#include "emm_wrap.cpp"
#include "emm_vec.hxx"
#include "tempSpaceManaging.cpp"
#include "lineReadWriters.cpp"

#ifdef USEOMP
#include <omp.h>
#endif
//using namespace std;

template < typename T, typename int_t> class separableTransformBase;

template < typename T, typename int_t> class separableTransform {
	/* separableTransform computes
	//  y = Transf( x | p ) ,  
    //  with:
	//     x : N-D coordinate vector
	//     y : scalar transformed position in 0th dimension.
	//     p : parameter vector that specifies the transform. 
	//
	//  Call as:
	//    separableTransform<..> & Transf = separableTransform(); // should construct a suitable child class instead!! 
	//    for each locatation i {
	//      for (j = maxDimDifferent ; j>=0 ; j-- ) { // maxDimDifferent is maximum dimension in which point i (might) differs from point i-1
	//			Transf.update( j , x_i[j] );
	//      }
	//      y_i = Transf.currentPos();
	//    }
	//
	// if you iterate over a 1D range in dim[0] you can also call the convenience (and extra performance) methods:
	//    y_i = Transf.read( x_i[0]);
	//  or (preferably):
	//    const T * buffer = Transf.read( range_in_dim0 ); // buffer = pointer to first element in range.
	//
	//  These methods should be overloaded in child classes for extra performance; a default implementation is provided.
	*/
protected:
	vector<T> transformOut ; 
	int_t * multiIdxDim1; // 'hack' to allow reading multiple lines on any type.
public: 
	separableTransform() {
		transformOut = vector<T>(0);
	};
	typedef T value_type;
	typedef T* iterator ;

	virtual void update( int dim, int_t newindex ) = 0;		// pure virtual
	virtual void updateMulti( int dim, int_t * newindex , int numlines ) {  // overload in derived classes and return true if succesfull, 
		if (dim==1) {
			multiIdxDim1 = newindex;
			if (transformOut.size()<(numlines) ) {
				transformOut.resize(numlines);
			}
		} else {
			mexErrMsgTxt("Can only initiate multiple lines for dimension 1.");
		}
	}
	virtual bool isConstantInDim( int dim ) {
		// return true only if transform does not depend on dim.
		return false;
	}
	virtual value_type currentPos() = 0;							// pure virtual

	virtual value_type read( int_t newindex ) {
		update(0, newindex);
		return currentPos();
	}; 
	virtual iterator readMulti( int_t newindex , int numlines) {
		// NOTE: this default implementation is (nececarilly) substantially inefficient.
		//     Better to use readMulti on a range or use a transform implementation that does not use the separability! 
		iterator outp = & transformOut[0];
		for (int lineno =0 ; lineno<numlines; ++lineno, ++outp) {
			update(1, multiIdxDim1[lineno] );
			update(0, newindex );
			*outp= currentPos();
		}
		return  & transformOut[0];
	}; 
	virtual iterator read( range<int> & r ) { // read range from current line, overloaded versions might modify r
		if (transformOut.size()<(r.last-r.first+1) ) {
			transformOut.resize(r.last-r.first+1);
		}
		for ( int k = 0; k<=r.last-r.first ; k++ ) {
			update(0, r.first+k);
			transformOut[k] = currentPos();
		}
		return & transformOut[0];
	};
	virtual iterator readMulti( range<int> & r , int numlines) { // read range from the nlines current lines, overloaded versions might modify r
		// REQUIRES: calling updateMulti for dim1 with the same number of lines  (numlines).
		// iterator iterates over lines first, then over different samples 
		//  i.e. the different locations of 1 line are separated by numlines elements in memory.
		if (transformOut.size()<(r.last-r.first+1) * numlines ) {
			transformOut.resize( (r.last-r.first+1) * numlines );
		}
		for (int lineno =0 ; lineno<numlines; ++lineno) {
			iterator outp = & transformOut[lineno];
			update(1, multiIdxDim1[lineno] );
			for ( int k = 0; k<=r.last-r.first ; k++ ) {
				update(0, r.first+k);
				*outp = currentPos();
				outp += numlines;
			}
		}
		return & transformOut[0];
	};
};

template < typename T, typename int_t> class separableTransformAdjoint {
	/* separableTransform computes
	//  y = T( x | p ) ,  
    //  with:
	//     x : N-D coordinate vector
	//     y : scalar transformed position in 0th dimension.
	//     p : parameter vector that specifies the transform. T is assumed to be linear in p.
	//  then with separableTransformAdjoint you can compute
	//     pmul =  sum_i d( T( x_i | p)  )/d( p ) * z_i
	//  with 
	//     z_i : corresponds to y (same size; i.e. all positions of x)
	//     pmul : output vector, same size as p.
	//  i.e. the multiplication of the adjoint with the derivative of T w.r.t. the transform parameters.
	//  Call as:
	//    separableTransformAdjoint<..> & Tadj = separableTransformAdjoint(); // should construct a suitable child class instead!! 
	//    for each locatation i {
	//      Tadj.setAdjointPos( y_i );
	//      for (j =0 ; j <= maxDimDifferent; j++ ) { // maxDimDifferent is maximum dimension in which point i (might) differs from point i+1
	//			Tadj.updateAdjoint( j , x_i[j] );
	//      }
	//    }
	//
	// if you iterate over a 1D range in dim[0] you can also call the convenience (and extra performance) methods:
	//    Tadj.writeAdjoint( x_i[0], y_i );
	//  or (preferably):
	//    Tadj.writeAdjoint( range_in_dim0 , ptr_to_first_element_range_y );
	//
	//  These methods should be overloaded in child classes for extra performance; a default implementation is provided in the base class
	*/
public: 
	typedef T value_type;
	typedef T* iterator ;

	virtual void setAdjointPos( value_type dimg) =0 ; // pure virtual
	virtual void updateAdjoint( int dim, int_t newindex ) = 0 ; // pure virtual
	virtual void writeAdjoint( int_t newindex, value_type dimg ) {
		setAdjointPos( dimg );
		updateAdjoint( 0 , newindex) ;
	};
	virtual void writeAdjoint( range<int> & r, iterator dimg ) {
		// iterator points to start of range!!
		for ( int k = 0; k<=r.last-r.first ; k++ ) {
			setAdjointPos( *dimg );
			updateAdjoint(0, r.first+k);
			++dimg;
		}

	};
};


template < int maxNumDims, typename T , typename int_t> class transformGeneral : public separableTransform< T , int_t >  {
	// Non interpolating fully general 1D transformation.
public:
	typedef separableTransform< T , int_t > parent;
	typedef typename parent::iterator iterator;
private:
	T * transformptr[maxNumDims+1];
	int_t ndims_in;
	int_t steps[maxNumDims];
public:
	transformGeneral( T* transformptr_, int_t ndims_in_, int_t * steps_ ){
		if (ndims_in_>maxNumDims) {
			mexErrMsgTxt("Too many dimensions.");
		}
		ndims_in        = ndims_in_;
		for (int k = 0; k<=ndims_in; k++ ) {
			transformptr[k] = transformptr_;
		}
		for (int k = 0; k<ndims_in; k++ ) {
			steps[k] = steps_[k];
		}
	};
	void update(int dim, int_t newindex) {
		transformptr[dim] = transformptr[dim+1] + newindex * steps[dim];
	};
	T currentPos() {
		return transformptr[0][0];
	}
	iterator read( range<int> & r ) { // read range from current line
		update(0, r.first);
		return transformptr[0];
	};			
};

template < int maxNumDims, typename T , typename int_t> class transformGeneralAdjoint : public separableTransformAdjoint< T , int_t >  {
public:
	typedef separableTransformAdjoint< T , int_t > parent;
	typedef typename parent::value_type value_type;
private:
	T * transformptr[maxNumDims+1];
	int_t ndims_in;
	int_t steps[maxNumDims];
public:
	transformGeneralAdjoint( T* transformptr_, int_t ndims_in_, int_t * steps_ ) {
		if (ndims_in_>maxNumDims) {
			mexErrMsgTxt("Too many dimensions.");
		}
		ndims_in        = ndims_in_;
		for (int k = 0; k<=ndims_in; k++ ) {
			transformptr[k] = transformptr_;
		}
		for (int k = 0; k<ndims_in; k++ ) {
			steps[k] = steps_[k];
		}
	};
	void setAdjointPos( value_type dimg) {
		transformptr[0][0] = dimg;
	}
	void updateAdjoint( int dim, int_t newindex ) {
		transformptr[dim] = transformptr[dim+1] + newindex * steps[dim];
	};
};

template < int maxNumDims, typename interpolateT, typename T , typename mapT, typename int_t> class transformGeneral1Dinterp : public separableTransform< T , int_t >  {
public:
	typedef separableTransform< T , int_t > parent;
	typedef typename parent::iterator iterator;
private:
	T * transforms[maxNumDims];
	int_t ndims_in;
	int_t size[maxNumDims];
	interpolateT interpolators[maxNumDims];
	mapT offset[maxNumDims];
	mapT step[maxNumDims];
	
public:
	transformGeneral1Dinterp( T* transforms_[], interpolateT * interpolators_, int_t ndims_in_, mapT * map){
		/* transforms_    : ndims_in+1 element array with base of memory locations in which transform[i] can store the i dimensional transform
		   interpolators_ : ndims_in array with interpolators, interpolators[i] (vector) interpolates transform[i+1] 
		   ndims_in_      : actual number of dimensions of input points
		   map			      : 2 x ndims_in: [offset;step] in each dimension.
		*/
		if (ndims_in_>maxNumDims) {
			mexErrMsgTxt("Too many dimensions.");
		}
		ndims_in        = ndims_in_;
		for (int k=0;k<=ndims_in; k++) {
			transforms[k] = transforms_[k];
		}
		for (int k=0;k<ndims_in; k++) {
			interpolators[k] = interpolators_[k];
		}
		for (int k=0;k<ndims_in; k++) {
			offset[k] = map[k*2+0];
			step[k]   = map[k*2+1];
		}
	};
	void update( int dim, int_t newindex ) {
		mapT newpos = offset[dim] + newindex * step[dim];
		interpolators[dim].sample( transforms[dim], newpos );
	};
	T currentPos() {
		return transforms[0][0];
	}
	iterator read( range<int> & r ) { // read range from current line, overloaded versions might modify r
		if (this->transformOut.size()<(r.last-r.first+1) ) {
			this->transformOut.resize(r.last-r.first+1);
		}
		int dim = 0;
		interpolators[dim].sample_range( & this->transformOut[0], offset[dim] + r.first * step[dim], step[dim], (r.last-r.first+1) );		/*
		for ( int k = 0; k<=r.last-r.first ; k++ ) {
			int_t newindex = r.first+k;
			mapT newpos = offset[dim] + newindex * step[dim];
			interpolators[dim].sample( transforms[dim], newpos );
			transformOut[k] = transforms[0][0];
		} // */
		return & this->transformOut[0];
	};
};

template < int maxNumDims, typename interpolateT, typename T , typename mapT, typename int_t> class transformGeneral1DinterpAdjoint : public separableTransformAdjoint< T , int_t >  {
public:
	typedef separableTransformAdjoint< T , int_t > parent;
	typedef typename parent::value_type value_type;
private:
	T * transforms[maxNumDims];
	int_t ndims_in;
  int_t ndims_out;
	int_t size[maxNumDims];
	interpolateT interpolators[maxNumDims];
	mapT offset[maxNumDims];
	mapT step[maxNumDims];
	
public:
	transformGeneral1DinterpAdjoint( T* transforms_[], interpolateT * interpolators_, int_t ndims_in_, mapT * map, int_t ndims_out_ = -1){
		/* transforms_    : ndims_in+1 element array with base of memory locations in which transform[i] can store the i dimensional transform
		   interpolators_ : ndims_in array with interpolators, interpolators[i] (vector) interpolates transform[i+1] 
		   ndims_in_      : actual number of dimensions of input point
		   map		   	    : 2 x ndims_in: [offset;step] in each dimension.
       ndims_out_     : number of dimensions/elements in output point
		*/
		if (ndims_in_>maxNumDims) {
			mexErrMsgTxt("Too many dimensions.");
		}
		ndims_in        = ndims_in_;
        if (ndims_out_==-1)
            ndims_out       = ndims_in_;
        else
            ndims_out       = ndims_out_;
		for (int k=0; k <= ndims_in_; k++) {
			transforms[k] = transforms_[k];
		}
		for (int k=0; k < ndims_in_; k++) {
			interpolators[k] = interpolators_[k];
		}
		for (int k=0; k < ndims_in_; k++) {
			offset[k] = map[k*2+0];
			step[k]   = map[k*2+1];
		}
	};
	inline void setAdjointPos( value_type dimg ) {
		transforms[0][0] = dimg;
	}
	inline void updateAdjoint( int dim, int_t newindex ) {
		mapT newpos = offset[dim] + newindex * step[dim];
		//interpolators[dim].sample( transforms[dim], newpos );
		//interpolators[dim]->sampleAdjoint( transforms[dim], newpos , true);
    interpolators[dim]->adjoint( newpos, transforms[dim]);
	};
};



template < int maxNumDims, typename T , typename int_t> class transformAffine1D : public separableTransform< T , int_t >  {
public:
	typedef separableTransform< T , int_t > parent;
	typedef typename parent::iterator iterator;
private: 
	T transform[maxNumDims];
	T intermediatePos[maxNumDims+1];
//	vector<T> transformOut; 
	typedef vec<T, 4> vT; 
	vT base;
	int_t size_dim0;
	bool cachefullline;
	bool needupdate;
public:
	transformAffine1D( T* transformptr_, int_t ndims_in_ , int_t size_dim0_, int_t maxnumlines=1){
		/* Compute sum_i transformptr_[i] * curindex[i] , i=0..ndims_in_-1
		    size_dim0_ = the size in dimension 0, i.e. the maximum range requested in read
			maxnumlines = maximum number of lines read simultanuously (with updateMulti, readMulti)
		*/
		if (ndims_in_>maxNumDims) {
			mexErrMsgTxt("Too many dimensions.");
		};
		for (int dim =0 ; dim<ndims_in_; dim++) {
			transform[dim]= transformptr_[dim];
		};
		cachefullline = (ndims_in_>1) && (transform[1]==0);
		size_dim0 = size_dim0_;
		for (int dim = 0;dim <= ndims_in_; dim++ ) { // last element set = ndims_in_
			intermediatePos[ dim ] = transformptr_[ ndims_in_ ];
		};
		this->transformOut = vector<T>( size_dim0*maxnumlines + vT::length-1 );
		T init[vT::length];
		for (int k=0;k<vT::length;k++) {
			init[k]=k;
		}
		base = vT(&init[0]);
		needupdate = true;
	}
	inline T currentPos() {
		return intermediatePos[0];
	}
	inline void update( int dim, int_t newindex ) {
		intermediatePos[dim] = intermediatePos[dim+1] + newindex * transform[dim];
		if ((dim>0) && (transform[dim]!=0)) {
			needupdate = true;
		}
	}
	bool isConstantInDim( int dim ) {
		// return true only if transform does not depend on dim.
		return (transform[dim]==0);
	}
	iterator read( range<int> & r ) { // read range from current line
		/*if (transformOut.size()<(r.last - r.first + vT::length) ) {
			mexErrMsgTxt("should no longer resize temporary storage vector.");
			//transformOut.resize(r.last-r.first+10);
		}*/
		T ip1 = intermediatePos[0+1];
		T T0 = transform[0];
		typename vector<T>::iterator outp = this->transformOut.begin();
		int firstcomp = r.first;
		int lastcomp  = r.last;
		if (cachefullline) {
			firstcomp = 0;
			if (needupdate) {
				lastcomp = size_dim0-1;
				needupdate = false;
			} else {
				lastcomp = -vT::length; // don't do anything, just read previous computed line as the current line is equal.
			}
		}
		vT curidx = base + firstcomp;
		int nToDo = (lastcomp - firstcomp )/vT::length + 1;
		for ( ; nToDo>0 ; --nToDo ) {
			( curidx * T0 + ip1).storea( &*outp );
			curidx += vT::length;
			outp += vT::length;
		}
/*/		int_t curidx = r.first;
		int nToDo = r.last-r.first+1;
		for ( ; nToDo>0 ; --nToDo ) {
			*outp = ip1 + curidx * T0;
			++curidx;
			++outp;
		}// */
		return & (this->transformOut[r.first-firstcomp]);
	};
private:
	template < int numlines > iterator readMulti_internal( range<int> &r ) {
		int_t curidx = r.first;
		int nToDo = r.last-r.first+1;
		T ip2 = intermediatePos[0+2];
		T T0 = transform[0];
		T T1 = transform[1];
		// initialize offsets:
		T init[numlines];
		for( int lineno = 0 ; lineno<numlines ; ++lineno) {
			init[ lineno ] = T1 * this->multiIdxDim1[lineno] + ip2;
		}
		typedef vec<T, numlines> vType;
		vType offset1( &init[0] );
		typedef vecptr<T *, numlines> iType;
		iType lineit( &(this->transformOut[0]) );
		for ( ; nToDo>0 ; --nToDo , ++curidx, ++lineit) {
			*lineit = offset1 + curidx * T0;
		}
		return & (this->transformOut[0]);
	};
	iterator readMulti_internal( range<int> &r , int numlines) { 
		typename vector<T>::iterator outp = this->transformOut.begin();
		int_t curidx = r.first;
		int nToDo = r.last-r.first+1;
		T ip2 = intermediatePos[0+2];
		T T0 = transform[0];
		T T1 = transform[0];
		for ( ; nToDo>0 ; --nToDo , ++curidx) {
			T ip1 = ip2 + curidx * T0;
			for (int lineno = 0 ; lineno<numlines ; ++lineno, ++outp) {
				*outp = ip1 + this->multiIdxDim1[lineno] * T1;
			}
		}
		return & (this->transformOut[0]);
	};
public:
	iterator readMulti( range<int> & r , int numlines) { 
		if (numlines==4) {
			return readMulti_internal<4>( r );
		} else if (numlines==8) {
			return readMulti_internal<8>( r );
		} else
			return readMulti_internal( r , numlines);
	};
};

template < int maxNumDims, typename T , typename int_t> class transformAffine1DAdjoint : public separableTransformAdjoint< T , int_t >  {
public:
	typedef separableTransformAdjoint< T , int_t > parent;
	typedef typename parent::value_type value_type;
private: 
	T intermediatePos[maxNumDims+2];
	T * transform; 
	int ndims_in;
public:
	transformAffine1DAdjoint( T* transformptr_, int_t ndims_in_ ) {
		transform = transformptr_;
		ndims_in = ndims_in_;
		if (ndims_in_>maxNumDims) {
			mexErrMsgTxt("Too many dimensions.");
		};
		for (int dim = 0;dim <= ndims_in_; dim++ ) { // last element set = ndims_in_
			intermediatePos[ dim ] = 0;
		};
	}
	inline void setAdjointPos( value_type dimg ) {
		intermediatePos[0] = dimg;
	}
	inline void updateAdjoint( int dim, int_t newindex ) {
		// intermediatePos[dim] = intermediatePos[dim+1] + newindex * transform[dim];
		transform[dim] += newindex * intermediatePos[dim];
		intermediatePos[dim+1] += intermediatePos[dim];
		intermediatePos[dim] = 0;
		if (dim==ndims_in-1) { // should also output offset, update that with highest dimension.
			transform[dim+1] += intermediatePos[dim+1];
			intermediatePos[dim+1] =0;
		}
	};

};





template < typename T, typename int_t> class separableTransformND {
protected:
	int ndims_out;
	vector<T> transformOut ; 
public: 
	separableTransformND(int ndims_out_) : ndims_out(ndims_out_) {
		transformOut = vector<T>(0);
	};
	typedef T* value_type;
	typedef T* iterator ;
	virtual void update( int dim, int_t newindex ) = 0;		// pure virtual
	virtual value_type currentPos() = 0;							// pure virtual
	virtual value_type read( int_t newindex ) {
		update(0, newindex);
		return currentPos();
	}; 
	virtual iterator read( range<int_t> & r ) { // read range from current line
		if ((int_t) transformOut.size()<(r.last-r.first+1)*ndims_out ) {
			transformOut.resize((r.last-r.first+1)*ndims_out);
		}
        read( r, & transformOut[0] );
		return & transformOut[0];
	};
    virtual void read( range<int_t> &r, iterator buffer ) {
		for ( int_t k = 0; k<=r.last-r.first ; k++ ) {
			update(0, r.first+k);
			T * tmp = currentPos();
			for (int k2=0; k2<ndims_out; k2++ ) {
				buffer[k*ndims_out+k2] = tmp[k2];
			}
		}
    }
};


template < int maxNumDims, typename T , typename int_t, typename int_steps = int_t> class transformGeneralND : public separableTransformND< T , int_t >  {
	/* NOTE: if iterator read( range<int> & r ) is used, it is required that steps[0] ==1 */
public:
	typedef separableTransformND< T , int_t >  parent;
	typedef typename parent::value_type value_type;
	typedef typename parent::iterator iterator;
private:
	T * transformptr[maxNumDims+1];
	int_steps steps[maxNumDims];
public:
	transformGeneralND( T* transformptr_, int ndims_out_, int_steps * steps_ ) 
        : separableTransformND< T , int_t >(ndims_out_) {
		if (this->ndims_out>maxNumDims) {
			mexErrMsgTxt("Too many dimensions.");
		}
		for (int k = 0; k<=this->ndims_out; k++ ) {
			transformptr[k] = transformptr_;
		}
		for (int k = 0; k<this->ndims_out; k++ ) {
			steps[k] = steps_[k];
		}
	};
	void update(int dim, int_t newindex) {
		transformptr[dim] = transformptr[dim+1] + newindex * steps[dim];
	};
	value_type currentPos() {
		return &transformptr[0][0];
	}
	iterator read( range<int_t> & r ) { // read range from current line
		update(0, r.first);
		return transformptr[0];
	};		
    // use baseline 'void read( range<int> &r, iterator buffer ) ' as we need to copy the data. 
};


template < int maxNumDims, typename interpolateT, typename T , typename mapT, typename int_t> class transformGeneralNDinterp : public separableTransformND< T , int_t >  {
private:
	T * transforms[maxNumDims];
	int_t size[maxNumDims];
	interpolateT interpolators[maxNumDims];
	mapT offset[maxNumDims];
	mapT step[maxNumDims];
public:
	transformGeneralNDinterp( T* transforms_[], interpolateT * interpolators_, int ndims_in_, mapT * map, int ndims_out_ /*=ndims_in_*/): separableTransformND< T , int_t >(ndims_out_){
		/* transforms_    : ndims_in+1 element array with base of memory locations in which transform[i] can store the i dimensional transform
		   interpolators_ : ndims_in array with interpolators, interpolators[i] (vector) interpolates transform[i+1] 
		   ndims_in_      : actual number of dimensions of input point
		   map			      : 2 x ndims_in: [offset;step] in each dimension.
       ndims_out_     : number of dimensions of output point.
		*/
		if ( ndims_in_ > maxNumDims) {
			mexErrMsgTxt("Too many dimensions.");
		}
		for (int k=0;k <= ndims_in_; k++) {
			transforms[k] = transforms_[k];
		}
		for (int k=0;k < ndims_in_; k++) {
			interpolators[k] = interpolators_[k];
		}
		for (int k=0;k < ndims_in_; k++) {
			offset[k] = map[k*2+0];
			step[k]   = map[k*2+1];
		}
	};
	void update( int dim, int_t newindex ) {
		mapT newpos = offset[dim] + newindex * step[dim];
		//interpolators[dim].sample( newpos, transforms[dim]);
		interpolators[dim]->get_coefficients( transforms[dim], newpos);
	};
	T* currentPos() {
		return &transforms[0][0];
	}

};


template < int maxNumDims, typename T , typename int_t> class transformAffineND : public separableTransformND< T , int_t >  {
private: 
    int ndims_in;
    T transform[maxNumDims * maxNumDims];
	T intermediatePos[maxNumDims*(maxNumDims+1)];
	vector<T> transformOut; 
public:
	transformAffineND( T* transformptr_, int ndims_in_, int ndims_out_ )
        : separableTransformND< T , int_t >(ndims_out_),
          ndims_in(ndims_in_) {
		if ( this->ndims_out > maxNumDims || ndims_in > maxNumDims) {
			mexErrMsgTxt("Too many dimensions.");
		};
		for (int dim =0 ; dim < this->ndims_out * ndims_in; dim++) {
			transform[dim]= transformptr_[dim];
		};
		for (int dim = 0;dim <= ndims_in; dim++ ) { // last element set = ndims_in_
			for (int k = 0; k < this->ndims_out; k++ ) {
				intermediatePos[ this->ndims_out * dim +k ] = transformptr_[ this->ndims_out * ndims_in +k ];
			}
		};
		transformOut = vector<T>(0);
	}
    //transformAffineND( T* transformptr_, int_t ndims_ ) : transformAffineND( transformptr_, ndims_, ndims_ ) {};
    
	T * currentPos() {
		return &intermediatePos[0];
	}
	inline void update( int dim, int_t newindex ) {
		for (int k = 0; k < this->ndims_out; k++ ) {
			intermediatePos[ this->ndims_out * dim+k] = intermediatePos[ this->ndims_out*(dim+1)+k] + newindex * transform[ this->ndims_out*dim + k ];
		}
	}
};
//template <> iterateSeparableTransform


#ifdef BUILDTESTFUN_TRANSFORMCORE
// Test code to test the majority of the code in this function. Mainly used to debug the code during development.
void self_test_mex( mxArray * out ) {


}


void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
	// Only compile code below for performing a code test.
	if (nlhs!=1) {
		mexErrMsgTxt("One output required.");
	}
	plhs[0] = mxCreateStructMatrix(2,1,0, NULL);
	self_test_mex( plhs[0] );
	return;
}
#endif
