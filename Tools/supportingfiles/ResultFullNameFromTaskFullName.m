function [resultFullName] = ResultFullNameFromTaskFullName( taskFullName )
% [resultFullName] = ResultFullNameFromTaskFullName( taskFullName )
%
% Creates the result file name from a task file name for run_on_cluster and run_job_*. 
%
% INPUTS:
%    taskFullName : full task name
% OUTPUTS:
%    resultFullName : full name of result file. 
%
% 13-2-2018, D. Poot, Erasmus MC: Created

[path, filename, ext] = fileparts( taskFullName );
resultFullName = fullfile( path, ['Result' filename(5:end), ext] );
% Requirements: 
% - As Task files are searched by looking for files starting with 'Task',
%   the result files should replace that. 
% - Currently result files are in the same folder as the task files
% - result file names should not clash (hence using the identifying part of the task file name).
