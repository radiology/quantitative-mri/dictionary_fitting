function out = load_clusterconfig( varargin ) 
% clusterconfig = load_clusterconfig( newconfig )
% 
% Loads the computer specific cluster configuration. If it does not exists
% the user is asked to type in the information. 
% You can update the cluster config with an structure with updated values and/or
% option-value argument pairs. 
%
% Structure with fields:
%  taskPath  : Full path to task files. Should be accessible from computer running 
%              'run_on_cluster' and the computer evaluating 'submitTasks'
%              Both computers can have a different clusterconfig, but both taskPath should point to 
%              the same physical location. E.g. Mounting personal cluster scratch with SFTP net drive on drive S
%              you can set taskPath on your windows PC as 'S:\matlabTasks\'
%              and on the appnodes as: '/scratch/yourname/matlabTasks'
%  execPath  : Full path to executables on this computer. 
%  storeThresholdBytes : Store variables with more bytes in a separate file (and thus not in the 
%                        task specific file). Implicitly assumes large variables are reused across tasks. 
%                        Setting this to different values on different machines has no effect 
%                        on correctness of results, but may consume extra diskspace and compute time. 
%  executionMode  : integer specifing if tasks should be started from this computer. 
%                   1 = none; use this to only store the task files from this computer (typical on your (windows) workstation)
%                   2 = local-blocking; use this to run tasks inside the current MATLAB. No parallel computation, effectively only caches results. Avoids storing argument and task files. 
%                   3 = local-parallel; use this to run tasks in parallel on this computer. Dont start too many tasks! it will overwhelm system resources.
%                   4 = SGE Bigrsub; use this on an appnode to submit tasks to the cluster. 
%  executionModeList : List of available execution modes
%
% Function known to use load_clusterconifig:
%  run_on_cluster
%  submitTasks
%
% NOTE: The run_job_*.m functions do not use load_clusterconfig. 
%
% 13-2-2018, D.Poot, Erasmus MC : Created. 
 
persistent clusterconfig
persistent clusterconfigfile % full file name persistent to avoid dependency on pwd when updating configuration
executionModeList = {'none','local-blocking','local-parallel', 'SGE Bigrsub'}; % gives text description of executionMode as implemented in submitTasks. Dont update/reorder as that changes the meaning of stored executionMode values. 
                                                                               % Also used (implicitly) in run_on_cluster to apply opts.cacheOnly==true. 

if isempty(clusterconfig)
    clusterconfigfile = getclusterconfigfile; 
    if exist( clusterconfigfile, 'file' )
        clusterconfigfile = which( clusterconfigfile);
        clusterconfig = load( clusterconfigfile );
    else
        clusterconfigfile = fullfile( pwd, clusterconfigfile);
        if exist( 'clusterconfig.mat', 'file' )
            clusterconfig = load( clusterconfigfile );
        else
            disp('load_clusterconfig.m could not find a cluster configuration file specific for this computer.')
            disp('Please provide the requested information below the following explanatory help text of load_clusterconfig:')
            help( mfilename );
            clusterconfig = struct; 
            clusterconfig.taskPath = input( 'taskPath : ', 's');
            clusterconfig.execPath = input( 'execPath : ', 's');
            clusterconfig.storeThresholdBytes = input( 'storeThresholdBytes (integer) : ');
            tmp = [num2cell((1:numel(executionModeList))') executionModeList(:)]';
            clusterconfig.executionMode = input( sprintf( 'executionMode (integer%s): ', sprintf(', %d="%s"',tmp{:})) );
        end;
        save( clusterconfigfile , '-struct', 'clusterconfig') 
    end;
end;
if nargin >= 1
    backupconfig = clusterconfig;
    clusterconfig = parse_defaults_optionvaluepairs( clusterconfig,  varargin{:} );
    if ~isequal( clusterconfig, backupconfig)
%         clusterconfigfile = getclusterconfigfile;
        save( clusterconfigfile , '-struct', 'clusterconfig');
    end;
end;
out = clusterconfig;
out.executionModeList = executionModeList; 

function [clusterconfigfile] = getclusterconfigfile 
clusterconfigfile = ['clusterconfig_' getComputerName() '.mat'];