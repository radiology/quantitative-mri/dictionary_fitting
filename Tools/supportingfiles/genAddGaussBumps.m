function [img, Jinfo]= genAddGaussBumps(dimSizes, points, intensity, sigma, threshold)
% [img, jacInfo]= genAddGaussBumps(dimSizes, points, intensity, sigma, threshold)
% OR
%  [JTmulpoint,  Jmulpoint ] = genAddGaussBumps(dimSizes, 'jacmulfun',   intensity,       sigma, threshold)
%  [JTmulintens, Jmulintens] = genAddGaussBumps(dimSizes,      points, 'jacmulfun',       sigma, threshold)
%  [JTmulsigma,  Jmulsigma ] = genAddGaussBumps(dimSizes,      points,   intensity, 'jacmulfun', threshold)
%
% Generates an N-dimensional image with additive gauss bumps.
% INPUTS:
%  dimSizes  : N-long integer vector with size of image
%  points    : k by N matrix with the N dimensional coordinates of the k points.
%  intensity : k long vector with the peak intensity of the Gaus bump; default = 1.
%			  (much faster when it is constant)
%  sigma     : Width of the gauss distribution, in pixels, default = 1.
%             (1 or k) x (1 or N) matrix. 
%  threshold : fraction
%
% OUTPUTS:
%   img : dimSizes size image in which the bumps are placed.
%   REMOVED OUTPUT (12-6-2018, D.Poot): filt : filterbank filter used internaly as approximation of the gauss. 
%   jacInfo : jacobian information structure.
% 
%   Jmulpoint,  JTmulpoint  : function to multiply a single vector with  D[ img(i) , points( j ) )
%   Jmulintens, JTmulintens : function to multiply a single vector with  D[ img(i) , intensity( j ) )
%   Jmulsigma,  JTmulsigma  : function to multiply a single vector with  D[ img(i) , sigma( j ) )
%        all these functions have signature Jx = Jfun( jinfo, x )
%   The 'Jmul' functions do perform:
%           Jx( j ) = sum_i  D[ img(i) , points/intensity/sigma( j ) ) * x( i )
%           with Jx a column vector of length numel(points/intensity/sigma)
%           and x a column vector of length numel(img)
%   The 'JTmul' functions to perform:
%           Jx( i ) = sum_j  D[ img(i) , points/intensity/sigma( j ) ) * x( j )
%           with x a column vector of length numel(points/intensity/sigma)
%           and Jx a column vector of length numel(img)
%   use @(J,X,flag) wrapJacobianMul(J, X, flag, Jmul, JTmul , false) to create 
%   a jacobian multiply function for some optimization routines. 
%
% Created by Dirk Poot, University of Antwerp.
% Date created : ?
% modified: 23-5-2008
% 12-6-2018: added jacobian multiply outputs and jacobian multiply functions
%            Added test function. 

mayusemex = true;
if nargin<3 || isempty(intensity)
    intensity = 1;
    one_intensity = mayusemex;
else
    one_intensity = mayusemex && all(intensity(:)==intensity(1));
end;
if nargin<4 || isempty(sigma)
    sigma =1;
end;
if nargin<5 || isempty(threshold)
    threshold = .001;
end;
if threshold >=1
    error('threshold should be < 1');
end;
if isequal( points, 'jacmulfun' );
    img  = @genAddGaussBumps_jacmulPoints_L;
    Jinfo = @genAddGaussBumps_jacmulPoints_R;
    return;
end;
if isequal( intensity, 'jacmulfun' );
    img  = @genAddGaussBumps_jacmulIntens_L;
    Jinfo = @genAddGaussBumps_jacmulIntens_R;
    return;
end;
if isequal( sigma, 'jacmulfun' );
    img  = @genAddGaussBumps_jacmulSigma_L;
    Jinfo = @genAddGaussBumps_jacmulSigma_R;
    return;
end;
if numel(intensity)~= size(points,1)
    if numel(intensity)==1
        if ~one_intensity
            intensity = repmat(intensity,size(points,1),1);
        end;
    else
        error('size of intensity does not match with points');
    end;
end;
ndims = length(dimSizes);
Coord = cell(ndims,1);

gausscale = 1./(2*sigma.^2);
threshdist = sqrt(-log(threshold)./gausscale);

if one_intensity && numel(gausscale)==1
    % use fast mex file, which (unfortunately) currently does not support
    % different intensities for the different bumps.
    filtlen = ceil(2*threshdist);
    nsteps = 30;
    rfilt = exp(-gausscale * linspace(-filtlen/2,filtlen/2,filtlen*nsteps+1).^2); 
    filt = fliplr(reshape(rfilt(2:end),nsteps,filtlen)');
    filt(:,end+1) = [rfilt(1);filt(1:end-1,1)];
    % filt = filt/max(sum(filt));
    sizeandscale = [dimSizes+2*filtlen-1;(filtlen-1)*ones(1,numel(dimSizes))-size(filt,1)/2;ones(1,numel(dimSizes))]';
    pointsc = mat2cell(points, size(points,1), ones(1,size(points,2)));
    transform = [];
    img =  MI_ND_dirk( pointsc , sizeandscale , transform, filt, [], [], []);
    sel = cell(1,numel(dimSizes));
    for k=1:numel(sel)
        sel{k} = filtlen:dimSizes(k)+2*filtlen-1-filtlen;
    end;
    img = img(sel{:});
    if ~isequal(intensity,1)
        img = img * intensity(1);
    end;
    Jinfo = [];
else
    % general case (full MATLAB code; quite slow)
    
    if size( threshdist, 1)==1
        threshdist = threshdist( ones(size(points,1),1),: );
    end;
    if numel(intensity)==1
        intensity = intensity( ones( size(points,1), 1) );
    end;
    if size( gausscale , 1 )==1
        gausscale = gausscale( ones( size(points,1),1),:);
    end;
    if size( gausscale , 2 )==1
        gausscale = gausscale( :, ones( ndims,1) );
    end;
    
    % Compute start (st) and end (ed) indices for each point in each dimension as:
    % st = max(        1, points - threshdist )
    % ed = min( dimSizes, points + threshdist ) 
    st = max(        1, ceil(  bsxfun(@minus, points , threshdist ) ) );
    ed =  floor( bsxfun(@plus, points , threshdist  ) );
    for m = 1 : ndims
        ed( ed(:, m)> dimSizes(m) ,m ) = dimSizes(m);
    end;
    Jinfo = struct( 'points', points,'ed',ed,'st',st,'dimSizes',dimSizes,'gausscale',gausscale,'intensity',intensity,'sigma',sigma);
%     img=zeros(dimSizes);
    img = reshape( genAddGaussBumps_jacmulIntens_R( Jinfo, intensity ), dimSizes);
%     for k = find( all( ed >= st , 2) )' % loop over all points that have a contribution
%         bump = intensity(k);
%         for m = 1 : ndims
%             Coord{m} = st( k, m ) : ed( k, m );
%             bump = bump(:) * exp(- gausscale( k, m ) * (Coord{m} - points(k,m)).^2);
%         end;
%         img(Coord{:}) = img(Coord{:}) + reshape( bump, ed(k,:)-st(k,:)+1);
%     end;
end;

function img = genAddGaussBumps_jacmulIntens_R( jinfo, intensity )
% Jx = genAddGaussBumps_jacmulPoints_R( jinfo, x )
% Jx(i) = sum_j  D[ img(i) , intensity( j ) ) * x( j )
% numel( Jx ) = numel( img )
% numel(  x ) = numel( intensity ) 
ndims = length(jinfo.dimSizes);
Coord = cell(ndims,1);
img = zeros( jinfo.dimSizes );
ed = jinfo.ed;
st = jinfo.st;
gausscale = jinfo.gausscale;
points = jinfo.points;
for k = find( all( ed >= st , 2) )'
    bump = intensity(k);
    for m = 1 : ndims
        Coord{m} = st( k, m ) : ed( k, m );
        bump = bump(:) * exp(- gausscale( k, m ) * (Coord{m} - points(k,m)).^2);
    end;
    img(Coord{:}) = img(Coord{:}) + reshape( bump, ed(k,:) - st(k,:) + 1 );
end;
img = img(:);

function Jx = genAddGaussBumps_jacmulIntens_L( jinfo, x )
% Jx = genAddGaussBumps_jacmulIntens_L( jinfo, x )
% Jx(j) = sum_j  D[ img(i) , intens( j ) ) * x( i )
% numel( Jx ) = numel( intens )
% numel(  x ) = numel( img ) 
ndims = length( jinfo.dimSizes );
Coord = cell(ndims,1);
ed = jinfo.ed;
st = jinfo.st;
gausscale = jinfo.gausscale;
points = jinfo.points;
x = reshape( x, jinfo.dimSizes );
Jx = zeros( size( jinfo.intensity ) );
for k = find( all( jinfo.ed>=jinfo.st , 2) )'
    % recusively reduce patch:
    for m = 1 : ndims
        Coord{m} = st(k,m):ed(k,m);
    end;
    tmp = x( Coord{:} ) ;
    for m = 1 : ndims 
        
        f  = exp(- gausscale( k, m ) * (Coord{m} - points(k,m)).^2);
        tmp = f * reshape( tmp, numel( f ), []);
    end;
    % now each element of tmp contains 1 scalar; value; derivative 
    Jx( k ) = tmp;
end;
Jx = Jx(:);

function Jx = genAddGaussBumps_jacmulPoints_R( jinfo, x )
% Jx = genAddGaussBumps_jacmulPoints_R( jinfo, x )
% Jx(i) = sum_j  D[ img(i) , points( j ) ) * x( j )
% numel( Jx ) = numel( img )
% numel(  x ) = numel( points ) 
ndims = length(jinfo.dimSizes);
Coord = cell(ndims,1);
ed = jinfo.ed;
st = jinfo.st;
gausscale = jinfo.gausscale;
points = jinfo.points;
x = reshape( x, size( points ) );
Jx = zeros( jinfo.dimSizes );

for k = find( all( ed >= st , 2) )'
    bump = jinfo.intensity(k);
    dbump = 0; 
    for m = 1 : ndims
        Coord{m} = st(k,m):ed(k,m);
        f  = exp(- gausscale( k, m ) * (Coord{m} - points(k,m)).^2);
        df = f .* (2 * gausscale( k, m ) * (Coord{m} - points(k,m)) ) ; 
        dbump = dbump(:) * f + bump(:) * df * x( k, m );
        if m<ndims
            bump = bump(:) * f;
        end;
    end;
    Jx( Coord{:} ) = Jx( Coord{:} ) + reshape( dbump, ed(k,:) - st(k,:) + 1 );
end;
Jx = Jx(:);

function Jx = genAddGaussBumps_jacmulPoints_L( jinfo, x )
% Jx = genAddGaussBumps_jacmulPoints_R( jinfo, x )
% Jx(j) = sum_i  D[ img(i) , points( j ) ) * x( i )
% numel( Jx ) = numel( points )
% numel(  x ) = numel( img ) 
ndims = length( jinfo.dimSizes );
Coord = cell(ndims,1);
ed = jinfo.ed;
st = jinfo.st;
gausscale = jinfo.gausscale;
points = jinfo.points;
tmp = cell( ndims + 1, 1);
x = reshape( x, jinfo.dimSizes );
Jx = zeros( size( jinfo.points ) );
for k = find( all( jinfo.ed>=jinfo.st , 2) )'
    % recusively reduce patch:
    for m = 1 : ndims
        Coord{m} = st(k,m):ed(k,m);
    end;
    tmp{1} = x( Coord{:} ) ;
    for m = 1 : ndims 
        
        f  = exp(- gausscale( k, m ) * (Coord{m} - points(k,m)).^2);
        df = f .* (2 * gausscale( k, m ) * (Coord{m} - points(k,m)) ) ; 
        
        tmp{m+1} = df * reshape( tmp{1} , numel(df ), []);
        for m2 = 1 : m
            tmp{ m2 } = f * reshape( tmp{ m2 } , numel( f ), []);
        end;
    end;
    % now each element of tmp contains 1 scalar; value; derivative 
    Jx( k , :) = [tmp{2:end}] * jinfo.intensity(k);
end;
Jx = Jx(:);

function Jx = genAddGaussBumps_jacmulSigma_R( jinfo, x )
% Jx = genAddGaussBumps_jacmulSigma_R( jinfo, x )
% Jx(i) = sum_j  D[ img(i) , sigma( j ) ) * x( j )
% numel( Jx ) = numel( img )
% numel(  x ) = numel( sigma ) 
ndims = length(jinfo.dimSizes);
Coord = cell(ndims,1);
ed = jinfo.ed;
st = jinfo.st;
gausscale = jinfo.gausscale;
points = jinfo.points;
rsigma3 = sqrt(2*gausscale).^3;
szsigma = size( jinfo.sigma );
x = reshape( x, szsigma );
if szsigma(1)==1
    x = x( ones( size(points,1),1),:);
end;
if szsigma(2)==1
    x = x( :, ones( size(points,2),1));
end;
Jx = zeros( jinfo.dimSizes );

for k = find( all( ed >= st , 2) )'
    bump = jinfo.intensity(k);
    dbump = 0; 
    for m = 1 : ndims
        Coord{m} = st(k,m):ed(k,m);
        f  = exp(- gausscale( k, m ) * (Coord{m} - points(k,m)).^2);
        df = f .* ( (Coord{m} - points(k,m)).^2 .* rsigma3(k,m) ) ; 
        dbump = dbump(:) * f + bump(:) * df * x( k, m );
        if m<ndims
            bump = bump(:) * f;
        end;
    end;
    Jx( Coord{:} ) = Jx( Coord{:} ) + reshape( dbump, ed(k,:) - st(k,:) + 1 );
end;
Jx = Jx(:);

function Jx = genAddGaussBumps_jacmulSigma_L( jinfo, x )
% Jx = genAddGaussBumps_jacmulSigma_L( jinfo, x )
% Jx(j) = sum_j  D[ img(i) , sigma( j ) )  * x( i )
% numel( Jx ) = numel( sigma )
% numel(  x ) = numel( img ) 
ndims = length( jinfo.dimSizes );
Coord = cell(ndims,1);
ed = jinfo.ed;
st = jinfo.st;
gausscale = jinfo.gausscale;
points = jinfo.points;
rsigma3 = sqrt(2*gausscale).^3;
tmp = cell( ndims + 1, 1);
x = reshape( x, jinfo.dimSizes );
Jx = zeros( size( jinfo.points ) );
for k = find( all( jinfo.ed>=jinfo.st , 2) )'
    % recusively reduce patch:
    for m = 1 : ndims
        Coord{m} = st(k,m):ed(k,m);
    end;
    tmp{1} = x( Coord{:} ) ;
    for m = 1 : ndims 
        
        f  = exp(- gausscale( k, m ) * (Coord{m} - points(k,m)).^2);
        df = f .* ( (Coord{m} - points(k,m)).^2 .* rsigma3(k,m) ) ; 
        
        tmp{m+1} = df * reshape( tmp{1} , numel(df ), []);
        for m2 = 1 : m
            tmp{ m2 } = f * reshape( tmp{ m2 } , numel( f ), []);
        end;
    end;
    % now each element of tmp contains 1 scalar; value; derivative 
    Jx( k , :) = [tmp{2:end}] * jinfo.intensity(k);
end;
szsigma = size( jinfo.sigma );
if szsigma(1)==1
    Jx = sum(Jx,1);
end;
if szsigma(2)==1
    Jx = sum(Jx,2);
end;
Jx = Jx(:);

function test()
%% test 1 point with reference
pnt1 = [6.234 4.45]
x1= (1:17)- pnt1(1);
x2 = (1:9) - pnt1(2);
[X1,X2] = ndgrid( x1, x2 );
sigm = 1.4;
ref = exp(-(X1.^2+X2.^2)/(2*sigm.^2));
img= genAddGaussBumps( size(ref), pnt1, 1, sigm ); % mex version 
[max(abs(ref(:))), max(abs(img(:))), max(abs(ref(:)-img(:)))]
ref = ref*1.234;
img= genAddGaussBumps( size(ref), pnt1, 1.234, sigm ); % MATLAB version
[max(abs(ref(:))), max(abs(img(:))), max(abs(ref(:)-img(:)))]
%% test multi points:
sz = [20 23];
points = bsxfun( @times, sz+1, rand(100, numel(sz)) );
intens = rand(size(points,1),1);
sigms = 1+rand(size(points,1),1);
img = genAddGaussBumps( sz , points, intens, sigms );
ref =0;
for k= 1 : size(points,1)
    ref = ref + genAddGaussBumps( sz , points(k,:), intens(k), sigms(k) );
end;
[max(abs(ref(:))), max(abs(img(:))), max(abs(ref(:)-img(:)))]
imagebrowse( cat(3, img, ref, img-ref))
%% multiply with jacobian with respect to points:
sz = [12 13];
points = bsxfun( @times, sz+1, rand(7, numel(sz)) );
intens = rand(size(points,1),1);
sigms = 1+rand(size(points,1),1);
fun = @(pnt) genAddGaussBumps( sz , pnt, intens, sigms );
[JTmul, Jmul]= fun( 'jacmulfun' );
dum = validateJacobian( fun,  points, @(J,X,flag) wrapJacobianMul(J, X, flag, Jmul, JTmul , false) );
imagebrowse( cat(3, dum.J,  dum.Jest ) )
%% multiply with jacobian with respect to intensity:
fun = @(intens) genAddGaussBumps( sz , points, intens, sigms );
[JTmul, Jmul]= fun( 'jacmulfun' );
dum = validateJacobian( fun,  intens, @(J,X,flag) wrapJacobianMul(J, X, flag, Jmul, JTmul , false) );
%% multiply with jacobian with respect to sigma:
fun = @(sigms) genAddGaussBumps( sz , points, intens, sigms, 1e-12 );
[JTmul, Jmul]= fun( 'jacmulfun' );
dum = validateJacobian( fun,  sigms, @(J,X,flag) wrapJacobianMul(J, X, flag, Jmul, JTmul , false) );
imagebrowse( cat(3, dum.J,  dum.Jest, dum.J-dum.Jest, dum.significantdifferences, dum.errorest ) )