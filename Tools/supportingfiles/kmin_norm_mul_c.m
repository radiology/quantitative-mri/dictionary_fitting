function [out, ind] = kmin_norm_mul_c(A, B, k)
% [out ind] = kmin_norm_mul_c(A, B [,k]).  
%   A  = points A : npntA x ndim ;
%   B  = points B : npntB x ndim x ncol
%   k  = number of closest neighbors : scalar integer
%
%   This function computes:
%
%   dst(i,j) =  sum(  abs( A(i, :) * permute( B(j, :, :) ,[2 3 1]) ).^2 );
%   if k is provided: 
%       [dst_sort ,index]  = sort( dst , 2 , 'descend')
%       dst_out = dst_sort(:, 1:k )';
%       index_out = index( :, 1:k )';
%       -> Two outputs are required (dst_out, index_out).
%   else
%       -> One output is required (dst).
%
%   ****  Created by Dirk Poot, Erasmus MC, 11-3-2017  ****);
                     
error('This function is implemented in a mex file.')
end
function test();
%%
%% Test script to verify (fast) mex result against (simple) reference implementation
% Currently, many tests fail due to irrelevant roundoff differences (due to order of processing)
% So the checks below should be made less strict. (not test for exact equality of 'out') 

% %
A = randn(11,10);
B = randn(15,10,1);

[out1 ] = kmin_norm_mul_referenceImplementation( A, B );
[out2 ] = kmin_norm_mul_c(A, B);
if ~isequal(out1,out2)
    error('test1 fail');
end;
%%
k =7;
[out1 , ind1] = kmin_norm_mul_referenceImplementation( A, B ,k );
[out2 , ind2] = kmin_norm_mul_c(A, B, k);
if ~isequal(out1,out2) || ~isequal(ind1,ind2)
    error('test2 fail');
end;

%%
A = randn(1000,10);
B = randn(1231,10,15);
tic
[out1 ] = kmin_norm_mul_referenceImplementation( A, B );toc,tic
[out2 ] = kmin_norm_mul_c(A, B);toc
if ~isequal(out1,out2)
    error('test3 fail');
end;
%%
k =7;
tic
[out1 , ind1] = kmin_norm_mul_referenceImplementation( A, B ,k );toc,tic
[out2 , ind2] = kmin_norm_mul_c(A, B, k);toc
if ~isequal(out1,out2) || ~isequal(ind1,ind2)
    error('test4 fail');
end;

%% Large
A = randn(13753,11);
B = randn(12321,11,5);
k = 17;
tic
[out1 , ind1] = kmin_norm_mul_referenceImplementation( A, B ,k );tu1=toc,tic
[out2 , ind2] = kmin_norm_mul_c(A, B, k);tu2=toc
nflop = size(A,1)*size(A,2)*size(B,1)*size(B,3);
clockfreq = 2.26e9;
nclockperflop = [tu1 tu2]*clockfreq/nflop
if ~isequal(out1,out2) || ~isequal(ind1,ind2)
    error('test5 fail');
end;

%% Complex values:
A = randn(121,11);
B = randn(200,11,5)+1i*randn(200,11,5);
k = 3;
tic
[out1 , ind1] = kmin_norm_mul_referenceImplementation( A, B ,k );toc,tic
[out2 , ind2] = kmin_norm_mul_c(A, B, k);toc
if ~isequal(out1,out2) || ~isequal(ind1,ind2)
    error('test6 fail');
end;

%% Complex values2:
A = randn(121,12)+1i*randn(121,12);
B = randn(200,12,4);
k = 3;
tic
[out1 , ind1] = kmin_norm_mul_referenceImplementation( A, B ,k );toc,tic
[out2 , ind2] = kmin_norm_mul_c(A, B, k);toc
if ~isequal(out1,out2) || ~isequal(ind1,ind2)
    error('test7 fail');
end;
%% Complex values3:
A = randn(123,12)+1i*randn(123,12);
B = randn(203,12,4)+1i*randn(203,12,4);
tic
[out1 ] = kmin_norm_mul_referenceImplementation( A, B  );toc,tic
[out2 ] = kmin_norm_mul_c(A, B);toc
if ~isequal(out1,out2) || ~isequal(ind1,ind2)
    error('test8 fail');
end;
%%
k = 3;
tic
[out1 , ind1] = kmin_norm_mul_referenceImplementation( A, B ,k );toc,tic
[out2 , ind2] = kmin_norm_mul_c(A, B, k);toc
if ~isequal(out1,out2) || ~isequal(ind1,ind2)
    error('test9 fail');
end;
end