/*
   Helper function that returns some timing data in the first output that 
   TransformNDbspline.cpp
   defines.
   See TransformNDbspline.cpp for further info an compilation instructions.

   Created by Dirk Poot, Erasmus MC, 5-7-2012

*/

#define PERFORMANCECOUNTING // if defined: return some counts in the first (double) elements of the output. SO FOR DEBUG ONLY!!!
#include "TransformNDbspline.cpp"
