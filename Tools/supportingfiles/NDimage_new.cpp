#ifndef NDIMAGE
#define NDIMAGE
/* This file provides a NDimage
 *
 *  class NDimage: image type, templated over data type and dimensionality. Can be sampled by a sampler e.g. a bspline sampler.
 *
 *  See  function 'self_test_mex' for example usage.
 *
 * Created by Dirk Poot, Erasmus MC
 */


#include "math.h"
//#include <vector>
#include <algorithm>

#include "bspline_filter.cpp"
#include "RecursiveSamplingImplementation.hxx"
#include "tempSpaceManaging.cpp"

/*
template <int vlen> struct vec<BAD_TYPE, vlen> {
	enum { naturalLen =0 };
};
template <typename T1, typename T2> struct vecptrhelper_c {
	static inline T1 get( T1 in, T2 in_vec ) {
		return in_vec.ptr;
	}
};
template <typename T1 > struct vecptrhelper_c<T1 , BAD_TYPE >{
static inline T1 get( T1 in, BAD_TYPE in_vec ) {
		return in;
	}
};
template <typename T1, typename T2> inline T1 vecptrhelper(T1 in, T2 in_vec) {
	return vecptrhelper_c<T1, T2>::get(in, in_vec);
}
*/
/* combineVecfun : helper class for the sampling of vector images
 *
 * INPUTS:
 *
 */
template < typename ImagePointerType, typename outputPointerType, typename WeightsPointerType> class combineVecfun {
private:
    typedef typename iterator_traits< outputPointerType >::value_type procValueType;
    typedef typename iterator_traits< WeightsPointerType >::value_type WeightsValueType; 
    
	enum {vlen = vec<procValueType,16>::naturalLen};
	enum {doVectorize = AND< vecTptr<outputPointerType,vlen>::supported, vecTptr<ImagePointerType,vlen>::supported>::RET};
	typedef typename IF< doVectorize  , vecTptr<outputPointerType,vlen>, BAD_TYPE>::RET vecPtrTypeOut;
	typedef typename IF< doVectorize  , vecTptr<ImagePointerType,vlen>, BAD_TYPE>::RET vecPtrTypeImage;
    
public:
    static inline void sample( outputPointerType out, IndexType veclen, ImagePointerType image, WeightsPointerType weights, int_t * stepsTable, int nsteps ) {
        // dummy baseline/reference implementation, stressing cache (operating from too many lines 'simultaneously')
        // and reading weights and stepsTable too often (veclen * nsteps reads from image, stepsTable and weights )
        // probably should 'autocast' to use vec (if possible) and process all of a limitted (fixed) number of stepsTable at a time
        // (e.g. 4 'stepsTable', and good vec_vlen )
        const int maxStreams = 6; 
        
        if (nsteps >= maxStreams ) {
            // Process in blocks of steps; to avoid reading from too many streams of memory. 
            int step = 0;
            for ( int k = 0 ; k < veclen; ++k ) {
                out[k]= 0;
            }
            while (step < nsteps ) {
                //int curnumsteps = (nsteps - step> maxStreams ? maxStreams : nsteps - step); // min( nsteps - step, maxStreams )
                int curnumsteps = nsteps - step;
                if (curnumsteps > maxStreams) {
                    curnumsteps = maxStreams ; 
                };
                if ( (curnumsteps==6)  && (maxStreams>=6) ) { // second condition added to make sure the compiler sees when the code (and test) can be optimized away (i.e. when maxStreams is lower). 
                    outputPointerType curout( out ) ;
                    ImagePointerType curimage( image + stepsTable[ step ] );
                    int_t step1 = stepsTable[ step +1 ]-stepsTable[ step ];
                    int_t step2 = stepsTable[ step +2 ]-stepsTable[ step ];
                    int_t step3 = stepsTable[ step +3 ]-stepsTable[ step ];
                    int_t step4 = stepsTable[ step +4 ]-stepsTable[ step ];
                    int_t step5 = stepsTable[ step +5 ]-stepsTable[ step ];
                    /*WeightsValueType weight0 = weights[ step     ];
                    WeightsValueType weight1 = weights[ step + 1 ];
                    WeightsValueType weight2 = weights[ step + 2 ];
                    WeightsValueType weight3 = weights[ step + 3 ];
                    WeightsValueType weight4 = weights[ step + 4 ];
                    WeightsValueType weight5 = weights[ step + 5 ];*/
                    int nToDo = veclen;
                    if (doVectorize && (nToDo>=vlen) ) {
                        vecPtrTypeOut out_vec(curout);
                        vecPtrTypeImage image_vec(curimage);
                        sample_6streams( out_vec, nToDo, vlen, image_vec, step1  , step2  , step3  , step4  , step5   , 
                                                               weights+step /*weight0  , weight1, weight2, weight3, weight4, weight5 */ );
                        curout = vecptrhelper(curout, out_vec);
                        curimage = vecptrhelper(curimage, image_vec);
                    }
                    // end case:
   //* 
                    sample_6streams( curout, nToDo, 1, curimage, step1  , step2  , step3  , step4  , step5   , 
                                                      weights+step /* weight0  , weight1, weight2, weight3, weight4, weight5 */);
                    
/*/                 
                    int vlen_ = 1; 
                    for (  ; nToDo >= vlen_  ; nToDo-=vlen_, curout+=vlen_, curimage+=vlen_) {
                        curout[0] += ( curimage[  0    ]*weight0 + curimage[ step1 ]*weight1 + curimage[ step2 ]*weight2 )
                                   + ( curimage[ step3 ]*weight3 + curimage[ step4 ]*weight4 + curimage[ step5 ]*weight5 ) ;
                    } 
                    /* /
                    for ( int k = 0 ; k < veclen ; ++k , ++curimage) {
                        out[k] += ( curimage[  0    ]*weight0 + curimage[ step1 ]*weight1 + curimage[ step2 ]*weight2 )
                                + ( curimage[ step3 ]*weight3 + curimage[ step4 ]*weight4 + curimage[ step5 ]*weight5 ) ;
                    } // */
                } else {
                    ImagePointerType curimage = image ;
                    for ( int k = 0 ; k < veclen ; ++k , ++curimage) {
                        procValueType temp = 0;
                        for (int steplp = step ; steplp < step + curnumsteps; ++steplp ) {
                            temp += curimage[ stepsTable[ steplp ]  ] * weights[ steplp ];
                        }
                        out[k] += temp;
                    }
                }
                step += curnumsteps;
            }
        } else {
            for ( int k = 0 ; k < veclen ; ++k ) {
                procValueType temp = 0;
                for (int step = 0 ; step < nsteps ; ++step ) {
                    temp += image[ stepsTable[ step ] + k ] * weights[ step ];
                }
                out[k]= temp;
            }
        }
    }
    
    static inline void sampleAdjoint( outputPointerType out, IndexType veclen, ImagePointerType image, WeightsPointerType weights, const int_t * stepsTable, int nsteps ) {
        // dummy baseline/reference implementation, stressing cache (operating from too many lines 'simultaneously')
        // and reading weights and stepsTable too often (veclen * nsteps reads from image, stepsTable and weights )
        // probably should 'autocast' to use vec (if possible) and process all of a limitted (fixed) number of stepsTable at a time
        // (e.g. 4 'stepsTable', and good vec_vlen )
        typedef typename iterator_traits< outputPointerType >::value_type procValueType;
        for ( int k = 0 ; k < veclen ; ++k ) {
            procValueType temp = out[k];
            for (int step = 0 ; step < nsteps ; ++step ) {
                image[ stepsTable[ step ] + k ] += temp * weights[ step ];
            }
        }
    }
private:
    template <typename outputPointerType_, typename ImagePointerType_> static __forceinline void sample_6streams( outputPointerType_  & curout, int & nToDo, const int vlen_,  ImagePointerType_ & curimage, int_t            step1  , int_t            step2  , int_t            step3  , int_t            step4  , int_t            step5   , 
                                                                                                                                                                    WeightsPointerType weights /* WeightsValueType   weight0 , WeightsValueType weight1, WeightsValueType weight2, WeightsValueType weight3, WeightsValueType weight4, WeightsValueType weight5 */ ) {
        
        // Weights should be obtained outside of loop as otherwise compiler might have to assume aliasing with out. 
        WeightsValueType weight0 = weights[ 0 ];
        WeightsValueType weight1 = weights[ 1 ];
        WeightsValueType weight2 = weights[ 2 ];
        WeightsValueType weight3 = weights[ 3 ];
        WeightsValueType weight4 = weights[ 4 ];
        WeightsValueType weight5 = weights[ 5 ];
        for (  ; nToDo >= vlen_  ; nToDo-=vlen_, curout+=vlen_, curimage+=vlen_) {
            curout[0] += ( curimage[  0    ]*weight0 + curimage[ step1 ]*weight1 + curimage[ step2 ]*weight2 )
                       + ( curimage[ step3 ]*weight3 + curimage[ step4 ]*weight4 + curimage[ step5 ]*weight5 ) ;
        }        
    }
/*    static inline void sample_6streams(  BAD_TYPE curout, int & nToDo, const int vlen_, BAD_TYPE curimage, int_t            step1  , int_t            step2  , int_t            step3  , int_t            step4  , int_t            step5   , 
                                                                              WeightsValueType   weight0 , WeightsValueType weight1, WeightsValueType weight2, WeightsValueType weight3, WeightsValueType weight4, WeightsValueType weight5 ) {
    };*/
};



template <int SpaceDimension, typename ImagePointerType> class NDimage;
template <int SpaceDimension, typename ImagePointerType> class NDvecImage;

template <typename ImagePointerType> class NDimage_base {
public:
    enum BoundaryCondition { zeroExtend, mirror, wrap };
private:
    const int ndims;
    const int subType;
    const IndexType vlen;
    BoundaryCondition mirror_boundary;
protected:
    // Protected constructor to only allow public instantiation of subclasses. 
    NDimage_base( int ndims_, int subType_, IndexType vlen_=1) : ndims(ndims_), subType(subType_), vlen(vlen_) {
        if ( (ndims_<1) || (ndims_>6) ) {
            mexErrMsgTxt("Image dimension should be in [1 ... 6].");
        }
        if ((subType!=0) && (subType!=1)) {
            mexErrMsgTxt("Only NDimage (subType==0) and NDvecImage (subType==1) are known to NDimage_base.");
        }
        mirror_boundary = zeroExtend; // default: zero boundary
    }
public:
    int Ndims() { return ndims; };
    IndexType Vlen() { return vlen; };
    BoundaryCondition getBoundarycondition() { return mirror_boundary; };
    void setBoundarycondition( int mirror_boundary_ ) { 
        if (mirror_boundary_  == 0) 
            mirror_boundary = zeroExtend ;
        else if (mirror_boundary_  == 1) 
            mirror_boundary = mirror ;
        else if (mirror_boundary_ == 2) 
            mirror_boundary = wrap ;
        else
            mexErrMsgTxt("Invalid boundary condition selected.");
    }
    template< typename OutputValueType, typename locationType, typename samplerType> OutputValueType sample( locationType * point, samplerType & sampler) {
        // Sampling function. Calls child sampling functions.
        // Cannot be done with virtual overloading due to templates, but also I would like to (be able to) avoid the virtual call
        // by templating iteration routine over the actual subclass of the correct dimension.
        if (subType==0) {
            if (ndims==1) {
                return ((NDimage<1,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==2) {
                return ((NDimage<2,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==3) {
                return ((NDimage<3,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==4) {
                return ((NDimage<4,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==5) {
                return ((NDimage<5,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==6) {
                return ((NDimage<6,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else {
                return 0; // cannot be reached, constructor forbids.
            }
        } else { // subType ==1
            mexErrMsgTxt("Vector image does not return scalar.");
            return 0;
        }
        
        
    }
    template< typename OutputPointerType, typename locationType, typename samplerType> void sample(OutputPointerType out,  locationType * point, samplerType & sampler) {
        // Sampling function. Calls child sampling functions.
        // Cannot be done with virtual overloading due to templates, but also I would like to (be able to) avoid the virtual call
        // by templating iteration routine over the actual subclass of the correct dimension.
        if (subType==0) {
            typedef typename iterator_traits< OutputPointerType >::value_type OutputValueType;
            if (ndims==1) {
                *out = ((NDimage<1,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==2) {
                *out = ((NDimage<2,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==3) {
                *out = ((NDimage<3,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==4) {
                *out = ((NDimage<4,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==5) {
                *out = ((NDimage<5,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else if (ndims==6) {
                *out = ((NDimage<6,ImagePointerType>*)this)->template sample<OutputValueType>(point, sampler );
            } else {
                ; // cannot be reached, constructor forbids.
            }
        } else { // subType ==1
            if (ndims==1) {
                ((NDvecImage<1,ImagePointerType>*)this)->template sample<OutputPointerType>(out, point, sampler );
            } else if (ndims==2) {
                ((NDvecImage<2,ImagePointerType>*)this)->template sample<OutputPointerType>(out, point, sampler );
            } else if (ndims==3) {
                ((NDvecImage<3,ImagePointerType>*)this)->template sample<OutputPointerType>(out, point, sampler );
            } else if (ndims==4) {
                ((NDvecImage<4,ImagePointerType>*)this)->template sample<OutputPointerType>(out, point, sampler );
            } else if (ndims==5) {
                ((NDvecImage<5,ImagePointerType>*)this)->template sample<OutputPointerType>(out, point, sampler );
            } else if (ndims==6) {
                ((NDvecImage<6,ImagePointerType>*)this)->template sample<OutputPointerType>(out, point, sampler );
            } else {
                ; // cannot be reached, constructor forbids.
            }
        }
    }
    template< typename OutputPointerType, typename locationType, typename samplerType> void sampleAndDerivative(OutputPointerType out, OutputPointerType Dout,  locationType * point, samplerType & sampler) {
        // Sampling function. Calls child sampling functions.
        // Cannot be done with virtual overloading due to templates, but also I would like to (be able to) avoid the virtual call
        // by templating iteration routine over the actual subclass of the correct dimension.
        if (subType==0) {
            if (ndims==1) {
                ((NDimage<1,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==2) {
                ((NDimage<2,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==3) {
                ((NDimage<3,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==4) {
                ((NDimage<4,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==5) {
                ((NDimage<5,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==6) {
                ((NDimage<6,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else {
                ; // cannot be reached, constructor forbids.
            }
        } else { // subType ==1
            if (ndims==1) {
                ((NDvecImage<1,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==2) {
                ((NDvecImage<2,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==3) {
                ((NDvecImage<3,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==4) {
                ((NDvecImage<4,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==5) {
                ((NDvecImage<5,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else if (ndims==6) {
                ((NDvecImage<6,ImagePointerType>*)this)->template sampleAndDerivative<OutputPointerType>(out, Dout, point, sampler );
            } else {
                ; // cannot be reached, constructor forbids.
            }
        }
    }
    template< typename OutputPointerType, typename locationType, typename samplerType> void sampleAdjoint(OutputPointerType out,  locationType * point, samplerType & sampler) {
        // Sampling function. Calls child sampling functions.
        // Cannot be done with virtual overloading due to templates, but also I would like to (be able to) avoid the virtual call
        // by templating iteration routine over the actual subclass of the correct dimension.
        if (subType==0) {
            //typedef typename iterator_traits< OutputPointerType >::value_type OutputValueType;
            if (ndims==1) {
                ((NDimage<1,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==2) {
                ((NDimage<2,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==3) {
                ((NDimage<3,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==4) {
                ((NDimage<4,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==5) {
                ((NDimage<5,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==6) {
                ((NDimage<6,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else {
                ; // cannot be reached, constructor forbids.
            }
        } else { // subType ==1
            if (ndims==1) {
                ((NDvecImage<1,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==2) {
                ((NDvecImage<2,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==3) {
                ((NDvecImage<3,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==4) {
                ((NDvecImage<4,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==5) {
                ((NDvecImage<5,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else if (ndims==6) {
                ((NDvecImage<6,ImagePointerType>*)this)->template sampleAdjoint<OutputPointerType>(out, point, sampler );
            } else {
                ; // cannot be reached, constructor forbids.
            }
        }
    }
};

using std::iterator_traits;
/* NDimage_filtered_base :
 * base class for filtered images.
 * Implements the evaluation of the filters, computes the start and end indices
 * and specifies which of the recursive sub methods should be called. */
template <int SpaceDimension, typename ImagePointerType> class NDimage_filtered_base : public NDimage_base<ImagePointerType> {
public:
protected:
    enum RecursiveSubMethod { full_regular, partial, full_stepped , none};
    typedef typename iterator_traits< ImagePointerType >::value_type ImageValueType;
    
    typedef ImageValueType WeightsValueType;
    ImagePointerType origin;
    
    WeightsValueType * filtcoeffs;//std::vector<WeightsValueType> filtcoeffs;
    WeightsValueType * Dfiltcoeffs;//std::vector<WeightsValueType> Dfiltcoeffs;
    OffsetValueType * stepsTable;//std::vector<OffsetValueType> stepsTable;
    IndexType size[ SpaceDimension ];
    int_t stride[ SpaceDimension ];
public:
    static int tempBytesNeeded(const int maxFilterLength) {
        return SpaceDimension*maxFilterLength * (sizeof(WeightsValueType)*2 + sizeof( OffsetValueType ))
#ifdef ADDGUARDS
                + 3*8
#endif
                        ;
    }
protected:
    NDimage_filtered_base( ImagePointerType origin_, const IndexType * size_, const int_t * stride_, const int maxFilterLength, int subType_, IndexType vlen_, tempspace & tmp)
    : NDimage_base<ImagePointerType>( SpaceDimension, subType_, vlen_)/*,
            filtcoeffs( SpaceDimension*maxFilterLength ),
            Dfiltcoeffs( SpaceDimension*maxFilterLength ),
            stepsTable( SpaceDimension*maxFilterLength )*/            
    {        
        filtcoeffs = tmp.get<WeightsValueType>( SpaceDimension*maxFilterLength  );
        Dfiltcoeffs= tmp.get<WeightsValueType>( SpaceDimension*maxFilterLength );
        stepsTable= tmp.get<OffsetValueType>( SpaceDimension*maxFilterLength );
        for (int i=0;i<SpaceDimension;i++) {
            size[i]=size_[i];
        }
        for (int i=0;i<SpaceDimension;i++) {
            stride[i]=stride_[i];
        }
        //ndims = ndims_;
        origin = origin_;
        
    };
    
    template< bool getDeriv, typename locationType, typename IndexType, typename samplerType>
    inline RecursiveSubMethod fillLocAndFilt( ImagePointerType & sampleorigin, IndexType loc[], int startEnds[], // output arguments.
                                locationType * point, samplerType & sampler ) {                   // inputs arguments. 
        sampleorigin= this->origin;
        bool allinrange = true;
        locationType samplerdelay = sampler.delay();
        int length = sampler.length();
        for (int i=0;i<SpaceDimension;i++) {
            locationType pti = point[i] - samplerdelay ;
            loc[i] = (IndexType) floor(pti);
            pti -= loc[i];
            sampler.get_coefficients( &filtcoeffs[length *i], pti);
            if (getDeriv) {
                sampler.get_coefficientsDerivative( &Dfiltcoeffs[length *i], pti, 1);
            }
            allinrange &= (loc[i]>=0) && (loc[i] + length  <= size[i]);
            sampleorigin += loc[i] * stride[i];
        }
        if (!allinrange) {
            if( this->getBoundarycondition()==0 ) {
                // zero boundary conditions
                for (int i=0;i<SpaceDimension;i++) {
                    using std::min;
                    using std::max;

                    int newstart = (int) max( (IndexType) 0, -loc[i]);
                    int newstop  = (int) min( (IndexType) length , size[i]-loc[i] ) ;
                    if (newstop<=newstart) {
                        return none;
                    }
                    sampleorigin += newstart * stride[i];
                    startEnds[i*2  ] = length *i + newstart;
                    startEnds[i*2+1] = length *i + newstop;
                }
                return partial;
            } else {
                // mirror boundary conditions or wrap around. 
                sampleorigin = this->origin;
                for (int i = 0 ; i < SpaceDimension; ++i ) {
                    for (int j = 0; j < length; j++) {
                        int tmp = j+loc[i];
                        if( this->getBoundarycondition()==1 ) { //mirror. 
                            while ((tmp < 0)||(tmp >= size[i] )){
                                if (tmp < 0) {
                                    tmp = -1 - tmp; // reflect at  -.5, so -1 becomes 0. 
                                } else if (tmp >= size[i] ) {
                                    tmp = 2*size[i]-1-tmp; // reflect at size[i]-.5 so size[i] becomes size[i]-1
                                }
                            }
                        } else { // wrap around:
                            // using conditional jump (and loop in exceptional cases).
                            // Could alternatively use a modulo operation. tmp %= size[i]
                            while (tmp < 0) {
                                tmp += size[i];
                            }
                            while ( tmp >= size[i] ) {
                                tmp -= size[i];
                            }                            
                        }
                        stepsTable[j+ length*i] = tmp * stride[i];
                    }
                }
                if (samplerType::fixedlength==-1) {
                    // only fill startEnds when needed because sampler has no compile time known length.
                    for (int i=0;i<SpaceDimension;i++) {
                        startEnds[i*2  ] = length *i ;
                        startEnds[i*2+1] = length *(i+1) ;
                    }
                }
                return full_stepped;
            }
        } else {
            if (samplerType::fixedlength==-1) {
                // only fill startEnds when needed because sampler has no compile time known length.
                for (int i=0;i<SpaceDimension;i++) {
                    startEnds[i*2  ] = length *i ;
                    startEnds[i*2+1] = length *(i+1) ;
                }
            }
            return full_regular;
        }
    };

};

/* class NDimage
 * Implements the scalar valued ND image
 */
template <int SpaceDimension, typename ImagePointerType> class NDimage : public NDimage_filtered_base<SpaceDimension, ImagePointerType> {
private:
    typedef typename iterator_traits< ImagePointerType >::value_type ImageValueType;
    typedef ImageValueType WeightsValueType;
    //std::vector<WeightsValueType> filtcoeffs;
    //std::vector<WeightsValueType> Dfiltcoeffs;
    //std::vector<OffsetValueType> stepsTable;
    typedef WeightsValueType * WeightsPointerType;
protected:
    typedef NDimage_filtered_base<SpaceDimension, ImagePointerType> parent;
    typedef typename parent::RecursiveSubMethod RecursiveSubMethod;
    //IndexType size[ SpaceDimension ];
    //int_t stride[ SpaceDimension ];
public:
    NDimage( ImagePointerType origin_, const IndexType * size_, const int_t * stride_, const int maxFilterLength, tempspace & tmp)
    : NDimage_filtered_base<SpaceDimension, ImagePointerType>(origin_, size_, stride_, maxFilterLength, 0, 1, tmp) /*,
            filtcoeffs( SpaceDimension*maxFilterLength ),
            Dfiltcoeffs( SpaceDimension*maxFilterLength ),
            stepsTable( SpaceDimension*maxFilterLength )            */
    {
        /*for (int i=0;i<SpaceDimension;i++) {
            size[i]=size_[i];
        }
        for (int i=0;i<SpaceDimension;i++) {
            stride[i]=stride_[i];
        }*/
    };
    
    template< typename OutputValueType, typename locationType, typename samplerType> OutputValueType sample( locationType * point, samplerType & sampler) {
        /*
        ImagePointerType sampleorigin = this->origin;
        
        bool allinrange = true;
        IndexType loc[SpaceDimension];
        locationType samplerdelay = sampler.delay();
        int length = sampler.length();
        int startEnds[2*SpaceDimension];
        for (int i=0;i<SpaceDimension;i++) {
            locationType pti = point[i] - samplerdelay ;
            loc[i] = (IndexType) floor(pti);
            pti -= loc[i];
            sampler.get_coefficients( &filtcoeffs[length *i], pti);
            allinrange &= (loc[i]>=0) && (loc[i] + length  <= size[i]);
            sampleorigin += loc[i] * stride[i];
        }
        if (!allinrange) {
            if( this->getBoundarycondition()==0 ) {
                // zero boundary conditions
                for (int i=0;i<SpaceDimension;i++) {
                    using std::min;
                    using std::max;
                    
                    int newstart = (int) max( (IndexType) 0, -loc[i]);
                    int newstop  = (int) min( (IndexType) length , size[i]-loc[i] ) ;
                    if (newstop<=newstart) {
                        return 0;
                    }
                    sampleorigin += newstart * stride[i];
                    startEnds[i*2  ] = length *i + newstart;
                    startEnds[i*2+1] = length *i + newstop;
                }
                return RecursiveSamplingImplementation_GetSample<SpaceDimension, -1 , ImagePointerType, OutputValueType, WeightsPointerType, 0>
                        ::GetSample( sampleorigin, stride, &filtcoeffs[0] , startEnds );
            }
            else {
                // mirror boundary conditions
                sampleorigin = this->origin;
                for (int i=0;i<SpaceDimension;i++) {
                    
                    for (int j=0;j<length;j++) {
                        int tmp = j+loc[i];
                        
                        while ((tmp < 0)||(tmp >= size[i] )){
                            if (tmp < 0) {
                                tmp = -1 - tmp;
                            } else if(tmp >= size[i] ) {
                                tmp = 2*size[i]-1-tmp;
                            }
                        }
                        
                        stepsTable[j+ length*i] = tmp * stride[i];
                    }
                }
                if (samplerType::fixedlength==-1) {
                    // only fill startEnds when needed because sampler has no compile time known length.
                    for (int i=0;i<SpaceDimension;i++) {
                        startEnds[i*2  ] = length *i ;
                        startEnds[i*2+1] = length *(i+1) ;
                    }
                }
                return RecursiveSamplingImplementation_GetSample<SpaceDimension, samplerType::fixedlength , ImagePointerType, OutputValueType, WeightsPointerType, USE_STEPS>
                        ::GetSample( sampleorigin, &stepsTable[0], &filtcoeffs[0] , startEnds );
            }
        } else {
            if (samplerType::fixedlength==-1) {
                // only fill startEnds when needed because sampler has no compile time known length.
                for (int i=0;i<SpaceDimension;i++) {
                    startEnds[i*2  ] = length *i ;
                    startEnds[i*2+1] = length *(i+1) ;
                }
            }
            return RecursiveSamplingImplementation_GetSample<SpaceDimension, samplerType::fixedlength , ImagePointerType, OutputValueType, WeightsPointerType, 0>
                    ::GetSample( sampleorigin, stride, &filtcoeffs[0], startEnds);
        }*/
        ImagePointerType sampleorigin ;
        IndexType loc[SpaceDimension];
        int startEnds[2*SpaceDimension];
        RecursiveSubMethod m = this->template fillLocAndFilt<false>( sampleorigin, loc, startEnds, // output arguments.
                                                     point, sampler );
        
        switch (m) {
            case RecursiveSubMethod::full_regular :
                return RecursiveSamplingImplementation_GetSample<SpaceDimension, samplerType::fixedlength , ImagePointerType, OutputValueType, WeightsPointerType, 0>
                        ::GetSample( sampleorigin, this->stride, &this->filtcoeffs[0], startEnds);
            case RecursiveSubMethod::partial :
                return RecursiveSamplingImplementation_GetSample<SpaceDimension, -1 , ImagePointerType, OutputValueType, WeightsPointerType, 0>
                        ::GetSample( sampleorigin, this->stride, &this->filtcoeffs[0] , startEnds );
            case RecursiveSubMethod::full_stepped:
                return RecursiveSamplingImplementation_GetSample<SpaceDimension, samplerType::fixedlength , ImagePointerType, OutputValueType, WeightsPointerType, USE_STEPS>
                        ::GetSample( sampleorigin, &this->stepsTable[0], &this->filtcoeffs[0] , startEnds );
            case RecursiveSubMethod::none:
                return 0;
        } 
        return 0; // cannot reach. 
    }
    
    /*  // Transform list of points:
     * template< typename OutputPointerType, typename locationType, typename samplerType> void sample(OutputPointerType out,  locationType * point, int npoints, samplerType & sampler) {
     * IndexType loc[SpaceDimension];
     * locationType samplerdelay = sampler.delay();
     * int length = sampler.length();
     * int startEnds[2*SpaceDimension];
     * int * filtStartIdxPointer = &filtStartidx[0];
     * WeightsPointerType filtCoefficientsPointer = &filtcoeffs[0];
     * sampler.get_indexAndCoefficients( filtStartIdxPointer, filtCoefficientsPointer, point, npoints*SpaceDimension);
     *
     * for (int pointidx = 0 ; pointidx < npoints ; ++pointidx , filtStartIdxPointer+=SpaceDimension, filtCoefficientsPointer+= SpaceDimension*length ){
     * bool allinrange = true;
     * ImagePointerType sampleorigin = this->origin;
     *
     * for ( int dim = 0; dim< SpaceDimension; ++dim ){
     * allinrange &= (filtStartIdxPointer[ dim ]>=0) && (filtStartIdxPointer[ dim ] + length  <= size[ dim ]);
     * sampleorigin += filtStartIdxPointer[ dim ] * stride[ dim ];
     * }
     * if (!allinrange) {
     * for (int dim = 0; dim < SpaceDimension; ++dim ) {
     * using std::min;
     * using std::max;
     *
     * int newstart = (int) max( (IndexType) 0, -filtStartIdxPointer[ dim ]);
     * int newstop  = (int) min( (IndexType) length , size[ dim ] - filtStartIdxPointer[ dim ] ) ;
     * if (newstop<=newstart) {
     * return 0;
     * }
     * sampleorigin += newstart * stride[ dim ];
     * startEnds[ dim * 2     ] = dim * length + newstart;
     * startEnds[ dim * 2 + 1 ] = dim * length + newstop;
     * }
     * return RecursiveSamplingImplementation_GetSample< SpaceDimension,                       -1 , ImagePointerType, OutputValueType, WeightsPointerType, 0>
     * ::GetSample( sampleorigin, stride, filtCoefficientsPointer, startEnds );
     * } else {
     * if (samplerType::fixedlength==-1) {
     * // only fill startEnds when needed because sampler has no compile time known length.
     * for (int dim = 0; dim < SpaceDimension; ++dim ) {
     * startEnds[ dim * 2     ] =  dim   * length ;
     * startEnds[ dim * 2 + 1 ] = (dim+1)* length ;
     * }
     * }
     * return RecursiveSamplingImplementation_GetSample< SpaceDimension, samplerType::fixedlength , ImagePointerType, OutputValueType, WeightsPointerType, 0>
     * ::GetSample( sampleorigin, stride, filtCoefficientsPointer, startEnds);
     * }
     * }
     * } */
    
    template< typename OutputPointerType, typename locationType, typename samplerType> void sampleAndDerivative( OutputPointerType out, OutputPointerType Dout, locationType * point, samplerType & sampler) {
        /*ImagePointerType sampleorigin = this->origin;
        bool allinrange = true;
        IndexType loc[SpaceDimension];
        locationType samplerdelay = sampler.delay();
        int length = sampler.length();
        int startEnds[2*SpaceDimension];
        for (int i=0;i<SpaceDimension;i++) {
            locationType pti = point[i] - samplerdelay ;
            loc[i] = (IndexType) floor(pti);
            pti -= loc[i];
            sampler.get_coefficients( &filtcoeffs[length *i], pti);
            sampler.get_coefficientsDerivative( &Dfiltcoeffs[length *i], pti, 1);
            allinrange &= (loc[i]>=0) && (loc[i] + length  <= size[i]);
            sampleorigin += loc[i] * stride[i];
        }
        typedef typename iterator_traits< OutputPointerType >::value_type OutputValueType;
        typedef OutputValueType * tempOutputPointerType;
        OutputValueType temp_out[ SpaceDimension +1 ];
        if (!allinrange) {
            if( this->getBoundarycondition()==0 ) {
                // zero boundary conditions
                for (int i=0;i<SpaceDimension;i++) {
                    using std::min;
                    using std::max;
                    
                    int newstart = (int) max( (IndexType) 0, -loc[i]);
                    int newstop  = (int) min( (IndexType) length , size[i]-loc[i] ) ;
                    if (newstop<=newstart) {
                        return ;
                    }
                    sampleorigin += newstart * stride[i];
                    startEnds[i*2  ] = length *i + newstart;
                    startEnds[i*2+1] = length *i + newstop;
                }
                RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, -1 , ImagePointerType, tempOutputPointerType, WeightsPointerType, 0>
                        ::GetSampleAndDerivative( temp_out, sampleorigin, stride, &filtcoeffs[0], &Dfiltcoeffs[0], startEnds  );
            }
            else {
                // mirror boundary conditions
                sampleorigin = this->origin;
                
                for (int i=0;i<SpaceDimension;i++) {
                    
                    for (int j=0;j<length;j++) {
                        int tmp = j+loc[i];
                        while ((tmp < 0)||(tmp >= size[i] )){
                            if (tmp < 0) {
                                tmp = -1 - tmp;
                            } else if(tmp >= size[i] ) {
                                tmp = 2*size[i]-1-tmp;
                            }
                        }
                        
                        stepsTable[j+ length*i] = tmp*stride[i];
                    }
                }
                if (samplerType::fixedlength==-1) {
                    // only fill startEnds when needed because sampler has no compile time known length.
                    for (int i=0;i<SpaceDimension;i++) {
                        startEnds[i*2  ] = length *i ;
                        startEnds[i*2+1] = length *(i+1) ;
                    }
                }
                RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, samplerType::fixedlength, ImagePointerType, tempOutputPointerType, WeightsPointerType, USE_STEPS>
                        ::GetSampleAndDerivative( temp_out, sampleorigin, &stepsTable[0], &filtcoeffs[0], &Dfiltcoeffs[0], startEnds  );
            }
        }
        else {
            if (samplerType::fixedlength==-1) {
                // only fill startEnds when needed because sampler has no compile time known length.
                for (int i=0;i<SpaceDimension;i++) {
                    startEnds[i*2  ] = length *i ;
                    startEnds[i*2+1] = length *(i+1) ;
                }
            }
            RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, samplerType::fixedlength, ImagePointerType, tempOutputPointerType, WeightsPointerType, 0>
                    ::GetSampleAndDerivative( temp_out, sampleorigin, stride, &filtcoeffs[0], &Dfiltcoeffs[0], startEnds );
        }*/
        ImagePointerType sampleorigin ;
        IndexType loc[SpaceDimension];
        int startEnds[2*SpaceDimension];
        RecursiveSubMethod m = this->template fillLocAndFilt<true>( sampleorigin, loc, startEnds, // output arguments.
                                                     point, sampler );

        typedef typename iterator_traits< OutputPointerType >::value_type OutputValueType;
        typedef OutputValueType * tempOutputPointerType;
        OutputValueType temp_out[ SpaceDimension +1 ];
        switch (m) {
            case RecursiveSubMethod::full_regular :
                RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, samplerType::fixedlength, ImagePointerType, tempOutputPointerType, WeightsPointerType, 0>
                    ::GetSampleAndDerivative( temp_out, sampleorigin, this->stride, &this->filtcoeffs[0], &this->Dfiltcoeffs[0], startEnds ); 
                break;
            case RecursiveSubMethod::partial :
                RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, -1 , ImagePointerType, tempOutputPointerType, WeightsPointerType, 0>
                        ::GetSampleAndDerivative( temp_out, sampleorigin, this->stride, &this->filtcoeffs[0], &this->Dfiltcoeffs[0], startEnds  );
                break;
            case RecursiveSubMethod::full_stepped:
                RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, samplerType::fixedlength, ImagePointerType, tempOutputPointerType, WeightsPointerType, USE_STEPS>
                        ::GetSampleAndDerivative( temp_out, sampleorigin, &this->stepsTable[0], &this->filtcoeffs[0], &this->Dfiltcoeffs[0], startEnds  );
                break;
            case RecursiveSubMethod::none:
                for (int i = 0 ; i<=SpaceDimension ; ++i ) {
                    temp_out[i]=0;
                }
                break;
        }         
        *out = temp_out[0];
        for ( int d = 0 ; d < SpaceDimension ; ++d ) {
            Dout[ d ] = temp_out[ d+1 ];
        }
    }
    // should add sample and derivative function, where
    // a recursive sampleAndDerivative method of RecursiveSamplingImplementation_GetSample is called
    // cost of the ND sampleAndDerivative function should be
    //  2 * SpaceDimension * filterLength (coefficients and derivative coefficients of the samplers)
    // + 2 * filterLength^SpaceDimension  ( value and derivative in 1st dimension, coefficient is actually lower than 2 since the image values need to be loaded only once)
    // +  O( filterLength^(SpaceDimension-1) )  (value roundup and derivatives in other dimensions.)
    template< typename InputPointerType, typename locationType, typename samplerType> void sampleAdjoint( InputPointerType out,  locationType * point, samplerType & sampler) {
        /*ImagePointerType sampleorigin = this->origin;
        bool allinrange = true;
        IndexType loc[SpaceDimension];
        locationType samplerdelay = sampler.delay();
        int length = sampler.length();
        int startEnds[2*SpaceDimension];
        int tmp;
        for (int i=0;i<SpaceDimension;i++) {
            locationType pti = point[i] - samplerdelay ;
            loc[i] = (IndexType) floor(pti);
            pti -= loc[i];
            sampler.get_coefficients( &filtcoeffs[length *i], pti);
            allinrange &= (loc[i]>=0) && (loc[i] + length  <= size[i]);
            sampleorigin += loc[i] * stride[i];
        }
        typedef typename iterator_traits< InputPointerType >::value_type InputValueType;
       if (!allinrange) {
            if( this->getBoundarycondition()==0 ) {
                // zero boundary conditions
                for (int i=0;i<SpaceDimension;i++) {
                    using std::min;
                    using std::max;
                    
                    int newstart = (int) max( (IndexType) 0, -loc[i]);
                    int newstop  = (int) min( (IndexType) length , size[i]-loc[i] ) ;
                    if (newstop<=newstart) {
                        return ;
                    }
                    sampleorigin += newstart * stride[i];
                    startEnds[i*2  ] = length *i + newstart;
                    startEnds[i*2+1] = length *i + newstop;
                }
                RecursiveSamplingImplementation_MultiplyJacobianWithValue<SpaceDimension, -1 , ImagePointerType, InputValueType, WeightsPointerType, 0 >
                        ::MultiplyJacobianWithValue(  sampleorigin, stride, &filtcoeffs[0], startEnds, *out);
            }
            else {
                // mirror boundary conditions
                sampleorigin = this->origin;
                
                for (int i=0;i<SpaceDimension;i++) {
                    using std::min;
                    using std::max;
                    
                    for (int j=0;j<length;j++) {
                        tmp = j+loc[i];
                        while ((tmp < 0)||(tmp >= size[i] )){
                            if (tmp < 0) {
                                tmp = -1 - tmp;
                            } else if(tmp >= size[i] ) {
                                tmp = 2*size[i]-1-tmp;
                            }
                        }
                            
                            stepsTable[j+ length*i] = tmp*stride[i];
                    }
                }
                
                if (samplerType::fixedlength==-1) {
                    // only fill startEnds when needed because sampler has no compile time known length.
                    for (int i=0;i<SpaceDimension;i++) {
                        startEnds[i*2  ] = length *i ;
                        startEnds[i*2+1] = length *(i+1) ;
                    }
                }
                RecursiveSamplingImplementation_MultiplyJacobianWithValue<SpaceDimension, samplerType::fixedlength, ImagePointerType, InputValueType, WeightsPointerType, USE_STEPS >
                        ::MultiplyJacobianWithValue(  sampleorigin, &stepsTable[0], &filtcoeffs[0], startEnds, *out);
            }
        }
        else {
            if (samplerType::fixedlength==-1) {
                // only fill startEnds when needed because sampler has no compile time known length.
                for (int i=0;i<SpaceDimension;i++) {
                    startEnds[i*2  ] = length *i ;
                    startEnds[i*2+1] = length *(i+1) ;
                }
            }
            RecursiveSamplingImplementation_MultiplyJacobianWithValue<SpaceDimension, samplerType::fixedlength , ImagePointerType, InputValueType, WeightsPointerType, 0>
                    ::MultiplyJacobianWithValue(  sampleorigin, stride , &filtcoeffs[0] , startEnds, *out);
        }
         */
        ImagePointerType sampleorigin ;
        IndexType loc[SpaceDimension];
        int startEnds[2*SpaceDimension];
        RecursiveSubMethod m = this->template fillLocAndFilt<false>( sampleorigin, loc, startEnds, // output arguments.
                                                     point, sampler );
        typedef typename iterator_traits< InputPointerType >::value_type InputValueType;
        switch (m) {
            case RecursiveSubMethod::full_regular :
                RecursiveSamplingImplementation_MultiplyJacobianWithValue<SpaceDimension, samplerType::fixedlength , ImagePointerType, InputValueType, WeightsPointerType, 0>
                    ::MultiplyJacobianWithValue(  sampleorigin, this->stride , &this->filtcoeffs[0] , startEnds, *out);
                break;
            case RecursiveSubMethod::partial :
                RecursiveSamplingImplementation_MultiplyJacobianWithValue<SpaceDimension, -1 , ImagePointerType, InputValueType, WeightsPointerType, 0 >
                        ::MultiplyJacobianWithValue(  sampleorigin, this->stride, &this->filtcoeffs[0], startEnds, *out);
                break;
            case RecursiveSubMethod::full_stepped:
                RecursiveSamplingImplementation_MultiplyJacobianWithValue<SpaceDimension, samplerType::fixedlength, ImagePointerType, InputValueType, WeightsPointerType, USE_STEPS >
                        ::MultiplyJacobianWithValue(  sampleorigin, &this->stepsTable[0], &this->filtcoeffs[0], startEnds, *out);
                break;
            case RecursiveSubMethod::none:
                break;
        }   
    }
};

template <class T> inline T pow_template(const T base, unsigned const exponent)
{
    // (parentheses not required in next line)
    return (exponent == 0) ? 1 : (base * pow_template(base, exponent-1));
};

/* class NDvecImage
 * Implements the vector valued ND image
 */

template <int SpaceDimension, typename ImagePointerType> class NDvecImage : public NDimage_filtered_base<SpaceDimension, ImagePointerType> {
private:
    typedef typename std::iterator_traits< ImagePointerType >::value_type ImageValueType;
    typedef ImageValueType WeightsValueType;
    typedef WeightsValueType * WeightsPointerType;
    /*std::vector<WeightsValueType> filtcoeffs;
    std::vector<WeightsValueType> Dfiltcoeffs;
    std::vector<OffsetValueType> stepsTable;*/
    int_t * nzji; //std::vector<int_t> nzji;
    WeightsValueType * weights; //std::vector<WeightsValueType> weights;
protected:
    typedef NDimage_filtered_base<SpaceDimension, ImagePointerType> parent;
    typedef typename parent::RecursiveSubMethod RecursiveSubMethod;
        
    //IndexType size[ SpaceDimension ];
    //int_t stride[ SpaceDimension ];
    ImageValueType * tempspaceblock;
public:
    static int tempBytesNeeded(const IndexType vlen_, const int maxFilterLength) {
        return parent::tempBytesNeeded(maxFilterLength) 
                + pow_template( maxFilterLength, SpaceDimension) * (sizeof(WeightsValueType) + sizeof( int_t ))
                + vlen_ *  (SpaceDimension + 1) * sizeof( ImageValueType) 
#ifdef ADDGUARDS
                + 3*8
#endif
                        ;
    }        
    NDvecImage( ImagePointerType origin_, const IndexType vlen_, const IndexType * size_, const int_t * stride_, const int maxFilterLength, tempspace & tmp)
        : NDimage_filtered_base<SpaceDimension, ImagePointerType>(origin_, size_, stride_, maxFilterLength, 1, vlen_, tmp) /*,
                filtcoeffs( SpaceDimension*maxFilterLength ) ,
                Dfiltcoeffs( SpaceDimension*maxFilterLength ) ,
                stepsTable( SpaceDimension*maxFilterLength ) */ // , 
                //nzji( pow_template( maxFilterLength, SpaceDimension) ) ,
                //weights( pow_template( maxFilterLength , SpaceDimension) )  
        {
            nzji = tmp.get< int_t> (  pow_template( maxFilterLength, SpaceDimension) );
            weights = tmp.get< WeightsValueType> ( pow_template( maxFilterLength , SpaceDimension) );
        /*for (int i=0;i<SpaceDimension;i++) {
            size[i]=size_[i];
        }
        for (int i=0;i<SpaceDimension;i++) {
            stride[i]=stride_[i];
        }*/
        int NumWeightsPerDimension = 4;  // Maximum filter length. 
        //tempspaceblock = (ImageValueType *) mxMalloc( sizeof(ImageValueType) * vlen_ *  SpaceDimension *(2 + NumWeightsPerDimension * (3 + SpaceDimension) ) /2 ); // required for RecursiveSamplingImplementation_GetVecSampleAndDerivative
        //tempspaceblock = (ImageValueType *) mxMalloc( sizeof(ImageValueType) * vlen_ *  (SpaceDimension + 1)  ); // required for RecursiveSamplingImplementation_GetVecSampleAndDerivative_Alt
        tempspaceblock = tmp.get< ImageValueType >( vlen_ *  (SpaceDimension + 1)  ); // required for RecursiveSamplingImplementation_GetVecSampleAndDerivative_Alt
        

    };

    template< typename OutputPointerType, typename locationType, typename samplerType> void sample(OutputPointerType out,  locationType * point, samplerType & sampler) {
        /*ImagePointerType sampleorigin = this->origin;
        bool allinrange = true;
        IndexType loc[SpaceDimension];
        locationType samplerdelay = sampler.delay();
        int length = sampler.length();
        int startEnds[2*SpaceDimension];
        for (int i=0;i<SpaceDimension;i++) {
            locationType pti = point[i] - samplerdelay ;
            loc[i] = (IndexType) floor(pti);
            pti -= loc[i];
            sampler.get_coefficients( &filtcoeffs[length *i], pti);
            allinrange &= (loc[i]>=0) && (loc[i] + length  <= size[i]);
            sampleorigin += loc[i] * stride[i];
        };

        WeightsPointerType weightsPointer =&weights[0];
        int_t * nzjiPointer = &nzji[0];
        if (!allinrange) {
            // TODO:  If mirror boundary conditions, force different interpretation of stride by specifying gridOffsetTable0=USE_STEPS in RecursiveSamplingImplementation calls.
            // Also update code here to fill such steps table. Call as the allinrange call because with mirror boundary we have all samples.
            // Only difference: gridOffsetTable0=USE_STEPS (and the associated parameter interpretation differences)
            if( this->getBoundarycondition()==0 ) {
                // zero boundary conditions
                for (int i=0;i<SpaceDimension;i++) {
                    using std::min;
                    using std::max;

                    int newstart = (int) max( (IndexType) 0, -loc[i]);
                    int newstop  = (int) min( (IndexType) length , size[i]-loc[i] ) ;
                    if (newstop<=newstart) {
                        for (int d= 0 ; d< this->Vlen(); ++d ){
                            out[d]=0;
                        }
                        return ;
                    }
                    sampleorigin += newstart * stride[i];
                    startEnds[i*2  ] = length *i + newstart;
                    startEnds[i*2+1] = length *i + newstop;
                }
                RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, -1, 0 >
                        ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &filtcoeffs[0], stride, startEnds );
            }
            else {
                // mirror boundary conditions
                sampleorigin = this->origin;

                for (int i=0;i<SpaceDimension;i++) {

                    for (int j=0;j<length;j++) {
                        int tmp = j+loc[i];
                        while ((tmp < 0)||(tmp >= size[i] )){
                            if (tmp < 0) {
                                tmp = -1 - tmp;
                            } else if(tmp >= size[i] ) {
                                tmp = 2*size[i]-1-tmp;
                            }
                        }

                        stepsTable[j+ length*i] = tmp*stride[i];
                    }
                }
                if (samplerType::fixedlength==-1) {
                    // only fill startEnds when needed because sampler has no compile time known length.
                    for (int i=0;i<SpaceDimension;i++) {
                        startEnds[i*2  ] = length *i ;
                        startEnds[i*2+1] = length *(i+1) ;
                    }
                }
                RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, samplerType::fixedlength, USE_STEPS >
                        ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &filtcoeffs[0], &stepsTable[0], startEnds );
            }
        }
        else {
            if (samplerType::fixedlength==-1) {
                // only fill startEnds when needed because sampler has no compile time known length.
                for (int i=0;i<SpaceDimension;i++) {
                    startEnds[i*2  ] = length *i ;
                    startEnds[i*2+1] = length *(i+1) ;
                }
            }
            RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, samplerType::fixedlength, 0 >
                    ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &filtcoeffs[0], stride, startEnds );
        }*/
        ImagePointerType sampleorigin ;
        IndexType loc[SpaceDimension];
        int startEnds[2*SpaceDimension];
        RecursiveSubMethod m = this->template fillLocAndFilt<false>( sampleorigin, loc, startEnds, // output arguments.
                                                     point, sampler );
            
        WeightsPointerType weightsPointer =&weights[0];
        int_t * nzjiPointer = &nzji[0];
        switch (m) {
            case RecursiveSubMethod::full_regular :
                RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, samplerType::fixedlength, 0 >
                    ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &this->filtcoeffs[0], this->stride, startEnds );
                break;
            case RecursiveSubMethod::partial :
                RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, -1, 0 >
                        ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &this->filtcoeffs[0], this->stride, startEnds );
                break;
            case RecursiveSubMethod::full_stepped:
                RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, samplerType::fixedlength, USE_STEPS >
                        ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &this->filtcoeffs[0], &this->stepsTable[0], startEnds );
                break;
            case RecursiveSubMethod::none:
                for (int d= 0 ; d< this->Vlen(); ++d ){
                    out[d]=0;
                }
                break;
        }          
        WeightsPointerType weightsPointerOrig =&weights[0];
        int_t * nzjiPointerOrig = &nzji[0];
        int numweights = (int) (weightsPointer  - weightsPointerOrig);
        combineVecfun< ImagePointerType, OutputPointerType , WeightsPointerType>
                ::sample( out, this->Vlen(), sampleorigin, weightsPointerOrig, nzjiPointerOrig, numweights );
    }        
        
        
    template< typename OutputPointerType, typename locationType, typename samplerType> void sampleAndDerivative( OutputPointerType out, OutputPointerType Dout, locationType * point, samplerType & sampler) {
        /*ImagePointerType sampleorigin = this->origin;
        bool allinrange = true;
        IndexType loc[SpaceDimension];
        locationType samplerdelay = sampler.delay();
        int length = sampler.length();
        int startEnds[2*SpaceDimension];
        for (int i=0;i<SpaceDimension;i++) {
            locationType pti = point[i] - samplerdelay ;
            loc[i] = (IndexType) floor(pti);
            pti -= loc[i];
            sampler.get_coefficients( &filtcoeffs[length *i], pti);
            sampler.get_coefficientsDerivative( &Dfiltcoeffs[length *i], pti, 1);
            allinrange &= (loc[i]>=0) && (loc[i] + length  <= size[i]);
            sampleorigin += loc[i] * stride[i];
        }
        typedef typename iterator_traits< OutputPointerType >::value_type OutputValueType;
        typedef OutputValueType * tempOutputPointerType;
        OutputValueType temp_out[ (SpaceDimension +1 ) ];
        if (!allinrange) {
            if( this->getBoundarycondition()==0 ) {
                // zero boundary conditions
                for (int i=0;i<SpaceDimension;i++) {
                    using std::min;
                    using std::max;

                    int newstart = (int) max( (IndexType) 0, -loc[i]);
                    int newstop  = (int) min( (IndexType) length , size[i]-loc[i] ) ;
                    if (newstop<=newstart) {
                        for (int d= 0 ; d< this->Vlen(); ++d ){
                            out[d]=0;
                        }
                        return ;
                    }
                    sampleorigin += newstart * stride[i];
                    startEnds[i*2  ] = length *i + newstart;
                    startEnds[i*2+1] = length *i + newstop;
                }
                for ( int vec_idx = 0 ; vec_idx < this->Vlen(); vec_idx++ ) { // TODO: this loop is somewhat inefficient; probably it would be faster when moved into RecursiveSamplingImplementation_GetSampleAndDerivative
                    // Else could at least do some vectorization (using vec) on the in/output classes.
                    RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, -1 , ImagePointerType, tempOutputPointerType, WeightsPointerType, 0>
                            ::GetSampleAndDerivative( temp_out, sampleorigin+vec_idx, stride, &filtcoeffs[0], &Dfiltcoeffs[0], startEnds  );
                    out[vec_idx] = temp_out[0];
                    for ( int d = 0 ; d < SpaceDimension ; ++d ) {
                        Dout[ vec_idx + d *this->Vlen() ] = temp_out[ d+1 ];
                    }
                }
            }
            else {
                // mirror boundary conditions
                sampleorigin = this->origin;

                for (int i=0;i<SpaceDimension;i++) {

                    for (int j=0;j<length;j++) {
                        int tmp = j+loc[i];
                        while ((tmp < 0)||(tmp >= size[i] )){
                            if (tmp < 0) {
                                tmp = -1 - tmp;
                            } else if(tmp >= size[i] ) {
                                tmp = 2*size[i]-1-tmp;
                            }
                        }

                        stepsTable[j+ length*i] = tmp*stride[i];
                    }
                }
                if (samplerType::fixedlength==-1) {
                    // only fill startEnds when needed because sampler has no compile time known length.
                    for (int i=0;i<SpaceDimension;i++) {
                        startEnds[i*2  ] = length *i ;
                        startEnds[i*2+1] = length *(i+1) ;
                    }
                }

                //* 
                RecursiveSamplingImplementation_GetVecSampleAndDerivative_Alt
                <SpaceDimension, samplerType::fixedlength, ImagePointerType, tempOutputPointerType, WeightsPointerType, USE_STEPS >
                ::GetVecSampleAndDerivative(
                            tempspaceblock, this->Vlen(), 
                            sampleorigin, this->Vlen(), 
                           &stepsTable[0], &filtcoeffs[0], &Dfiltcoeffs[0], startEnds );
                for (int vec_idx =0 ; vec_idx < this->Vlen(); vec_idx++ ) { 
                    out[vec_idx] = tempspaceblock[ vec_idx ];
                }
                for (int vec_idx =0 ; vec_idx < this->Vlen() * SpaceDimension ; vec_idx++ ) { 
                    Dout[ vec_idx  ] = tempspaceblock[ vec_idx +  this->Vlen() ];
                }
                /* /
                            int vec_idx =0;
                            const int vlen = 4; 
                            typedef vecTptr<ImagePointerType, vlen> ImageVPointerType;
                            typedef vecptr<OutputValueType *, vlen> OutputVPointerType;
                            ImageVPointerType sampleoriginV(sampleorigin);
                            OutputValueType tempV_out[ (SpaceDimension +1 ) * vlen ];
                            OutputVPointerType tempV_outptr(tempV_out);
                            for ( ; vec_idx+vlen <= this->Vlen(); vec_idx+= vlen ) { // TODO: this loop is somewhat inefficient; probably it would be faster when moved into RecursiveSamplingImplementation_GetSampleAndDerivative
                                // Else could at least do some vectorization (using vec) on the in/output classes.
                                RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, samplerType::fixedlength, ImageVPointerType, OutputVPointerType, WeightsPointerType, USE_STEPS>
                                        ::GetSampleAndDerivative( tempV_outptr, sampleoriginV + vec_idx, &stepsTable[0], &filtcoeffs[0], &Dfiltcoeffs[0], startEnds );
                                for( int idx=0; idx< vlen ; idx++ ){
                                  out[vec_idx+idx] = temp_out[idx];
                                }
                                for ( int d = 0 ; d < SpaceDimension ; ++d ) {
                                  for( int idx=0; idx< vlen ; idx++ ){
                                    Dout[ vec_idx + d *this->Vlen()+idx ] = temp_out[ (d+1)*vlen + idx ];
                                  }
                                }
                            }

                //* /
                            for (int vec_idx =0 ; vec_idx < this->Vlen(); vec_idx++ ) { // TODO: this loop is somewhat inefficient; probably it would be faster when moved into RecursiveSamplingImplementation_GetSampleAndDerivative
                                // Else could at least do some vectorization (using vec) on the in/output classes.
                                RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, samplerType::fixedlength, ImagePointerType, tempOutputPointerType, WeightsPointerType, USE_STEPS>
                                        ::GetSampleAndDerivative( temp_out, sampleorigin+vec_idx, &stepsTable[0], &filtcoeffs[0], &Dfiltcoeffs[0], startEnds );
                                out[vec_idx] = temp_out[0];
                                for ( int d = 0 ; d < SpaceDimension ; ++d ) {
                                    Dout[ vec_idx + d *this->Vlen() ] = temp_out[ d+1 ];
                                }
                            } // 
                 * /
                }
            }
            else {
                if (samplerType::fixedlength==-1) {
                    // only fill startEnds when needed because sampler has no compile time known length.
                    for (int i=0;i<SpaceDimension;i++) {
                        startEnds[i*2  ] = length *i ;
                        startEnds[i*2+1] = length *(i+1) ;
                    }
                }
                for ( int vec_idx = 0 ; vec_idx < this->Vlen(); vec_idx++ ) {
                    RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, samplerType::fixedlength, ImagePointerType, tempOutputPointerType, WeightsPointerType, 0>
                            ::GetSampleAndDerivative( temp_out, sampleorigin+vec_idx, stride, &filtcoeffs[0], &Dfiltcoeffs[0], startEnds );
                    out[vec_idx] = temp_out[0];
                    for ( int d = 0 ; d < SpaceDimension ; ++d ) {
                        Dout[ vec_idx + d *this->Vlen() ] = temp_out[ d+1 ];
                    }
                }
            }*/
        ImagePointerType sampleorigin ;
        IndexType loc[SpaceDimension];
        int startEnds[2*SpaceDimension];
        RecursiveSubMethod m = this->template fillLocAndFilt<true>( sampleorigin, loc, startEnds, // output arguments.
                                                     point, sampler );
        typedef typename iterator_traits< OutputPointerType >::value_type OutputValueType;
        typedef OutputValueType * tempOutputPointerType;
        OutputValueType temp_out[ (SpaceDimension +1 ) ];
        
        switch (m) {
            case RecursiveSubMethod::full_regular :
                for ( int vec_idx = 0 ; vec_idx < this->Vlen(); vec_idx++ ) {
                    RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, samplerType::fixedlength, ImagePointerType, tempOutputPointerType, WeightsPointerType, 0>
                            ::GetSampleAndDerivative( temp_out, sampleorigin+vec_idx, this->stride, &this->filtcoeffs[0], &this->Dfiltcoeffs[0], startEnds );
                    out[vec_idx] = temp_out[0];
                    for ( int d = 0 ; d < SpaceDimension ; ++d ) {
                        Dout[ vec_idx + d *this->Vlen() ] = temp_out[ d+1 ];
                    }
                }
                break;
            case RecursiveSubMethod::partial :
                for ( int vec_idx = 0 ; vec_idx < this->Vlen(); vec_idx++ ) { // TODO: this loop is somewhat inefficient; probably it would be faster when moved into RecursiveSamplingImplementation_GetSampleAndDerivative
                    // Else could at least do some vectorization (using vec) on the in/output classes.
                    RecursiveSamplingImplementation_GetSampleAndDerivative<SpaceDimension, -1 , ImagePointerType, tempOutputPointerType, WeightsPointerType, 0>
                            ::GetSampleAndDerivative( temp_out, sampleorigin+vec_idx, this->stride, &this->filtcoeffs[0], &this->Dfiltcoeffs[0], startEnds  );
                    out[vec_idx] = temp_out[0];
                    for ( int d = 0 ; d < SpaceDimension ; ++d ) {
                        Dout[ vec_idx + d *this->Vlen() ] = temp_out[ d+1 ];
                    }
                }
                break;
            case RecursiveSubMethod::full_stepped:
                RecursiveSamplingImplementation_GetVecSampleAndDerivative_Alt
                <SpaceDimension, samplerType::fixedlength, ImagePointerType, tempOutputPointerType, WeightsPointerType, USE_STEPS >
                ::GetVecSampleAndDerivative(
                            tempspaceblock, this->Vlen(), 
                            sampleorigin, this->Vlen(), 
                           &this->stepsTable[0], &this->filtcoeffs[0], &this->Dfiltcoeffs[0], startEnds );
                for (int vec_idx =0 ; vec_idx < this->Vlen(); vec_idx++ ) { 
                    out[vec_idx] = tempspaceblock[ vec_idx ];
                }
                for (int vec_idx =0 ; vec_idx < this->Vlen() * SpaceDimension ; vec_idx++ ) { 
                    Dout[ vec_idx  ] = tempspaceblock[ vec_idx +  this->Vlen() ];
                }
                break;
            case RecursiveSubMethod::none:
                for (int d= 0 ; d< this->Vlen(); ++d ){
                    out[d]=0;
                }
                break;
        }        
        
    }
        
    // should add sample and derivative function, where
    // a recursive sampleAndDerivative method of RecursiveSamplingImplementation_GetSample is called
    // cost of the ND sampleAndDerivative function should be
    //  2 * SpaceDimension * filterLength (coefficients and derivative coefficients of the samplers)
    // + 2 * filterLength^SpaceDimension  ( value and derivative in 1st dimension, coefficient is actually lower than 2 since the image values need to be loaded only once)
    // +  O( filterLength^(SpaceDimension-1) )  (value roundup and derivatives in other dimensions.)
    template< typename OutputPointerType, typename locationType, typename samplerType> void sampleAdjoint(OutputPointerType out,  locationType * point, samplerType & sampler) {
        /*ImagePointerType sampleorigin = this->origin;
        bool allinrange = true;
        IndexType loc[SpaceDimension];
        locationType samplerdelay = sampler.delay();
        int length = sampler.length();
        int startEnds[2*SpaceDimension];
        for (int i=0;i<SpaceDimension;i++) {
            locationType pti = point[i] - samplerdelay ;
            loc[i] = (IndexType) floor(pti);
            pti -= loc[i];
            sampler.get_coefficients( &filtcoeffs[length *i], pti);
            allinrange &= (loc[i]>=0) && (loc[i] + length  <= size[i]);
            sampleorigin += loc[i] * stride[i];
        };

        WeightsPointerType weightsPointer =&weights[0];
        int_t * nzjiPointer = &nzji[0];
        if (!allinrange) {
            if( this->getBoundarycondition()==0 ) {                    
                // zero boundary conditions
                for (int i=0;i<SpaceDimension;i++) {
                    using std::min;
                    using std::max;

                    int newstart = (int) max( (IndexType) 0, -loc[i]);
                    int newstop  = (int) min( (IndexType) length , size[i]-loc[i] ) ;
                    if (newstop<=newstart) {
                        return ;
                    }
                    sampleorigin += newstart * stride[i];
                    startEnds[i*2  ] = length *i + newstart;
                    startEnds[i*2+1] = length *i + newstop;
                }
                RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, -1, 0 >
                        ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &filtcoeffs[0], stride, startEnds );
            }
            else {
                // mirror boundary conditions
                sampleorigin = this->origin;

                for (int i=0;i<SpaceDimension;i++) {

                    for (int j=0;j<length;j++) {
                        int tmp = j+loc[i];

                        while ((tmp < 0)||(tmp >= size[i] )){
                            if (tmp < 0) {
                                tmp = -1 - tmp;
                            } else if(tmp >= size[i] ) {
                                tmp = 2*size[i]-1-tmp;
                            }
                        }
                        stepsTable[j+ length*i] = tmp*stride[i];
                    }
                }
                if (samplerType::fixedlength==-1) {
                    // only fill startEnds when needed because sampler has no compile time known length.
                    for (int i=0;i<SpaceDimension;i++) {
                        startEnds[i*2  ] = length *i ;
                        startEnds[i*2+1] = length *(i+1) ;
                    }
                }
                RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, samplerType::fixedlength, USE_STEPS >
                        ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &filtcoeffs[0], &stepsTable[0], startEnds );
            }
        }
        else {
            if (samplerType::fixedlength==-1) {
                // only fill startEnds when needed because sampler has no compile time known length.
                for (int i=0;i<SpaceDimension;i++) {
                    startEnds[i*2  ] = length *i ;
                    startEnds[i*2+1] = length *(i+1) ;
                }
            }
            RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, samplerType::fixedlength, 0 >
                    ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &filtcoeffs[0], stride, startEnds );
        }
        WeightsPointerType weightsPointerOrig =&weights[0];
        int_t * nzjiPointerOrig = &nzji[0];
        int numweights = (int) (weightsPointer  - weightsPointerOrig);
        combineVecfun< ImagePointerType, OutputPointerType , WeightsPointerType>
                ::sampleAdjoint( out, this->Vlen(), sampleorigin, weightsPointerOrig, nzjiPointerOrig, numweights );
        */
        ImagePointerType sampleorigin ;
        IndexType loc[SpaceDimension];
        int startEnds[2*SpaceDimension];
        RecursiveSubMethod m = this->template fillLocAndFilt<false>( sampleorigin, loc, startEnds, // output arguments.
                                                     point, sampler );
        WeightsPointerType weightsPointer =&weights[0];
        int_t * nzjiPointer = &nzji[0];
       
        switch (m) {
            case RecursiveSubMethod::full_regular :
                RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, samplerType::fixedlength, 0 >
                    ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &this->filtcoeffs[0], this->stride, startEnds );
                break;
            case RecursiveSubMethod::partial :
                RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, -1, 0 >
                        ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &this->filtcoeffs[0], this->stride, startEnds );
                break;
            case RecursiveSubMethod::full_stepped:
                RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< WeightsPointerType , int_t * , WeightsPointerType, SpaceDimension, samplerType::fixedlength, USE_STEPS >
                        ::GetWeightsAndNonZeroIndices( weightsPointer, nzjiPointer, 1. , 0, &this->filtcoeffs[0], &this->stepsTable[0], startEnds );
                break;
            case RecursiveSubMethod::none:
                return;
                break;
        } 
        
        WeightsPointerType weightsPointerOrig =&weights[0];
        int_t * nzjiPointerOrig = &nzji[0];
        int numweights = (int) (weightsPointer  - weightsPointerOrig);
        combineVecfun< ImagePointerType, OutputPointerType , WeightsPointerType>
                ::sampleAdjoint( out, this->Vlen(), sampleorigin, weightsPointerOrig, nzjiPointerOrig, numweights );
    }
};

template <typename ImagePointerType> int newNDimage_tempBytesNeeded( const IndexType vlen_, int SpaceDimension, const int maxFilterLength) {
   if (vlen_==1) {
        if (SpaceDimension==1) {
            return NDimage<1,ImagePointerType>::tempBytesNeeded(maxFilterLength);
        } else if (SpaceDimension==2) {
            return NDimage<2,ImagePointerType>::tempBytesNeeded(maxFilterLength);
        } else if (SpaceDimension==3) {
            return NDimage<3,ImagePointerType>::tempBytesNeeded(maxFilterLength);
        } else if (SpaceDimension==4) {
            return NDimage<4,ImagePointerType>::tempBytesNeeded(maxFilterLength);
        } else if (SpaceDimension==5) {
            return NDimage<5,ImagePointerType>::tempBytesNeeded(maxFilterLength);
        } else if (SpaceDimension==6) {
            return NDimage<6,ImagePointerType>::tempBytesNeeded(maxFilterLength);
        } else {
            mexErrMsgTxt("Image dimension should be in [1 ... 6].");
            return 0;
        }
    } else {
        if (SpaceDimension==1) {
            return NDvecImage<1,ImagePointerType>::tempBytesNeeded(vlen_, maxFilterLength);
        } else if (SpaceDimension==2) {
            return NDvecImage<2,ImagePointerType>::tempBytesNeeded(vlen_, maxFilterLength);
        } else if (SpaceDimension==3) {
            return NDvecImage<3,ImagePointerType>::tempBytesNeeded(vlen_, maxFilterLength);
        } else if (SpaceDimension==4) {
            return NDvecImage<4,ImagePointerType>::tempBytesNeeded(vlen_, maxFilterLength);
        } else if (SpaceDimension==5) {
            return NDvecImage<5,ImagePointerType>::tempBytesNeeded(vlen_, maxFilterLength);
        } else if (SpaceDimension==6) {
            return NDvecImage<6,ImagePointerType>::tempBytesNeeded(vlen_, maxFilterLength);
        } else {
            mexErrMsgTxt("Image dimension should be in [1 ... 6].");
            return 0;
        }
    }     
}

template <typename ImagePointerType> NDimage_base<ImagePointerType> * newNDimage( ImagePointerType origin_, const IndexType vlen_, const IndexType * size_, const int_t * stride_, int SpaceDimension, const int maxFilterLength, tempspace & tmp) {
    if (vlen_==1) {
        if (SpaceDimension==1) {
            return new NDimage<1,ImagePointerType>(origin_, size_, stride_, maxFilterLength, tmp);
        } else if (SpaceDimension==2) {
            return new NDimage<2,ImagePointerType>(origin_, size_, stride_, maxFilterLength, tmp);
        } else if (SpaceDimension==3) {
            return new NDimage<3,ImagePointerType>(origin_, size_, stride_, maxFilterLength, tmp);
        } else if (SpaceDimension==4) {
            return new NDimage<4,ImagePointerType>(origin_, size_, stride_, maxFilterLength, tmp);
        } else if (SpaceDimension==5) {
            return new NDimage<5,ImagePointerType>(origin_, size_, stride_, maxFilterLength, tmp);
        } else if (SpaceDimension==6) {
            return new NDimage<6,ImagePointerType>(origin_, size_, stride_, maxFilterLength, tmp);
        } else {
            mexErrMsgTxt("Image dimension should be in [1 ... 6].");
            return NULL;
        }
    } else {
        if (SpaceDimension==1) {
            return new NDvecImage<1,ImagePointerType>(origin_, vlen_, size_, stride_, maxFilterLength, tmp);
        } else if (SpaceDimension==2) {
            return new NDvecImage<2,ImagePointerType>(origin_, vlen_, size_, stride_, maxFilterLength, tmp);
        } else if (SpaceDimension==3) {
            return new NDvecImage<3,ImagePointerType>(origin_, vlen_, size_, stride_, maxFilterLength, tmp);
        } else if (SpaceDimension==4) {
            return new NDvecImage<4,ImagePointerType>(origin_, vlen_, size_, stride_, maxFilterLength, tmp);
        } else if (SpaceDimension==5) {
            return new NDvecImage<5,ImagePointerType>(origin_, vlen_, size_, stride_, maxFilterLength, tmp);
        } else if (SpaceDimension==6) {
            return new NDvecImage<6,ImagePointerType>(origin_, vlen_, size_, stride_, maxFilterLength, tmp);
        } else {
            mexErrMsgTxt("Image dimension should be in [1 ... 6].");
            return NULL;
        }
    }    
};
template <typename ImagePointerType> NDimage_base<ImagePointerType> * newNDimage( ImagePointerType  origin_, const IndexType * size_, const int_t * stride_, int SpaceDimension, const int maxFilterLength) {
    return newNDimage(origin_, 1, size_, stride_, SpaceDimension, maxFilterLength);
}

#endif