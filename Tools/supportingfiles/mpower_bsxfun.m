function [out] = mpower_bsxfun( mat, pow )
% [out] = mpower_bsxfun( mat, pow );
%
% Faster version with scalar expansion of :
%   size_mat = size(mat);
%   size_pow = size(pow);
%   if any( size_pow(1:2) ~=1), error('a single pow should be specified per matrix' );end;
%   size_out = max( size_mat, size_pow )
%   for i = 1 : prod(size_out(3:end))
%      out(:,:,i) = mat(:,:,i)^pow(i)
%   end;
% NOTE: this function only is faster than the above loop if scalar expansion is
%  used. 
%
% INPUTS:
%   mat : array of N xN matrices; size(mat) = [N N other_dims_mat]
%   pow : power of each matrix; size(pow) = [1 1 other_dims_pow]
%       Required: 
%           other_dims_mat(i)==1 or other_dims_pow(i)==1 or other_dims_mat(i)==other_dims_pow(i)
% OUTPUTS:
%   out : out(:,:,i,j,...) = mat(:,:,i,j,...)^pow(1,1,i,j,...) (with scalar expansion of mat and pow
%
% 4-11-2017, D.Poot, Erasmus MC: Created


% Using relation:
% r = a^b 
% == 
% [v,e] = eig(a)
% r = v * diag(diag(e).^b) /v
size_mat = size(mat);
size_pow = size(pow);
if any( size_pow(1:2) ~=1), 
    error('a single pow should be specified per matrix' );
end;
Vmat = mat;
emat = zeros([1 size_mat(2:end)]);
for k = 1 : prod( size_mat(3:end) );
    [Vmat(:,:,k), D] = eig( mat(:,:,k ));
    emat(1,:,k) = diag(D);
end;
epow = bsxfun(@power, emat, pow);
VD = bsxfun(@times, Vmat, epow);
size_out = size(VD);
equalsize = size_out==[size_mat ones(1,numel(size_out)-numel(size_mat))];
sourcedims = find( equalsize); % includes [1 2]
expanddims = find(~equalsize);

permord = [1 expanddims 2 sourcedims(3:end)]; % permute all expanded dims to after dim 1, as they will be reshaped into dim 1 below to make matrix division with V efficient. 
size_outp = size_out(permord);
VDp = reshape( permute( VD, permord) ,[ prod(size_outp(1:numel(expanddims)+1)) size_outp(numel(expanddims)+2:end)] ) ;
for k = 1 : prod( size_mat(3:end) );
    VDp(:,:,k) = VDp(:,:,k)/Vmat(:,:,k);
end;
out = ipermute( reshape(VDp, size_outp),permord) ;

function test()
%% Tests mpower_bsxfun
% Scalar case:
a=randn(3);
p = 2.2345;
c = a^p;
cf = mpower_bsxfun(a,p);
if ~isequal(c,cf) % maybe allow for some roundoff error. 
    error('wrong answer');
end;
%% multiple a, single power (convenient, but not speed improvement compared to reference)
a=randn(3,3,2,1,8);
p = 1.9023;
c = a;
for k=1:size(a(:,:,:),3)
    c(:,:,k) = a(:,:,k)^p;
end;
cf = mpower_bsxfun(a,p);
if ~isequal(c,cf) % maybe allow for some roundoff error. 
    error('wrong answer');
end;
%% single a, multiple power (speed improvement compared to reference)
a=randn(3,3);
p = 1.3023+.2*randn(1,1,4,34);
c = zeros([3 3 4 34]);
for k=1:numel(p)
    c(:,:,k) = a^p(k);
end;
cf = mpower_bsxfun(a,p);
if ~isequal(c,cf) % maybe allow for some roundoff error. 
    error('wrong answer');
end;
%% multiple a, equal multiple power (no speed improvement compared to reference)
a=randn(3,3,4,34);
p = 1.3023+.2*randn(1,1,4,34);
c = zeros([3 3 4 34]);
for k=1:numel(p)
    c(:,:,k) = a(:,:,k)^p(k);
end;
cf = mpower_bsxfun(a,p);
if ~isequal(c,cf) % maybe allow for some roundoff error. 
    error('wrong answer');
end;
%% multiple a,  multiple power expand (speed improvement compared to reference)
a=randn(3,3,34);
p = 1.3023+.2*randn(1,1,1,39);
c = zeros([3 3 34 39]);
for kp= 1:numel(p)
    for ka = 1 : size(a,3)
        c(:,:,ka,kp) = a(:,:,ka)^p(kp);
    end;
    
end;
cf = mpower_bsxfun(a,p);
if ~isequal(c,cf) % maybe allow for some roundoff error. 
    error('wrong answer');
end;
%% multiple a,  multiple power expand (speed improvement compared to reference)
a=randn(3,3,1,134);
p = 1.3023+.2*randn(1,1,119);
c = zeros([3 3 39 34]);
tic
for kp= 1:numel(p)
    for ka = 1 : size(a,4)
        c(:,:,kp,ka) = a(:,:,ka)^p(kp);
    end;
    
end;
tref = toc; tic;
cf = mpower_bsxfun(a,p);
topt = toc;
disp(sprintf('Test expand multiple a, multiple p: Ref %f sec, Opt %f sec',tref, topt))
if ~isequal(c,cf) % maybe allow for some roundoff error. 
    error('wrong answer');
end;