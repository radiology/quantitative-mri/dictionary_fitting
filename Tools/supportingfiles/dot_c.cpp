/* This is a mex file for MATLAB
 * On windows with Visual Studio compile from within MATLAB (>=) 2013b with:
 *   mex dot_c.cpp -DINCLUDE_SSE3 -DMEX
 * On Linux:
 *   mex dot_c.cpp CXXFLAGS="\$CXXFLAGS -msse4a -msse4.2 -march=native"
 * optional compiler arguments : -v (verbose) , -g (debug symbols), -O (force optimizations in debug mode)
 *
 * NOTE: older versions of MATLAB (R2011b with visual studio 9.0(?)) dont inline 
 * double precision complex value functions making it run 5 times slower in some cases 
 * (mainly: dot(a,b,1) with size(a,1)<=11)
 *
 * [Out, Cl] = dot(A, B [, dim] )
 * This routine computes the same as the builtin function Out = dot(A , B , dim) 
 * But with substantially lower computation time.
 *
 *
 *  Created by Dirk Poot, Erasmus MC, 14-8-2014
 */
#include "mex.h"
//#include  <stdlib.h>
#include <cmath>
#include <algorithm>
#include "guardpointer.cpp"
#include "emm_vecptr.hxx"
#include "standard_templates.hxx"

//using std::complex;
// double double
double conjmul( double A, double B) {
    return A*B;
}
complex<double> conjmul( complex<double> A, double B) {
    return conj(A)*B;
}
complex<double> conjmul( double A, complex<double> B) {
    return A*B;
}
complex<double> conjmul( complex<double> A, complex<double> B) {
    return conj(A)*B;
}
vec<double,4> conjmul( vec<double,4> A, vec<double,4> B) {
    return A*B;
}
complex<vec<double,4> > conjmul( complex<vec<double,4> > A, vec<double,4> B) {
    return conj(A)*B;
}
complex<vec<double,4> > conjmul( vec<double,4> A, complex<vec<double,4> > B) {
    return B*A;
}
complex<vec<double,4> > conjmul( complex<vec<double,4> > A, complex<vec<double,4> >  B) {
    return conj(A)*B;
}

// float float
float conjmul( float A, float B) {
    return A*B;
}
complex<float> conjmul( complex<float> A, float B) {
    return conj(A)*B;
}
complex<float> conjmul( float A, complex<float> B) {
    return A*B;
}
complex<float> conjmul( complex<float> A, complex<float> B) {
    return conj(A)*B;
}

vec<float,4> conjmul( vec<float,4> A, vec<float,4> B) {
    return A*B;
}
complex<vec<float,4> > conjmul( complex<vec<float,4> > A, vec<float,4> B) {
    return conj(A)*B;
}
complex<vec<float,4> > conjmul( vec<float,4> A, complex<vec<float,4> > B) {
    return B*A;
}
complex<vec<float,4> > conjmul( complex<vec<float,4> > A, complex<vec<float,4> >  B) {
    return conj(A)*B;
}

// double float
float conjmul( double A, float B) {
    return (float) A*B;
}
complex<float> conjmul( complex<double> A, complex<float> B) {
    return conj(complex<float>(A))*B;
}
complex<float> conjmul( complex<double> A, float B) {
    return conj(complex<float>(A))*B;
}

vec<float,4> conjmul( vec<double,4> A, vec<float,4> B) {
    return vec<float,4>(A)*B;
}
complex<vec<float,4> > conjmul( complex< vec<double,4> > A, vec<float,4> B) {
    return conj(complex<vec<float,4> >(A))*B;
}
complex<vec<float,4> > conjmul( vec<double,4> A, complex<vec<float,4> > B) {
    return vec<float,4>(A) * B;
}
complex<vec<float,4> > conjmul( complex< vec<double,4> > A, complex< vec<float,4> >  B) {
    return conj(complex< vec<float,4> >(A))*B;
}

//float double
float conjmul( float A, double B) {
    return (float) A*B;
}
complex<float> conjmul( complex<float> A, complex<double> B) {
    return conj(A)*complex<float>(B);
}
complex<float> conjmul( float A, complex<double> B) {
    return A*complex<float>(B);
}

vec<float,4> conjmul( vec<float,4> A, vec<double,4> B) {
    return (A)* vec<float,4>(B);
}
complex<vec<float,4> > conjmul( complex< vec<float,4> > A, vec<double,4> B) {
    return conj(A)*vec<float,4>(B);
}
complex<vec<float,4> > conjmul( vec<float,4> A, complex<vec<double,4> > B) {
    return (A) * complex< vec<float,4> >(B);
}
complex<vec<float,4> > conjmul( complex< vec<float,4> > A, complex< vec<double,4> >  B) {
    return conj(A)*complex< vec<float,4> >(B);
}

// sum
inline complex<double> sum( const complex< vec<double, 4> > & v) {
    return complex<double>( sum(v.real() ), sum(v.imag() ) );
}
inline complex<float> sum( const complex< vec<float, 4> > & v) {
    return complex<float>( sum(v.real() ), sum(v.imag() ) );
}

template < typename T> T zero() {
    return T::zero();
}
template <> double zero() {
    return 0.;
}
template <> float zero() {
    return 0.;
}
template <typename T> complex< T > zero() {
    return complex< T >( zero<T>() , zero<T>() );
}
        
template <typename value_type> struct mxClassIDtemplate {
};
template<> struct mxClassIDtemplate< double > {
    static const mxClassID ClassID =  mxDOUBLE_CLASS ;
};
template<> struct mxClassIDtemplate< float > {
    static const mxClassID ClassID =  mxSINGLE_CLASS ;
};

template< typename TOut, typename TA, typename TB> void times( TOut Out, TA A, TB B,  mwSize cumsz )
{
	/* Compute Out = conj(A).*B  (in MATLAB notation).
	 */
    typedef typename vec_Type<TA>::Type vecTypeA;
    typedef typename vec_Type<TB>::Type vecTypeB;
    typedef typename vec_Type<TOut>::Type vecTypeOut;
    const int vlen = vec_Type<TOut>::vlen;
    struct vlen_not_equal { int a; };
    //typedef IF< AND< EQUAL_INT<vlen, vec_Type<TA>::vlen >::RET ,EQUAL_INT<vlen, vec_Type<TB>::vlen >::RET >::RET  , int , WARN<vlen_not_equal>::RET >::RET vlen_unequal_assert_type;
    //typedef IF< EQUAL_INT<vlen, vec_Type<TA>::vlen >::RET , int , WARN<vlen_not_equal>::RET > vlen_unequal_assert_type;
    
    vecTypeOut Outvec(Out);
    vecTypeA Avec(A);
    vecTypeB Bvec(B);
    int k=0;
	for ( ; k <= cumsz-vlen ; k += vlen ) {
        *Outvec = conjmul( *Avec, *Bvec);
        ++Outvec;++Avec;++Bvec;
    }
    for ( ; k < cumsz; ++k) {
        Out[k] = conjmul( A[k], B[k] );
    }
};

template< typename TOut, typename TA, typename TB > void dot1(TOut Out, TA A, TB B, mwSize veclen, mwSize ncols) {
    typedef typename vec_Type<TA>::Type vecTypeA;
    typedef typename vec_Type<TB>::Type vecTypeB;
    typedef typename vec_Type<TOut>::Type vecTypeOut;
    typedef typename iterator_traits< vecTypeOut >::value_type vecvalT;
    typedef typename iterator_traits< TOut >::value_type valT;
    const int vlen = vec_Type<TOut>::vlen;

    if ( (vlen==1) || (vlen*3>veclen) ){
        // cannot vectorise or vectorisation will be ineficient:
        for ( int colnr =0 ; colnr < ncols ; ++colnr){
            valT s = 0.;
            for ( int k=0 ; k < veclen; ++k, ++A,++B) {
                s += conjmul( *A, *B );
            }
            *Out = s;
            ++Out;
        }
    } else {
        // Can do vectorised
        for ( int colnr =0 ; colnr < ncols ; ++colnr){
            vecTypeA Avec(A);
            vecTypeB Bvec(B);

            vecvalT s_vec( valT(0.) );
            int k=0;
            for ( ; k <= veclen-vlen ; k += vlen ) {
                s_vec += conjmul( *Avec, *Bvec);
                ++Avec;++Bvec;
            }
            A+=k;
            B+=k;
            valT s = sum( s_vec );
            for ( ; k < veclen; ++k, ++A, ++B) {
                s += conjmul( *A, *B );
            }
            *Out = s;
            ++Out;
        }
    }
}

template< typename TOut, typename TA, typename TB> void dot2(TOut Out, TA A, TB B, mwSize nrows, mwSize veclen, mwSize ncols) {
    typedef typename vec_TypeT<TOut>::Type vecTypeOut;
    typedef typename vec_TypeT<TA>::Type vecTypeA;
    typedef typename vec_TypeT<TB>::Type vecTypeB;
    typedef typename iterator_traits< vecTypeOut >::value_type vecvalT;
    typedef typename iterator_traits< TOut >::value_type valT;
    const int vlen = vec_Type<TOut>::vlen;

    if ( (vlen==1) || (vlen>nrows) ){
        // cannot vectorise or vectorisation will be ineficient:
        for ( int colnr =0 ; colnr < ncols ; ++colnr, A+=nrows*veclen, B+=nrows*veclen){
            TA Ac = A;
            TB Bc = B;
            for ( int rownr = 0 ; rownr < nrows ; ++rownr, ++Ac,++Bc) {
                TA Av = Ac;
                TB Bv = Bc;

                valT s = 0;
                for ( int k=0 ; k < veclen; ++k, Av+=nrows, Bv+=nrows) {
                    s += conjmul( *Av, *Bv );
                }
                *Out = s;
                ++Out;
            }
        }
	} else {
        // Can do vectorised
        for ( int colnr =0 ; colnr < ncols ; ++colnr, A+=nrows*veclen, B+=nrows*veclen){
            TA Ac = A;
            TB Bc = B;
			int rownr = 0;
			vecTypeOut Cv(Out);
            for (  ; rownr <= nrows-vlen ; rownr+=vlen, Ac+=vlen,Bc+=vlen) {
				vecTypeA Av(Ac);
				vecTypeB Bv(Bc);

				vecvalT s( valT(0.) );
				for (int k=0 ; k < veclen ; ++k, Av+=nrows, Bv+=nrows ) {
					s += conjmul( *Av, * Bv);
				}
				*Cv = s;
				Cv+=vlen;
			}
			Out+=rownr;
			for (  ; rownr < nrows ; ++rownr, ++Ac,++Bc) {
                TA Av = Ac;
                TB Bv = Bc;

                valT s = 0;
                for ( int k=0 ; k < veclen; ++k, Av+=nrows, Bv+=nrows) {
                    s += conjmul( *Av, *Bv );
                }
                *Out = s;
                ++Out;
            }
        }
    }
}

template< typename TOut, typename TA, typename TB> void dot(TOut Out, TA A, TB B, int dim, const mwSize * sz, int ndim) {
    if ((dim>=ndim) || (sz[dim]==1) ) {
        // dot in a scalar dimension. That is just element wise times.
        mwSize cumsz=1;
        for (int k = 0 ; k < ndim; ++k) {
            cumsz *= sz[k];
        }
        times( Out, A, B, cumsz );
    } else if (sz[dim]>0) { // if sz[dim]==0, then there is nothing to sum and we should return a all zeros matrix of the right size.
        if (dim==0) {
            // dot over first dimension. 
            mwSize cumsz=1;
            for (int k = 1 ; k < ndim; ++k) {
                cumsz *= sz[k];
            }
            dot1(Out, A, B, sz[0], cumsz);
        } else {
            // Dot dimension is the first dimension:
            mwSize cumsz1=1;
            mwSize cumsz2=1;
            
            for (int k = 0 ; k < dim; ++k) {
                cumsz1 *= sz[k];
            }
            for (int k = dim+1 ; k < ndim; ++k) {
                cumsz2 *= sz[k];
            }
            dot2(Out, A, B, cumsz1, sz[dim], cumsz2);
        }
    }
    
};

template <typename value_typeA, typename value_typeB, typename value_typeOut>
void dot_delegator(const mxArray * A,const mxArray * B, mxArray * & mxOut ,
                  int ndim, mwSize * outsz, int dim, const mwSize * sz) {
    typedef const value_typeA * dataPtrAConst;
    typedef const value_typeB * dataPtrBConst;
    typedef value_typeOut * dataPtrOut;
    typedef typename guard_pointer_type< dataPtrOut >::type realPtrOut;
    typedef typename guard_pointer_type< dataPtrAConst >::type realPtrAConst;
    typedef typename guard_pointer_type< dataPtrBConst >::type realPtrBConst;
    typedef typename guard_pointer_type< complex_pointer< dataPtrOut > >::type complexPtrOut;
    typedef typename guard_pointer_type< complex_pointer< dataPtrAConst > >::type complexPtrAConst;
    typedef typename guard_pointer_type< complex_pointer< dataPtrBConst > >::type complexPtrBConst;
    
            
    if (mxIsComplex(A) || mxIsComplex(B)) {
        // complex output
    	mxOut = mxCreateNumericArray(ndim, outsz, mxClassIDtemplate< value_typeOut >::ClassID, mxCOMPLEX );
        complexPtrOut Cd( mxGetComplexPtr< dataPtrOut >( mxOut ) );
        
        if (mxIsComplex(A)) {
            complexPtrAConst Ad( mxGetComplexPtr< dataPtrAConst >( A ) );
            
            if (mxIsComplex(B) ) {
                complexPtrBConst Bd( mxGetComplexPtr< dataPtrBConst >( B ) );
                dot( Cd, Ad, Bd, dim, sz, ndim);
            } else {
                realPtrBConst Bd( mxGetPtr< dataPtrBConst >( B ) );
                dot( Cd, Ad, Bd, dim, sz, ndim);
            }
        } else {
            // A not complex, so B has to be:
            realPtrAConst    Ad( mxGetPtr< dataPtrAConst >( A ) );
            complexPtrBConst Bd( mxGetComplexPtr< dataPtrBConst >( B ) );
            
            dot( Cd, Ad, Bd, dim, sz, ndim);
        }
        
    } else {
        //no complex output:
        realPtrAConst Ad( mxGetPtr< dataPtrAConst >( A ) );
        realPtrBConst Bd( mxGetPtr< dataPtrBConst >( B ) );
        mxOut = mxCreateNumericArray(ndim, outsz, mxClassIDtemplate< value_typeOut >::ClassID, mxREAL);
        realPtrOut  Cd( mxGetPtr< dataPtrOut >( mxOut ) );
        
        dot( Cd, Ad, Bd, dim, sz, ndim);
    }
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
/* out = dot(A, B [,ndim])
*/
	/* Check for proper number of arguments. */
	if ((nrhs!=2) && (nrhs!=3) ) {
		mexErrMsgTxt("Two or three inputs required: Out = dot(A, B [,dim]). ");
	} else if((nlhs!=0)&& (nlhs!=1)) {
		mexErrMsgTxt("There can be at most one output.");
	}

	/* parse inputs */
    const mxArray * A = prhs[0];
    const mxArray * B = prhs[1];
    
	// Check  A: 
    int ndim = mxGetNumberOfDimensions(A);
    mxClassID classA = mxGetClassID(A);
    mxClassID classB = mxGetClassID(B);
	if (    (ndim != mxGetNumberOfDimensions(B))  
         || ((classA!=mxDOUBLE_CLASS)&&(classA!=mxSINGLE_CLASS)) 
         || ((classB!=mxDOUBLE_CLASS)&&(classB!=mxSINGLE_CLASS))  ) { 
		mexErrMsgTxt("A and B should have the same number of dimensions and be of double class (for this mex function)");
	}
    const mwSize * sz = mxGetDimensions( A );
    const mwSize * szB = mxGetDimensions( B );
    int dim = -1;
    if (nrhs==3) {
        const mxArray * C = prhs[2];
        if ((mxGetNumberOfElements(C)!=1) || (mxIsComplex(C)) ) { 
            mexErrMsgTxt("Dimension argument must be a positive integer scalar within indexing range.");
        }
        double dimd = mxGetScalar(C);
        dim = (int) dimd;
        if ( (dimd!=(double) dim) || (dim<=0) ) {
            mexErrMsgTxt("Dimension argument must be a positive integer scalar within indexing range.");
        }
        dim = dim - 1; // convert from MATLAB dimension number (1 based) to C dimension number (0 based)
        if (dim>ndim) {
            dim = ndim+1; // prevent out of bounds access of outsz.
        }
    }
    for (int k = 0 ; k < ndim; ++k) {
        if (sz[k]!=szB[k]) {
            // check if we happen to have 2 differently oriented vectors. Only allowed when no dim argument is provided.
            if ( (nrhs!=2) && (ndim==2) && (sz[0]==szB[1]) && (sz[1]==szB[0]) && ( (sz[0]==1) || (sz[1]==1) ) ) {
                if (sz[0]==1) {
                    dim = 1;
                }
                break;
            } else {
                mexErrMsgTxt("A and B should have the same size, or both can be vectors when the dim argument is ommitted.");
            }
        }
        if ((dim==-1) && (sz[k]!=1) ) {
            // set to first non scalar dimension
            dim = k;
        }
    }
    if (dim==-1) { // special case that we do not want a crash on: 2 scalars.
        dim = 0;
    }
#define MAXNUMDIMS 10    
    mwSize outsz[ MAXNUMDIMS ];
    if (ndim>MAXNUMDIMS) {
        mexErrMsgTxt("Too many dimensions in input, increase MAXNUMDIMS in dot.cpp");
    }
    for (int k = 0 ; k < ndim; ++k) {
        outsz[k]=sz[k];
    }
    if (dim < ndim) {
        outsz[dim] = 1;
    }
    
    if (classA==mxDOUBLE_CLASS) {
        if (classB==mxDOUBLE_CLASS) {
            dot_delegator<double, double, double>( A, B, plhs[0] ,ndim, outsz, dim, sz) ;
        } else {
            dot_delegator<double, float, float>( A, B, plhs[0] ,ndim, outsz, dim, sz) ;
        }  
    } else {
        if (classB==mxDOUBLE_CLASS) {
            dot_delegator<float, double, float>( A, B, plhs[0] ,ndim, outsz, dim, sz) ;
        } else {
            dot_delegator<float, float, float>( A, B, plhs[0] ,ndim, outsz, dim, sz) ;
        }
    }
        
   /* typedef double * dataPtr;
    typedef const double * dataPtrConst;
    typedef guard_pointer_type< dataPtr >::type realPtr;
    typedef guard_pointer_type< dataPtrConst >::type realPtrConst;
    typedef guard_pointer_type< complex_pointer< dataPtr > >::type complexPtr;
    typedef guard_pointer_type< complex_pointer< dataPtrConst > >::type complexPtrConst;
    
            
    if (mxIsComplex(A) || mxIsComplex(B)) {
        // complex output
    	plhs[0] = mxCreateNumericArray(ndim, outsz, mxDOUBLE_CLASS, mxCOMPLEX );
        complexPtr Cd( mxGetComplexPtr< dataPtr >( plhs[0] ) );
        
        if (mxIsComplex(A)) {
            complexPtrConst Ad( mxGetComplexPtr< dataPtrConst >( A ) );
            
            if (mxIsComplex(B) ) {
                complexPtrConst Bd( mxGetComplexPtr< dataPtrConst >( B ) );
                dot( Cd, Ad, Bd, dim, sz, ndim);
            } else {
                realPtr Bd( mxGetPtr< dataPtr >( B ) );
                dot( Cd, Ad, Bd, dim, sz, ndim);
            }
        } else {
            // A not complex, so B has to be:
            realPtrConst    Ad( mxGetPtr< dataPtrConst >( A ) );
            complexPtrConst Bd( mxGetComplexPtr< dataPtrConst >( B ) );
            
            dot( Cd, Ad, Bd, dim, sz, ndim);
        }
        
    } else {
        //no complex output:
        realPtrConst Ad( mxGetPtr< dataPtrConst >( A ) );
        realPtrConst Bd( mxGetPtr< dataPtrConst >( B ) );
        plhs[0] = mxCreateNumericArray(ndim, outsz, mxDOUBLE_CLASS, mxREAL);
        realPtr  Cd( mxGetPtr< dataPtr >( plhs[0] ) );
        
        dot( Cd, Ad, Bd, dim, sz, ndim);
    }*/
        
     
}

