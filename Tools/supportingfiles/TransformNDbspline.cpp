#ifdef BUILDING_CORE // Code of routine starts at #else

/*
 * This ransformNDbspline.cpp is a MEX-file for MATLAB.
 * Created by Dirk Poot, Erasmus Medical Center
 * Build with: (add -g for debugging (add -O for optimizations while debugging); -v for verbose mode )
 * mex TransformNDbspline.cpp -I".\Tools\supportingfiles\" -g -DINCLUDE_SSE3
 *
 * Rebuild from TransformCoreGeneral_c, now iterators, better design, more general transforms.
 * To compile a function test (call without arguments):
 * // mex TransformNDbspline.cpp -DBUILDTESTFUN_TRANSFORMCOREGENERAL
 * // mex TransformNDbspline.cpp -I".\Tools\supportingfiles\" -v CXXFLAGS="\$CXXFLAGS -fopenmp -mssse3 -mtune=core2" LDFLAGS="\$LDFLAGS -fopenmp"
 * mex TransformNDbspline.cpp -I".\Tools\supportingfiles\" -v -DINCLUDE_SSE3 CXXFLAGS="\$CXXFLAGS -mssse3 -mtune=core2" LDFLAGS="\$LDFLAGS "
 *
 * Or on Linux:
 * // mex TransformNDbspline.cpp -DUSEOMP -I"./" -I"./Tools/supportingfiles/" -v CXXFLAGS="\$CXXFLAGS -fopenmp -march=barcelona" LDFLAGS="\$LDFLAGS -fopenmp"
 * // mex TransformNDbspline.cpp -DUSEOMP -I"./" -I"./Tools/supportingfiles/" -v CXXFLAGS="\$CXXFLAGS -fopenmp -march=barcelona" LDFLAGS="\$LDFLAGS -fopenmp"
 * Without Open MP (i.e. without using multiple cores):
 * mex TransformNDbspline.cpp -I"./" -I"./Tools/supportingfiles/" -v CXXFLAGS="\$CXXFLAGS -march=barcelona -std=c++0x" LDFLAGS="\$LDFLAGS " -DMEX
 *
 * // Set  'OMP_NUM_THREADS' to the desired number of threads before (!)
 * // calling TransformCoreGeneral_c2 (or any other OMP enabled mex file)
 */



#ifndef ARTEMPLATES
#define ARTEMPLATES	// include AR templates only once.

template <typename int_t, typename readType, typename tempType, typename outType, typename ARType  >
        void readAndFilterWithAR(  readType in   , range< int_t > r_in,
        ARType  AR, int_t nAR,   // use * __restrict
        tempType temp , range< int_t > r_temp,
        outType  out  , range< int_t > r_out )
        /* Read image line and prefilter with the specified AR model.
         * In MATLAB syntax:
         *   out = flip(filter(1,ar,flip(filter(1,ar,in))))
         *
         * Input:in(i) = / in[ i  ]   for  r_in.first<= i <= r_in.last
         *               \  0             for  i<r_in.first | i>r_in.last
         * temp[i] = in(i) - sum_{1<= j <nAR} temp[i-j]*AR[j]
         * out[i]  = temp[i] - sum_{1<= j <nAR} out[i+j]*AR[j]
         * compute out[i] for r_out.first <= i <= r_out.last
         * Temp overlaps with out, with temp_start<= i <=r_out.last
         *
         */
{
    // out = filter(1,ar, x) =
    // for k=1:numel(out); out(k)= -sum(out(k-1:-1:max(1,k-numel(ar)+1)).*ar(2:min(k,numel(ar))))/ar(1)+x(k)/ar(1);end;
    using std::max;
    using std::min;
    using std::iterator_traits;
    typedef typename iterator_traits< tempType >::value_type outValueT;
    
    int_t i_start = max(r_in.first, r_temp.first);
    outValueT zero = (outValueT) 0.0;// = Numeric(outValueT).Zero();
    // clear temp (needed for reverse filter, and when nAR==0)
    if (( r_temp.first > r_out.first) || ( r_temp.first > r_in.first) || (r_temp.last<r_out.last) ) {
        ERROR("range of out and in should be contained in range of temp.");
    }
    int_t i = min( r_out.first, r_in.first ); // we wont need any elements prior to r_out.first
    for(; i <= min(r_in.first-1,r_out.last) ; i++) {
        temp[i] = zero;
    }
    // lead in (not all of the AR filter can be used)
    for(; i <= min( i_start + nAR-2, r_out.last ) ; i++) {
        outValueT tmp = ((i<=r_in.last) ? in[i] : zero);
        for (int_t j= i-i_start;j >0; j--) { // start with largest delay to break dependency chains.
            tmp -=  temp[i-j] * AR[j];
        }
        temp[i] = tmp;
    }
    // main part forward filter:
    for(; i<=r_out.last;i++) {
        outValueT tmp = ((i<=r_in.last) ? in[i] : zero);
        for (int_t j= nAR-1;j >0; j--) { // start with largest delay to break dependency chains.
            tmp -= temp[i-j] * AR[j];
        }
        temp[i] = tmp;
    }
    if (nAR>0) {
        i--;
        out[i] = temp[i];
        i--;
        // lead in -reverse- filter:
        for(; i>= max(r_out.last-nAR+2,r_out.first) ;i--) {
            outValueT tmp = zero;
            for (int_t j= r_out.last-i;j >0; j--) {  // start with largest delay to break dependency chains.
                tmp -=  (outValueT) out[i+j] * AR[j];
            }
            out[i] = temp[i] + tmp;
        }
        // main part -reverse- filter:
        for(; i>=r_out.first ; i--) {
            outValueT tmp = zero;
            for (int_t j= nAR-1;j >0; j--) { // start with largest delay to break dependency chains.
                tmp -= (outValueT) out[i+j] * AR[j];
            }
            out[i] = temp[i] + tmp;
        }
    } else {
        // just copy to output:
        i--;
        for(; i>=r_out.first ; i--) {
            out[i] = temp[i];
        }
        
    }
};
template< typename T> struct range_s { T first; T last; T step; };

template <typename int_t, typename readType, typename tempType, typename outType, typename ARType  >
        void readAndFilterImgWithAR(  readType in   , range_s< int_t > * r_in, int ndimsIn,
        ARType  AR, int_t nAR,   // use * __restrict
        tempType temp , range_s< int_t > * r_temp,
        outType  out  , range_s< int_t > * r_out )
{ /* Filter the image in in each dimension with the AR filter.
 * INPUTS:
 * in   : input data
 * temp : temporary storage, It should be possible to static_cast tempType to readType (e.g. add const)
 * and to outType. (which basically means that the actual storage format should be equal)
 * out  : output location
 */
    
    // Do the prefiltering:
    
    range_s< int_t > cur_r_in[MaxNrOfDims];
    range_s< int_t > cur_r_out[MaxNrOfDims];
    for (int k = 0 ; k < ndimsIn; ++k) {
        if ( (r_temp[k].first > r_out[k].first ) || (r_temp[k].last < r_out[k].last ) ||
                (r_temp[k].first > r_in[k].first  ) || (r_temp[k].last < r_in[k].last  )     ) {
            ERROR("range of in and out should be contained in range of temp in each dimension.");
        }
        cur_r_in[k]       = r_in[k]; //copy input range and step
        cur_r_out[k]      = r_in[k]; // set output range and step to input (out initially stored in temp)
        cur_r_out[k].step = r_temp[k].step; // set step of out to step of temp (since out is stored in temp)
    }
    readType cur_in = in;
    
    // Filter all 'columns' in all dimensions:
    for (int filterdim= ndimsIn-1; filterdim>=0; filterdim-- ) {
        outType cur_out;
        if (filterdim>0) {
            cur_out = static_cast<outType>( temp );
        } else {
            cur_out = out;
            // set all steps of out to those of out.
            for (int k = 0 ; k < ndimsIn; ++k) {
                cur_r_out[k].step = r_out[k].step;
            }
        }
        // set (to be) processed dimension to output dimensions.
        cur_r_out[filterdim].first = r_out[filterdim].first;
        cur_r_out[filterdim].last  = r_out[filterdim].last;
        
        // get cumulative sizes.
        int_t postsz = 1;
        for (int k = ndimsIn-1 ; k > filterdim ; --k) {
            postsz *= (cur_r_in[ k ].last-cur_r_in[ k ].first+1) ;
        }
        int_t presz =1 ;
        for (int k = filterdim-1; k>=0; --k) {
            presz *= (cur_r_in[ k ].last-cur_r_in[ k ].first+1) ;
        }
        
        // set column position and initial offsets:
        int_t curpos[MaxNrOfDims];
        ptrdiff_t curoffset_i[MaxNrOfDims+1];
        ptrdiff_t curoffset_t[MaxNrOfDims+1];
        ptrdiff_t curoffset_o[MaxNrOfDims+1];
        
        curoffset_i[ndimsIn]=0;
        curoffset_t[ndimsIn]=0;
        curoffset_o[ndimsIn]=0;
        for (int k=ndimsIn-1 ; k >= 0 ; --k) {
            if (k==filterdim) {
                curpos[k]=0;
            } else {
                curpos[k] = cur_r_in[ k ].first;
            }
            curoffset_i[k] = curoffset_i[k+1] + cur_r_in [k].step * curpos[k];
            curoffset_t[k] = curoffset_t[k+1] + r_temp   [k].step * curpos[k];
            curoffset_o[k] = curoffset_o[k+1] + cur_r_out[k].step * curpos[k];
        }
        
        // set ranges that should be processed in this dimension:
        range<int_t> rin;
        range<int_t> rtemp;
        range<int_t> rout;
        rin.first  = cur_r_in[filterdim].first;
        rin.last   = cur_r_in[filterdim].last;
        rtemp.first= r_temp  [filterdim].first;
        rtemp.last = r_temp  [filterdim].last;
        rout.first = cur_r_out[filterdim].first;
        rout.last  = cur_r_out[filterdim].last;
        
        // loop over all 'columns':
        for (int k2 =0 ; k2<presz*postsz; k2++) { // iterate 'slow' over last dimensions (they are assumed to be far apart in memory)
            
            //for (int k1 =0 ; k1< presz; k1++ ) { // iterate 'fast' over first dimensions( they are assumed to be close in memory)
            
            // setup iterators:
            step_iterator< readType , int_t > inl(  cur_in + curoffset_i[0], cur_r_in[filterdim].step );
            step_iterator< tempType , int_t > tmpl( temp   + curoffset_t[0], r_temp  [filterdim].step );
            step_iterator< outType  , int_t > outl( cur_out+ curoffset_o[0], cur_r_out[filterdim].step );
            // do actual processing:
            readAndFilterWithAR(inl , rin, AR, nAR, tmpl, rtemp, outl, rout );
            
            // move to next column:
            int d1=0;
            while (true) {
                if (d1==filterdim) { // step over filterdim since that is the current 'column' dimension
                    ++d1;
                }
                if (d1>=ndimsIn) { // avoid invalid memory accesses and terminate final loop.
                    break;
                }
                ++curpos[d1]; // move to next position.
                if (curpos[d1] <= cur_r_in[ d1 ].last) {
                    break;
                }
                curpos[d1] = cur_r_in[ d1 ].first; // reset column position.
                ++d1;
            }
            while (d1>=0) {
                curoffset_i[d1]= curoffset_i[d1+1] + curpos[d1]* cur_r_in [d1].step;
                curoffset_t[d1]= curoffset_t[d1+1] + curpos[d1]* r_temp   [d1].step;
                curoffset_o[d1]= curoffset_o[d1+1] + curpos[d1]* cur_r_out[d1].step;
                d1--;
            }
            //}
        }
        // in first loop, prepare cur_in for the next loop (since it should point to the output of the current loop)
        if (filterdim==ndimsIn-1) {
            cur_in = static_cast<readType>( temp );
            for (int k = 0 ; k < ndimsIn; ++k) {
                cur_r_in[k].step = r_temp[k].step;
            }
        }
        // update range in input (only modified in filterdim
        cur_r_in[ filterdim ].first = cur_r_out[ filterdim ].first;
        cur_r_in[ filterdim ].last  = cur_r_out[ filterdim ].last;
    }
};

/*
 * The following function contains the main code of this mex file. It is compiled with 2 options (all 4 combinations):
 * DERIVATIVE   : Also compute derivative.
 * ADJOINT      : evaluate adjoint operation.
 * NOTE that in adjoint mode, the image is read from 'out' and stored into 'in'
 * The adjoint mode is required by iterative reconstruction methods (like conjugated gradient).
 * Unless you use such a method, you don't want to use the adjoint operation.
 *
 */
#endif // end of ifdef ARTEMPLATES


#ifdef DERIVATIVE
#define DERIVWRITEARGS  , writeType dOut
#define DERIVWRITEARGSs , dOut
#else
#define DERIVWRITEARGS
#define DERIVWRITEARGSs
#endif
#ifdef ADJOINT
#define FILTERLINEWITHF_SPEC filterLineWithF_Specialized_adj
#define FILTERLINEWITHF      filterLineWithF_adj
#else
#define FILTERLINEWITHF_SPEC filterLineWithF_Specialized
#define FILTERLINEWITHF      filterLineWithF
#endif
//#ifndef NOTALIASED
//#define NOTALIASED __restrict
//#endif

template <typename int_t,  typename type_i, typename ptrType_o, typename posType, typename filtType >
        inline void filterLineWithF(  type_i & /*NOTALIASED */ img_i, ptrType_o line_o,
        range< int_t > rng_m ,
        posType pos, filtType & F )
        /* filters the elements img_i image i with the filter F.
         */
{
    using std::iterator_traits;
    typedef typename iterator_traits<posType>::value_type posElType;
    typedef typename iterator_traits<ptrType_o>::value_type outValueType;
    
    for( int_t m = rng_m.first ; m<= rng_m.last; m++ ) {
        // do the actual sampling.
        // need template after img_i (c++ standard requires) to specify that we actually want to use a templated member function.
        //line_o[m] = img_i->template sample < outValueType , posElType, filtType> ( pos +  m *img_i->Ndims() , F ) ;
        img_i->template sample < ptrType_o> ( line_o + m * img_i->Vlen() , pos +  m *img_i->Ndims() , F ) ;
    }
};

template <typename int_t,  typename type_i, typename ptrType_o, typename posType, typename filtType >
        inline void filterLineWithFAndDerivative(  type_i & /*NOTALIASED */ img_i, ptrType_o line_o, ptrType_o Dline_o,
        range< int_t > rng_m ,
        posType pos, filtType & F )
        /* filters the elements img_i image i with the filter F.
         */
{
    using std::iterator_traits;
    typedef typename iterator_traits<posType>::value_type posElType;
    typedef typename iterator_traits<ptrType_o>::value_type outValueType;
    
    for( int_t m = rng_m.first ; m<= rng_m.last; m++ ) {
        // do the actual sampling.
        // need template after img_i (c++ standard requires) to specify that we actually want to use a templated member function.
        //line_o[m] = img_i->template sample < outValueType , posElType, filtType> ( pos +  m *img_i->Ndims() , F ) ;
        img_i->template sampleAndDerivative < ptrType_o> ( line_o + m * img_i->Vlen() , Dline_o + m * img_i->Vlen() *img_i->Ndims() , pos +  m *img_i->Ndims() , F ) ;
    }
};


template <typename int_t,  typename type_i, typename ptrType_o, typename posType, typename filtType >
        inline void filterLineWithF_adj(  type_i & /*NOTALIASED */ img_i, ptrType_o line_o,
        range< int_t > rng_m ,
        posType pos, filtType & F )
        /* filters the elements img_i image i with the filter F.
         */
{
    using std::iterator_traits;
    typedef typename iterator_traits<posType>::value_type posElType;
    typedef typename iterator_traits<ptrType_o>::value_type outValueType;
    
    for( int_t m = rng_m.first ; m<= rng_m.last; m++ ) {
        // do the actual sampling.
        // need template after img_i (c++ standard requires) to specify that we actually want to use a templated member function.
        //line_o[m] = img_i->template sample < outValueType , posElType, filtType> ( pos +  m *img_i->Ndims() , F ) ;
        img_i->template sampleAdjoint < ptrType_o > ( line_o + m * img_i->Vlen() , pos +  m *img_i->Ndims() , F  ) ;
    }
};

/*
 * template <typename int_t,  typename type_i, typename ptrType_o, typename posType, typename filtType >
 * inline void FILTERLINEWITHF(  type_i in, ptrType_o out  DERIVWRITEARGS,
 * range< int_t > rng_m , range<int_t> rng_i,
 * posType pos, filtType F)
 * {
 * if (F.subtype() == 0) {
 * typedef filtType::specialization0 filtType_c;
 * filtType_c F_c = filtType_c(F);
 * FILTERLINEWITHF_SPEC(in,out DERIVWRITEARGSs, rng_m, rng_i, pos, F_c);
 * } else if (F.subtype() == 1) {
 * typedef filtType::specialization1 filtType_c;
 * filtType_c F_c = filtType_c(F);
 * FILTERLINEWITHF_SPEC(in,out DERIVWRITEARGSs, rng_m, rng_i, pos, F_c);
 * } else if (F.subtype() == 2) {
 * typedef filtType::specialization2 filtType_c;
 * filtType_c F_c = filtType_c(F);
 * FILTERLINEWITHF_SPEC(in,line_o DERIVWRITEARGSs, rng_m, rng_i, pos, F_c);
 * } else {
 * mexErrMsgTxt("Bad subtype value of F.");
 * }
 * }*/
#undef FILTERLINEWITHF_SPEC
#undef FILTERLINEWITHF

//#ifdef ADJOINT // first build normal function then adjoint, only construct filterline when both are defined.


template <typename int_t, typename type_i, typename ptrType_o, typename ptrType_Tline, typename filtType>
        inline void filterLine_F_Adj ( type_i & img_i, ptrType_o line_o,
        range< int_t > rng_m, ptrType_Tline pos ,
        filtType & F )
{
    filterLineWithF_adj<int_t,  type_i, ptrType_o, ptrType_Tline, filtType>( img_i, line_o, rng_m, pos, F );
    
}

template <typename int_t, typename type_i, typename ptrType_o, typename ptrType_Tline, typename filtType>
        inline void filterLine_F_Derivative ( type_i & img_i, ptrType_o line_o, ptrType_o Dline_o,
        range< int_t > rng_m, ptrType_Tline pos ,
        filtType & F )
{
    filterLineWithFAndDerivative<int_t,  type_i, ptrType_o, ptrType_Tline, filtType>( img_i, line_o, Dline_o, rng_m, pos, F );
    
}

template <typename int_t, typename type_i, typename ptrType_o, typename ptrType_Tline, typename filtType>
        inline void filterLine_F ( type_i & img_i, ptrType_o line_o,
        range< int_t > rng_m, ptrType_Tline pos ,
        filtType & F )
{
    typedef typename filtType::value_type  filtValueType;
    typedef filtValueType *  filtPointerType;
    if ( typeid(F)==typeid(bspline_coefficients<3, filtPointerType, filtValueType>) ) { //bSplineFilter<3, filtType::value_type, true>) ) {
        typedef bSplineCoeffs<3, filtValueType> filtTyped ;
        filtTyped Fd = filtTyped();
        filterLineWithF<int_t,  type_i, ptrType_o, ptrType_Tline, filtTyped >( img_i, line_o, rng_m, pos, Fd );
    } else if ( typeid(F)==typeid(bspline_coefficients<1, filtPointerType, filtValueType>) ) { //bSplineFilter<3, filtType::value_type, true>) ) {
        typedef bSplineCoeffs<1, filtValueType> filtTyped ;
        filtTyped Fd = filtTyped();
        filterLineWithF<int_t,  type_i, ptrType_o, ptrType_Tline, filtTyped >( img_i, line_o, rng_m, pos, Fd );
    } else if ( typeid(F)==typeid(bspline_coefficients<2, filtPointerType, filtValueType>) ) { //bSplineFilter<3, filtType::value_type, true>) ) {
        typedef bSplineCoeffs<2, filtValueType> filtTyped ;
        filtTyped Fd = filtTyped();
        filterLineWithF<int_t,  type_i, ptrType_o, ptrType_Tline, filtTyped >( img_i, line_o, rng_m, pos, Fd );
    } else {
        // general call:
        filterLineWithF<int_t,  type_i, ptrType_o, ptrType_Tline, filtType >( img_i, line_o, rng_m, pos, F );
    }
    
}
#undef ARDERIVWRITEARGSs


//#endif // end of ADJOINT (i.e. after both normal and adjoint functions are created.)

#undef DERIVWRITEARGS
#undef DERIVWRITEARGSt
#undef DERIVWRITEARGSs
#undef DERIVFILTARGS
#undef DERIVFILTARGSs
#undef FILTERLINEWITHF_ADJ
#undef FILTERLINEWITHF

#else // else of ifdef BUILDING_CORE

#ifdef _OPENMP
//#define USEOMP
#endif

#define ADDGUARDS
//#define NEW_NDIMAGE

#include <assert.h>
#include "mex.h"
//#include <cmath>
#include "math.h"
#include <complex>
#include <algorithm>
#include <typeinfo>
typedef mwSize IndexType;
typedef ptrdiff_t int_t ;

//#include "emm_wrap.cpp"
#include "emm_vecptr.hxx"
#include "tempSpaceManaging.cpp"
//#include "lineReadWriters.cpp"
#include "step_iterators.cpp"

//#ifdef NEW_NDIMAGE
#include "NDimage_new.cpp"
//#else
//	#include "NDimage.cpp"
//#endif
#include "subspaceIterator.cpp"
#include "SeparableTransform_core_c.cpp"
#include <iterator>
#include <bitset>


#ifdef USEOMP
#include <omp.h>
#endif

//#define PERFORMANCECOUNTING // if defined: return some counts in the first (double) elements of the output. SO FOR DEBUG ONLY!!!
// Compile TransformNDbspline_timing.cpp to get this functionality.
#ifdef PERFORMANCECOUNTING
#include <Windows.h>
typedef volatile __int64 timeCntT;
timeCntT time_used[5];
#pragma intrinsic(__rdtsc)
#endif

#define MaxNrOfDims 6

const int n_rowscompOutArea = 5;
#define ERROR mexErrMsgTxt

#define BUILDING_CORE
#include __FILE__	// non derivative, non adjoint functions
//#define ADJOINT
//#include __FILE__	// non derivative, adjoint functions
//#undef ADJOINT
//#define DERIVATIVE
//#undef FILTERLINE_F // FILTERLINE_F builds differently with/without derivative.
//#include __FILE__	// derivative, adjoint functions
//#define ADJOINT
//#include __FILE__	// derivative, non adjoint functions
//#undef ADJOINT
template< typename T> bool is_nan( T x ) {return x!=x;};

template< typename clipT, typename int_t > vector< clipT > build_clipranges( const mxArray * clip, int_t * size, int nprocdims, int * procdims) {
    /*  build_clipranges
     *  INPUTS:
     *    clip: MATLAB empty or cell array with at least max( prodims[0..nprocdims-1] ) elements
     *          clip{ i } should have size ..
     *    size : nprocdims int array containing the size of the image to be clipped.
     *    nprocdims : number of (spatial) dimensions
     *    procdims  : nprocdims int array containing the spatial dimensions.
     * OUPUTS:
     *   vector of length nprocdims with <clipT> objects.
     *        clipT should be clippedRangeND<double>
     */
    vector< clipT > clips(nprocdims);
    bool doClip = !mxIsEmpty(clip);
    if (doClip) {
        if ((mxGetNumberOfElements( clip ) != nprocdims) | (!mxIsCell( clip ) ) ) {
            mexErrMsgTxt("clip_i/o should be a ndims(T) element cell array, with each element Q_k  a ( nplanes_k  x  (ndims(T)-k+2) )  double matrix, where the subset of all allowable coordinate vectors x is given by  Q*[x;-1] <=0 (if Q = [A b] =>  A*x <= b, which is the standard linear program form in MATLAB).");
        }
        for (int dim = 0 ; dim < nprocdims ; dim++ ) {
            const mxArray * clipel = mxGetCell( clip, dim );
            const double * ptr = NULL;
            int nplanes = 0;
            if (clipel != NULL) {
                if ((mxGetNumberOfDimensions( clipel )!=2) | (mxGetClassID( clipel ) != mxDOUBLE_CLASS) | mxIsComplex( clipel ) | !((mxGetN(clipel)==nprocdims-dim+1)|| mxIsEmpty(clipel)) ) {
                    mexErrMsgTxt("Element k of clip_i should be a nplanes(k) x (nprocdims - k + 2) non complex double matrix.");
                }
                
                ptr = mxGetPr(clipel);
                nplanes = (int) mxGetM(clipel);
            }
            range<double> rng ={0, (double) size[dim]-1};
            //				    pointer to A  ,     pointer to b                      ,  number of half spaces,  dimensionality , outer limits.
            clips[dim] = clipT(    ptr,   ptr + nplanes * (nprocdims-dim),      nplanes,        nprocdims-dim,         rng       );
        }
    } else {
        for (int dim = 0 ; dim < nprocdims ; dim++ ) {
            const double * ptr = NULL;
            int nplanes = 0;
            range<double> rng ={ 0, (double) size[dim] -1 };
            //				    pointer to A  ,     pointer to b                      ,  number of half spaces,  dimensionality , outer limits.
            clips[dim] = clipT(    ptr,   ptr + nplanes * (nprocdims-dim),      nplanes,        nprocdims-dim,         rng       );
        }
    }
    return clips;
}

template< typename T , typename int_t> T* adp( T* p, int_t step) {
    if (p==NULL) {
        return p;
    } else {
        return p+step;
    }
}

#undef BUILDING_CORE
#ifndef NOMEXFUN
struct     areaInfo {double * ptr ; mwSize  n;};


#ifdef BUILDTESTFUN_TRANSFORMCOREGENERAL
// Test code to test the majority of the code in this function. Mainly used to debug the code during development.
#include <bitset>
void self_test_mex( mxArray * out ) {
    std::bitset<5> bl(6);
    bl[4]=1;
    
    
}
#endif


void mexFunction( int nlhs, mxArray *plhs[],
        int nrhs, const mxArray *prhs[] )
{
#ifdef BUILDTESTFUN_TRANSFORMCOREGENERAL
    // Only compile code below for performing a code test.
    if (nlhs!=1) {
        mexErrMsgTxt("One output required.");
    }
    plhs[0] = mxCreateStructMatrix(2,1,0, NULL);
    self_test_mex( plhs[0] );
    return;
#else
    /*  out = transformCorePlane_c(in, Ti, mapT, newszT, dimorders, F, AR, bsplineord)
% Transforms 'in' to 'out'
% pseudocode of function:
in  =  permute(in, dimorders(1,:) );
 
T   =  permute(Ti, dimorders(2,:) );
for all 'columns' j of in
    in_j = column j of in
    % filter in forward and backwards with AR model:
    inf_j = flip( filter(1,AR, flip(filter(1,ar, in_j) )))
    for k=1:newszT
        pos_k = T( k , j);
        posi_k = floor( pos_k );
        posf_k = (pos_k - posi_k) * ( size(F,2)-bsplineord );
 
        out(k,j) = F(:,posf_k)' * inf_j( posi_k + [0:size(F,1)-1] , : );
        % bsplineord-Bspline interpolation between columns of F
        % (implicitly) replace elements outside in with zero.
out = ipermute(out , dimorders(3,:) );
%
% INPUTS:
%  in	: (N)-Dimensional single or double image;
%  .....
% OUTPUTS:
%  out    : (N+k)-Dimensional transformed 'in'
%  doutdT : (N+k)-Dimensional derivative of out w.r.t. to T.
     */
    
    /* Integer types used:
     *  IndexType : size of dimension (changes type depending on 'largeArrayDims' mex compile option
     *  int_t     : ptrdiff_t; a signed integer valid for computing steps in pointers.
     *              Hence used for linear indexing/computing index steps etc.
     *  int       : - counting & listing dimensions.
     *              - length and width of F
     */
#ifdef PERFORMANCECOUNTING
    timeCntT tstart_initialization = __rdtsc();
#endif
    
    /* Check for proper number of arguments. */
    if ((nrhs!=9)) {
        mexErrMsgTxt("Nine inputs required: [out ] = TransformNDbspline( in, T, mapT, newsz, F, bsplineord, AR, clip_i, boundary_confitions.");
    } else if((nlhs!=1)&&(nlhs!=2)) {
        mexErrMsgTxt("One or two outputs required.");
    }
    const mxArray * in                  = prhs[0]; // N-D array, single or double.
    const mxArray * T                   = prhs[1]; // N-D array, double, or 1xN vector
    const mxArray * mapT                = prhs[2]; // 2 x N matrix, double, [offset;step] in T for point in out; point_t = point_out .* mapT(2,:) + mapT(1,:)
    const mxArray * newSz               = prhs[3]; // ndimsT-1 element vector
    const mxArray * F                   = prhs[4]; // length x nsteps, double
    const mxArray * bsplineord          = prhs[5]; // scalar.
    const mxArray * AR                  = prhs[6]; // [] or vector.
    const mxArray * clip_o              = prhs[7]; // [] or ndims element cell array with half planes of 'o' image (output for normal, input for adjoint).
    const mxArray * boundary_condition	= prhs[8]; // scalar indicating zero (0) or mirror (1) or wrap around (2) boundary conditions.
    
    bool computeDerivative = (nlhs>1);
    
    int_t tempspacebytesneeded = 0;
    
    /****************************  parse inputs **************************************/
    
    //***** in: get ndimsIn, sizeIn, imgclass, pointers, complex or not, inElementSize
    int ndimsIn = (int) mxGetNumberOfDimensions( in );
    if ( ndimsIn > MaxNrOfDims ) {
        mexErrMsgTxt("Input has too many dimensions, increase 'MaxNrOfDims' in this mex file");
    }
    const IndexType * sizeInr = mxGetDimensions( in );
    IndexType sizeIn[MaxNrOfDims+1];
    for(int dim = 0; dim < ndimsIn; dim++) {
        sizeIn[ dim ] = sizeInr[ dim ];
    };
    if ( (ndimsIn==2) && (sizeIn[1]==1) ) {
        ndimsIn=1;
    };
    mxClassID imgclass = mxGetClassID(prhs[0]);
    if ((imgclass!=mxDOUBLE_CLASS) ){// && (imgclass!= mxSINGLE_CLASS)) {
        mexErrMsgTxt("Input should be double. (Other types can be added if meaningfull interpolation can be performed.)");
    }
    double * inR = mxGetPr(prhs[0]);
    double * inI = NULL;
    bool iscomplex= mxIsComplex(prhs[0]);
    mxComplexity iscomplexArg = mxREAL; // later overwritten if iscomplex
    if (iscomplex) {
        inI = mxGetPi(prhs[0]);
        iscomplexArg = mxCOMPLEX;
    };
    int_t inElementSize = mxGetElementSize( in ) ;
    
    // Compute steps of in .
    int_t stepIn[MaxNrOfDims+1];
    stepIn[0] = 1;
    for( int dim = 1; dim < ndimsIn; dim++ ) {
        stepIn[ dim ] = stepIn[ dim-1 ] * sizeIn[ dim-1 ];
    }
    stepIn[ ndimsIn ] = 0; // extra element to always save step of tag-along dimension.
    
    
    
    //***** newSz , get: ndimsOut, sizeOut, doAdjoint, nTagDims, procdims, ntagdims, nprocdims
    int_t ndimsOut = mxGetNumberOfElements(newSz);
    if ( (ndimsOut > MaxNrOfDims) || mxIsComplex( newSz ) ||  (mxGetClassID( newSz ) != mxDOUBLE_CLASS)) {
        mexErrMsgTxt("newszT should be a ndimsOut element double vector.");
    }

    for( int dim = ndimsIn; dim < ndimsOut; dim++ ) { // extend size vector of input with 1's. 
        sizeIn[ dim ] = 1; 
        stepIn[ dim ] = 0;
    }
    int nTagDims = 0;
    IndexType tagDimLength = 1;
    int nprocdims=0;
    int procdims[ MaxNrOfDims ]; // array listing spatial dimension.
    double * newSzd = mxGetPr(newSz);
    IndexType sizeOut[ MaxNrOfDims ]; // full size of out
    IndexType size_i[ MaxNrOfDims ]; // Spatial size of 'i' (ignoring tag along dimensions)
    IndexType size_o[ MaxNrOfDims ]; // Spatial size of 'o' (ignoring tag along dimensions)
    sizeOut[ 0 ] = 1; sizeOut[ 1 ] = 1; // always need these initialized, even when ndimsT-1==1
    bool doAdjoint = false;
    for (int dim = 0 ; dim < ndimsOut; dim++ ) {
        if ( is_nan( newSzd[dim] )  ) {
            if ( (nprocdims ==0 ) && (dim <ndimsIn)) {
                nTagDims++;
                tagDimLength *= sizeIn[dim];
            } else {
                mexErrMsgTxt("(Currently) only first dimensions can be a 'tag along' dimension and all 'tag along' dimensions should be present in input.");
            }
            sizeOut[ dim ] = sizeIn[ dim ];
        } else {
            if (nprocdims==0)
                doAdjoint = ( newSzd[dim] < 0 );
            if (doAdjoint) {
                sizeOut[ dim ] = (IndexType) (-newSzd[ dim ]);
            } else {
                sizeOut[ dim ] = (IndexType) newSzd[ dim ];
            }
            if (sizeOut[dim]<=0)
                mexErrMsgTxt("newszT should be a ndimsOut element double vector with positive (or all negative=>adjoint) elements.");
            
            if (doAdjoint) {
                size_i[ nprocdims ] = sizeOut[ dim ];
                size_o[ nprocdims ] = sizeIn[ dim ] ;
            } else {
                size_i[ nprocdims ] = sizeIn[ dim ] ;
                size_o[ nprocdims ] = sizeOut[ dim ];
            }
            procdims[ nprocdims ] = dim;
            nprocdims++;
        }
    }
    if (computeDerivative && doAdjoint) {
        mexErrMsgTxt("Cannot compute derivative of Adjoint.");
    }
    // Compute steps of out.
    int_t stepOut[ MaxNrOfDims + 1 ];
    stepOut[ 0 ] = 1;
    for ( int dim = 1 ; dim < ndimsOut ; dim++ ) {
        stepOut[ dim ] = stepOut[ dim - 1 ] * sizeOut[ dim - 1 ];
    }
    stepOut[ ndimsOut ] = 0; // extra element to always save step of tag-along dimension.
    
    int_t ndims_i = (doAdjoint ? ndimsOut : ndimsIn);
    int_t step_i[ MaxNrOfDims ]; // memory step in 'i' for each spatial dimension (ignoring tag along dimensions, to correspond to size_i)
    int_t step_o[ MaxNrOfDims ]; // memory step in 'o' for each spatial dimension (ignoring tag along dimensions, to correspond to size_i)
    for ( int dim = 0 ; dim < nprocdims ; dim++ ) {
        if (doAdjoint) {
            step_i[ dim ] = stepOut[ procdims[ dim ] ];
            step_o[ dim ] = stepIn[ procdims[ dim ] ];
        } else {
            step_o[ dim ] = stepOut[ procdims[ dim ] ];
            step_i[ dim ] = stepIn[ procdims[ dim ] ];
        }
    }
    
    // T : get ndimT, sizeT, pointer, isAffineTransform
    int ndimsT = (int) mxGetNumberOfDimensions( T );
    const IndexType * sizeT = mxGetDimensions( T );
    if ( (mxGetClassID( T ) != mxDOUBLE_CLASS) || mxIsComplex( T ) || (ndimsT>ndims_i+1) || (sizeT[0] != nprocdims) ) {
        mexErrMsgTxt("Transform T should be non complex double matrix with at most the same number of spatial dimensions as i, size( T, 1) should be nprocdims");
    }
    double * Td = mxGetPr( T );
    bool isAffineTransform = (ndimsT==2) && (sizeT[1] == nprocdims+1) ; // TODO: add check for size_o == size(T,2...end). Then isAffineTransform should be false.
    
    // mapT : get pointer
    double * mapTd = NULL;
    if ( !mxIsEmpty( mapT ) ) {
        if (isAffineTransform) {
            mexErrMsgTxt("MapT should be empty when T specifies an affine transformation.");
        }
        if ((mxGetClassID( mapT ) != mxDOUBLE_CLASS) || mxIsComplex( mapT ) ||  (mxGetM(mapT)!=2) || (mxGetN(mapT)!=ndims_i) ) {
            mexErrMsgTxt("MapT should be 2 x ndims_i non complex double matrix");
        }
        mapTd = mxGetPr(mapT);
    }
    
    
    
    
    // bsplineord: get bsplineordi
    if ((mxGetNumberOfElements( bsplineord )<1) | (mxGetNumberOfElements( bsplineord )>2) | mxIsComplex( bsplineord ) ) {
        mexErrMsgTxt("bsplineord should be a non complex scalar integer, or 2 element integer vector, in the range [0 .. 3].");
    }
    int bsplineordi = (int) mxGetScalar( bsplineord ) ;
    if ( (bsplineordi<-3) | (bsplineordi>3)) {
        mexErrMsgTxt("bsplineord should be 0, 1, 2, or 3, or -2,-3 for OMOMS interpolation.");
    }
    int bsplineordT =   (int) (mxGetPr( bsplineord)[mxGetNumberOfElements( bsplineord )-1]); // bspline order of interpolation of T. It has previously been checked that bsplineord has size 1 or 2.
    
    double mapTprocdim[ 2 * MaxNrOfDims ];
    // Now that every input influencing interpretion of T is known. Check size of T:
    if (!isAffineTransform) { // affine already completely checked
        if (mapTd==NULL) {
            // Case a: T input directly specifies transform for each output point.
            // check if size of 'o' and size of T match.
            for (int dim = 0 ; dim < nTagDims ; dim++ ) {
                if ( sizeT[ dim + 1 ] != 1 ) {
                    mexErrMsgTxt("Size of T should be 1 in tag along dimensions.");
                }
            }
            for (int dim = 0 ; dim < nprocdims; dim++) {
                int dimT = procdims[ dim ] + 1;
                IndexType szT  = (dimT < ndimsT ? sizeT[ dimT ] : 1 );
                if ( szT != size_o[ dim ] ) {
                    mexErrMsgTxt("Size of T does not match size of 'o' (Out, or In if adjoint) and mapT not specified.");
                }
            }
            
        } else { // case b: mapTd != NULL
            // check if all points of output  (or input if doAdjoint) map to valid sample locations in T.
            int_t cumszT = sizeT[0];
            tempspacebytesneeded += ndimsT * sizeof(double*); // assume sizeof( double * ) == sizeof( float *) == sizeof( any_data_type * )
            for (int dim = 0; dim < nprocdims ; dim++ ) {
                int dimT = procdims[ dim ] + 1;
                IndexType szT  = (dimT < ndimsT ? sizeT[ dimT ] : 1 );
                tempspacebytesneeded += cumszT * inElementSize;
                cumszT *= szT;
                double p1 = mapTd[ 0 + 2*procdims[ dim ] ] - (bsplineordT-1)*.5;    // first index.
                double p2 = p1 + mapTd[1 + 2*procdims[ dim ] ] * (size_o[ dim ] -1);// last index.
                if ((p1<0) || (p2<0) || (floor(p1) >= szT - bsplineordT) || ( floor(p2) >= szT -bsplineordT ) ) {
                    mexErrMsgTxt("Invalid mapT, all points of  should map to inside of T.");
                }
                mapTprocdim[ 0 + 2 * dim ] = p1;
                mapTprocdim[ 1 + 2 * dim ] = mapTd[ 1 + 2* procdims[dim] ];
                
            }
        }
    }
    
    // F : get pointer, mF, nFsteps, filt
    mxClassID filtclass = mxGetClassID( F );
    if ((mxGetNumberOfDimensions( F )>2) | (filtclass != mxDOUBLE_CLASS) | mxIsComplex( F )) {
        mexErrMsgTxt("F should be a double precision non complex matrix. (Other types can easily be added, if meaningfull interpolation can be performed)");
    }
    double * Fd =  mxGetPr( F );
    int mF	    = (int) mxGetM( F );				// F should not be too large for int.
    int nFsteps = (int) mxGetN( F ) - bsplineordi;	// F should not be too large for int.
    
    
    typedef coefficients_base<double * , double, int> filtType;
    filtType * filt; // holds interpolation filter.
    if ((Fd==NULL) || (nFsteps<2)) {
        // No Filterbank provided, so direct interpolation filter:
        if (bsplineordi>=0) {
            filt = newbSplineSamplingCoefficients<double, int>( bsplineordi );
        } else {
            filt = newOMOMSSamplingCoefficients<double, int>( -bsplineordi );
        }
    } else {
        // Filterbank provided, so use that:
        filt = newFilterbankCoefficients( Fd , mF, nFsteps, bsplineordi );
    }
    
    
    // AR: get prefilterwithAR, pointer, nAR, ARrunoutLen, add to tempspacebytesneeded
    bool prefilterwithAR = !mxIsEmpty(AR);
    double * ARd = NULL;
    int_t nAR, ARrunoutLen;
    if (prefilterwithAR) {
        if ((mxGetNumberOfDimensions( AR )>2) | (mxGetClassID( AR ) != mxDOUBLE_CLASS) | mxIsComplex( AR )) {
            mexErrMsgTxt("AR should be a double precision non complex vector. (Other types can relatively easily be added in the source code)");
        }
        ARd =  mxGetPr( AR );
        nAR  = mxGetNumberOfElements( AR );
        ARrunoutLen = (int_t) ARd[0]; // read runoutlength value from first element of AR vector
        
        int_t nel = tagDimLength;
        for (int dim = 0 ; dim < nprocdims ; dim++ ) {
            nel *= ( size_i[ dim ] + 2 * ARrunoutLen ); //Size extended by ARrunoutLen in all spatial dimensions.
        }
        tempspacebytesneeded += nel * ((iscomplex) ? 2 : 1 ) * inElementSize;
        
    }
    
    
    
    
    
    
    
    // Get multi threading info:
#ifdef USEOMP
    int olddynamic =  omp_get_dynamic();
    omp_set_dynamic(1);
    int numThreadsUse = omp_get_max_threads();//omp_get_num_procs();
//	printf("omp_get_dynamic: %d, omp_get_num_procs: %d, omp_get_max_threads: %d \n",olddynamic,numThreadsUse, omp_get_max_threads() );
    if (size_o[ nprocdims ]<numThreadsUse) {
        numThreadsUse = size_o[ nprocdims ];
    }
    if ( nprocdims < 1) {
        numThreadsUse = 1;  // TODO: don't we want to use multiple threads?
    }
    omp_set_num_threads( numThreadsUse );
    int maxthreadsuse = numThreadsUse; //omp_get_max_threads();
#else
    int maxthreadsuse = 1;
#endif
    
    
    if (isAffineTransform)
        tempspacebytesneeded += sizeof(double) * (nprocdims-1);
    if ( tagDimLength != 1 )
        tempspacebytesneeded += newNDimage_tempBytesNeeded< double *>( tagDimLength, nprocdims, (int) filt->length());
                //sizeof(double) * tagDimLength * (nprocdims+1)+8; // Reserve temp space for newNDimage call (NDvecImage constructor). 
    // Allocated tempspace
    char * tempspaceptr = NULL;
    if (tempspacebytesneeded>0) {
        tempspacebytesneeded += 16;
#ifdef ADDGUARDS
        tempspacebytesneeded += 10 * 8;
#endif
        tempspaceptr = (char *) mxMalloc( tempspacebytesneeded * maxthreadsuse);
    }
    tempspace tmp = tempspace(tempspaceptr, tempspacebytesneeded);
    
    /* Create output. */
    if ( ndimsOut == 1 ) {
        sizeOut[1] = 1;
        plhs[0] = mxCreateNumericArray((int)   2     , sizeOut, imgclass, iscomplexArg );
    } else {
        plhs[0] = mxCreateNumericArray((int) ndimsOut, sizeOut, imgclass, iscomplexArg );
    }
    
    double * outR = mxGetPr(plhs[0]);
    double * outI = NULL;
    if (iscomplex) {
        outI = mxGetPi(plhs[0]);
    }
    
    
    double * doutdposR = NULL;
    double * doutdposI = NULL;
    if (computeDerivative) {
        IndexType sizeDOut[ MaxNrOfDims +1 ]; // full size of out
        for (int dim = 0 ; dim < nTagDims; ++dim ) {
          sizeDOut[ dim ] = sizeOut[ dim ];
        }
        sizeDOut[ nTagDims ] = nprocdims;
        for (int dim = 0 ; dim < nprocdims ; ++dim ){
            sizeDOut[ procdims[ dim ]+ 1 ] = sizeOut[ procdims[ dim ] ];
        }
        
        plhs[1] = mxCreateNumericArray( ndimsOut + 1, sizeDOut , imgclass , iscomplexArg);
        doutdposR = mxGetPr( plhs[ 1 ] );
        if (iscomplex) {
            doutdposI = mxGetPi( plhs[ 1 ] );
        }
    }
    
    /* Final initialisations */
    
    // read clipping planes:
    bool doClip_o = !mxIsEmpty(clip_o);
    typedef clippedRangeND<double> clipT;
    vector< clipT > clips_o;
    clips_o = build_clipranges<clipT>( clip_o , size_o, nprocdims, procdims );
    
    
    // prepare transformation:
    typedef separableTransformND<double, IndexType> transformType;
    transformType * transformptr = NULL;
    
    typedef coefficients_base<double *, double> interpolateT;
    vector< interpolateT *> interpolators(0);
    
    // Construct 'fullT'; object returning transform for each output point:
    int_t stepT[ MaxNrOfDims ];
    double ** transformed = NULL; // for each dimension the intermediate transformed block.
    if (isAffineTransform) {
        transformptr = new transformAffineND<MaxNrOfDims, double , IndexType>( Td, nprocdims , nprocdims );
    } else {
        // store steps of dimensions after permutation of T.
        stepT[0] = sizeT[0];
        for(int dim = 0 ; dim < nprocdims ; dim++ ) {
            int dimT = procdims[ dim ] + 1;
            IndexType szT  = (dimT < ndimsT ? sizeT[ dimT ] : 1 );
            stepT[ dim + 1 ] = stepT[ dim ] * szT;
        }
        if (mapTd==NULL ) {
            transformptr = new transformGeneralND<MaxNrOfDims, double, IndexType, int_t>( Td, nprocdims, stepT );
        } else {
            interpolators.resize( nprocdims );
            transformed = tmp.get<double *>( nprocdims+1 );
            transformed[ nprocdims ] = Td;
            for (int dim = nprocdims-1 ; dim >= 0; dim-- ) {
                // Get residual size of T
                int_t prodszT = sizeT[0];
                for (int k = 0 ; k < dim; k++) {
                    int dimT = procdims[ k ] + 1;
                    IndexType szT  = (dimT < ndimsT ? sizeT[ dimT ] : 1 );
                    prodszT *= szT ;
                }
                transformed[ dim ] = tmp.get<double>( prodszT );
                int dimT = procdims[ dim ] + 1;
                IndexType szT  = (dimT < ndimsT ? sizeT[ dimT ] : (IndexType) 1 );
                interpolators[ dim ] = newVector_interpolate( transformed[dim+1], prodszT, szT, bsplineordT);
            }
            transformptr = new transformGeneralNDinterp< MaxNrOfDims, interpolateT *, double , double, IndexType>( &transformed[0], &interpolators[0], nprocdims, mapTprocdim, nprocdims);
        }
        
    }
    transformType & transform = transformptr[0];
    
#ifdef PERFORMANCECOUNTING
    timeCntT tend_initialization = __rdtsc();
    time_used[0] = tend_initialization - tstart_initialization;
#endif
    
    if (prefilterwithAR && !doAdjoint) {
        // TODO: check prefiltering
        range_s< int_t > r_in[ MaxNrOfDims ];
        range_s< int_t > r_out[ MaxNrOfDims ];
        
        int_t nel = 1;
        r_in[0].step  = tagDimLength;
        r_out[0].step = tagDimLength;
        for (int dim = 0 ; dim < nprocdims ; dim++ ) {
            nel *= size_i[dim]+2*ARrunoutLen; // immediately allocate the full expanded size image.
            r_in[dim].first =0;
            r_in[dim].last = size_i[dim]-1;
            r_out[dim].first = -ARrunoutLen;
            r_out[dim].last  = size_i[dim]-1+ARrunoutLen;
        }
        double * inRp_raw = tmp.get<double>( nel );
        double * inRp = inRp_raw + ARrunoutLen;
        for (int dim = 1 ; dim < nprocdims ; dim++ ) {
            r_in[dim].step = step_i[ dim ];
            r_out[dim].step = r_out[dim-1].step * (sizeIn[dim-1]+2*ARrunoutLen);
            inRp += ARrunoutLen*r_out[dim].step;
        }
        
        
        readAndFilterImgWithAR(     inR   , r_in,  nprocdims,
                ARd, nAR,
                inRp , r_out,
                inRp , r_out );
        
        inR = inRp_raw;
        if (iscomplex) {
            double * inIp_raw = tmp.get<double>( nel );
            double * inIp = inIp_raw + ARrunoutLen;
            for (int dim = 1 ; dim < nprocdims ; dim++ ) {
                inIp += ARrunoutLen*r_out[dim].step;
            }
            
            
            readAndFilterImgWithAR(     inI   , r_in,  nprocdims,
                    ARd, nAR,
                    inIp , r_out,
                    inIp , r_out );
            
            inI = inIp_raw;
        }
        
        
        for (int dim = 1 ; dim < nprocdims ; dim++ ) {
            step_i[ dim ] = r_out[dim].step ;
        }
    }
    
    typedef NDimage_base< double * > imgT;
    int boundary_condition_i = (int) mxGetScalar( boundary_condition ) ;
    imgT * imgR_i;
    imgT * imgI_i;
    {
    tempspace tmpi = tmp; // as imgR_i and imgI_i recieve separate calls and only use temp space within individual calls, they can use the same temp space. 
    imgR_i = newNDimage< double *>( (doAdjoint ? outR : inR ), tagDimLength, size_i, step_i, nprocdims, (int) filt->length() , tmp); // conversion to int OK, as interpolation filter length does not exceed maximum value of int
    imgR_i->setBoundarycondition( boundary_condition_i );
    if (iscomplex) {
        imgI_i = newNDimage< double *>( (doAdjoint ? outI : inI ), tagDimLength, size_i, step_i, nprocdims, (int) filt->length() , tmpi);
        imgI_i->setBoundarycondition( boundary_condition_i );
    }
    } // limit scope of tmpi. 
    double * imgPointer_o = (doAdjoint ? inR : outR  );
    double * imgPointerI_o = (doAdjoint ? inI : outI  );
// create iterator over 'o'
    typedef NDsubspace< double, clipT, IndexType > subRitT;
    vector<int_t> steps_ov( nprocdims );
    for (int dim= 0 ; dim < nprocdims ; dim++ ) {
        steps_ov[dim] = step_o[dim];
    }
    subRitT subrit( imgPointer_o, steps_ov, clips_o );
    typedef subRitT::iterator_lineT line_itT;
    vector< int_t > curindx_i( nprocdims+1 ); // linear indices into 'i'
    vector< int_t > curindx_o( nprocdims+1 ); // linear indices into 'o'
    curindx_i[ nprocdims ] = 0;
    curindx_o[ nprocdims ] = 0;
    
    /* Start the actual computations:*/
#ifdef USEOMP
#pragma omp parallel for default(none) shared(areaInfoList, inR, inI, imgPointer_o, outI, doutdposR, doutdposI, tempspace, n_tempspacebytes, tempspaceusedbytes , nCompArea, T, ndims, n_in_Tdim, n_out_Tdim, n_sdim, step_in_Tdim, step_in_sdim, step_out_Tdim, step_out_sdim, compOutAreaOffset, dpos_Tdim, dpos_sdim, dimFilt, dimSteps, sizeIn, sizeOut, stepIn_p, stepOut_p, imgclass,F, dFdPos, mF, nFsteps, AR, nAR, ARrunoutLen, doAdjoint) //private()
#endif
    
    for( line_itT lineit = subrit.begin_lines(); lineit != subrit.end() ; ++lineit ) {
        
#ifdef USEOMP
        int thread_num = omp_get_thread_num();
        tempspace tmpl = tmp.getParalellPart();
#else
        int thread_num = 0;
        tempspace tmpl = tmp;
#endif
        
        const vector< IndexType > & curpos = lineit.curPos();
        range< IndexType > rng_o = lineit.linerange();
        
        for (int dim = subrit.highestDimUpdatedLastStep(); dim > 0; dim-- ) {
            curindx_o[ dim ] = curindx_o[ dim + 1 ] + step_o[  dim  ] * curpos[ dim ];
            transform.update( dim, curpos[dim] );
        }
        
        curindx_o[0] = curindx_o[1] + step_o[ 0 ] * curpos[ 0 ];
        double * tranformline = transform.read( rng_o ) ;
        
        rng_o.last = rng_o.last - rng_o.first;
        rng_o.first = 0;
        
        if ( prefilterwithAR ) {
            // Hack to adjust T in all procdims to take ARrunoutLen into account (as introduced by the prefiltering)
            for (int k1 =0; k1 < nprocdims * (rng_o.last+1); k1++) {
                tranformline[k1] += ARrunoutLen;
            }
        }
        
        if (imgclass==mxDOUBLE_CLASS) {
            double * outline = imgPointer_o + curindx_o[0];
            if (doAdjoint)  {
                filterLine_F_Adj< IndexType >( imgR_i , outline, rng_o, tranformline, (*filt));
            } else if (computeDerivative) {
                double * Doutline = doutdposR + curindx_o[0] * nprocdims;
                filterLine_F_Derivative< IndexType >( imgR_i, outline, Doutline, rng_o, tranformline, (*filt));
            } else {
                filterLine_F< IndexType >( imgR_i , outline, rng_o, tranformline, (*filt));
            }
            if (iscomplex) {
                double * outlineI = imgPointerI_o + curindx_o[0];
                if (doAdjoint)  {
                    filterLine_F_Adj< IndexType >( imgI_i , outlineI, rng_o, tranformline, (*filt));
                } else if (computeDerivative) {
                    double * DoutlineI = doutdposI + curindx_o[0] * nprocdims;
                    filterLine_F_Derivative< IndexType >( imgI_i, outlineI, DoutlineI, rng_o, tranformline, (*filt));
                } else {
                    filterLine_F< IndexType >( imgI_i , outlineI, rng_o, tranformline, (*filt));
                }
            }
        } else if (imgclass==mxSINGLE_CLASS) {
            mexErrMsgTxt("single precision images not implemented yet.");
            /*float * inRf = (float *) inR;
             * float * inIf = (float *) inI;
             * float * outRf = (float *) outR;
             * float * outIf = (float *) outI;
             * float * doutdposRf = (float *) doutdposR;
             * float * doutdposIf = (float *) doutdposI;
             *
             * TransformCore<int>( adp(inRf, curindxIn[1]) , adp( inIf, curindxIn[1])  ,					  							    rng_in, step_in_Tdim, nTag, step_in_Tag ,
             * tranformline,
             * adp( outRf, curindxOut[0]), adp( outIf, curindxOut[0]), adp(doutdposRf, curindxOut[0]), adp(doutdposIf, curindxOut[0]), rng_o   ,   step_out_Tdim, step_out_Tag,
             * filt, ARd, (int) nAR, (int) ARrunoutLen, tmpl, doAdjoint);*/
        } else {
            mexErrMsgTxt("Bad type.");
        }
        
    }
#ifdef USEOMP
    omp_set_dynamic(olddynamic);
#endif
    delete &transform;
    delete filt;
#ifdef ADDGUARDS
    if (isAffineTransform) {
        //tmp.checkGuard( Tscratch, ndimsT );
    } else {
        if (mapTd!=NULL ) {
            tmp.checkGuard( transformed , ndimsT );
            for (int dim = ndimsT-2 ; dim>=0; dim-- ) {
                int_t prodszT = 1;
                for (int k = 0 ; k <= dim; k++) {
                    prodszT *= sizeT[k];
                }
                tmp.checkGuard( transformed[ dim ], prodszT );
            }
        }
    }
#endif
#ifdef PERFORMANCECOUNTING
    timeCntT tend_processing = __rdtsc();
    time_used[1] = tend_processing - tend_initialization;
#endif
    if (tempspacebytesneeded>0) {
        mxFree(tempspaceptr);
    }
    delete imgR_i;
    if (iscomplex) 
        delete imgI_i;
#ifdef PERFORMANCECOUNTING
    timeCntT tend_mexfunction = __rdtsc();
    time_used[2] = tend_mexfunction - tend_processing;
    for (int k = 0 ; k<2; ++k) {
        outR[k] = (double) time_used[k];
    }
#endif
#endif	// end of else of ifdef BUILDTESTFUN_TRANSFORMCOREGENERAL
}
#endif // end of ifndef NOMEXFUN
#endif // end of else of ifdef BUILDING_CORE
