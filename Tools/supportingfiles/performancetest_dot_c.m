% shows corretness and performance of c implementation of dot for single and double
% 
% Overall the runtime of the (single threaded) mex implementation is about half to one third of the 
% reference implementation (using 4 CPU cores). With the additional benefit of requiring no additional memory. 
% 
%
%
% For no apparent reason some combinations perform (horribly) bad
% Specifically dim 1 in test 10 (A=single, B=complex double) and 11 (A=double, B = complex single)
% taking 180 CPU cycles per operation (should be around 8-10).
% Inspected dissassembly (mex dot_c.cpp -DINCLUDE_SSE3 -DMEX -v -g -O)
% but found no (performance significant) differences with other tests that do run fast
% (specifically thoroughly compared test 10, dim 1 and dim 2).
% It also is probably not due to cache misses as reducing test size (so that it fits in cache)
% does not change performance. Also, using the non vectorized version in these cases
% does improve performance. 
% Preliminary hypothesis(/conclusion): some CPU limitation causing some microcode stall/error and interupt 
% CPU used: Intel Xeon E5520 @2.27GHz, running Windows 7 enterprise, SP1. 
%       
% As these problem cases are not expected to be common use cases, no further solution is sought. 
%
%
% 4-4-2017: Created by Dirk Poot, Erasmus MC.


[typeA, typeB, complexA, complexB] = ndgrid( [1 2],[1 2],[0 1],[0 1]);


for testnr = 1 : numel(typeA)
    %%
    sizeAB = [12 65 55 73]; 

    if complexA( testnr )
        testnameA = 'complex ';
        A = randn(sizeAB)+1i*randn(sizeAB); 
    else
        testnameA = '';
        A = randn(sizeAB); 
    end;
    if typeA( testnr )==1
        testnameA = [testnameA 'double'];
    else
        A = single(A);
        testnameA = [testnameA 'single'];
    end;
    
    if complexB( testnr )
        testnameB = 'complex ';
        B = randn(sizeAB)+1i*randn(sizeAB); 
    else
        testnameB = '';
        B = randn(sizeAB); 
    end;
    if typeB( testnr )==1
        testnameB = [testnameB 'double'];
    else
        B = single(B);
        testnameB = [testnameB 'single'];
    end;
    testname = ['A=' testnameA ', B=' testnameB];
        
    % %
    numrepeats = 5; 
    tu1 = zeros(1,4);
    tu1b = tu1;
    tu2 = tu1;
    tu3 = tu1;
    diffmagn = tu1;
    for dim = 1 : numel(tu1);
        tic; 
        for k=1:numrepeats; 
            out1 = dot( A, B, dim ); 
        end, 
        tu1(dim) = toc; 
        if exist('dot_c','file')
            tic; 
            for k=1:numrepeats; 
                out1b= dot_c( A, B, dim ); 
            end, 
            tu1b(dim) = toc; 
        else
            tu1b = zeros(0,size(tu1,2));
            out1b = out1;
        end;
        tic; 
        for k=1:numrepeats; 
            out2 = sum(conj(A).*B, dim ); 
        end, 
        tu2(dim) = toc;
        diffmagn(dim) = max(reshape(abs(out1b-out2),[],1));
    %     tic; for k=1:nlp; out3 = builtin('dot',r, c, dim ); end, tu3(dim) = toc;
    end;

    % Display results:
    disp(' ');
    disp(['Test ' num2str(testnr) ': size [' num2str(sizeAB) '], ' testname ])
    clockrate = 2.26e9; % CPU freq of PC Dirk @2017
    time2clocksperflop = @(t) t*clockrate/(prod(sizeAB)*numrepeats);
    % nclocksperflop = [tu1; tu1b; tu2;tu3]/(prod(sz)*numrepeats)*2.26e9
    disp(['dot      : ' num2str(time2clocksperflop( tu1 ))])
    disp(['dot_c    : ' num2str(time2clocksperflop( tu1b ))])
    disp(['reference: ' num2str(time2clocksperflop( tu2 ))])
    disp(['difference: ' num2str( diffmagn ) '  = max( abs( dot_c - refererence ) ) ' ])
end;