function [img]= genGaussBumps(dimSizes, points, GausPointsSz)
% [img]= genGaussBumps(dimSizes, points, GausPointsSz)
% generates N-dimensional non overlapping gauss bumps.
% dimSizes, N-long integer vector with size of image
% points : k by N matrix with coordinates of points.
% GausPointsSz  = 1/(2*sigma^2)
% GausPointsSz can be a matrix (non isotropic bumps).
%
% Created by Dirk Poot, University of Antwerp.
% Date created : ?
% last modified: 23-5-2008

ndims = length(dimSizes);
Coords = cell(ndims,1);
for k=1:ndims
    Coords{k} = 1:dimSizes(k);
end;
[Coords{:}] = ndgrid(Coords{:});

dist=inf(dimSizes);

for k=1:size(points,1);
    coorddist = zeros(numel(dist),ndims);
    for l=1:ndims
        coorddist(:,l) = Coords{l}(:)-points(k,l);
    end;
    dist(:) = min(dist(:),sum((coorddist * GausPointsSz) .* coorddist,2));
end;
img = exp( -dist );
