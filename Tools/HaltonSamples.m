function [out] = HaltonSamples( lowerbound, upperbound, numsamples )
% [out] = HaltonSamples( lowerbound, upperbound, numsamples )
%
% Creates samples from a Halton sequence. This is set of pseudo random vectors 
% which has a low discrepancy in N dimensional space, which means that the
% average density is more uniform than creating random samples with rand.
% The resulting set of numbers only has a low discrepancy if the
% dimensionality (length of lower- and upperbound) is relatively low (say <
% 6), else too many samples should be drawn. (at least about 30000 are
% needed for 6 dimensions, about 510k for 7 dimensions, and about 10M for 8
% dimensions).
% This function uses HaltonSequence to generate the actual sequences. 
%
% Functionally this function performs:
%  out(:,i) = (upperbound-lowerbound) .* rand( numel(lowerbound),1) + lowerbound
%
% INPUTS:
%  lowerbound  : vector with the lower edge in all N dimensions.
%  upperbound  : vector with the upper edge in all N dimensions.
%  numsamples  : scalar integer with the number of random vectors created.
%
% OUPUTS:
%  out         : N x numsamples , pseudo random vectors.
%
% Created by Dirk Poot, Erasmus MC, 22-5-2015

npar = numel(lowerbound);
p = [2 3 5 7 11 13 17 19 23 29 31 37];%p = primes(37);
if npar > numel(p)
    error('Halton sequence only provides quasi uniform samples for a low number of parameters.')
end;

v = cell(numel(lowerbound),1);
for k = 1 : numel(lowerbound)
    v{k} = HaltonSequence( numsamples , p(k), .5) * (upperbound(k)-lowerbound(k))+lowerbound(k);
end;
out = vertcat(v{:});
