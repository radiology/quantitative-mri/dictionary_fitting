function [output] = latextable(inpdata, heads, rowspec, digits, caption, color, varargin)
% LATEXTABLE: outputs a latex table with the content specified
% [outstr] = latextable( tabledata, heads, rowspec, digits, caption, color, -options-)
%
% INPUTS:
% tabledata : 2D matrix or 2D cell array with content of the table
%             if size(inpdata,3)==2: then inpdata(:,:,2) is interpretted as the 
%             uncertainty which is placed between braces after the value.
%             Use 'emptynumber' to not display a value. (see help emptynumber)
% heads   : cell vector of strings with the column names (empty = no head)
% rowspec : cell vector of strings with the row names (empty = no row start)
% digits  : the number of digits, scalar or specify for each column.
% caption : string with the caption text or 2 element cell array with the
%           caption as first argument and the label as second. 
%           NOTE: the caption is escaped to prevent destroying the table.
%           Valid latex code should not be modified. 
%           (currently not fully)
% color   : background color of each cell. 
%           size(inpdata) cell array or [size(inpdata) 3 ] array with RGB
%           values. The cell array may contain color names known by latex as strings
% Options come in option-name , value pairs (see parse_defaults_optionvaluepairs)
%  dispMatlabTable: default = false; if true, displays and outputs the table
%           in the MATLAB command window in a more (directly/ASCI) readable format. 
%           Usefull for viewing(/debugging) the table in MATLAB, but not for exporting
%           to latex.
%  adjustwidth : string or 2 element cell array specifying the horizontal adjustment of the table.
%           Usefull if the table does just falls of on the right hand side of a page. 
%           First (or only) value: horizontal shift of left margin, so set to e.g. '-2cm'
%           Uses the package \usepackage{changepage} (as suggested by http://tex.stackexchange.com/questions/33345/making-a-table-fit-on-a-page-by-moving-it-left  
%           documentation: http://ctan.triasinformatica.nl/macros/latex/contrib/changepage/changepage.pdf)
%       
% OUTPUTS: 
% outstr  : string (vector) with the latex source code of the table. This
%           is printed to the screen if no output argument provided. 
%           Uses the packages :
%           \usepackage{dcolumn}       % if digits>0
%           \usepackage[table]{xcolor} % if color is provided.

% Default values:
opt = struct;
opt.dispMatlabTable = false;
opt.adjustwidth = '';

if nargin==1 && isequal(inpdata,'test')
    test_latextable;
end;
if nargin<2
    heads = [];
end;
if ~isempty(heads)
    if numel(heads)~=size(inpdata,2)
        error('number of elements in heads (%d) should match number of columns of tabledata (%d)', numel(heads), size(inpdata,2));
    end;
end;
if nargin<3
    rowspec = [];
end;
if ~isempty(rowspec)
    if numel(rowspec)~=size(inpdata,1)
        error('number of elements in rowspec should match number of rows');
    end;
end;

if nargin<4 || isempty(digits)
    % use 4 significant figures by default:
    digits = max(0,-floor(log10(max(abs(inpdata(:,:,1)),[],1)))+3);
end;
if nargin<5 || isempty(caption)
    caption = '(empty caption)';
end;
if ~iscell(caption)
    caption = {caption};
end;
if nargin<6 
    color = [];
end;
    
if nargin==7 && ~isstruct(varargin{1})
    opt.dispMatlabTable = varargin{1};
else
    opt = parse_defaults_optionvaluepairs( opt, varargin{:});
end;
if numel(digits)==1 && size(inpdata,2)>1
    digits = repmat(digits,1,size(inpdata,2));
end;
if numel(digits)~=size(inpdata,2)
    error('digits should be scalar or specified for each column');
end;
nrows = size(inpdata,1);
ncols = size(inpdata,2);
isfloats = isnumeric(inpdata);
if size(inpdata,3)==2; 
    if isfloats
        hasstd = ~all( isemptynumber( inpdata(:,:,2) ),1);
    else
        hasstd = ~all( cellfun(@isempty,inpdata(:,:,2) ),1);
    end;
else
    hasstd = false( 1, size(inpdata,2));
end;
if ~isempty(color) 
    if size(color,1)~=1 && size(color,1)~=nrows
        error('number of rows of color should match number of rows of data (or be scalar)');
    elseif size(color,2)~=1 && size(color,2)~=ncols
        error('number of column of color should match number of columns of data (or be scalar)');
    end;
end;

% create a string for each cell, including column specification, column header and row header.
% the cell content should not include the structure coding ('&' and '\\')
cellstr = cell(2+nrows,1+ncols,2); 

% print row heads:
if ~isempty(rowspec)
    cellstr{1,1} = 'c|';
    if numel(rowspec) == 1
        cellstr(3,1) = {escapeTablestructure( rowspec{:} )};
    else
        cellstr(3:end,1) = escapeTablestructure( rowspec{:} );
    end
end;

% print column heads:
if ~isempty(heads)        
    heads_escaped = escapeTablestructure( heads{:} );
    for col = 1 : ncols
        if hasstd(col)
            cellstr{2,col+1} = ['\multicolumn{2}{ c|}{' heads_escaped{col} '}'];
        else
            cellstr{2,col+1} = heads_escaped{col};
        end;
    end;
end;
    
% print data:
if isfloats
    % floating point data:
    for col=1:ncols
        cellstr{1,1+col} = sprintf('D{.}{.}{%d}|',digits(col)) ;
        if hasstd(col)
            cellstr{1,1+col,2} = cellstr{1,1+col,1};
            cellstr{1,1+col,1}(end)=[];
        else
            %fixup column heading (should be in multicolumn{1} for dcolumn columns:
            cellstr{2,col+1} = ['\multicolumn{1}{ c|}{' cellstr{2,col+1} '}'];
            if col==1 && isempty(rowspec)
                % fixup muticolumn left vertical line if no preceeding column.
                cellstr{2,col+1}(17)='|';
            end;
        end;
        numformatstr = sprintf('%%.%df',digits(col));
        for valnr = 1: 1+hasstd(col)
            for row = 1:nrows
                cellstr{2+row,1+col,valnr} = sprintf( numformatstr , inpdata(row,col,valnr) );
                if ~isfinite(inpdata(row,col,valnr))
                    if isemptynumber( inpdata(row,col,valnr) ) 
                        cellstr{2+row,1+col,valnr} = '' ;
                    else
                        cellstr{2+row,1+col,valnr} =['$', cellstr{2+row,1+col,valnr} ,'$'];
                    end;
                end;
            end;
        end;
    end;
else
    % just text data:
    inpdata_escaped = reshape( escapeTablestructure( inpdata{:} ) , size(inpdata) );
    for col = 1:ncols
        if hasstd(col)
            cellstr(1,1+col)= {'cc|'};
        else
            cellstr(1,1+col) = {'c|'};
        end;
    end;
    cellstr(3:end,2:end,1:size(inpdata_escaped,3)) = inpdata_escaped ;
end;
% put braces around std
for col = (1: ncols)
    if hasstd(col) 
        for row = 2+(1:nrows)
            if ~isempty(cellstr{row, 1+col ,2 })
                cellstr{row, 1+col , 2} = ['(' cellstr{row, 1+col ,2 } ')'];
            end;
        end;
    end;
end;

% select what to print
printmask = true(size(cellstr));
printmask(:,[true ~hasstd],2 )= false; % set printing std on row heading and non std columns to false.
printmask(1,:,:) = false; % don't print column specification; it's handled separately.
printmask(2,:,(2-isempty(heads)):2) = false; % don't print std on column headings (they are multicolumn).
printmask(:,1,(2-isempty(rowspec)):2) = false; % don't print std on row headings (that column is single column).

% Permute to get std closeby
cellstr = permute(cellstr,[1 3 2]);
printmask = permute(printmask,[1 3 2]);

noutpstr = 0;
noutpstr = noutpstr+1;

if opt.dispMatlabTable
    % display table in command window:
    %(unfortunately we cant do color there)
    dcellstr = cellstr;
    outpstr{noutpstr} = ['Table : ' caption{1}];
    if ~isempty(heads)   
        dcellstr(2,1,2:end)=heads;
    else
        dcellstr(2,1,2:end)={[]};
    end;
    colwidth = cellfun(@numel, dcellstr);
    colwidth(1,:)=0;
    datacolwidth = max(colwidth(3:end,:,:),[],1);
    fullcolwidth = max(colwidth(2,1,:), sum(datacolwidth,2));
    datacolwidth(1,1,:) = fullcolwidth-datacolwidth(1,2,:);
%     usecolwidth = squeeze( max(sum(colwidth,2),[],1) );

    headpatt = sprintf('%%%ds  ',fullcolwidth);
    datapatt = sprintf('%%%ds ',datacolwidth);
    
    for row = 2 : size(cellstr,1)
        curprint = printmask(row,:,:);
        if any(curprint(:))
            noutpstr = noutpstr+1;
            if row==2
                outpstr{noutpstr} = sprintf( headpatt, dcellstr{row,1,:} );
            else
                outpstr{noutpstr} = sprintf( datapatt, dcellstr{row,:} );
            end;
        end;
    end;
else 
    % create latex table:

    % set color
    if ~isempty(color)
        if size(color,1)==1
            % color columns:
            for col = 1: ncols
                colorspec = sprintf('%f,',color(1,col,:));
                for valnr = 1: 2
                    cellstr{1,valnr,col+1} = ['>{\columncolor[rgb]{' colorspec(1:end-1) '}}' cellstr{1,valnr,col+1}];
                end;
            end;
        elseif size(color,2)==1
            % color rows:
            for row = 1: nrows
                curprint = printmask(row+2,:,:);
                i = find(curprint);i = i(1);
                colorspec =sprintf('%f,',color(row,1,:));
                cellstr{row+2,i} = ['{\rowcolor[rgb]{' colorspec(1:end-1) '}}'  cellstr{row+2,i}];
            end;
        else
            % color cells:
            for row = 1: nrows
                for col = 1 : ncols
                    if all(isfinite(color(row,col,:)))
                        colorspec = sprintf('%f,',color(row,col,:));
                        for valnr = 1: 2
                            cellstr{row+2,valnr,col+1} = ['{\cellcolor[rgb]{' colorspec(1:end-1) '}}'  cellstr{row+2,valnr ,col+1}];
                        end;
                    end;
                end;
            end;
        end;
    end;

    % construct table string:
    if ~isempty( opt.adjustwidth )
        if ischar(opt.adjustwidth)
            opt.adjustwidth = {opt.adjustwidth, ''};
        end;
        adjuststrstart = ['\begin{adjustwidth}{' opt.adjustwidth{1} '}{' opt.adjustwidth{2} '} '];
        adjuststrend = ' \end{adjustwidth}';
    else
        adjuststrstart = '';
        adjuststrend = '';
    end;
    outpstr{noutpstr} = ['\begin{table} ' adjuststrstart ' \begin{tabular}{|' cellstr{1,:,:} '} \hline'];


    for row=1:size(cellstr,1)
        curprint = printmask(row,:,:);
        if any(curprint(:))
            noutpstr = noutpstr+1;
            outpstr{noutpstr} = sprintf('%s& ',cellstr{row,curprint});
            outpstr{noutpstr}(end-1:end)='\\';
            if row==2
                % add a horizontal line after the heads.
                outpstr{noutpstr} = [outpstr{noutpstr} ' \hline'];
            end;
        end;
    end;
    noutpstr = noutpstr+1;
    if numel(caption)>1
        label = ['\label{' caption{2} '}'];
    else
        label ='';
    end;
    outpstr{noutpstr} = ['\hline \end{tabular} \caption{' escapeTablestructure( caption{1} ) '}' label adjuststrend ' \end{table}'];
end;

if nargout<=0
    for row=1:noutpstr
        disp(outpstr{row});
    end;
else
    output = outpstr;
end;


function [S] = escapeTablestructure(varargin)
% escapes char array S so that it cannot demolish the table
% So we escape & , \\ , %

persistent regexpVer 
if isempty(regexpVer )
    regexpVer = strcmp(regexprep('_','\_','\\$1'),'\$1'); % never gives warning and can discriminate between al known versions.
end;
% escapes: '\^{}'
if opt.dispMatlabTable
    S= varargin;
elseif regexpVer
    % MATLAB 7:
    % regexptranslate('escape', '\\')
    inputtext = varargin;
    % inputtext = {'test1','test_er','#te\&st','\#te_st\','$d_3$','test$_6^4$\\'}
    %%
    mathsted = regexp(inputtext, '\$','start');
    
    % textpatt & textrepl :
    % 1) put an \ in front of   %&_#  , if there is none (any other character or beginning of the string)
    %       (?: )  = group without capturing tokens
    %       (?<!X ) = lookbehind, match if X is not found.
    %       [^abc] = not any of the characters abc
    %       ^      = beginning of string
    %       |      = or
    %       ( )    = group and capture token
    %       [abc]  = any of the characters abc 
    % 2) replace \\ by \textbackslash{}
    textpatt = {'(?<![\\])([%&_#])','\\\\'};
    textrepl = {'\\$1'               ,'\\textbackslash\{\}'};
    mathpatt = {'(?<![\\])([%&#])' ,'\\\\'};
    mathrepl = {'\\$1'               ,'\\textbackslash\{\}'};
    S=regexprep(inputtext,textpatt,textrepl);
    for k= find(~cellfun(@isempty, mathsted))
        if mod(numel(mathsted{k}),2)==1
            warning('math mode not properly started/ended');
        else
            curS = inputtext{k};
            sted = [1 mathsted{k} numel(curS)+1];
            tmp = cell( 1, numel(sted) );
            for k2 = 1 : 2 : numel(sted)-1
                tmp{k2} = regexprep(curS(sted(k2):sted(k2+1)-1) , textpatt, textrepl);
                if k2+2<=numel(sted)
                    tmp{k2+1} = regexprep(curS(sted(k2+1):sted(k2+2)-1) , mathpatt, mathrepl);
                end;
            end;
            S{k} = [tmp{:}];
        end;
    end;
    %S = regexprep(varargin,{'([\\%&])','\\\\'},{'\\$1','\\textbackslash\{\}'});
else
    % MATLAB 6:
    S=regexprep(varargin,'([\\\{\}_\^])','\$1','tokenize');
end;
if nargin==1
    S=S{1};
end;
end;

function test_latextable
%% Create a whole set of tables, to test if all functionality works correct.
dta = randn(4,3)
disp('simple data, no heads, no row specification, different number of digits.');
latextable(dta, [], [], [0 2 2])

disp('simple data, with heads, no row specification');
latextable(dta, {'col_1','col$_2$','column 3'}, [], [2 2 2],'test Table 2; all with 2 decimals');

disp('simple data, no heads, with row specification');
latextable(dta, [], {'row1','row$_2$','row 3','row4'}, [0 0 0],'test Table 3; all with 0 decimals');

dtam = dta;
dtam(1,1)=inf;
dtam(2,2)=nan;
dtam(3,3)=-inf;
dtam(4,3)=nan;
disp('data with nan/inf, with heads, with row specification');
latextable(dtam, {'col_1','col$_2$','column 3'}, {'row1','row$_2$','row 3','row4'}, [2 2 0],'test Table 4; [2 2 0] decimals');

dta2 = randn(4,3,2);
dta2(1,1,1)=inf;
dta2(1,2,2)=inf;
dta2(2,1,1)=nan;
dta2(2,2,2)=nan;
dta2(3,3,1)=nan;
dta2(4,3,2)=nan;
disp('data with std&nan, with heads, with row specification');
latextable( dta2 , {'col_1','col$_2$','column 3'}, {'row1','row$_2$','row 3','row4'}, [2 2 0],'test Table 5');

disp('simple data, no heads, no row specification, different number of digits, column color.');
tstcolor = rand(1,3,3);
latextable(dta, [], [], [0 2 2],'test Table 6 (check precent: % )',tstcolor);

disp('simple data, no heads, no row specification, different number of digits, row color.');
tstcolor = rand(4,1,3);
latextable(dta, [], [], [0 2 2],'test Table 7 (check hat:  ^ )',tstcolor);

disp('simple data, no heads, no row specification, different number of digits, color cells.');
tstcolor = .5+.5*rand(4,3,3);
latextable(dta, [], [], [0 2 2],'test Table 8 (check slash \ )',tstcolor);

disp('data with std&nan, with heads, with row specification, color columns');
tstcolorCol = rand(1,size(dta2,2),3);
latextable( dta2 , {'col_1','col$_2$','column 3'}, {'row1','row$_2$','row 3','row4'}, [2 2 0],'test Table 9 (check slash / )',tstcolorCol);

disp('data with std&nan, with heads, with row specification, color cells.');
tstcolorCel = rand(size(dta2,1),size(dta2,2),3);
latextable( dta2 , {'col_1','col$_2$','column 3'}, {'row1','row$_2$','row 3','row4'}, [2 2 0],'test Table 10 (check star empersand *& )',tstcolorCel);
end;
end