function mask = makeThresholdMask( images, thresholdLimits , weights )
% mask = makeThresholdMask( images, thresholdLimits, weights )
%
% INPUTS:
%  images : ND image array as used by fit_MRI; first dimension is different contrasts, 
%           other dimensions are spatial
%           N = size(images, 1)
%  thresholdLimits : N x 2 matrix with for each image the minimum and maximum value to include
%           mask_i(k) = is_true: thresholdLimits(i,1) <= images(i,k) <= thresholdLimits(i,2)
%  weight : for each image the weight of the individual mask
%           Allows AND combining of masks: weight = 1/size(images,1) 
%           Allows OR combining of masks : weight = 1     (default)
%           Allows counting: weight = 1/thresholdNumberOfMasks
%   
% OUTPUTS:
%  mask : size_images(2:end) logical mask
%         mask = ( sum_i=1..N  mask_i * weight_i ) >=1
%
% 22-3-2018, D.Poot, Erasmus MC: Created

if nargin<3 || isempty(weights)
    weights = 1;
end;
if numel(weights)==1
    weights = repmat(weights, size(images,1),1);
end;
sz_image = size( images );
mask = reshape( sum( bsxfun(@times, double(weights(:)) ,  double( bsxfun(@le, thresholdLimits(:,1), images) & bsxfun(@le, images, thresholdLimits(:,2))) ) , 1) >=1, sz_image(2:end));
