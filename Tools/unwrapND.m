function [angl, magn] = unwrapND( complex_image )
% [phase, magn] = unwrapND( complex_image )
% Computes the unwrapped angle of the N dimensional complex_image.
%
% INPUTS:
% complex_image : N dimensional complex array
% 
% OUTPUTS:
% phase  : unwrapped phase of complex image. 
% 
% Created by Dirk Poot, Erasmus MC, 25-2-2016

noiselevel = .1;

ndim = ndims( complex_image );

G = cell(1,ndim);
for dim = 1:ndim;
    G{dim} = mod(diff(angle(complex_image),[],dim)+pi,2*pi)-pi;
end;

initial = angle( complex_image );
maxw = .01;
weights = min( abs(complex_image)*(maxw/ noiselevel) , maxw );
bad = ~isfinite(complex_image) | complex_image==0;
weights(bad) =0;
initial(bad) =0 ; 
angl = integrateDiff( G, initial, 'values',initial , 'periodic',true,'weights',weights);